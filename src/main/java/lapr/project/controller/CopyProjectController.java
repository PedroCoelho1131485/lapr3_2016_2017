/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import lapr.project.model.Project;
import lapr.project.model.ProjectRegistry;

/**
 * Represents an instance of CopyProjectController.
 *
 *
 */
public class CopyProjectController {

    /**
     * Project Handler of project.
     */
    private ProjectRegistry projectHandler;

    /**
     * Project selected.
     */
    private Project project;

    /**
     * Creates an instance of CopyProjectController.
     *
     * @param pr
     */
    public CopyProjectController(ProjectRegistry pr) {
        this.projectHandler = pr;
        if (this.projectHandler.getActiveProject() == null) {
            throw new IllegalArgumentException("You must open a project before you can copy it!");
        }
        this.project = new Project(this.projectHandler.getActiveProject());
    }

    /**
     * Returns the project's name.
     *
     * @return The project's name.
     */
    public String getProjectName() {
        return this.project.getName();
    }

    /**
     * Returns the project's description.
     *
     * @return The project's description.
     */
    public String getProjectDescription() {
        return this.project.getDescription();
    }

    /**
     * Sets the project's name.
     *
     * @param name The new name of the project.
     */
    public void setProjectName(String name) {
        this.project.setName(name);
    }

    /**
     * Sets the project's description.
     *
     * @param description The new description of the project.
     */
    public void setProjectDescription(String description) {
        this.project.setDescription(description);
    }

    /**
     * Registers the copy of the project.
     *
     * @return Returns true if successfully changed.
     */
    public boolean registerCopy() {
        return projectHandler.registerCopy(this.project);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.List;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import lapr.project.model.NetworkAnalysisResult;

/**
 *
 * @author cdsn_
 */
public class NetworkAnalysisResultsList {

    /**
     * List of NetworkAnalysisResults
     */
    private List<NetworkAnalysisResult> naList;

    /**
     * Build an instance of NetworkAnalysisResultsList
     */
    public NetworkAnalysisResultsList() {
        this.naList = new ArrayList<>();
    }

    /**
     * Creates a clone of the target naList
     *
     * @param source The target NetworkAnalysisResult list to clone.
     */
    public NetworkAnalysisResultsList(NetworkAnalysisResultsList source) {
        naList = new ArrayList(source.naList);
        Collections.copy(naList, source.naList);
    }

    /**
     * Modify the list
     *
     * @param naList
     */
    public void setNetworkList(List<NetworkAnalysisResult> naList) {
        this.naList.clear();
        this.naList.addAll(naList);
    }

    /**
     * Return Network Analysis Result list
     *
     * @return naList
     */
    public List<NetworkAnalysisResult> getNAList() {
        return this.naList;
    }

    /**
     * Add NetworkAnalysisResult to list
     *
     * @param na
     * @return Returns true if added and false otherwise
     */
    public boolean addNetworkAnalysisResult(NetworkAnalysisResult na) {

        if (this.naList.contains(na)) {
            return false;
        } else {
            naList.add(na);
            return true;
        }

    }

    /**
     * Remove NetworkAnalysisResult
     *
     * @param na
     */
    public boolean removeNetworkAnalysisResult(NetworkAnalysisResult na) {
        return this.naList.remove(na);
    }

    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }
        NetworkAnalysisResultsList otherNetworkAnalysisResult = (NetworkAnalysisResultsList) outroObjeto;
        return this.naList.equals(otherNetworkAnalysisResult.getNAList());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.naList);
        return hash;
    }

}

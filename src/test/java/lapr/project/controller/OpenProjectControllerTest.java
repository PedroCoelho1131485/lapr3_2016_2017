/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.ArrayList;
import java.util.List;
import lapr.project.model.Junction;
import lapr.project.model.Project;
import lapr.project.model.ProjectRegistry;
import lapr.project.model.Segment;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Utilizador
 */
public class OpenProjectControllerTest {

    private ProjectRegistry pr;
    private Project p;
    private OpenProjectController op;

    public OpenProjectControllerTest() {
        pr = new ProjectRegistry();
        p = new Project("nome", "descricao");
        Junction junction = new Junction("name", 1, 1);
        Junction junction1 = new Junction("name1", 1, 1);
        Segment s = new Segment();
        s.setStartNode(junction1);
        s.setEndNode(junction);
        p.addSegment(s);
        pr.addProject(p);
        op = new OpenProjectController(pr);

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getProjectList method, of class OpenProjectController.
     */
    @Test
    public void testGetProjectList() {
        System.out.println("getProjectList");
        OpenProjectController instance = op;
        List<String> expResult = new ArrayList();
        expResult.add(p.getName());
        List<String> result = instance.getProjectList();
        assertEquals(expResult, result);

    }

    /**
     * Test of selectProject method, of class OpenProjectController.
     */
    @Test
    public void testSelectProject() {
        System.out.println("selectProject");
        String name = "nome";
        OpenProjectController instance = op;
        String expResult = "nome";
        String result = instance.selectProject(name);
        assertEquals(expResult, result);

    }

}

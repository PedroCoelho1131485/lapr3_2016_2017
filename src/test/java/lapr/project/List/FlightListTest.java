package lapr.project.List;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import lapr.project.model.Flight;
import lapr.project.model.FlightPlan;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class FlightListTest {

    

    private Flight f;
    private FlightList fl;
    private FlightPlan fp;

    public FlightListTest() {
        fp = new FlightPlan();
        fl = new FlightList();
        Flight f = new Flight("name", "type", "1", 1, "1", fp);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of size method, of class FlightList.
     */
    @Test
    public void testSize() {
        System.out.println("size");
        FlightList instance = fl;
        int expResult = 1;
        instance.addFlight(f);
        int result = instance.size();
        assertEquals(expResult, result, 0);

    }

    /**
     * Test of getFlightList method, of class FlightList.
     */
    @Test
    public void testGetFlightList() {
        System.out.println("getFlightList");
        FlightList instance = fl;

        List<Flight> expResult = new ArrayList();
        expResult.add(f);
        instance.addFlight(f);
        List<Flight> result = instance.getFlightList();
        assertEquals(expResult, result);

    }

    /**
     * Test of newFlight method, of class FlightList.
     */
    @Test
    public void testNewFlight() {
        System.out.println("newFlight");
        FlightList instance = fl;
        Flight f = new Flight();
        Flight expResult = f;
        Flight result = instance.newFlight();
        assertEquals(expResult, result);
        System.out.println("Result : " + result);
    }

    /**
     * Test of addFlight method, of class FlightList.
     */
    @Test
    public void testAddFlight() {
        System.out.println("addFlight");
     
        FlightList instance = fl;
        boolean expResult = true;
        boolean result = instance.addFlight(f);
        assertEquals(expResult, result);

    }

    /**
     * Test of removeFlight method, of class FlightList.
     */
    @Test
    public void testRemoveFlight() {
        System.out.println("removeFlight");

        FlightList instance = fl;
        boolean expResult = false; //flight will be removed
        boolean result = instance.removeFlight(f);
        assertEquals(expResult, result);

    }

    /**
     * Test of iterator method, of class FlightList.
     */
    @Test
    public void testIterator() {
        System.out.println("iterator");
        FlightList instance = fl;
        Iterator<Flight> ite = instance.iterator();
        Iterator<Flight> expResult = ite;
        Iterator<Flight> result = ite;
        assertEquals(expResult, result);
        System.out.println("expResult : " + expResult);
        System.out.println("result : " + result);
    }

}

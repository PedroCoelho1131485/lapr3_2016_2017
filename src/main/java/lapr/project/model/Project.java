
package lapr.project.model;

import lapr.project.AdjacencyMap.Graph;
import lapr.project.List.AircraftList;
import lapr.project.List.AirportList;
import lapr.project.List.FlightList;
import lapr.project.List.FligthPlanList;
import lapr.project.List.JunctionList;
import lapr.project.List.NetworkAnalysisResultsList;
import lapr.project.List.SegmentList;
import java.util.Objects;

/**
 *
 *
 */
public class Project {

    /**
     * Name of project
     */
    private String name;

    /**
     * Description of project
     */
    private String description;

    /**
     * Intance of AirNetwork
     */
    private AirNetwork airNetwork;

    /**
     * Intance of SegmentList
     */
    private SegmentList segmentList;

    /**
     * Intance of AirportList
     */
    private AirportList airportList;

    /**
     * Intance of AircraftList
     */
    private AircraftList aircraftList;

    /**
     * Intance of FlightPlanList
     */
    private FligthPlanList flightPlanList;

    /**
     * Intance of NetworkAnalysisResultsList
     */
    private NetworkAnalysisResultsList naList;

    /**
     * Intance of JunctionList
     */
    private JunctionList junctionList;

    /**
     * Intance of FlightList
     */
    private FlightList flightList;

    /**
     * Intance of ImportableRegistry
     */
    private ImportableRegistry imp;

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
        this.airNetwork = new AirNetwork();
        this.segmentList = new SegmentList();
        airportList = new AirportList();
        aircraftList = new AircraftList();
        flightPlanList = new FligthPlanList();
        this.naList = new NetworkAnalysisResultsList();
        junctionList = new JunctionList();
        this.flightList = new FlightList();
        this.imp = new ImportableRegistry();
    }

    /**
     * Creates an instance of project with the same characteristics as the
     * target project, creating a clone.
     *
     * @param source The target project to copy.
     */
    public Project(Project source) {
        name = source.getName();
        description = source.getDescription();
        this.airNetwork = new AirNetwork(source.getAirNetwork());
        aircraftList = new AircraftList(source.getAircraftList());
        airportList = new AirportList(source.getAirportList());
        segmentList = new SegmentList(source.getSegmentList());
        flightPlanList = new FligthPlanList(source.getFlightPlanList());
        naList = new NetworkAnalysisResultsList(source.getNetworkAnalysisResultsList());
        junctionList = new JunctionList(source.getJunctionList());
        this.flightList = new FlightList(source.getFlightList());
        this.imp = new ImportableRegistry();

    }

    public Project() {
        this.name = "em";
        this.description = "emp";
        this.airNetwork = new AirNetwork();
        this.segmentList = new SegmentList();
        airportList = new AirportList();
        aircraftList = new AircraftList();
        flightPlanList = new FligthPlanList();
        this.airNetwork = new AirNetwork();
        this.naList = new NetworkAnalysisResultsList();
        junctionList = new JunctionList();
        this.flightList = new FlightList();
        this.imp = new ImportableRegistry();
    }

    /**
     * @return name of Project
     */
    public String getName() {
        return name;
    }

    /**
     * @param name New name of Project
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return description of Project
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description new description of Project
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return AirNetwork graph
     */
    public Graph<Junction, Segment> getGraph() {
        return this.airNetwork.getGraph();
    }

    /**
     * Creates a new junction that can be added to the road map.
     *
     * @return New junction.
     */
    public Junction newJunction() {
        return new Junction();
    }

    /**
     * Adds a junction to the air network.
     *
     * @param junction Junction to be added.
     * @return Returns true if the junction is added or false if the air network
     * already contains it.
     */
    public boolean addJunction(Junction junction) {
        return this.airNetwork.addJunction(junction);
    }

    /**
     * Adds an aircraft to the aircraftList.
     *
     * @param air aircraft to be added.
     * @return Returns true if the aircraft is added or false if the list
     * already contains it.
     */
    public boolean addAircraft(Aircraft air) {
        return this.aircraftList.addAircraft(air);
    }

    public Junction findJunction(String id) {
        return airNetwork.findJunction(id);
    }

    public boolean addSegment(Segment node) {
        return airNetwork.addSegment(node);
    }

    public SegmentList getSegmentList() {
        return this.segmentList;
    }

    public AirNetwork getAirNetwork() {
        return this.airNetwork;
    }

    public AirportList getAirportList() {
        return airportList;
    }

    public void setAirportList(AirportList airportList) {
        this.airportList = airportList;
    }

    public AircraftList getAircraftList() {
        return aircraftList;
    }

    public void setAircraftList(AircraftList aircraftList) {
        this.aircraftList = aircraftList;
    }

    public NetworkAnalysisResultsList getNetworkAnalysisResultsList() {
        return naList;
    }

    /**
     * Validates a project in order to determine if it possess all the required
     * attributes present.
     *
     * @param validateNetwork
     * @param validateAircrafts
     * @return True if the project is valid and false if not.
     */
    public boolean validate(boolean validateNetwork, boolean validateAircrafts) {
        if (this.name == null || this.name.trim().isEmpty()) {
            throw new IllegalArgumentException("The name of the project cannot "
                    + "be empty.");
        }

        if (this.description == null || this.description.trim().isEmpty()) {
            throw new IllegalArgumentException("The description of the project "
                    + "cannot be empty.");
        }

        if (validateNetwork && this.airNetwork.getGraph().getNumVertices() < 2) {
            throw new IllegalArgumentException("The project must have at least "
                    + "two intersections.");
        }

        if (validateNetwork && this.airNetwork.getGraph().getNumEdges() == 0) {
            throw new IllegalArgumentException("The project must have at least "
                    + "one segment.");
        }

        return true;
    }

    public FligthPlanList getFlightPlanList() {
        return flightPlanList;
    }

    public void setFlightPlanList(FligthPlanList flightPlanList) {
        this.flightPlanList = flightPlanList;
    }

    public NetworkAnalysisResultsList getNaList() {
        return naList;
    }

    public void setNaList(NetworkAnalysisResultsList naList) {
        this.naList = naList;
    }

    public JunctionList getJunctionList() {
        return junctionList;
    }

    public void setJunctionList(JunctionList junctionList) {
        this.junctionList = junctionList;
    }

    public FlightList getFlightList() {
        return this.flightList;
    }

    public ImportableRegistry getImportableRegistry() {
        return this.getImp();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.name);
        hash = 79 * hash + Objects.hashCode(this.description);
        hash = 79 * hash + Objects.hashCode(this.airNetwork);
        hash = 79 * hash + Objects.hashCode(this.segmentList);
        hash = 79 * hash + Objects.hashCode(this.airportList);
        hash = 79 * hash + Objects.hashCode(this.aircraftList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Project other = (Project) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.airNetwork, other.airNetwork)) {
            return false;
        }
        if (!Objects.equals(this.segmentList, other.segmentList)) {
            return false;
        }
        if (!Objects.equals(this.airportList, other.airportList)) {
            return false;
        }
        if (!Objects.equals(this.aircraftList, other.aircraftList)) {
            return false;
        }
        if (!Objects.equals(this.flightPlanList, other.flightPlanList)) {
            return false;
        }
        if (!Objects.equals(this.naList, other.naList)) {
            return false;
        }
        if (!Objects.equals(this.junctionList, other.junctionList)) {
            return false;
        }
        if (!Objects.equals(this.flightList, other.flightList)) {
            return false;
        }
        if (!Objects.equals(this.imp, other.imp)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Project{" + "name=" + name + ", description=" + description + ", airNetwork=" + airNetwork + '}';
    }

    /**
     * @return the imp
     */
    public ImportableRegistry getImp() {
        return imp;
    }

}

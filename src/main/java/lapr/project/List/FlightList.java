/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.List;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import lapr.project.model.Flight;

/**
 *
 * @author cdsn_
 */
public class FlightList {

    private List<Flight> flightList;

    private static int number;

    public FlightList() {
        this.flightList = new ArrayList<>();
        number = 0002;
    }

    /**
     * Creates a clone of the target flightlist
     *
     * @param source The target flight list to clone.
     */
    public FlightList(FlightList source) {
        flightList = new ArrayList(source.flightList);
        Collections.copy(flightList, source.flightList);
    }

    /**
     * Return the number of flights.
     *
     * @return size
     */
    public int size() {
        return this.flightList.size();
    }

    /**
     * Return list of flights
     *
     * @return flightList
     */
    public List<Flight> getFlightList() {
        return this.flightList;
    }

    /**
     * Creates a new Flight.
     *
     * @return New Flight.
     */
    public Flight newFlight() {
        return new Flight();
    }

    private String nameFlight(Flight f) {
        String nome = f.getFlightPlan().getAircraft().getCompany();
        nome = nome.substring(0, 2);
        return nome = nome + number;
    }

    /**
     * Add an flight to the flight list.
     *
     * @param f flight to be added.
     * @return True if the flight was added , otherwise returns false.
     */
    public boolean addFlight(Flight f) {
        String nome = nameFlight(f);

        for (Flight f1 : getFlightList()) {

            f.setName(nome);
            number++;

        }

        return this.getFlightList().add(f);
    }

    /**
     * Remove the Flight received by parameter Flight list
     *
     * @param flight Flight to be removed.
     * @return True if the Flight was added , otherwise returns false.
     */
    public boolean removeFlight(Flight flight) {
        return this.flightList.remove(flight);
    }

    /**
     * Returns an iterator to allow running through the list of Flights.
     *
     * @return Iterator.
     */
    public Iterator<Flight> iterator() {
        return this.flightList.iterator();
    }

}

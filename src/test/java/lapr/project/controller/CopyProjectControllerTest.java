
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import lapr.project.model.Junction;
import lapr.project.model.Project;
import lapr.project.model.ProjectRegistry;
import lapr.project.model.Segment;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro Oliveira
 */
public class CopyProjectControllerTest {

    private ProjectRegistry pr;
    private Project p;

    public CopyProjectControllerTest() {
        pr = new ProjectRegistry();
        p = new Project("nome", "descricao");
        Junction junction = new Junction("name", 1, 1);
        Junction junction1 = new Junction("name1", 1, 1);
        Segment s = new Segment();
        s.setStartNode(junction1);
        s.setEndNode(junction);
        p.addSegment(s);
        pr.setActiveProject(p);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getProjectName method, of class CopyProjectController.
     */
    @Test
    public void testGetAndSetProjectName() {
        System.out.println("getProjectName");
        CopyProjectController instance = new CopyProjectController(pr);
        String expResult = "nome";
        instance.setProjectName(expResult);
        String result = instance.getProjectName();
        assertEquals(expResult, result);

    }

    /**
     * Test of getProjectDescription method, of class CopyProjectController.
     */
    @Test
    public void testGetAndSetProjectDescription() {
        System.out.println("getProjectDescription");
        CopyProjectController instance = new CopyProjectController(pr);
        String expResult = "descricao1";
        instance.setProjectDescription(expResult);
        String result = instance.getProjectDescription();
        assertEquals(expResult, result);

    }

    /**
     * Test of registerCopy method, of class CopyProjectController. IMPLICA BASE
     * DADOS
     */
    @Test
    public void testRegisterCopy() {
        System.out.println("registerCopy");
        CopyProjectController instance = new CopyProjectController(pr);

        boolean expResult = true;
        boolean result = instance.registerCopy();
        assertEquals(expResult, result);

    }

}

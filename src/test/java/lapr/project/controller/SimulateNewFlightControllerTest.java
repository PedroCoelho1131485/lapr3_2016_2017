/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.ArrayList;
import java.util.List;
import lapr.project.model.Aircraft;
import lapr.project.model.AircraftModel;
import lapr.project.model.Airport;
import lapr.project.model.FlightPlan;
import lapr.project.model.Junction;
import lapr.project.model.NetworkAnalysis;
import lapr.project.model.NetworkAnalysisResult;
import lapr.project.model.Project;
import lapr.project.model.ProjectRegistry;
import lapr.project.model.Segment;
import lapr.project.model.Wind;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro Oliveira
 */
public class SimulateNewFlightControllerTest {

    private FlightPlan fp;
    private ProjectRegistry pr;
    private Project p;
    private NetworkAnalysis na;
    private NetworkAnalysisResult nar;
    private SimulateNewFlightController snf;
    private Segment s;

    public SimulateNewFlightControllerTest() {
        Wind w = new Wind(1.5, 1.5, "name");
        Junction junction = new Junction("name", 1, 1);
        Junction junction1 = new Junction("name1", 1, 1);
        s = new Segment("name", w, junction, junction1, "norte");
        nar = new NetworkAnalysisResult();

        AircraftModel am = new AircraftModel();
        Airport a = new Airport("a", "name", "town", "country", 1, 1, 1);
        Airport a1 = new Airport("a1", "name", "town", "country", 1, 1, 1);
        Aircraft air = new Aircraft("a", "company", 1, 1, 1, am, 1);
        fp = new FlightPlan(air, a, a1, "nome");
        nar.setAircraft(air);

        p = new Project();

        p.getFlightPlanList().addFlightPlan(fp);

        na = new NetworkAnalysis(p, fp);
        pr = new ProjectRegistry();
        pr.setActiveProject(p);
        snf = new SimulateNewFlightController(pr);
        snf.setFlightPlan(fp);

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setFlightPlan method, of class SimulateNewFlightController.
     */
    @Test
    public void testGetAndSetFlightPlan() {
        System.out.println("setFlightPlan");

        SimulateNewFlightController instance = snf;
        FlightPlan expResult = fp;
        instance.setFlightPlan(fp);

        assertEquals(expResult, instance.getFlightPlan());

    }

    /**
     * Test of createNetworkAnalysis method, of class
     * SimulateNewFlightController.
     */
    @Test
    public void testCreateNetworkAnalysis() {
        System.out.println("createNetworkAnalysis");
        SimulateNewFlightController instance = snf;

        instance.createNetworkAnalysis();

    }

    /**
     * Test of setPassengers method, of class SimulateNewFlightController.
     */
    @Test
    public void testGetAndSetPassengers() {
        System.out.println("setPassengers");
        int passengersExecutive = 1;
        int passengersEconomic = 1;
        SimulateNewFlightController instance = snf;
        instance.createNetworkAnalysis();
        instance.setPassengers(passengersExecutive, passengersEconomic);

        assertEquals(passengersExecutive + passengersEconomic, instance.getPassengers());

    }

    /**
     * Test of setCrew method, of class SimulateNewFlightController.
     */
    @Test
    public void testGetAndSetCrew() {
        System.out.println("setCrew");
        int crew = 1;
        SimulateNewFlightController instance = snf;
        instance.createNetworkAnalysis();
        instance.setCrew(crew);

        assertEquals(crew, instance.getCrew());
    }

    /**
     * Test of setCargoLoad method, of class SimulateNewFlightController.
     */
    @Test
    public void testGetAndSetCargoLoad() {
        System.out.println("setCargoLoad");
        double cargoLoad = 1;
        SimulateNewFlightController instance = snf;
        instance.createNetworkAnalysis();
        instance.setCargoLoad(cargoLoad);
        assertEquals(cargoLoad, instance.getCargoLoad(), 0);

    }

    /**
     * Test of setFuelLoad method, of class SimulateNewFlightController.
     */
    @Test
    public void testGetAndSetFuelLoad() {
        System.out.println("setFuelLoad");
        double fuelLoad = 1;
        SimulateNewFlightController instance = snf;

        instance.createNetworkAnalysis();
        instance.setFuelLoad(fuelLoad);
        assertEquals(fuelLoad, instance.getFuelLoad(), 0);
    }

    /**
     * Test of calculate method, of class SimulateNewFlightController.
     */
    @Test
    public void testCalculate() {
        System.out.println("calculate");
        int num = 0;
        SimulateNewFlightController instance = snf;          //----------------SO QND OS PATHS FUNCIONAREM
        instance.calculate(num);

    }

    /**
     * Test of getListOfSegments method, of class SimulateNewFlightController.
     */
    @Test
    public void testGetListOfSegments() {
        System.out.println("getListOfSegments");
        SimulateNewFlightController instance = snf;
        List<Segment> expResult = new ArrayList();
        expResult.add(s);

        List<Segment> result = instance.getListOfSegments();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail. //----------------SO QND OS PATHS FUNCIONAREM
        fail("The test case is a prototype.");

    }

    /**
     * Test of getAircraft method, of class SimulateNewFlightController.
     */
    @Test
    public void testGetAircraft() {
        System.out.println("getAircraft");
        SimulateNewFlightController instance = snf;

        instance.createNetworkAnalysis();

        String expResult = "a";
        String result = instance.getAircraft();
        assertEquals(expResult, result);

    }

    /**
     * Test of getTravellingTime method, of class SimulateNewFlightController.
     */
    @Test
    public void testGetTravellingTime() {
        System.out.println("getTravellingTime");
        SimulateNewFlightController instance = snf;
        double expResult = 0.0;
        double result = instance.getTravellingTime();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getEnergyConsumption method, of class
     * SimulateNewFlightController.
     */
    @Test
    public void testGetEnergyConsumption() {
        System.out.println("getEnergyConsumption");
        SimulateNewFlightController instance = snf;
        double expResult = 0.0;
        double result = instance.getEnergyConsumption();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}

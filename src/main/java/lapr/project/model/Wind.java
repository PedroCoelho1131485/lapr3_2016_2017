
package lapr.project.model;

/**
 *
 *
 */
public class Wind {

    /**
     * Wind Angle
     */
    private double windAngle;

    /**
     * Wind speed(m/s)
     */
    private double windSpeed;

    private String windName;

    private static int cont;

    public Wind(Double angle, Double speed, String name) {

        this.windAngle = angle;
        this.windSpeed = speed;
        this.windName = "wind" + cont;
        cont++;
    }

    public Wind() {
        this.windName = "wind" + cont;
        this.windAngle = 0;
        this.windSpeed = 0;
        cont++;
    }

    /**
     * @return the windName
     */
    public String getWindName() {
        return windName;
    }

    /**
     * @param windName the windName to set
     */
    public void setWindName(String windName) {
        this.windName = windName;
    }

    /**
     * @return windAngle
     */
    public double getWindAngle() {
        return windAngle;
    }

    /**
     * Modify windAngle
     *
     * @param windAngle
     */
    public void setWindAngle(double windAngle) {
        this.windAngle = windAngle;
    }

    /**
     * Return windSpeed
     *
     * @return windSpeed
     */
    public double getWindSpeed() {
        return windSpeed;
    }

    /**
     * Modify WindSpeed
     *
     * @param windSpeed
     */
    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    @Override
    public String toString() {
        return "Wind Angle: " + this.windAngle
                + "\nWind Speed: " + this.windSpeed + " m/s"
                + "\nWind Name: " + this.getWindName();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.windAngle) ^ (Double.doubleToLongBits(this.windAngle) >>> 32));
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.windSpeed) ^ (Double.doubleToLongBits(this.windSpeed) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Wind other = (Wind) obj;
        if (Double.doubleToLongBits(this.windAngle) != Double.doubleToLongBits(other.windAngle)) {
            return false;
        }
        if (Double.doubleToLongBits(this.windSpeed) != Double.doubleToLongBits(other.windSpeed)) {
            return false;
        }
        return true;
    }

    /**
     * Validate the wind.
     *
     * @return True if wind is valid else returns false.
     */
    public boolean validate() {
        if (this.windSpeed < 0) {
            throw new IllegalArgumentException("Speed should be greater than"
                    + " or equal to 0");
        }

        return true;
    }

}

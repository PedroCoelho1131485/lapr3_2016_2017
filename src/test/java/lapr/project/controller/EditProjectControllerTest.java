/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import lapr.project.model.ImportXML;
import lapr.project.model.Project;
import lapr.project.model.ProjectRegistry;
import lapr.project.model.datalayer.DataBaseManager;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Utilizador
 */
public class EditProjectControllerTest {

    EditProjectController instance;

    public EditProjectControllerTest() {

    }

    /**
     * Test of getProjectName method, of class EditProjectController.
     */
    @Test
    public void testFullController() {
        System.out.println("testFullController");
        Project project = new Project();
        ProjectRegistry ph = new ProjectRegistry();
        ph.setActiveProject(project);
        ImportXML importXml = new ImportXML();
        importXml.importNetwork(project, "Network.xml");
        importXml.importAircrafts(project, "Aircraft.xml");
        importXml.importAirports(project, "Airports.xml");
        project.setName("proj_teste_3");
        project.setDescription("5 node test set");

        DataBaseManager.getInstance().getProjectData().insertProject(project);

        ph.setProjectByName("proj_teste_3");
        instance = new EditProjectController(ph);

        String name = instance.getProjectName();
        assertTrue("The name project should be proj_teste_1 but was" + name, name.equals("proj_teste_3"));

        name = instance.getProjectDescription();
        assertTrue("The description project should be 5 node test set but was" + name, name.equals("5 node test set"));

        instance.setProjectName("proj_teste_1_edit");
        name = instance.getProjectName();
        assertTrue("The name project should be proj_teste_1_edit but was" + name, name.equals("proj_teste_1_edit"));

        instance.setProjectDescription("Description_edit");
        name = instance.getProjectDescription();
        assertTrue("The description project should be Description_edit but was" + name, name.equals("Description_edit"));

        assertTrue("Result should be true", instance.registerChanges());

        assertTrue("should be true", DataBaseManager.getInstance().getProjectData().deleteProject("proj_teste_1"));
        assertTrue("should be true", DataBaseManager.getInstance().getProjectData().deleteProject("proj_teste_1_edit"));

    }
}

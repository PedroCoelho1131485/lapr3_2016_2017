
package lapr.project.model;

import java.util.Objects;

/**
 *
 *
 */
public class Airport {

    private String IATA; //sigla
    private String name;
    private String town;
    private String country;
    private double latitude;
    private double longitude;
    private double altitude;

    /**
     * class constructor
     *
     * @param IATA
     * @param name
     * @param town
     * @param country
     * @param latitude
     * @param longitude
     * @param altitude
     */
    public Airport(String IATA, String name, String town, String country, double latitude, double longitude, double altitude) {

        this.IATA = IATA;
        this.name = name;
        this.town = town;
        this.country = country;

        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
    }

    public Airport() {
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the town
     */
    public String getTown() {
        return town;
    }

    /**
     * @param town the town to set
     */
    public void setTown(String town) {
        this.town = town;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the altitude
     */
    public double getAltitude() {
        return altitude;
    }

    /**
     * @param altitude the altitude to set
     */
    public void setAltitude(double altitude) {
        this.altitude = altitude;
    }

    /**
     * @return the IATA
     */
    public String getIATA() {
        return IATA;
    }

    /**
     * @param IATA the IATA to set
     */
    public void setIATA(String IATA) {
        this.IATA = IATA;
    }

    /**
     * @return the latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude the latitude to set
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return the longitude
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude the longitude to set
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.IATA);
        hash = 97 * hash + Objects.hashCode(this.name);
        hash = 97 * hash + Objects.hashCode(this.town);
        hash = 97 * hash + Objects.hashCode(this.country);

        hash = 97 * hash + (int) (Double.doubleToLongBits(this.latitude) ^ (Double.doubleToLongBits(this.latitude) >>> 32));
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.longitude) ^ (Double.doubleToLongBits(this.longitude) >>> 32));
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.altitude) ^ (Double.doubleToLongBits(this.altitude) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Airport other = (Airport) obj;
        if (Double.doubleToLongBits(this.latitude) != Double.doubleToLongBits(other.latitude)) {
            return false;
        }
        if (Double.doubleToLongBits(this.longitude) != Double.doubleToLongBits(other.longitude)) {
            return false;
        }
        if (Double.doubleToLongBits(this.altitude) != Double.doubleToLongBits(other.altitude)) {
            return false;
        }
        if (!Objects.equals(this.IATA, other.IATA)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.town, other.town)) {
            return false;
        }
        if (!Objects.equals(this.country, other.country)) {
            return false;
        }

        return true;
    }

    /**
     * ValIATAate the airport.
     *
     * @return True if airport is valIATA else returns false.
     */
    public boolean validate() {

        if (this.IATA == null) {
            throw new IllegalArgumentException("Id can't be null");
        }

        if (this.country == null) {
            throw new IllegalArgumentException("country can't be null");
        }

        if (this.name == null) {
            throw new IllegalArgumentException("name can't be null");
        }

        if (this.town == null) {
            throw new IllegalArgumentException("town can't be null");
        }

        return true;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.ArrayList;
import java.util.List;
import lapr.project.model.Aircraft;
import lapr.project.model.Airport;
import lapr.project.model.ExportHTML;
import lapr.project.model.ExportableRegistry;
import lapr.project.model.FlightPlan;
import lapr.project.model.NetworkAnalysisResult;
import lapr.project.model.ProjectRegistry;

/**
 *
 * @author cdsn_
 */
public class ExportResultsController {

    /**
     * Instance of ProjectRegistry
     */
    private ProjectRegistry pr;

    /**
     * List of all NetworkAnalysis of project
     */
    private List<NetworkAnalysisResult> list;

    /**
     * List with up to 4 NetworkAnalysis to export
     */
    private List<NetworkAnalysisResult> listToExport;

    /**
     * Instance of ExportableRegistry
     */
    private ExportableRegistry er;

    /**
     * Instance of ExportHTML
     */
    private ExportHTML eHtml;

    /**
     * Instance of Aircraft
     */
    private Aircraft air;

    /**
     * Origin Airport
     */
    private Airport origin;

    /**
     * Destiny Airport
     */
    private Airport destiny;

    /**
     * FlightPlan
     */
    private FlightPlan fp;

    /**
     * Creates an instance of ExportResultsController receiving a
     * ProjectRegistry
     */
    public ExportResultsController(ProjectRegistry pr) {
        this.pr = pr;
        this.er = new ExportableRegistry();
        this.eHtml = new ExportHTML();
        this.list = this.pr.getActiveProject().getNetworkAnalysisResultsList().getNAList();
    }

    /**
     * Return all FlightPlans of project
     *
     * @return list of FlightPlan
     */
    public List<FlightPlan> getListFlightPlans() {
        return this.pr.getActiveProject().getFlightPlanList().getFlightPlanList();
    }

    /**
     * Select a FligtPlan
     */
    public void setFlightPlan(FlightPlan fp) {
        this.fp = fp;
    }

    /**
     * @return the air
     */
    public Aircraft getAir() {
        return air;
    }

    /**
     * @return the list
     */
    public List<NetworkAnalysisResult> getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(List<NetworkAnalysisResult> list) {
        this.list = list;
    }

    /**
     * @return the origin
     */
    public Airport getOrigin() {
        return origin;
    }

    /**
     * @param origin the origin to set
     */
    public void setOrigin(Airport origin) {
        this.origin = origin;
    }

    /**
     * @return the destiny
     */
    public Airport getDestiny() {
        return destiny;
    }

    /**
     * @param destiny the destiny to set
     */
    public void setDestiny(Airport destiny) {
        this.destiny = destiny;
    }

    /**
     * @param air the air to set
     */
    public void setAir(Aircraft air) {
        this.air = air;
    }

    /**
     * Return a flight plan list with the same flight plain selected
     *
     * @return list List of analyzes
     */
    public List<NetworkAnalysisResult> getListAnalysisByFlightPlan() {
        List<NetworkAnalysisResult> list = new ArrayList<>();

        for (NetworkAnalysisResult nar : this.list) {
            if (this.fp.getName().equals(nar.getFp().getName())) {
                list.add(nar);
            }
        }

        return list;
    }

    public List<Airport> getAirportsList() {
        return this.pr.getActiveProject().getAirportList().getAirports();
    }

    /**
     * Returns the analyzes with the origin and destination passed as parameter
     *
     * @param origin
     * @param destiny
     * @return list of NetworkAnalysisResults
     */
    public List<NetworkAnalysisResult> getAnalysisOriDest(Airport origin, Airport destiny) {
        List<NetworkAnalysisResult> list = new ArrayList<>();

        for (NetworkAnalysisResult nar : this.getList()) {
            if (nar.getOrigin().getName().equalsIgnoreCase(origin.getName()) && nar.getDestiny().getName().equalsIgnoreCase(destiny.getName())) {
                list.add(nar);
            }
        }

        return list;
    }

    /**
     * Return an AircraftList
     *
     * @return list
     */
    public List<Aircraft> getListAircrafts() {
        return this.pr.getActiveProject().getAircraftList().getAircraftList();
    }

    public List<NetworkAnalysisResult> getAnalysisByAircraft() {
        List<NetworkAnalysisResult> list = new ArrayList<>();

        for (NetworkAnalysisResult nar : this.getList()) {
            if (nar.getAircraft().getRegistration().equalsIgnoreCase(this.air.getRegistration())) {
                list.add(nar);
            }
        }

        return list;
    }

    public void selectAnalysisByAircraft(NetworkAnalysisResult na) {
        listToExport.add(na);
    }

    /**
     * Exports Selected Analyzes
     *
     * @param filePath Name of file
     */
    public void exportHTML(String filePath) {
        //this.eHtml.exportNetworkAnalysis(this.listToExport, filePath);
    }

}

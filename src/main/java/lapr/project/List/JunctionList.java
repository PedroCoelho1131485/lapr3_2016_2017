
package lapr.project.List;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import lapr.project.model.Junction;

/**
 *
 *
 */
public class JunctionList {

    /**
     * List of junctions
     */
    private List<Junction> junctionList;

    /**
     * Build an instance of JunctionList
     */
    public JunctionList() {
        this.junctionList = new ArrayList<>();
    }

    /**
     * Creates a clone of the target junctionList
     *
     * @param source The target junction list to clone.
     */
    public JunctionList(JunctionList source) {
        junctionList = new ArrayList(source.junctionList);
        Collections.copy(junctionList, source.junctionList);
    }

    /**
     * Modify the list
     *
     * @param junctionList
     */
    public void setJunctionList(List<Junction> junctionList) {
        this.junctionList.clear();
        this.junctionList.addAll(junctionList);
    }

    /**
     * Return junctionList
     *
     * @return junctionList
     */
    public List<Junction> getjunctionList() {
        return this.junctionList;
    }

    /**
     * Add junction to listJunction
     *
     * @param junction
     * @return Returns true if added and false otherwise
     */
    public boolean addJunction(Junction junction) {

        if (this.junctionList.contains(junction)) {
            return false;
        } else {
            junctionList.add(junction);
            return true;
        }

    }

    /**
     * Returns an iterator to allow running through the list of juntions.
     *
     * @return Iterator.
     */
    public Iterator<Junction> iterator() {
        return this.junctionList.iterator();
    }

    /**
     * Remove junction of listJunction
     *
     * @param junction
     * @return
     */
    public boolean removeJunction(Junction junction) {
        return this.junctionList.remove(junction);
    }

    public Junction getJunctionByName(String name) {
        for (Junction junction : junctionList) {
            if (junction.getName().equals(name)) {
                return junction;
            }
        }
        return null;
    }

}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.ArrayList;
import java.util.List;
import lapr.project.List.AirportList;
import lapr.project.model.Aircraft;
import lapr.project.model.Airport;
import lapr.project.model.FlightPlan;
import lapr.project.model.Project;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro Oliveira
 */
public class CreateFlightPlanControllerTest {

    private FlightPlan flightPlan;
    private Project p;

    private CreateFlightPlanController cfp;

    public CreateFlightPlanControllerTest() {
        Aircraft a = new Aircraft();
        p = new Project();
        cfp = new CreateFlightPlanController(p);
        cfp.createFlightPlan();
        cfp.setAircraftType(a);

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createFlightPlan method, of class CreateFlightPlanController.
     */
    @Test
    public void testCreateFlightPlan() {
        System.out.println("createFlightPlan");
        CreateFlightPlanController instance = new CreateFlightPlanController(p);
        instance.createFlightPlan();

    }

    /**
     * Test of getAircraftList method, of class CreateFlightPlanController.
     */
    @Test
    public void testGetAircraftList() {
        System.out.println("getAircraftList");
        CreateFlightPlanController instance = cfp;
        List<Aircraft> expResult = new ArrayList();
        Aircraft a = new Aircraft();
        expResult.add(a);
        p.getAircraftList().addAircraft(a);
        List<Aircraft> result = instance.getAircraftList();
        assertEquals(expResult, result);

    }

    /**
     * Test of setAircraftType method, of class CreateFlightPlanController.
     */
    @Test
    public void testGetandSetAircraftType() {
        System.out.println("setAircraftType");
        Aircraft expResult = new Aircraft();
        CreateFlightPlanController instance = cfp;

        instance.setAircraftType(expResult);
        assertEquals(expResult, instance.getAircraft());
    }

    /**
     * Test of setMaxNumberPassangers method, of class
     * CreateFlightPlanController.
     */
    @Test
    public void testgetAndSetMaxNumberPassangers() {

        System.out.println("setMaxNumberPassangers");
        int passengersExec = 1;
        int passengersEco = 1;
        CreateFlightPlanController instance = cfp;
        instance.setMaxNumberPassangers(passengersExec, passengersEco);
        assertEquals(passengersExec, instance.getPassengersExec());
//        assertEquals(passengersEco, instance.getPassengersEco());
    }

    /**
     * Test of setCrew method, of class CreateFlightPlanController.
     */
    @Test
    public void testGetAndSetCrew() {
        System.out.println("setCrew");
        int expResult = 1;
        CreateFlightPlanController instance = cfp;

        instance.setCrew(expResult);
        assertEquals(expResult, instance.getCrew());
    }

}

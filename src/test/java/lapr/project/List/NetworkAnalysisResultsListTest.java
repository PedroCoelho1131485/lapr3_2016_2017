/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.List;

import java.util.ArrayList;
import java.util.List;
import lapr.project.List.NetworkAnalysisResultsList;
import lapr.project.model.NetworkAnalysisResult;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class NetworkAnalysisResultsListTest {

    private NetworkAnalysisResult na;
    private NetworkAnalysisResultsList nar;

    public NetworkAnalysisResultsListTest() {
        nar = new NetworkAnalysisResultsList();
        na = new NetworkAnalysisResult();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setNetworkList method, of class NetworkAnalysisResultsList.
     */
    @Test
    public void testSetNetworkList() {
        System.out.println("setNetworkList");
        List<NetworkAnalysisResult> naList = new ArrayList();
        NetworkAnalysisResultsList instance = nar;
        NetworkAnalysisResult na = new NetworkAnalysisResult();
        instance.setNetworkList(naList);
        assertEquals(naList, instance.getNAList());
    }

    /**
     * Test of getNAList method, of class NetworkAnalysisResultsList.
     */
    @Test
    public void testGetNAList() {
        System.out.println("getNAList");
        NetworkAnalysisResultsList instance = nar;
        List<NetworkAnalysisResult> expResult = new ArrayList();
        expResult.add(na);
        List<NetworkAnalysisResult> result = instance.getNAList();
        instance.addNetworkAnalysisResult(na);
        assertEquals(expResult, result);

    }

    /**
     * Test of addNetworkAnalysisResult method, of class
     * NetworkAnalysisResultsList.
     */
    @Test
    public void testAddNetworkAnalysisResult() {
        System.out.println("addNetworkAnalysisResult");
    
        NetworkAnalysisResultsList instance = nar;
        boolean expResult = true;
        boolean result = instance.addNetworkAnalysisResult(na);
        assertEquals(expResult, result);

    }

    /**
     * Test of removeNetworkAnalysisResult method, of class
     * NetworkAnalysisResultsList.
     */
    @Test
    public void testRemoveNetworkAnalysisResult() {
        System.out.println("removeNetworkAnalysisResult");
   
        NetworkAnalysisResultsList instance = nar;
        instance.addNetworkAnalysisResult(na);
        boolean expResult = true;
        boolean result = instance.removeNetworkAnalysisResult(na);
        assertEquals(expResult, result);
   
    }

    /**
     * Test of equals method, of class NetworkAnalysisResultsList.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object outroObjeto = new NetworkAnalysisResultsList();
        NetworkAnalysisResultsList instance = new NetworkAnalysisResultsList();
        boolean expResult = true;
        boolean result = instance.equals(outroObjeto);
        assertEquals(expResult, result);
        //second case
        NetworkAnalysisResult na = new NetworkAnalysisResult();
        instance.addNetworkAnalysisResult(na);
        expResult = false;
        result = instance.equals(outroObjeto);
        assertEquals(expResult, result);
    }

}


package lapr.project.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author Diogo Guedes <1150613@isep.ipp.pt>
 */
public class JunctionTest {

    private Junction j;

    public JunctionTest() {
        j = new Junction();
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of getAndSetId method, of class AircraftModel.
     */
    @Test
    public void testGetAndSetName() {
        System.out.println("getAndSetName");
        Junction instance = j;
        String expResult = "TVB-21";
        instance.setName(expResult);
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAndSetLatitude method, of class AircraftModel.
     */
    @Test
    public void testGetAndSetLatitude() {
        System.out.println("getAndSetLatitude");
        Junction instance = j;
        double expResult = 23.2;
        instance.setLatitude(expResult);
        double result = instance.getLatitude();
        assertEquals(expResult, result,0.0);
    }

    /**
     * Test of getAndSetLongitude method, of class AircraftModel.
     */
    @Test
    public void testGetAndSetLongitude() {
        System.out.println("getAndSetLongitude");
        Junction instance = j;
        double expResult = 23.2;
        instance.setLongitude(expResult);
        double result = instance.getLongitude();
        assertEquals(expResult, result,0.0);
    }

    /**
     * Test of getName method, of class Junction.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Junction instance = new Junction();
        String expResult = "";
        instance.setName(expResult);
        String result = instance.getName();
        assertEquals(expResult, result);

    }

    /**
     * Test of setName method, of class Junction.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "";
        Junction instance = new Junction();
        instance.setName(name);
        assertEquals(name,instance.getName());
    }

    /**
     * Test of getLatitude method, of class Junction.
     */
    @Test
    public void testGetLatitude() {
        System.out.println("getLatitude");
        Junction instance = new Junction();
        double expResult = 0.0;
        instance.setLatitude(expResult);
        double result = instance.getLatitude();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setLatitude method, of class Junction.
     */
    @Test
    public void testSetLatitude() {
        System.out.println("setLatitude");
        double latitude = 0.0;
        Junction instance = new Junction();
        instance.setLatitude(latitude);
        assertEquals(latitude,instance.getLatitude(),0.0);
    }

    /**
     * Test of getLongitude method, of class Junction.
     */
    @Test
    public void testGetLongitude() {
        System.out.println("getLongitude");
        Junction instance = new Junction();
        double expResult = 0.0;
        instance.setLongitude(expResult);
        double result = instance.getLongitude();
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of setLongitude method, of class Junction.
     */
    @Test
    public void testSetLongitude() {
        System.out.println("setLongitude");
        double longitude = 0.0;
        Junction instance = new Junction();
        instance.setLongitude(longitude);
        assertEquals(longitude,instance.getLongitude(),0.0);
    }

    /**
     * Test of toString method, of class Junction.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String name = "001";
        double latitude = 30.0;
        double longitude = 1.0;
        Junction instance = new Junction();
        instance.setName(name);
        instance.setLongitude(longitude);
        instance.setLatitude(latitude);
        String expResult = "Junction{" + "id=" + name + ", latitude=" + latitude + ", longitude=" + longitude + '}';;
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Junction.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = (Junction) new Junction();
        Junction instance = new Junction();
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
        expResult = false;
        instance.setName("001");
        result = instance.equals(obj);
        assertEquals(expResult,result);
    }

    /**
     * Test of hashCode method, of class Junction.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Junction instance = new Junction();
        int expResult = 34391;
        int result = instance.hashCode();
        assertEquals(expResult, result);

    }

    /**
     * Test of validate method, of class Junction.
     */
    @Test
    public void testValidate() {
        System.out.println("validate");
        Junction instance = new Junction();
        instance.setName("001");
        boolean expResult = true;
        boolean result = instance.validate();
        assertEquals(expResult, result);
    }

}

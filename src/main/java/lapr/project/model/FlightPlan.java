/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author cdsn_
 */
public class FlightPlan {

    /**
     * Name of flight plan
     */
    private String name;

    /**
     * Aircraft type
     */
    private Aircraft aircraft;

    /**
     * Origin airfield
     */
    private Airport originAirfield;

    /**
     * Destiny airfield
     */
    private Airport desttinyAirfield;

    /**
     * Technical Stops and mandatory wayPoints
     */
    private List<Segment> stops;

    private List<Double> altitude;
    private List<Double> Vclimb;
    private List<Double> Vdesc;

    public FlightPlan(Aircraft air, Airport origin, Airport destiny, String name) {
        this.aircraft = air;
        this.originAirfield = origin;
        this.desttinyAirfield = destiny;
        this.name = name;
        this.stops = new ArrayList<>();
        this.altitude = new ArrayList<>();
        this.Vclimb = new ArrayList<>();
        this.Vdesc = new ArrayList<>();

    }

    public FlightPlan() {
        this.aircraft = null;
        this.originAirfield = null;
        this.desttinyAirfield = null;
        this.name = "FlightPlan";
        this.stops = new ArrayList<>();
        this.stops = new ArrayList<>();
        this.altitude = new ArrayList<>();
        this.Vclimb = new ArrayList<>();
        this.Vdesc = new ArrayList<>();
    }

    /**
     * @return the altitude
     */
    public List<Double> getAltitude() {
        return altitude;
    }

    /**
     * @param altitude the altitude to set
     */
    public void setAltitude(List<Double> altitude) {
        this.altitude = altitude;
    }

    /**
     * @return the Vclimb
     */
    public List<Double> getVclimb() {
        return Vclimb;
    }

    /**
     * @param Vclimb the Vclimb to set
     */
    public void setVclimb(List<Double> Vclimb) {
        this.Vclimb = Vclimb;
    }

    /**
     * @return the Vdesc
     */
    public List<Double> getVdesc() {
        return Vdesc;
    }

    /**
     * @param Vdesc the Vdesc to set
     */
    public void setVdesc(List<Double> Vdesc) {
        this.Vdesc = Vdesc;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the aircraft
     */
    public Aircraft getAircraft() {
        return aircraft;
    }

    /**
     * @return the maxPassengersClasseEco of Aircraft
     */
    public int getMaxPassengersClasseEco() {
        return this.aircraft.getNumberSeatsEconomic();
    }

    /**
     * @param maxPassengersClasseEco the maxPassengersClasseEco to set
     */
    public void setMaxPassengersClasseEco(int maxPassengersClasseEco) {
        this.aircraft.setNumberSeatsEconomic(maxPassengersClasseEco);
    }

    /**
     * @return the maxPassengersClasseExec
     */
    public int getMaxPassengersClasseExec() {
        return this.aircraft.getNumberSeatsExecutiv();
    }

    /**
     * @param maxPassengersClasseExec the maxPassengersClasseExec to set
     */
    public void setMaxPassengersClasseExec(int maxPassengersClasseExec) {
        this.aircraft.setNumberSeatsExecutiv(maxPassengersClasseExec);
    }

    /**
     * @param aircraft the aircraft to set
     */
    public void setAircraft(Aircraft aircraft) {
        this.aircraft = aircraft;
    }

    /**
     * @return the crew
     */
    public int getCrew() {
        return this.aircraft.getnElemCrew();
    }

    /**
     * @param crew the crew to set
     */
    public void setCrew(int crew) {
        this.aircraft.setnElemCrew(crew);
    }

    /**
     * @return the originAirfield
     */
    public Airport getOriginAirfield() {
        return originAirfield;
    }

    /**
     * @param originAirfield the originAirfield to set
     */
    public void setOriginAirfield(Airport originAirfield) {
        this.originAirfield = originAirfield;
    }

    /**
     * @return the desttinyAirfield
     */
    public Airport getDesttinyAirfield() {
        return desttinyAirfield;
    }

    /**
     * @param desttinyAirfield the desttinyAirfield to set
     */
    public void setDesttinyAirfield(Airport desttinyAirfield) {
        this.desttinyAirfield = desttinyAirfield;
    }

    /**
     * @return list of mandatory wayPoints and technical stops
     */
    public List<Segment> getStops() {
        return this.stops;
    }

    /**
     * @param s segment to add
     * @return
     */
    public boolean addSegmentsToStops(Segment s) {
        if (!this.stops.contains(s)) {
            return stops.add(s);
        } else {
            return false;
        }

    }

    /**
     * @param segment to be added to the list
     */
    public boolean addStop(Segment segment) {
        if (this.stops.contains(segment)) {
            return false;
        } else {
            this.stops.add(segment);
            return true;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FlightPlan other = (FlightPlan) obj;

        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.aircraft, other.aircraft)) {
            return false;
        }
        if (!Objects.equals(this.originAirfield, other.originAirfield)) {
            return false;
        }
        if (!Objects.equals(this.desttinyAirfield, other.desttinyAirfield)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Name: " + name;
    }

}

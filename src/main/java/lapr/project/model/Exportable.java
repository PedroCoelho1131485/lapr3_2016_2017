/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.List;

/**
 *
 * @author cdsn_
 */
public interface Exportable {

    /**
     * Exports the data contained in project, to file HTML
     *
     * @param nar Export the results from the network analysis results.
     * @param filePath Path to the file.
     */
    public void exportNetworkAnalysis(NetworkAnalysisResult nar, String filePath);
}

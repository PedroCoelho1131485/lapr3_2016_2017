
package lapr.project.model;

import lapr.project.List.CdragList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;

public class AircraftModelTest {

    private final AircraftModel air = new AircraftModel();

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    
    /**
     * Test of the default construtor of class AircraftModel
     * 
     */
    
    @Test
    public void testDefaultConstrutor() {
        
        System.out.println("Construtor Default");
        AircraftModel a = new AircraftModel();
        
        assertNotNull(a);
    }
    
    /**
     * Test of the construtor that receives the specific info to initiate an object, of class AircraftModel
     */
    
    @Test
    public void testInfoConstrutor() {
        
        System.out.println("Info Construtor");
        
        String modelId = "001";
        String maker = "Boeing";
        String Type = "Cargo"; //cargo or passenger or  mixed
        Motorization motorization = new Motorization();
        double emptyWeight = 1;
        double MaximumTakeOffWeight = 1;
        double MaximumFuelCapacity = 1;
        double maxPayLoad = 1;
        double aspectRatio = 1;
        double VMO = 1;
        double MMO = 1;
        double wingArea = 1;
        double wingSpan = 1;
        
       double e = 1;
     
        CdragList CDragList = new CdragList();
        
        AircraftModel a = new AircraftModel(modelId,maker,Type,motorization,emptyWeight,MaximumTakeOffWeight,MaximumFuelCapacity,
        maxPayLoad,aspectRatio,VMO,MMO,wingArea,wingSpan,e,CDragList);
        
        assertNotNull(a);
        assertEquals(modelId,a.getModelId());
        assertEquals(maker,a.getMaker());
        assertEquals(Type,a.getType());
        assertEquals(motorization,a.getMotorization());
        assertEquals(emptyWeight,a.getEmptyWeight(),0.0);
        assertEquals(MaximumTakeOffWeight,a.getMaximumTakeOffWeight(),0.0);
        assertEquals(MaximumFuelCapacity,a.getMaximumFuelCapacity(),0.0);
        assertEquals(maxPayLoad,a.getMaxPayLoad(),0.0);
        assertEquals(aspectRatio,a.getAspectRatio(),0.0);
        assertEquals(VMO,a.getVMO(),0.0);
        assertEquals(MMO,a.getMMO(),0.0);
        assertEquals(wingArea,a.getWingArea(),0.0);
        assertEquals(wingSpan,a.getWingSpan(),0.0);
        assertEquals(e,a.getE(),0.0);
        assertEquals(CDragList,a.getCDragList());
    }

    /**
     * Test of getAndSetHeight methods, of class AircraftModel.
     */
    @Test
    public void testGetAndSetHeight() {
        System.out.println("getAndSetHeight");
        AircraftModel instance = air;
        double expResult = 13.2;
        instance.getMotorization().setCruiseAltitude(expResult);
        double result = instance.getMotorization().getCruiseAltitude();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getAndSetMaxPayLoad methods, of class AircraftModel.
     */
    @Test
    public void testGetAndSetMaxPayLoad() {
        System.out.println("getAndSetMaxPayLoad");
        AircraftModel instance = air;
        double expResult = 13.2;
        instance.setMaxPayLoad(expResult);
        double result = instance.getMaxPayLoad();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getAndSetVMO methods, of class AircraftModel.
     */
    @Test
    public void testGetAndSetVMO() {
        System.out.println("getAndSetVMO");
        AircraftModel instance = air;
        double expResult = 13.2;
        instance.setVMO(expResult);
        double result = instance.getVMO();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getAndSetMMO method, of class AircraftModel.
     */
    @Test
    public void testGetAndSetMMO() {
        System.out.println("getAndSetMMO");
        AircraftModel instance = air;
        double expResult = 13.2;
        instance.setMMO(expResult);
        double result = instance.getMMO();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getAndSetModelId method, of class AircraftModel.
     */
    @Test
    public void testGetAndSetModelId() {
        System.out.println("getAndSetModelId");
        AircraftModel instance = air;
        String expResult = "V31";
        instance.setModelId(expResult);
        String result = instance.getModelId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAndSetMaker method, of class AircraftModel.
     */
    @Test
    public void testGetAndSetMaker() {
        System.out.println("getAndSetMaker");
        AircraftModel instance = air;
        String expResult = "Boeing";
        instance.setMaker(expResult);
        String result = instance.getMaker();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAndSetType method, of class AircraftModel.
     */
    @Test
    public void testGetAndSetType() {
        System.out.println("getAndSetType");
        AircraftModel instance = air;
        String expResult = "Passangers";
        instance.setType(expResult);
        String result = instance.getType();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAndSetMotor method, of class AircraftModel.
     */
    @Test
    public void testGetAndSetMotor() {
        System.out.println("getAndSetMotor");
        AircraftModel instance = air;
        String expResult = "TVB-21";
        instance.setMotor(expResult);
        String result = instance.getMotor();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAndSetEmptyWeight method, of class AircraftModel.
     */
    @Test
    public void testGetAndSetEmptyWeight() {
        System.out.println("getAndSetEmptyWeight");
        AircraftModel instance = air;
        double expResult = 100;
        instance.setEmptyWeight(expResult);
        double result = instance.getEmptyWeight();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getAndSetMaximumTakeOffWeight method, of class AircraftModel.
     */
    @Test
    public void testGetAndSetMaximumTakeOffWeight() {
        System.out.println("getAndSetMaximumTakeOffWeight");
        AircraftModel instance = air;
        double expResult = 100;
        instance.setMaximumTakeOffWeight(expResult);
        double result = instance.getMaximumTakeOffWeight();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getAndSetMaximumFuelCapacity method, of class AircraftModel.
     */
    @Test
    public void testGetAndSetMaximumFuelCapacity() {
        System.out.println("getAndSetMaximumFuelCapacity");
        AircraftModel instance = air;
        double expResult = 100;
        instance.setMaximumFuelCapacity(expResult);
        double result = instance.getMaximumFuelCapacity();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getAndSetCruiseSpeed method, of class AircraftModel.
     */
    @Test
    public void testGetAndSetCruiseSpeed() {
        System.out.println("getAndSetCruiseSpeed");
        AircraftModel instance = air;
        double expResult = 100;
        instance.getMotorization().setCruiseSpeed(expResult);
        double result = instance.getMotorization().getCruiseSpeed();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getAndSetWingArea method, of class AircraftModel.
     */
    @Test
    public void testGetAndSetWingArea() {
        System.out.println("getAndSetWingArea");
        AircraftModel instance = air;
        double expResult = 100;
        instance.setWingArea(expResult);
        double result = instance.getWingArea();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getAndSetNumberMotors method, of class AircraftModel.
     */
    @Test
    public void testGetAndSetNumberMotors() {
        System.out.println("getAndSetNumberMotors");
        AircraftModel instance = air;
        int expResult = 100;
        instance.setNumberMotors(expResult);
        int result = instance.getNumberMotors();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getAndSetMotorType method, of class AircraftModel.
     */
    @Test
    public void testGetAndSetMotorType() {
        System.out.println("getAndSetMotorType");
        AircraftModel instance = air;
        String expResult = "TVB-21";
        instance.setMotorType(expResult);
        String result = instance.getMotorType();
        assertEquals(expResult, result);
    }

     /**
     * Test of getAndSetWingSpan method, of class AircraftModel.
     */
    @Test
    public void testGetAndSetWingSpan() {
        System.out.println("getAndSetWingSpan");
        AircraftModel instance = air;
        double expResult = 100;
        instance.setWingSpan(expResult);
        double result = instance.getWingSpan();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getAndSetE method, of class AircraftModel.
     */
    @Test
    public void testGetAndSetE() {
        System.out.println("getAndSetE");
        AircraftModel instance = air;
        double expResult = 100;
        instance.setE(expResult);
        double result = instance.getE();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getAndSetAspectRatio method , of class AircraftModel
     */
    @Test
    public void testGetandSetAspectRatio() {
        System.out.println("getAndSetAspectRatio");
        AircraftModel instance = air;
        double expResult = 100;
        instance.setAspectRatio(100);
        double result = instance.getAspectRatio();
        assertEquals(expResult, result, 0.0);
    }
    
    /**
     * Test of the method equals (overried) of class AircraftModel
     */
    
    @Test
    public void testEquals() {
        
        System.out.println("equals");
        AircraftModel a = new AircraftModel();
        AircraftModel b = new AircraftModel();
        
        assertTrue(a.equals(b));
        
         String modelId = "001";
        String maker = "Boeing";
        String Type = "Cargo"; //cargo or passenger or  mixed
        Motorization motorization = new Motorization();
        double emptyWeight = 1;
        double MaximumTakeOffWeight = 1;
        double MaximumFuelCapacity = 1;
        double maxPayLoad = 1;
        double aspectRatio = 1;
        double VMO = 1;
        double MMO = 1;
        double wingArea = 1;
        double wingSpan = 1;

        double e = 1;
     
        CdragList CDragList = new CdragList();
        
        AircraftModel c = new AircraftModel(modelId,maker,Type,motorization,emptyWeight,MaximumTakeOffWeight,MaximumFuelCapacity,maxPayLoad,aspectRatio,VMO,MMO,wingArea,wingSpan,e,CDragList);
    
        assertFalse(a.equals(c));
    
    }
    
    /**
     * Test of the method toString (override) of class AircraftModel
     */
    
    @Test
    public void testToString() {
        
        System.out.println("ToString");
        
          String modelId = "001";
        String maker = "Boeing";
        String Type = "Cargo"; //cargo or passenger or  mixed
        Motorization motorization = new Motorization();
        double emptyWeight = 1;
        double MaximumTakeOffWeight = 1;
        double MaximumFuelCapacity = 1;
        double maxPayLoad = 1;
        double aspectRatio = 1;
        double VMO = 1;
        double MMO = 1;
        double wingArea = 1;
        double wingSpan = 1;

        double e = 1;
     
        CdragList CDragList = new CdragList();
        
        AircraftModel c = new AircraftModel(modelId,maker,Type,motorization,emptyWeight,MaximumTakeOffWeight,MaximumFuelCapacity,maxPayLoad,aspectRatio,VMO,MMO,wingArea,wingSpan,e,CDragList);
    
        String expected = "AircraftModel{" + "modelId=" + modelId + ", maker=" + maker + ", Type=" + Type + ", motorization=" + motorization + ", emptyWeight=" + emptyWeight + ", MaximumTakeOffWeight=" + MaximumTakeOffWeight + ", MaximumFuelCapacity=" + MaximumFuelCapacity + ", maxPayLoad=" + maxPayLoad + ", aspectRatio=" + aspectRatio + ", VMO=" + VMO + ", MMO=" + MMO + ", wingArea=" + wingArea + ", wingSpan=" + wingSpan + ", e=" + e + '}';
    
        String result = c.toString();
        
        assertEquals(expected,result);
        
        System.out.print(result);
    }

    /**
     * Test of getMaxPayLoad method, of class AircraftModel.
     */
    @Test
    public void testGetMaxPayLoad() {
        System.out.println("getMaxPayLoad");
        AircraftModel instance = new AircraftModel();
        double expResult = 0.0;
        instance.setMaxPayLoad(expResult);
        double result = instance.getMaxPayLoad();
        assertEquals(expResult, result, 0.0);
        expResult = 200.0;
        instance.setMaxPayLoad(expResult);
        result = instance.getMaxPayLoad();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setMaxPayLoad method, of class AircraftModel.
     */
    @Test
    public void testSetMaxPayLoad() {
        System.out.println("setMaxPayLoad");
        double maxPayLoad = 0.0;
        AircraftModel instance = new AircraftModel();
        instance.setMaxPayLoad(maxPayLoad);
        assertEquals(maxPayLoad,instance.getMaxPayLoad(),0.0);
        maxPayLoad = 200.0;
        instance.setMaxPayLoad(maxPayLoad);
        assertEquals(maxPayLoad,instance.getMaxPayLoad(),0.0);
    }

    /**
     * Test of getVMO method, of class AircraftModel.
     */
    @Test
    public void testGetVMO() {
        System.out.println("getVMO");
        AircraftModel instance = new AircraftModel();
        double expResult = 0.0;
        instance.setVMO(expResult);
        double result = instance.getVMO();
        assertEquals(expResult, result, 0.0);
        expResult = 1400.0;
        instance.setVMO(expResult);
        result = instance.getVMO();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setVMO method, of class AircraftModel.
     */
    @Test
    public void testSetVMO() {
        System.out.println("setVMO");
        double VMO = 0.0;
        AircraftModel instance = new AircraftModel();
        instance.setVMO(VMO);
        assertEquals(VMO,instance.getVMO(),0.0);
        VMO = 1400.0;
        instance.setVMO(VMO);
        assertEquals(VMO,instance.getVMO(),0.0);
    }

    /**
     * Test of getMMO method, of class AircraftModel.
     */
    @Test
    public void testGetMMO() {
        System.out.println("getMMO");
        AircraftModel instance = new AircraftModel();
        double expResult = 0.0;
        instance.setMMO(expResult);
        double result = instance.getMMO();
        assertEquals(expResult, result, 0.0);
        expResult = 200.0;
        instance.setMMO(expResult);
        result = instance.getMMO();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setMMO method, of class AircraftModel.
     */
    @Test
    public void testSetMMO() {
        System.out.println("setMMO");
        double MMO = 0.0;
        AircraftModel instance = new AircraftModel();
        instance.setMMO(MMO);
        assertEquals(MMO,instance.getMMO(),0.0);
        MMO = 200.0;
        instance.setMMO(MMO);
        assertEquals(MMO,instance.getMMO(),0.0);
    }

    /**
     * Test of getModelId method, of class AircraftModel.
     */
    @Test
    public void testGetModelId() {
        System.out.println("getModelId");
        AircraftModel instance = new AircraftModel();
        String expResult = "001";
        instance.setModelId(expResult);
        String result = instance.getModelId();
        assertEquals(expResult, result);
     
    }

    /**
     * Test of setModelId method, of class AircraftModel.
     */
    @Test
    public void testSetModelId() {
        System.out.println("setModelId");
        String modelId = "001";
        AircraftModel instance = new AircraftModel();
        instance.setModelId(modelId);
        assertEquals(modelId,instance.getModelId());
    }

    /**
     * Test of getMaker method, of class AircraftModel.
     */
    @Test
    public void testGetMaker() {
        System.out.println("getMaker");
        AircraftModel instance = new AircraftModel();
        String expResult = "Boeing";
        instance.setMaker(expResult);
        String result = instance.getMaker();
        assertEquals(expResult, result);

    }

    /**
     * Test of setMaker method, of class AircraftModel.
     */
    @Test
    public void testSetMaker() {
        System.out.println("setMaker");
        String maker = "Boeing";
        AircraftModel instance = new AircraftModel();
        instance.setMaker(maker);
        assertEquals(maker,instance.getMaker());
    }

    /**
     * Test of getType method, of class AircraftModel.
     */
    @Test
    public void testGetType() {
        System.out.println("getType");
        AircraftModel instance = new AircraftModel();
        String expResult = "Cruiser";
        instance.setType(expResult);
        String result = instance.getType();
        assertEquals(expResult, result);

    }

    /**
     * Test of setType method, of class AircraftModel.
     */
    @Test
    public void testSetType() {
        System.out.println("setType");
        String Type = "Cruiser";
        AircraftModel instance = new AircraftModel();
        instance.setType(Type);
        assertEquals(Type,instance.getType());
    }

    /**
     * Test of getMotor method, of class AircraftModel.
     */
    @Test
    public void testGetMotor() {
        System.out.println("getMotor");
        AircraftModel instance = new AircraftModel();
        String expResult = "Motor01";
        instance.setMotor(expResult);
        String result = instance.getMotor();
        assertEquals(expResult, result);
  
    }

    /**
     * Test of setMotor method, of class AircraftModel.
     */
    @Test
    public void testSetMotor() {
        System.out.println("setMotor");
        String motor = "Motor01";
        AircraftModel instance = new AircraftModel();
        instance.setMotor(motor);
        assertEquals(motor,instance.getMotor());
    }

    /**
     * Test of getEmptyWeight method, of class AircraftModel.
     */
    @Test
    public void testGetEmptyWeight() {
        System.out.println("getEmptyWeight");
        AircraftModel instance = new AircraftModel();
        double expResult = 0.0;
        instance.setEmptyWeight(expResult);
        double result = instance.getEmptyWeight();
        assertEquals(expResult, result, 0.0);
        expResult = 200.0;
        instance.setEmptyWeight(expResult);
        result = instance.getEmptyWeight();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setEmptyWeight method, of class AircraftModel.
     */
    @Test
    public void testSetEmptyWeight() {
        System.out.println("setEmptyWeight");
        double emptyWeight = 0.0;
        AircraftModel instance = new AircraftModel();
        instance.setEmptyWeight(emptyWeight);
        assertEquals(emptyWeight,instance.getEmptyWeight(),0.0);
        emptyWeight = 200.0;
        instance.setEmptyWeight(emptyWeight);
        assertEquals(emptyWeight,instance.getEmptyWeight(),0.0);
    }

    /**
     * Test of getMaximumTakeOffWeight method, of class AircraftModel.
     */
    @Test
    public void testGetMaximumTakeOffWeight() {
        System.out.println("getMaximumTakeOffWeight");
        AircraftModel instance = new AircraftModel();
        double expResult = 0.0;
        instance.setMaximumTakeOffWeight(expResult);
        double result = instance.getMaximumTakeOffWeight();   
        assertEquals(expResult, result, 0.0);
        expResult = 100.0;
        instance.setMaximumTakeOffWeight(expResult);
        result = instance.getMaximumTakeOffWeight();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setMaximumTakeOffWeight method, of class AircraftModel.
     */
    @Test
    public void testSetMaximumTakeOffWeight() {
        System.out.println("setMaximumTakeOffWeight");
        double MaximumTakeOffWeight = 0.0;
        AircraftModel instance = new AircraftModel();
        instance.setMaximumTakeOffWeight(MaximumTakeOffWeight);
        assertEquals(MaximumTakeOffWeight,instance.getMaximumTakeOffWeight(),0.0);
        MaximumTakeOffWeight = 100.0;
        instance.setMaximumTakeOffWeight(MaximumTakeOffWeight);
        assertEquals(MaximumTakeOffWeight,instance.getMaximumTakeOffWeight(),0.0);
    }

    /**
     * Test of getMaximumFuelCapacity method, of class AircraftModel.
     */
    @Test
    public void testGetMaximumFuelCapacity() {
        System.out.println("getMaximumFuelCapacity");
        AircraftModel instance = new AircraftModel();
        double expResult = 0.0;
        instance.setMaximumFuelCapacity(expResult);
        double result = instance.getMaximumFuelCapacity();
        assertEquals(expResult, result, 0.0);
        expResult = 100.0;
        instance.setMaximumFuelCapacity(expResult);
        result = instance.getMaximumFuelCapacity();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setMaximumFuelCapacity method, of class AircraftModel.
     */
    @Test
    public void testSetMaximumFuelCapacity() {
        System.out.println("setMaximumFuelCapacity");
        double MaximumFuelCapacity = 0.0;
        AircraftModel instance = new AircraftModel();
        instance.setMaximumFuelCapacity(MaximumFuelCapacity);
        assertEquals(MaximumFuelCapacity,instance.getMaximumFuelCapacity(),0.0);
        MaximumFuelCapacity = 100.0;
        instance.setMaximumFuelCapacity(MaximumFuelCapacity);
        assertEquals(MaximumFuelCapacity,instance.getMaximumFuelCapacity(),0.0);
    }

    /**
     * Test of getWingArea method, of class AircraftModel.
     */
    @Test
    public void testGetWingArea() {
        System.out.println("getWingArea");
        AircraftModel instance = new AircraftModel();
        double expResult = 30.0;
        instance.setWingArea(expResult);
        double result = instance.getWingArea();
        assertEquals(expResult, result, 0.0);
        expResult = -30.0;
        instance.setWingArea(expResult);
        result = instance.getWingArea();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setWingArea method, of class AircraftModel.
     */
    @Test
    public void testSetWingArea() {
        System.out.println("setWingArea");
        double wingArea = 30.0;
        AircraftModel instance = new AircraftModel();
        instance.setWingArea(wingArea);
        assertEquals(wingArea,instance.getWingArea(),0.0);
        wingArea = -30.0;
        instance.setWingArea(wingArea);
        assertEquals(wingArea,instance.getWingArea(),0.0);
    }

    /**
     * Test of getNumberMotors method, of class AircraftModel.
     */
    @Test
    public void testGetNumberMotors() {
        System.out.println("getNumberMotors");
        AircraftModel instance = new AircraftModel();
        int expResult = 0;
        instance.setNumberMotors(expResult);
        int result = instance.getNumberMotors();
        assertEquals(expResult, result,0);
        expResult = 4;
        instance.setNumberMotors(expResult);
        result = instance.getNumberMotors();
        assertEquals(expResult,result,0);
    }

    /**
     * Test of setNumberMotors method, of class AircraftModel.
     */
    @Test
    public void testSetNumberMotors() {
        System.out.println("setNumberMotors");
        int numberMotors = 0;
        AircraftModel instance = new AircraftModel();
        instance.setNumberMotors(numberMotors);
        assertEquals(numberMotors,instance.getNumberMotors(),0);
        numberMotors = 4;
        instance.setNumberMotors(numberMotors);
        assertEquals(numberMotors,instance.getNumberMotors(),0);
    }

    /**
     * Test of getMotorType method, of class AircraftModel.
     */
    @Test
    public void testGetMotorType() {
        System.out.println("getMotorType");
        AircraftModel instance = new AircraftModel();
        String expResult = "MotorType";
        instance.setMotorType(expResult);
        String result = instance.getMotorType();
        assertEquals(expResult, result);
 
    }

    /**
     * Test of setMotorType method, of class AircraftModel.
     */
    @Test
    public void testSetMotorType() {
        System.out.println("setMotorType");
        String motorType = "MotorType";
        AircraftModel instance = new AircraftModel();
        instance.setMotorType(motorType);
        assertEquals(motorType,instance.getMotorType());
    }

    /**
     * Test of getWingSpan method, of class AircraftModel.
     */
    @Test
    public void testGetWingSpan() {
        System.out.println("getWingSpan");
        AircraftModel instance = new AircraftModel();
        double expResult = 12.0;
        instance.setWingSpan(expResult);
        double result = instance.getWingSpan();
        assertEquals(expResult, result, 0.0);
        expResult = -2.0;
        instance.setWingSpan(expResult);
        result = instance.getWingSpan();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setWingSpan method, of class AircraftModel.
     */
    @Test
    public void testSetWingSpan() {
        System.out.println("setWingSpan");
        double wingSpan = 12.0;
        AircraftModel instance = new AircraftModel();
        instance.setWingSpan(wingSpan);
        assertEquals(wingSpan,instance.getWingSpan(),0.0);
        wingSpan = -2.0;
        instance.setWingSpan(wingSpan);
        assertEquals(wingSpan,instance.getWingSpan(),0.0);
    }

    /**
     * Test of getE method, of class AircraftModel.
     */
    @Test
    public void testGetE() {
        System.out.println("getE");
        AircraftModel instance = new AircraftModel();
        double expResult = 2.0;
        instance.setE(expResult);
        double result = instance.getE();
        assertEquals(expResult, result, 0.0);
        expResult = -2.0;
        instance.setE(expResult);
        result = instance.getE();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setE method, of class AircraftModel.
     */
    @Test
    public void testSetE() {
        System.out.println("setE");
        double e = 1.0;
        AircraftModel instance = new AircraftModel();
        instance.setE(e);
        assertEquals(e,instance.getE(),0.0);
        e = -2.0;
        instance.setE(e);
        assertEquals(e,instance.getE(),0.0);
    }

    /**
     * Test of getMotorization method, of class AircraftModel.
     */
    @Test
    public void testGetMotorization() {
        System.out.println("getMotorization");
        AircraftModel instance = new AircraftModel();
        Motorization expResult = new Motorization();
        instance.setMotorization(expResult);
        Motorization result = instance.getMotorization();
        assertEquals(expResult, result);

    }

    /**
     * Test of getAspectRatio method, of class AircraftModel.
     */
    @Test
    public void testGetAspectRatio() {
        System.out.println("getAspectRatio");
        AircraftModel instance = new AircraftModel();
        double expResult = 9.0;
        instance.setAspectRatio(expResult);
        double result = instance.getAspectRatio();
        assertEquals(expResult, result, 0.0);
        expResult = -2.0;
        instance.setAspectRatio(expResult);
        result = instance.getAspectRatio();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setAspectRatio method, of class AircraftModel.
     */
    @Test
    public void testSetAspectRatio() {
        System.out.println("setAspectRatio");
        double aspectRatio = 2.0;
        AircraftModel instance = new AircraftModel();
        instance.setAspectRatio(aspectRatio);
        assertEquals(aspectRatio,instance.getAspectRatio(),0.0);
        aspectRatio = -1.0;
        instance.setAspectRatio(aspectRatio);
        assertEquals(aspectRatio,instance.getAspectRatio(),0.0);
    }

    /**
     * Test of setMotorization method, of class AircraftModel.
     */
    @Test
    public void testSetMotorization() {
        System.out.println("setMotorization");
        Motorization motorization = new Motorization();
        AircraftModel instance = new AircraftModel();
        instance.setMotorization(motorization);
        assertEquals(motorization,instance.getMotorization());
        motorization.setName("Motorization001");
        motorization.setCruiseSpeed(513.0);
        instance.setMotorization(motorization);
        assertEquals(motorization,instance.getMotorization());
    }

    /**
     * Test of getCDragList method, of class AircraftModel.
     */
    @Test
    public void testGetCDragList() {
        System.out.println("getCDragList");
        AircraftModel instance = new AircraftModel();
        CdragList expResult = new CdragList();
        instance.setCDragList(expResult);
        CdragList result = instance.getCDragList();
        assertEquals(expResult, result);

    }

    /**
     * Test of setCDragList method, of class AircraftModel.
     */
    @Test
    public void testSetCDragList() {
        System.out.println("setCDragList");
        CdragList CDragList = new CdragList();
        AircraftModel instance = new AircraftModel();
        instance.setCDragList(CDragList);
        assertEquals(CDragList,instance.getCDragList());
    }

    /**
     * Test of hashCode method, of class AircraftModel.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        AircraftModel instance = new AircraftModel();
        int expResult = -972821935;
        int result = instance.hashCode();
        assertEquals(expResult, result,0);

    }

}

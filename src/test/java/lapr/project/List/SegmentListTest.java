/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.List;

import lapr.project.List.SegmentList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import lapr.project.model.Junction;
import lapr.project.model.Segment;
import lapr.project.model.Wind;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class SegmentListTest {

    private SegmentList sl;
    private Segment s;

    public SegmentListTest() {
        Wind w = new Wind(1.5, 1.5, "name");
        Junction junction = new Junction("name", 1, 1);
        Junction junction1 = new Junction("name1", 1, 1);
        s = new Segment("name", w, junction, junction1, "norte");
        sl = new SegmentList();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setSegmentList method, of class SegmentList.
     */
    @Test
    public void testSetSegmentList() {

        System.out.println("setSegmentList");
        List<Segment> segmentList = new ArrayList();
        segmentList.add(s);
        SegmentList instance = sl;
        instance.setSegmentList(segmentList);
        List<Segment> expResult = segmentList;
        assertEquals(expResult, instance.getsegmentList());
    }

    /**
     * Test of getsegmentList method, of class SegmentList.
     */
    @Test
    public void testGetsegmentList() {

        System.out.println("getsegmentList");
        SegmentList instance = sl;
        List<Segment> expResult = new ArrayList();
        expResult.add(s);
        instance.setSegmentList(expResult);
        List<Segment> result = instance.getsegmentList();
        assertEquals(expResult, result);

    }

    /**
     * Test of addSegment method, of class SegmentList.
     */
    @Test
    public void testAddSegment() {
        System.out.println("addSegment");

        SegmentList instance = sl;
        boolean expResult = true;
        boolean result = instance.addSegment(s);
        assertEquals(expResult, result);

    }

    /**
     * Test of removeSegment method, of class SegmentList.
     */
    @Test
    public void testRemoveSegment() {
        System.out.println("removeSegment");

        SegmentList instance = sl;
        instance.addSegment(s);
        boolean expResult = true;
        boolean result = instance.removeSegment(s);
        assertEquals(expResult, result);

    }

    /**
     * Test of size method, of class SegmentList.
     */
    @Test
    public void testSize() {
        System.out.println("size");
        SegmentList instance =sl;
        int expResult = 1;
        instance.addSegment(s);
        int result = instance.size();
        assertEquals(expResult, result);

    }

    /**
     * Test of newSegment method, of class SegmentList.
     */
    @Test
    public void testNewSegment() {
        System.out.println("newSegment");
        SegmentList instance = sl;
        Segment expResult = new Segment();
        Segment result = instance.newSegment();
        assertEquals(expResult, result);

    }

    /**
     * Test of getList method, of class SegmentList.
     */
    @Test
    public void testGetList() {
        System.out.println("getList");
        SegmentList instance = sl;
        List<Segment> expResult = new ArrayList();
        expResult.add(s);
        instance.addSegment(s);
        List<Segment> result = instance.getList();
        assertEquals(expResult, result);

    }

    /**
     * Test of equals method, of class SegmentList.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object outroObjeto = (SegmentList) new SegmentList();
        SegmentList instance = sl;
        boolean expResult = true;
        boolean result = instance.equals(outroObjeto);
        assertEquals(expResult, result);
        Object obj = null;
        expResult = false;
        result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class SegmentList.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        SegmentList instance = new SegmentList();
        StringBuilder sb = new StringBuilder();
        sb.append("Segment List: \n");
        for (Segment s : instance.getList()) {
            sb.append(s);
            sb.append("\n");
        }

        String expResult = sb.toString();
        String result = instance.toString();
        assertEquals(expResult, result);

    }

    /**
     * Test of hashCode method, of class SegmentList.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        SegmentList instance = new SegmentList();
        int expResult = 96;
        int result = instance.hashCode();
        assertEquals(expResult, result);

    }

    /**
     * Test of iterator method, of class SegmentList.
     */
    @Test
    public void testIterator() {
        System.out.println("iterator");
        SegmentList instance = new SegmentList();
        Iterator<Segment> ite = instance.iterator();
        Iterator<Segment> expResult = ite;
        Iterator<Segment> result = ite;
        assertEquals(expResult, result);

    }

    /**
     * Test of getSegmentByName method, of class SegmentList.
     */
    @Test
    public void testGetSegmentByName() {
        System.out.println("getSegmentByName");
        String name = "";
        SegmentList instance = new SegmentList();
        Segment expResult = new Segment();
        expResult.setName(name);
        instance.addSegment(expResult);
        Segment result = instance.getSegmentByName(name);
        assertEquals(expResult, result);

    }

}


package lapr.project.model;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 *
 */
public class ImportXML implements Importable {

    private final String onlyDigits = "[^-?\\d.E]";

    /**
     * Attributes found in the file.
     */
    private final String idAttribute = "id";
    private final String descriptionAttribute = "description";
    private final String modelIDAtribute = "model_ID";
    private final String bidirectional = "bidirectional";

    /**
     * Tags used in the decoding of the project file data.
     */
    private final String networkTag = "Network";
    private final String nodeTag = "node";
    private final String latitudeTag = "latitude";
    private final String longitudeTag = "longitude";
    private final String altitudeTag = "altitude";
    private final String segmentTag = "segment";
    private final String startNodeTag = "start_node";
    private final String endNodeTag = "end_node";
    private final String windSpeedTag = "wind_intensity";
    private final String windAngleTag = "wind_direction";
    private final String directionTag = "direction";
    private final String airportTag = "airport";
    private final String nameTag = "name";
    private final String townTag = "town";
    private final String countryTag = "country";
    private final String aircraftTag = "aircraft";
    private final String aircraftListTag = "aircraft_list";
    private final String makerTag = "maker";
    private final String typeTag = "type";
    private final String numberMotorsTag = "number_motors";
    private final String motorTag = "motor";
    private final String motorTypeTag = "motor_type";
    private final String TSFCTag = "TSFC";
    private final String WeightTag = "EWeight";
    private final String MTOWTag = "MTOW";
    private final String maxPayloadTag = "max_payload";
    private final String fuelTag = "fuel_capacity";
    private final String VMOTag = "VMO";
    private final String MMOTag = "MMO";
    private final String wingAreaTag = "wing_area";
    private final String wingSpanTag = "wing_span";
    private final String dragTag = "Cdrag_0";
    private final String eTag = "e";
    private final String cruiseAltitudeTag = "cruise_altitude";
    private final String cruiseSpeedTag = "cruise_speed";
    private final String lapseRateTag = "lapse_rate_factor";
    private final String thrust0Tag = "thrust_0";
    private final String thrustMaxSpeedTag = "thrust_max_speed";
    private final String maxSpeedTag = "max_speed";
    private final String aspectRatioTag = "aspect_ratio";
    private final String cDragTag = "iten";
    private final String speedDragTag = "speed";

    /**
     * Creates an instance of ImportXML, which will serve as a XML
     * importer,empty constructor.
     */
    public ImportXML() {
    }

    /**
     * Imports the data contained in a file into airNetwork, loading all the
     * segments and junctions found.
     *
     * @param project Project
     * @param filePath Path to the file which contains the data.
     */
    @Override
    public boolean importNetwork(Project project, String filePath) {
        try {
            // Start by opening the file containing the data.
            Document doc = readFile(filePath);

            Node networkNode = doc.getElementsByTagName(networkTag).item(0);

            if (networkNode.getNodeType() == Node.ELEMENT_NODE) {
                Element networkElement = (Element) networkNode;

                // Extracting the list of nodes
                extractNodes(networkElement, project);

                // Extracting the list of segments
                extractSegments(networkElement, project);

            }

            return project.validate(true, true);

        } catch (SAXException | IOException | ParserConfigurationException ex) {
            throw new IllegalArgumentException("An error occured while"
                    + " attempting to import the XML file.");
        }
    }

    @Override
    public boolean importAirports(Project project, String filePath) {
        try {
            // Start by opening the file containing the data.
            Document doc = readFile(filePath);

            Node networkNode = doc.getElementsByTagName(networkTag).item(0);

            if (networkNode.getNodeType() == Node.ELEMENT_NODE) {
                Element networkElement = (Element) networkNode;

                // Extracting the list of airports
                extractAirports(networkElement, project);

            }

            return project.validate(true, true);

        } catch (SAXException | IOException | ParserConfigurationException ex) {
            throw new IllegalArgumentException("An error occured while"
                    + " attempting to import the XML file.");
        }
    }

    @Override
    public boolean importAircrafts(Project project, String filePath) {
        try {
            // Start by opening the file containing the data.
            Document doc = readFile(filePath);

            Node networkNode = doc.getElementsByTagName(aircraftListTag).item(0);

            if (networkNode.getNodeType() == Node.ELEMENT_NODE) {
                Element networkElement = (Element) networkNode;

                // Extracting the list of nodes
                extractAircrafts(networkElement, project);

            }

            return project.validate(true, true);

        } catch (SAXException | IOException | ParserConfigurationException ex) {
            throw new IllegalArgumentException("An error occured while"
                    + " attempting to import the XML file.");
        }
    }

    /**
     * Extract all the junctions in an airNetwork.
     *
     * @param networkElement Element that contains all the junctions.
     * @param project Project that will contain the extracted junctions.
     */
    private void extractNodes(Element networkElement, Project project) {
        NodeList junctionNodeList
                = networkElement.getElementsByTagName(nodeTag);

        for (int i = 0; i < junctionNodeList.getLength(); i++) {
            Node junctionNode = junctionNodeList.item(i);

            if (junctionNode.getNodeType() == Node.ELEMENT_NODE) {
                Element junctionElement = (Element) junctionNode;

                Junction junction = project.newJunction();
                junction.setName(junctionElement.getAttribute(idAttribute));
                double lat
                        = Double.parseDouble(junctionElement.getElementsByTagName(
                                latitudeTag).item(0).getTextContent());
                double lon
                        = Double.parseDouble(junctionElement.getElementsByTagName(
                                this.longitudeTag).item(0).getTextContent());

                junction.setLatitude(lat);
                junction.setLongitude(lon);
                junction.validate();

                project.getJunctionList().addJunction(junction);
                project.addJunction(junction);
            }
        }
    }

    /**
     * Extracts all the segments in an air network.
     *
     * @param airNetworkElement Element that contains all the segments.
     * @param segmentList List of segments that will contain all the extracted
     * segments.
     */
    private void extractSegments(Element airNetworkElement,
            Project project) {
        NodeList sectionNodeList
                = airNetworkElement.getElementsByTagName(segmentTag);

        for (int i = 0; i < sectionNodeList.getLength(); i++) {
            Node segmentNode = sectionNodeList.item(i);

            if (segmentNode.getNodeType() == Node.ELEMENT_NODE) {
                Element segmentElement = (Element) segmentNode;

                Segment segment = new Segment();

                segment.setName(
                        segmentElement.getAttribute(idAttribute)
                );

                segment.setStartNode(project.findJunction(segmentElement.getElementsByTagName(
                        startNodeTag).item(0).getTextContent()));

                segment.setEndNode(project.findJunction(segmentElement.getElementsByTagName(
                        endNodeTag).item(0).getTextContent()));

                segment.setDirection(segmentElement.getElementsByTagName(
                        directionTag).item(0).getTextContent());

                segment.setWindAngle(Double.parseDouble(segmentElement.getElementsByTagName(
                        windAngleTag).item(0).getTextContent()));

                String wind = segmentElement.getElementsByTagName(
                        windSpeedTag).item(0).getTextContent();

                wind = wind.replaceAll(onlyDigits, "");

                segment.setWindSpeed(Double.parseDouble(wind));

                segment.validate();
                project.getSegmentList().addSegment(segment);
                project.addSegment(segment);

                if (segment.getDirection().equals(bidirectional)) {
                    Segment reverse = new Segment();
                    reverse.setName(segment.getName() + "Reverse");
                    reverse.setStartNode(segment.getEndNode());
                    reverse.setEndNode(segment.getStartNode());
                    reverse.setDirection(bidirectional);
                    reverse.setWindAngle(segment.getWindAngle() * (-1));
                    reverse.setWindSpeed(segment.getWindSpeed());
                    project.getSegmentList().addSegment(reverse);
                    project.addSegment(reverse);

                }

            }
        }
    }

    /**
     * Extract all the airports.
     *
     * @param networkElement Element that contains all the airports.
     * @param project Project that will contain the extracted airports.
     */
    private void extractAirports(Element networkElement, Project project) {
        NodeList airportNodeList
                = networkElement.getElementsByTagName(airportTag);

        for (int i = 0; i < airportNodeList.getLength(); i++) {
            Node airportNode = airportNodeList.item(i);

            if (airportNode.getNodeType() == Node.ELEMENT_NODE) {
                Element airportElement = (Element) airportNode;

                Airport air = new Airport();

                air.setIATA(airportElement.getAttribute(idAttribute));
                String names
                        = airportElement.getElementsByTagName(
                                nameTag).item(0).getTextContent();
                String town
                        = airportElement.getElementsByTagName(
                                townTag).item(0).getTextContent();
                String country
                        = airportElement.getElementsByTagName(
                                countryTag).item(0).getTextContent();

                double lat
                        = Double.parseDouble(airportElement.getElementsByTagName(
                                latitudeTag).item(0).getTextContent());
                double lon
                        = Double.parseDouble(airportElement.getElementsByTagName(
                                this.longitudeTag).item(0).getTextContent());

                String altitude = airportElement.getElementsByTagName(
                        altitudeTag).item(0).getTextContent();

                altitude = altitude.replaceAll(onlyDigits, "");

                air.setAltitude(Double.parseDouble(altitude));
                air.setCountry(country);
                air.setLatitude(lat);
                air.setLongitude(lon);
                air.setName(names);
                air.setTown(town);

                air.validate();
                project.getAirportList().addAirport(air);
            }
        }
    }

    /**
     * Extract all the aircrafts.
     *
     * @param networkElement Element that contains all the aircrafts.
     * @param project Project that will contain the extracted aircrafts.
     */
    private void extractAircrafts(Element networkElement, Project project) {
        NodeList aircraftNodeList
                = networkElement.getElementsByTagName(aircraftTag);

        for (int i = 0; i < aircraftNodeList.getLength(); i++) {
            Node aircraftNode = aircraftNodeList.item(i);

            if (aircraftNode.getNodeType() == Node.ELEMENT_NODE) {
                Element aircraftElement = (Element) aircraftNode;

                Aircraft air = new Aircraft();
                AircraftModel model = air.getAircraftModel();
                Motorization mot = model.getMotorization();

                model.setModelId(
                        aircraftElement.getAttribute(modelIDAtribute)
                );
                air.setRegistration(
                        aircraftElement.getAttribute(descriptionAttribute)
                );

                String maker
                        = aircraftElement.getElementsByTagName(
                                makerTag).item(0).getTextContent();
                String type
                        = aircraftElement.getElementsByTagName(
                                typeTag).item(0).getTextContent();

                int numberMotors
                        = Integer.parseInt((aircraftElement.getElementsByTagName(
                                numberMotorsTag).item(0).getTextContent()));

                String motor
                        = aircraftElement.getElementsByTagName(
                                motorTag).item(0).getTextContent();

                String motorType
                        = aircraftElement.getElementsByTagName(
                                motorTypeTag).item(0).getTextContent();

                String temp = aircraftElement.getElementsByTagName(
                        cruiseAltitudeTag).item(0).getTextContent();

                String cruiseAltitude = temp.replaceAll(onlyDigits, "");

                temp = aircraftElement.getElementsByTagName(
                        cruiseSpeedTag).item(0).getTextContent();

                String cruiseSpeed = temp.replaceAll(onlyDigits, "");

                temp = aircraftElement.getElementsByTagName(
                        TSFCTag).item(0).getTextContent();

                String TSFC = temp.replaceAll(onlyDigits, "");

                double lapseRate = Double.parseDouble(aircraftElement.getElementsByTagName(
                        lapseRateTag).item(0).getTextContent());

                temp = aircraftElement.getElementsByTagName(
                        thrust0Tag).item(0).getTextContent();

                String thrust0 = temp.replaceAll(onlyDigits, "");

                temp = aircraftElement.getElementsByTagName(
                        thrustMaxSpeedTag).item(0).getTextContent();

                String thrustMaxSpeed = temp.replaceAll(onlyDigits, "");;

                temp = aircraftElement.getElementsByTagName(
                        maxSpeedTag).item(0).getTextContent();

                String maxSpeed = temp.replaceAll(onlyDigits, "");

                temp = aircraftElement.getElementsByTagName(
                        WeightTag).item(0).getTextContent();

                String emptyWeight = temp.replaceAll(onlyDigits, "");

                temp = aircraftElement.getElementsByTagName(
                        MTOWTag).item(0).getTextContent();

                String MTOW = temp.replaceAll(onlyDigits, "");

                temp = aircraftElement.getElementsByTagName(
                        maxPayloadTag).item(0).getTextContent();

                String payload = temp.replaceAll(onlyDigits, "");

                temp = aircraftElement.getElementsByTagName(
                        fuelTag).item(0).getTextContent();

                String fuel = temp.replaceAll(onlyDigits, "");

                temp = aircraftElement.getElementsByTagName(
                        VMOTag).item(0).getTextContent();

                String VMO = temp.replaceAll(onlyDigits, "");

                temp = aircraftElement.getElementsByTagName(
                        MMOTag).item(0).getTextContent();

                String MMO = temp.replaceAll(onlyDigits, "");

                temp = aircraftElement.getElementsByTagName(
                        wingAreaTag).item(0).getTextContent();

                String wingArea = temp.replaceAll(onlyDigits, "");

                temp = aircraftElement.getElementsByTagName(
                        wingSpanTag).item(0).getTextContent();

                String wingSpan = temp.replaceAll(onlyDigits, "");

                double aspectRatio = Double.parseDouble(aircraftElement.getElementsByTagName(
                        aspectRatioTag).item(0).getTextContent());

                double e
                        = Double.parseDouble(aircraftElement.getElementsByTagName(
                                eTag).item(0).getTextContent());

                extractCDrag(networkElement, air);

                air.setCompany(maker);
                model.setMaker(maker);
                model.setType(type);
                mot.setNumberMotors(numberMotors);
                mot.setMotorType(motorType);
                mot.setMotor(motor);
                mot.setCruiseAltitude(Double.parseDouble(cruiseAltitude));
                mot.setCruiseSpeed(Double.parseDouble(cruiseSpeed));
                mot.setTSFC(Double.parseDouble(TSFC));
                mot.setLapseRateFactor(lapseRate);
                mot.setThurst_0(Double.parseDouble(thrust0));
                mot.setThurst_max_speed(Double.parseDouble(thrustMaxSpeed));
                mot.setMax_speed(Double.parseDouble(maxSpeed));
                model.setEmptyWeight(Double.parseDouble(emptyWeight));
                model.setMaximumTakeOffWeight(Double.parseDouble(MTOW));

                model.setMaxPayLoad(Double.parseDouble(payload));
                model.setMaximumFuelCapacity(Double.parseDouble(fuel));
                model.setVMO(Double.parseDouble(VMO));
                model.setMMO(Double.parseDouble(MMO));
                model.setWingArea(Double.parseDouble(wingArea));
                model.setWingSpan(Double.parseDouble(wingSpan));
                model.setAspectRatio(aspectRatio);
                model.setE(e);

                air.validate();
                project.addAircraft(air);

            }
        }
    }

    /**
     * Extract all the CDragFunctions
     *
     * @param networkElement Element that contains all the regimes.
     * @param project Project that will contain the extracted regimes.
     */
    private void extractCDrag(Element networkElement, Aircraft air) {
        NodeList dragNodeList
                = networkElement.getElementsByTagName(cDragTag);

        for (int i = 0; i < dragNodeList.getLength(); i++) {
            Node dragNode = dragNodeList.item(i);

            if (dragNode.getNodeType() == Node.ELEMENT_NODE) {
                Element dragElement = (Element) dragNode;

                Cdrag drag = new Cdrag();

                String temp = dragElement.getElementsByTagName(
                        speedDragTag).item(0).getTextContent();

                String speed = temp.replaceAll(onlyDigits, "");

                double cDrag = Double.parseDouble(dragElement.getElementsByTagName(
                        dragTag).item(0).getTextContent());

                drag.setCdrag_0(cDrag);
                drag.setSpeed(Double.parseDouble(speed));

                drag.validate();

                air.getAircraftModel().getCDragList().addCdrag(drag);
            }
        }
    }

    /**
     * Loads an XML file into a Document in order to ease its reading.
     *
     * @param filePath Path to the file.
     * @return Loaded file in Document format.
     * @throws ParserConfigurationException Thrown if there is a configuration
     * error.
     * @throws SAXException Thrown if the loading of the file into a document
     * fails.
     * @throws IOException Thrown if the file is not found.
     */
    private Document readFile(String filePath) throws
            ParserConfigurationException, SAXException, IOException {
        File file = new File(filePath);
        DocumentBuilderFactory docBuilderF = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilderF.newDocumentBuilder();
        Document doc = docBuilder.parse(file);
        doc.normalize();
        return doc;
    }

    @Override
    public boolean importCSV(String path, FlightPlan na) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

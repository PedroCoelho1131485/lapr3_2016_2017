package lapr.project.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;

public class AirportTest {

    private final Airport air;

    public AirportTest() {
        this.air = new Airport();
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of getAndSetName method, of class Airport.
     */
            
    @Test
    public void testGetAndSetName() {
        System.out.println("getAndSetName");
        Airport instance = air;
        String expResult = "john";
        instance.setName(expResult);
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAndSetTown method, of class Airport.
     */
    @Test
    public void testGetAndSetTown() {
        System.out.println("getAndSetTown");
        Airport instance = air;
        String expResult = "porto";
        instance.setTown(expResult);
        String result = instance.getTown();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAndSetCountry method, of class Airport.
     */
    @Test
    public void testGetAndSetCountry() {
        System.out.println("getAndSetCountry");
        Airport instance = air;
        String expResult = "france";
        instance.setCountry(expResult);
        String result = instance.getCountry();
        assertEquals(expResult, result);
    }


    /**
     * Test of getAndSetAltitude method, of class Airport.
     */
    @Test
    public void testGetAndSetAltitude() {
        System.out.println("getAndSetAltitude");
        Airport instance = air;
        double expResult = 23.3;
        instance.setAltitude(expResult);
        double result = instance.getAltitude();
        assertEquals(expResult, result,0.0);
    }

    /**
     * Test of getAndSetLatitude method, of class Airport.
     */
    @Test
    public void testGetAndSetLatitude() {
        System.out.println("getAndSetLatitude");
        Airport instance = air;
        double expResult = 23.3;
        instance.setLatitude(expResult);
        double result = instance.getLatitude();
        assertEquals(expResult, result,0.0);
    }

    /**
     * Test of getAndSetLongitude method, of class Airport.
     */
    @Test
    public void testGetAndSetLongitude() {
        System.out.println("getAndSetLongitude");
        Airport instance = air;
        double expResult = 23.3;
        instance.setLongitude(expResult);
        double result = instance.getLongitude();
        assertEquals(expResult, result,0.0);
    }

   /**
    * test of the method toString (override) of class Airport
    * 
    * return "Airport{" + "id=" + id + ", name=" + name + ", town=" + town + ", country=" + country + ", latitude=" + latitude + ", longitude=" + longitude + ", altitude=" + altitude + '}';
    */
    
    @Test
    public void testToString(){
        
        System.out.println("To String");
        Airport instance = air; //reference to the object
        
        String IATA = "FSC";
        String name = "Francisco Sá Carneiro";
        String town = "Porto";
        String country = "Portugal";
        double latitude = 47.0000; //not exactly
        double longitude = -8.0000; //not exactly
        double altitude = 80; //meters //not exactly
        
        instance.setAltitude(altitude);
        instance.setName(name);
        instance.setIATA(IATA);
        instance.setCountry(country);
        instance.setTown(town);
        instance.setLatitude(latitude);
        instance.setLongitude(longitude);
        
        String expected = "Airport{" + "IATA=" + IATA + ", name=" + name + ", town=" + town + ", country=" + country + ", latitude=" + latitude + ", longitude=" + longitude + ", altitude=" + altitude + '}';
        
        String result = instance.toString();
        
        assertEquals(expected,result);
        
        System.out.println(result);
    }
    
    /**
     * test of the method equals of class Airport (override)
     */
    
    @Test
    public void testEquals() {
        
        System.out.println("Equals");
        Airport a = new Airport();
        Airport b = new Airport();
        
        assertTrue(a.equals(b));
        
        String id = "FSC";
        String name = "Francisco Sá Carneiro";
        String town = "Porto";
        String country = "Portugal";
        double latitude = 47.0000; //not exactly
        double longitude = -8.0000; //not exactly
        double altitude = 80; //meters //not exactly
        
        Airport c = new Airport(id,name,town,country,latitude,longitude,altitude);
        
        assertFalse(a.equals(c));
    }

    /**
     * Test of getName method, of class Airport.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Airport instance = new Airport();
        String expResult = "Name";
        instance.setName(expResult);
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setName method, of class Airport.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "Porto";
        Airport instance = new Airport();
        instance.setName(name);
        assertEquals(name,instance.getName());
    }

    /**
     * Test of getTown method, of class Airport.
     */
    @Test
    public void testGetTown() {
        System.out.println("getTown");
        Airport instance = new Airport();
        String expResult = "Porto";
        instance.setTown(expResult);
        String result = instance.getTown();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTown method, of class Airport.
     */
    @Test
    public void testSetTown() {
        System.out.println("setTown");
        String town = "";
        Airport instance = new Airport();
        instance.setTown(town);
        assertEquals(town,instance.getTown());
    }

    /**
     * Test of getCountry method, of class Airport.
     */
    @Test
    public void testGetCountry() {
        System.out.println("getCountry");
        Airport instance = new Airport();
        String expResult = "Portugal";
        instance.setCountry(expResult);
        String result = instance.getCountry();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCountry method, of class Airport.
     */
    @Test
    public void testSetCountry() {
        System.out.println("setCountry");
        String country = "Portugal";
        Airport instance = new Airport();
        instance.setCountry(country);
        assertEquals(country,instance.getCountry());
    }

    /**
     * Test of getAltitude method, of class Airport.
     */
    @Test
    public void testGetAltitude() {
        System.out.println("getAltitude");
        Airport instance = new Airport();
        double expResult = 60.0;
        instance.setAltitude(expResult);
        double result = instance.getAltitude();
        assertEquals(expResult, result, 0.0);
        expResult = -1.0;
        instance.setAltitude(expResult);
        result = instance.getAltitude();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setAltitude method, of class Airport.
     */
    @Test
    public void testSetAltitude() {
        System.out.println("setAltitude");
        double altitude = 60.0;
        Airport instance = new Airport();
        instance.setAltitude(altitude);
        assertEquals(altitude,instance.getAltitude(),0.0);
        altitude = -1.0;
        instance.setAltitude(altitude);
        assertEquals(altitude,instance.getAltitude(),0.0);
    }

    /**
     * Test of getIATA method, of class Airport.
     */
    @Test
    public void testGetIATA() {
        System.out.println("getIATA");
        Airport instance = new Airport();
        String expResult = "OPO";
        instance.setIATA(expResult);
        String result = instance.getIATA();
        assertEquals(expResult, result);

    }

    /**
     * Test of setIATA method, of class Airport.
     */
    @Test
    public void testSetIATA() {
        System.out.println("setIATA");
        String IATA = "OPO";
        Airport instance = new Airport();
        instance.setIATA(IATA);
        assertEquals(IATA,instance.getIATA());
    }

    /**
     * Test of getLatitude method, of class Airport.
     */
    @Test
    public void testGetLatitude() {
        System.out.println("getLatitude");
        Airport instance = new Airport();
        double expResult = 3.0;
        instance.setLatitude(expResult);
        double result = instance.getLatitude();
        assertEquals(expResult, result, 0.0);
        expResult = -3.0;
        instance.setAltitude(expResult);
        result = instance.getAltitude();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setLatitude method, of class Airport.
     */
    @Test
    public void testSetLatitude() {
        System.out.println("setLatitude");
        double latitude = 5.0;
        Airport instance = new Airport();
        instance.setLatitude(latitude);
        assertEquals(latitude,instance.getLatitude(),0.0);
        latitude = -3.0;
        instance.setLatitude(latitude);
        assertEquals(latitude,instance.getLatitude(),0.0);
    }

    /**
     * Test of getLongitude method, of class Airport.
     */
    @Test
    public void testGetLongitude() {
        System.out.println("getLongitude");
        Airport instance = new Airport();
        double expResult = 4.0;
        instance.setLongitude(expResult);
        double result = instance.getLongitude();
        assertEquals(expResult, result, 0.0);
        expResult = -3.0;
        instance.setLongitude(expResult);
        result = instance.getLongitude();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setLongitude method, of class Airport.
     */
    @Test
    public void testSetLongitude() {
        System.out.println("setLongitude");
        double longitude = 4.0;
        Airport instance = new Airport();
        instance.setLongitude(longitude);
        assertEquals(longitude,instance.getLongitude(),0.0);
        longitude = -4.0;
        instance.setLongitude(longitude);
        assertEquals(longitude,instance.getLongitude(),0.0);
    }

    /**
     * Test of hashCode method, of class Airport.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Airport instance = new Airport();
        int expResult = 928005735;
        int result = instance.hashCode();
        assertEquals(expResult, result);

    }

    /**
     * Test of validate method, of class Airport.
     */
    @Test
    public void testValidate() {
        System.out.println("validate");
        Airport instance = new Airport();
        instance.setName("Airport");
        instance.setAltitude(60);
        instance.setCountry("Portugal");
        instance.setTown("Porot");
        instance.setIATA("OPO");
        instance.setLatitude(44.3319);
        instance.setLongitude(5.01302);
        boolean expResult = true;
        boolean result = instance.validate();
        assertEquals(expResult, result);
    }

}

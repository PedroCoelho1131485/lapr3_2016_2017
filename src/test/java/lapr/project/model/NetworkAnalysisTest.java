/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class NetworkAnalysisTest {

    private NetworkAnalysis na;

    public NetworkAnalysisTest() {
        na = new NetworkAnalysis();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getAircraft method, of class NetworkAnalysis.
     */
    @Test
    public void testGetAndSetAircraft() {
        System.out.println("getAircraft");
        NetworkAnalysis instance = na;
        Aircraft expResult = new Aircraft();
        instance.setAircraft(expResult);
        Aircraft result = instance.getAircraft();
        assertEquals(expResult, result);

    }

    /**
     * Test of getProject method, of class NetworkAnalysis.
     */
    @Test
    public void testGetAndSetProject() {
        System.out.println("getProject");
        NetworkAnalysis instance = na;
        Project expResult = new Project();
        instance.setProject(expResult);
        Project result = instance.getProject();
        assertEquals(expResult, result);

    }

    /**
     * Test of getPassengers method, of class NetworkAnalysis.
     */
    @Test
    public void testGetAndSetPassengers() {
        System.out.println("getPassengers");
        NetworkAnalysis instance = na;
        int expResult = 3;
        instance.setPassengers(expResult);
        int result = instance.getPassengers();
        assertEquals(expResult, result);

    }

    /**
     * Test of getCrew method, of class NetworkAnalysis.
     */
    @Test
    public void testGetAndSetCrew() {
        System.out.println("getCrew");
        NetworkAnalysis instance = na;
        int expResult = 2;
        instance.setCrew(expResult);
        int result = instance.getCrew();
        assertEquals(expResult, result);

    }

    /**
     * Test of getCargoLoad method, of class NetworkAnalysis.
     */
    @Test
    public void testGetAndSetCargoLoad() {
        System.out.println("getCargoLoad");
        NetworkAnalysis instance = na;
        double expResult = 3;
        instance.setCargoLoad(expResult);
        double result = instance.getCargoLoad();
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of getFuelLoad method, of class NetworkAnalysis.
     */
    @Test
    public void testGetAndSetFuelLoad() {
        System.out.println("getFuelLoad");
        NetworkAnalysis instance = new NetworkAnalysis();
        double expResult = 1;
        instance.setFuelLoad(expResult);
        double result = instance.getFuelLoad();
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of calculate method, of class NetworkAnalysis.
     */
    /*
    @Test
    public void testCalculate() {
        System.out.println("calculate");
        int num = 1;
        NetworkAnalysis instance = null;
        instance.calculate(num);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/

    /**
     * Test of calculateFastestPath method, of class NetworkAnalysis.
     */
    /*
    @Test
    public void testCalculateFastestPath() {
        System.out.println("calculateFastestPath");
        NetworkAnalysis instance = null;
        instance.calculateFastestPath();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/

    /**
     * Test of calculateShortestPath method, of class NetworkAnalysis.
     */
    /*
    @Test
    public void testCalculateShortestPath() {
        System.out.println("calculateShortestPath");
        NetworkAnalysis instance = null;
        instance.calculateShortestPath();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/

    /**
     * Test of findJunction method, of class NetworkAnalysis.
     */
    @Test
    public void testFindJunction() {
        System.out.println("findJunction");
        Airport air = new Airport();
        NetworkAnalysis instance = new NetworkAnalysis();
        Junction expResult = new Junction();
        Project p = new Project();
        instance.setProject(p);
        instance.getProject().getJunctionList().addJunction(expResult);
        Junction result = instance.findJunction(air);
        assertEquals(expResult, result);
 
    }

    /**
     * Test of setAircraft method, of class NetworkAnalysis.
     */
    @Test
    public void testSetAircraft() {
        System.out.println("setAircraft");
        Aircraft aircraft = new Aircraft();
        NetworkAnalysis instance = new NetworkAnalysis();
        instance.setAircraft(aircraft);
        assertEquals(aircraft,instance.getAircraft());
    }


    /**
     * Test of getProject method, of class NetworkAnalysis.
     */
    @Test
    public void testGetProject() {
        System.out.println("getProject");
        NetworkAnalysis instance = new NetworkAnalysis();
        Project expResult = new Project();
        instance.setProject(expResult);
        Project result = instance.getProject();
        assertEquals(expResult, result);
    }

    /**
     * Test of setProject method, of class NetworkAnalysis.
     */
    @Test
    public void testSetProject() {
        System.out.println("setProject");
        Project project = new Project();
        NetworkAnalysis instance = new NetworkAnalysis();
        instance.setProject(project);
        assertEquals(project,instance.getProject());
    }

    /**
     * Test of getPassengers method, of class NetworkAnalysis.
     */
    @Test
    public void testGetPassengers() {
        System.out.println("getPassengers");
        NetworkAnalysis instance = new NetworkAnalysis();
        int expResult = 0;
        instance.setPassengers(expResult);
        int result = instance.getPassengers();
        assertEquals(expResult, result);
   
    }

    /**
     * Test of setPassengers method, of class NetworkAnalysis.
     */
    @Test
    public void testSetPassengers() {
        System.out.println("setPassengers");
        int passengers = 0;
        NetworkAnalysis instance = new NetworkAnalysis();
        instance.setPassengers(passengers);
        assertEquals(passengers,instance.getPassengers(),0);
    }

    /**
     * Test of getCrew method, of class NetworkAnalysis.
     */
    @Test
    public void testGetCrew() {
        System.out.println("getCrew");
        NetworkAnalysis instance = new NetworkAnalysis();
        int expResult = 0;
        instance.setCrew(expResult);
        int result = instance.getCrew();
        assertEquals(expResult, result);

    }

    /**
     * Test of setCrew method, of class NetworkAnalysis.
     */
    @Test
    public void testSetCrew() {
        System.out.println("setCrew");
        int crew = 0;
        NetworkAnalysis instance = new NetworkAnalysis();
        instance.setCrew(crew);
        assertEquals(crew,instance.getCrew(),0);
    }

    /**
     * Test of getCargoLoad method, of class NetworkAnalysis.
     */
    @Test
    public void testGetCargoLoad() {
        System.out.println("getCargoLoad");
        NetworkAnalysis instance = new NetworkAnalysis();
        double expResult = 0.0;
        instance.setCargoLoad(expResult);
        double result = instance.getCargoLoad();
        assertEquals(expResult, result, 0.0);
 
    }

    /**
     * Test of setCargoLoad method, of class NetworkAnalysis.
     */
    @Test
    public void testSetCargoLoad() {
        System.out.println("setCargoLoad");
        double cargoLoad = 0.0;
        NetworkAnalysis instance = new NetworkAnalysis();
        instance.setCargoLoad(cargoLoad);
        assertEquals(cargoLoad,instance.getCargoLoad(),0.0);
    }

    /**
     * Test of getFuelLoad method, of class NetworkAnalysis.
     */
    @Test
    public void testGetFuelLoad() {
        System.out.println("getFuelLoad");
        NetworkAnalysis instance = new NetworkAnalysis();
        double expResult = 0.0;
        instance.setFuelLoad(expResult);
        double result = instance.getFuelLoad();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setFuelLoad method, of class NetworkAnalysis.
     */
    @Test
    public void testSetFuelLoad() {
        System.out.println("setFuelLoad");
        double fuelLoad = 0.0;
        NetworkAnalysis instance = new NetworkAnalysis();
        instance.setFuelLoad(fuelLoad);
        assertEquals(fuelLoad,instance.getFuelLoad(),0.0);
    }

    /**
     * Test of getAircraft method, of class NetworkAnalysis.
     */
    @Test
    public void testGetAircraft() {
        System.out.println("getAircraft");
        NetworkAnalysis instance = new NetworkAnalysis();
        Aircraft expResult = new Aircraft();
        instance.setAircraft(expResult);
        Aircraft result = instance.getAircraft();
        assertEquals(expResult, result);
    }

    /**
     * Test of setOri method, of class NetworkAnalysis.
     */
    @Test
    public void testSetOri() {
        System.out.println("setOri");
        Junction originNode = null;
        NetworkAnalysis instance = new NetworkAnalysis();
        instance.setOri(originNode);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getFp method, of class NetworkAnalysis.
     */
    @Test
    public void testGetFp() {
        System.out.println("getFp");
        NetworkAnalysis instance = new NetworkAnalysis();
        FlightPlan expResult = new FlightPlan();
        instance.setFp(expResult);
        FlightPlan result = instance.getFp();
        assertEquals(expResult, result);
    }

    /**
     * Test of calculate method, of class NetworkAnalysis.
     */
    @Test
    public void testCalculate() {
        System.out.println("calculate");
        int resp = 0;
        NetworkAnalysis instance = new NetworkAnalysis();
        NetworkAnalysisResult expResult = null;
        NetworkAnalysisResult result = instance.calculate(resp);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of calculateFastestPath method, of class NetworkAnalysis.
     */
    @Test
    public void testCalculateFastestPath() {
        System.out.println("calculateFastestPath");
        NetworkAnalysis instance = new NetworkAnalysis();
        Project p = new Project();
        Importable importable = p.getImportableRegistry().getImportableOfType("Network.xml");
        importable.importNetwork(p, "Network.xml");

        Importable importable1 = p.getImportableRegistry().getImportableOfType("Aircraft.xml");
        importable1.importAircrafts(p, "Aircraft.xml");

        Importable importable2 = p.getImportableRegistry().getImportableOfType("Airports.xml");
        importable2.importAirports(p, "Airports.xml");

        Airport air = p.getAirportList().getAirport("OPO");
        Airport air1 = p.getAirportList().getAirport("LIS");

        Aircraft aircraft = p.getAircraftList().getAircraftList().get(1);

        NetworkAnalysis na = new NetworkAnalysis();

        Junction ja = p.getAirNetwork().findJunction(air.getLatitude(), air.getLongitude());
        Junction jl = p.getAirNetwork().findJunction(air1.getLatitude(), air1.getLongitude());
 //       int cont = 0;/      

//        ArrayList<LinkedList<Junction>> paths = GraphAlgorithms.allPaths(p.getGraph(), ja, jl);
//       System.out.println(paths.size());

//        for (LinkedList<Junction> path : paths) {
//              System.out.println(path.get(cont).getName());
//              cont++;
//            for (Junction junction : path) {
//                System.out.println(junction.toString());
//            }
//        }

        FlightPlan fp=new FlightPlan();
        fp.setOriginAirfield(air);
        fp.setDesttinyAirfield(air1);
        na.setFp(fp);
        fp.setAircraft(aircraft);
        
        na.setAircraft(aircraft);
        na.setProject(p);
        
       
        na.setOri(na.findJunction(air));
        na.setDest(na.findJunction(air1));
        
        na.setPassengers(200);
        na.setCrew(10);
        na.setCargoLoad(2000);
        na.setFuelLoad(20000);
        
        na.calculate(1);
        NetworkAnalysisResult nar=p.getNetworkAnalysisResultsList().getNAList().get(0);
                //System.out.println(nar.getTasList().get(0));
        // System.out.println("Number motors " + Integer.toString(aircraft.getAircraftModel().getNumberMotors()));
        NetworkAnalysisResult expResult = nar;
        
        System.out.println("expResult = \n" + expResult.toString());
        //System.out.println("Result = \n" + result.toString());
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of calculateLessEnergyComsunption method, of class NetworkAnalysis.
     */
    @Test
    public void testCalculateLessEnergyComsunption() {
        System.out.println("calculateLessEnergyComsunption");
        Project p = new Project();
        Importable importable = p.getImportableRegistry().getImportableOfType("Network.xml");
        importable.importNetwork(p, "Network.xml");

        Importable importable1 = p.getImportableRegistry().getImportableOfType("Aircraft.xml");
        importable1.importAircrafts(p, "Aircraft.xml");

        Importable importable2 = p.getImportableRegistry().getImportableOfType("Airports.xml");
        importable2.importAirports(p, "Airports.xml");

        Airport air = p.getAirportList().getAirport("OPO");
        Airport air1 = p.getAirportList().getAirport("LIS");

        Aircraft aircraft = p.getAircraftList().getAircraftList().get(1);

        NetworkAnalysis na = new NetworkAnalysis();

        Junction ja = p.getAirNetwork().findJunction(air.getLatitude(), air.getLongitude());
        Junction jl = p.getAirNetwork().findJunction(air1.getLatitude(), air1.getLongitude());
//        int cont = 0;
//      
//
//        ArrayList<LinkedList<Junction>> paths = GraphAlgorithms.allPaths(p.getGraph(), ja, jl);
//        System.out.println(paths.size());
//
//        for (LinkedList<Junction> path : paths) {
//              System.out.println(path.get(cont).getName());
//              cont++;
//            for (Junction junction : path) {
//                System.out.println(junction.toString());
//            }
//        }

        FlightPlan fp=new FlightPlan();
        fp.setOriginAirfield(air);
        fp.setDesttinyAirfield(air1);
        na.setFp(fp);
        
        na.setAircraft(aircraft);
        na.setProject(p);
        na.setOri(na.findJunction(air));
        na.setDest(na.findJunction(air1));
       
        
        na.setPassengers(200);
        na.setCrew(10);
        na.setCargoLoad(2000);
        na.setFuelLoad(20000);
        Converter converter = new Converter();
        System.out.println("TAS : "   + converter.milesConverterMetres(0.84));
        na.calculate(2);
        NetworkAnalysisResult nar=p.getNetworkAnalysisResultsList().getNAList().get(0);
       //System.out.println(nar.getTasList().get(0));
                //System.out.println(nar.getTasList().get(0));
        // System.out.println("Number motors " + Integer.toString(aircraft.getAircraftModel().getNumberMotors()));
        NetworkAnalysisResult expResult = nar;
       // NetworkAnalysisResult result = na.calculateLessEnergyComsunption();
        System.out.println("expResult = \n" + expResult.toString());
        //System.out.println("Result = \n" + result.toString());
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of calculateShortestPath method, of class NetworkAnalysis.
     */
    @Test
    public void testCalculateShortestPath() {
     System.out.println("calculateShortest");
        Project p = new Project();
        Importable importable = p.getImportableRegistry().getImportableOfType("Network.xml");
        importable.importNetwork(p, "Network.xml");

        Importable importable1 = p.getImportableRegistry().getImportableOfType("Aircraft.xml");
        importable1.importAircrafts(p, "Aircraft.xml");

        Importable importable2 = p.getImportableRegistry().getImportableOfType("Airports.xml");
        importable2.importAirports(p, "Airports.xml");

        Airport air = p.getAirportList().getAirport("OPO");
        Airport air1 = p.getAirportList().getAirport("LIS");

        Aircraft aircraft = p.getAircraftList().getAircraftList().get(1);

        NetworkAnalysis na = new NetworkAnalysis();

        Junction ja = p.getAirNetwork().findJunction(air.getLatitude(), air.getLongitude());
        Junction jl = p.getAirNetwork().findJunction(air1.getLatitude(), air1.getLongitude());
//        int cont = 0;
//      
//
//        ArrayList<LinkedList<Junction>> paths = GraphAlgorithms.allPaths(p.getGraph(), ja, jl);
//        System.out.println(paths.size());
//
//        for (LinkedList<Junction> path : paths) {
//              System.out.println(path.get(cont).getName());
//              cont++;
//            for (Junction junction : path) {
//                System.out.println(junction.toString());
//            }
//        }

        FlightPlan fp=new FlightPlan();
        fp.setOriginAirfield(air);
        fp.setDesttinyAirfield(air1);
        na.setFp(fp);
        
        na.setAircraft(aircraft);
        na.setProject(p);
        na.setOri(na.findJunction(air));
        na.setDest(na.findJunction(air1));
       
        na.setOri(na.findJunction(air));
        na.setDest(na.findJunction(air1));
        
        na.setOri(na.findJunction(air));
        na.setDest(na.findJunction(air1));
        
        
        na.setPassengers(200);
        na.setCrew(10);
        na.setCargoLoad(2000);
        na.setFuelLoad(20000);
        
        na.calculate(0);
        NetworkAnalysisResult nar=p.getNetworkAnalysisResultsList().getNAList().get(0);
      // System.out.println(nar.getTasList().get(3));
                //System.out.println(nar.getTasList().get(3));
        // System.out.println("Number motors " + Integer.toString(aircraft.getAircraftModel().getNumberMotors()));
       NetworkAnalysisResult expResult = nar;
       // NetworkAnalysisResult result = na.calculateLessEnergyComsunption();
        System.out.println("expResult = \n" + expResult.toString());
        //System.out.println("Result = \n" + result.toString());
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getAltitude method, of class NetworkAnalysis.
     */
    @Test
    public void testGetAltitude() {
        System.out.println("getAltitude");
        NetworkAnalysis instance = new NetworkAnalysis();
        ArrayList expResult = null;
        ArrayList result = instance.getAltitude();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAltitude method, of class NetworkAnalysis.
     */
    @Test
    public void testSetAltitude() {
        System.out.println("setAltitude");
        ArrayList altitude = null;
        NetworkAnalysis instance = new NetworkAnalysis();
        instance.setAltitude(altitude);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getVclimb method, of class NetworkAnalysis.
     */
    @Test
    public void testGetVclimb() {
        System.out.println("getVclimb");
        NetworkAnalysis instance = new NetworkAnalysis();
        ArrayList expResult = null;
        ArrayList result = instance.getVclimb();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setVclimb method, of class NetworkAnalysis.
     */
    @Test
    public void testSetVclimb() {
        System.out.println("setVclimb");
        ArrayList Vclimb = null;
        NetworkAnalysis instance = new NetworkAnalysis();
        instance.setVclimb(Vclimb);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getVdesc method, of class NetworkAnalysis.
     */
    @Test
    public void testGetVdesc() {
        System.out.println("getVdesc");
        NetworkAnalysis instance = new NetworkAnalysis();
        ArrayList expResult = null;
        ArrayList result = instance.getVdesc();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setVdesc method, of class NetworkAnalysis.
     */
    @Test
    public void testSetVdesc() {
        System.out.println("setVdesc");
        ArrayList Vdesc = null;
        NetworkAnalysis instance = new NetworkAnalysis();
        instance.setVdesc(Vdesc);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of calculateCdrag method, of class NetworkAnalysis.
     */
    @Test
    public void testCalculateCdrag() {
        System.out.println("calculateCdrag");
        NetworkAnalysis instance = new NetworkAnalysis();
        double expResult = 0.0;
        double result = instance.calculateCdrag();
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}

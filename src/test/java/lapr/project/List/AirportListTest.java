
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.List;

import lapr.project.List.AirportList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import lapr.project.model.Airport;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class AirportListTest {

    private AirportList airportList;
        private Airport a;
    public AirportListTest() {
        this.airportList = new AirportList();
         a = new Airport("a","name","town","country",1,1,1);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getAirports method, of class AirportList.
     */
    @Test
    public void testGetAndSetAirports() {
      
        List<Airport> la = new ArrayList();
        System.out.println("getAirports");
        AirportList instance = airportList;

        la.add(a);
        instance.setAirports(la);

        List<Airport> result = instance.getAirports();
        assertEquals(la, result);

    }

    /**
     * Test of size method, of class AirportList.
     */
    @Test
    public void testSize() {
     
        System.out.println("size");
        AirportList instance = airportList;
        instance.addAirport(a);
        int expResult = 1;
        int result = instance.size();
        assertEquals(expResult, result);

    }

    /**
     * Test of newAirport method, of class AirportList.
     */
    @Test
    public void testNewAirport() {
        System.out.println("newAirport");
        AirportList instance = airportList;
        Airport expResult = new Airport();
        Airport result = instance.newAirport();
        assertEquals(expResult, result);

    }

    /**
     * Test of addAirport method, of class AirportList.
     */
    @Test
    public void testAddAirport() {
        System.out.println("addAirport");
        
        AirportList instance = airportList;
        instance.addAirport(a);
        boolean expResult = true;
        boolean result = instance.getAirports().contains(a);
        assertEquals(expResult, result);

    }

    /**
     * Test of removeAirport method, of class AirportList.
     */
    @Test
    public void testRemoveAirport() {
        System.out.println("removeAirport");
    
        AirportList instance = airportList;
        boolean expResult = false;
        instance.removeAirport(a);
        boolean result = instance.getAirports().contains(a);
        assertEquals(expResult, result);

    }

}


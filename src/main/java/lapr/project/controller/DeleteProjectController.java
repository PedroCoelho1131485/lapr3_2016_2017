/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.List;
import lapr.project.model.ProjectRegistry;

/**
 *
 * @author Diogo Guedes <1150613@isep.ipp.pt>
 */
public class DeleteProjectController {

    /**
     * The project handler.
     */
    private ProjectRegistry ph;

    /**
     * Creates an instance of OpenProjectController.
     *
     *
     * @param proj
     */
    public DeleteProjectController(ProjectRegistry proj) {
        this.ph = proj;
    }

    /**
     * Returns the list of existing projects in the database.
     *
     * @return list of existing projects
     */
    public List<String> getProjectList() {

        return ph.getProjectList();
    }

    /**
     * Deletes the project in use by name.
     *
     * @param name name of new project in use
     * @return
     */
    public boolean deleteProject(String name) {
        return this.ph.deleteProject(name);

    }

}

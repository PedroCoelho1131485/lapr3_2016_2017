
package lapr.project.model;

import lapr.project.List.CdragList;
import java.util.Objects;

/**
 *
 *
 */
public class AircraftModel {

    private String modelId;
    private String maker;
    private String type; //cargo or passenger or  mixed
    private Motorization motorization;
    private double emptyWeight;
    private double MaximumTakeOffWeight;
    private double MaximumFuelCapacity;
    private double maxPayLoad;
    private double aspectRatio;
    private double VMO;
    private double MMO;
    private double wingArea;
    private double wingSpan;

    private double e;
    private CdragList CDragList;

    public AircraftModel(String modelId, String maker, String type, Motorization motorization, double emptyWeight, double MaximumTakeOffWeight, double MaximumFuelCapacity, double maxPayLoad, double aspectRatio, double VMO, double MMO, double wingArea, double wingSpan, double e, CdragList CDragList) {
        this.modelId = modelId;
        this.maker = maker;
        this.type = type;
        this.motorization = motorization;
        this.emptyWeight = emptyWeight;
        this.MaximumTakeOffWeight = MaximumTakeOffWeight;
        this.MaximumFuelCapacity = MaximumFuelCapacity;
        this.maxPayLoad = maxPayLoad;
        this.aspectRatio = aspectRatio;
        this.VMO = VMO;
        this.MMO = MMO;
        this.wingArea = wingArea;
        this.wingSpan = wingSpan;
        this.e = e;
        this.CDragList = new CdragList();
    }

    //default constructor
    public AircraftModel() {
        this.modelId = "";
        this.maker = "";
        this.type = "";

        this.emptyWeight = 0.0;
        this.MaximumTakeOffWeight = 0.0;

        this.MaximumFuelCapacity = 0.0;
        this.wingArea = 0.0;
        this.aspectRatio = 0.0;

        this.e = 0.0;
        motorization = new Motorization();
        CDragList = new CdragList();
    }

    /**
     * @return the maxPayLoad
     */
    public double getMaxPayLoad() {
        return maxPayLoad;
    }

    /**
     * @param maxPayLoad the maxPayLoad to set
     */
    public void setMaxPayLoad(double maxPayLoad) {
        this.maxPayLoad = maxPayLoad;
    }

    /**
     * @return the VMO
     */
    public double getVMO() {
        return VMO;
    }

    /**
     * @param VMO the VMO to set
     */
    public void setVMO(double VMO) {
        this.VMO = VMO;
    }

    /**
     * @return the MMO
     */
    public double getMMO() {
        return MMO;
    }

    /**
     * @param MMO the MMO to set
     */
    public void setMMO(double MMO) {
        this.MMO = MMO;
    }

    /**
     * @return the modelId
     */
    public String getModelId() {
        return modelId;
    }

    /**
     * @param modelId the modelId to set
     */
    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    /**
     * @return the modelRegistration
     */
    public String getMaker() {
        return maker;
    }

    /**
     * @param maker
     */
    public void setMaker(String maker) {
        this.maker = maker;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the Motorization
     */
    public String getMotor() {
        return getMotorization().getMotor();
    }

    /**
     * @param motor
     */
    public void setMotor(String motor) {
        this.getMotorization().setMotor(motor);
    }

    /**
     * @return the emptyWeight
     */
    public double getEmptyWeight() {
        return emptyWeight;
    }

    /**
     * @param emptyWeight the emptyWeight to set
     */
    public void setEmptyWeight(double emptyWeight) {
        this.emptyWeight = emptyWeight;
    }

    /**
     * @return the MaximumTakeOffWeight
     */
    public double getMaximumTakeOffWeight() {
        return MaximumTakeOffWeight;
    }

    /**
     * @param MaximumTakeOffWeight the MaximumTakeOffWeight to set
     */
    public void setMaximumTakeOffWeight(double MaximumTakeOffWeight) {
        this.MaximumTakeOffWeight = MaximumTakeOffWeight;
    }

    /**
     * @return the MaximumFuelCapacity
     */
    public double getMaximumFuelCapacity() {
        return MaximumFuelCapacity;
    }

    /**
     * @param MaximumFuelCapacity the MaximumFuelCapacity to set
     */
    public void setMaximumFuelCapacity(double MaximumFuelCapacity) {
        this.MaximumFuelCapacity = MaximumFuelCapacity;
    }

    /**
     * @return the wingArea
     */
    public double getWingArea() {
        return wingArea;
    }

    /**
     * @param wingArea the wingArea to set
     */
    public void setWingArea(double wingArea) {
        this.wingArea = wingArea;
    }

    /**
     * @return the numberMotors
     */
    public int getNumberMotors() {
        return this.getMotorization().getNumberMotors();
    }

    /**
     * @param numberMotors the numberMotors to set
     */
    public void setNumberMotors(int numberMotors) {
        this.getMotorization().setNumberMotors(numberMotors);
    }

    /**
     * @return the motortype
     */
    public String getMotorType() {
        return this.getMotorization().getMotorType();
    }

    /**
     * @param motortype the motortype to set
     */
    public void setMotorType(String motortype) {
        this.getMotorization().setMotorType(motortype);
    }

    /**
     * @return the wingSpan
     */
    public double getWingSpan() {
        return wingSpan;
    }

    /**
     * @param wingSpan the wingSpan to set
     */
    public void setWingSpan(double wingSpan) {
        this.wingSpan = wingSpan;
    }

    /**
     * @return the e
     */
    public double getE() {
        return e;
    }

    /**
     * @param e the e to set
     */
    public void setE(double e) {
        this.e = e;
    }

    public Motorization getMotorization() {
        return motorization;
    }

    @Override
    public String toString() {
        return "AircraftModel{" + "modelId=" + modelId + ", maker=" + maker + ", type=" + type + ", motorization=" + motorization + ", emptyWeight=" + emptyWeight + ", MaximumTakeOffWeight=" + MaximumTakeOffWeight + ", MaximumFuelCapacity=" + MaximumFuelCapacity + ", maxPayLoad=" + maxPayLoad + ", aspectRatio=" + aspectRatio + ", VMO=" + VMO + ", MMO=" + MMO + ", wingArea=" + wingArea + ", wingSpan=" + wingSpan + ", e=" + e + '}';
    }

    /**
     * @return the aspectRatio
     */
    public double getAspectRatio() {
        return aspectRatio;
    }

    /**
     * @param aspectRatio the aspectRatio to set
     */
    public void setAspectRatio(double aspectRatio) {
        this.aspectRatio = aspectRatio;
    }

    /**
     * @param motorization the motorization to set
     */
    public void setMotorization(Motorization motorization) {
        this.motorization = motorization;
    }

    public CdragList getCDragList() {
        return CDragList;
    }

    public void setCDragList(CdragList CDragList) {
        this.CDragList = CDragList;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.modelId);
        hash = 67 * hash + Objects.hashCode(this.maker);
        hash = 67 * hash + Objects.hashCode(this.type);
        hash = 67 * hash + Objects.hashCode(this.motorization);
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.emptyWeight) ^ (Double.doubleToLongBits(this.emptyWeight) >>> 32));
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.MaximumTakeOffWeight) ^ (Double.doubleToLongBits(this.MaximumTakeOffWeight) >>> 32));
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.MaximumFuelCapacity) ^ (Double.doubleToLongBits(this.MaximumFuelCapacity) >>> 32));
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.maxPayLoad) ^ (Double.doubleToLongBits(this.maxPayLoad) >>> 32));
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.aspectRatio) ^ (Double.doubleToLongBits(this.aspectRatio) >>> 32));
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.VMO) ^ (Double.doubleToLongBits(this.VMO) >>> 32));
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.MMO) ^ (Double.doubleToLongBits(this.MMO) >>> 32));
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.wingArea) ^ (Double.doubleToLongBits(this.wingArea) >>> 32));
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.wingSpan) ^ (Double.doubleToLongBits(this.wingSpan) >>> 32));
        hash = 67 * hash + (int) (Double.doubleToLongBits(this.e) ^ (Double.doubleToLongBits(this.e) >>> 32));
        hash = 67 * hash + Objects.hashCode(this.CDragList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AircraftModel other = (AircraftModel) obj;
        if (Double.doubleToLongBits(this.emptyWeight) != Double.doubleToLongBits(other.emptyWeight)) {
            return false;
        }
        if (Double.doubleToLongBits(this.MaximumTakeOffWeight) != Double.doubleToLongBits(other.MaximumTakeOffWeight)) {
            return false;
        }
        if (Double.doubleToLongBits(this.MaximumFuelCapacity) != Double.doubleToLongBits(other.MaximumFuelCapacity)) {
            return false;
        }
        if (Double.doubleToLongBits(this.maxPayLoad) != Double.doubleToLongBits(other.maxPayLoad)) {
            return false;
        }
        if (Double.doubleToLongBits(this.aspectRatio) != Double.doubleToLongBits(other.aspectRatio)) {
            return false;
        }
        if (Double.doubleToLongBits(this.VMO) != Double.doubleToLongBits(other.VMO)) {
            return false;
        }
        if (Double.doubleToLongBits(this.MMO) != Double.doubleToLongBits(other.MMO)) {
            return false;
        }
        if (Double.doubleToLongBits(this.wingArea) != Double.doubleToLongBits(other.wingArea)) {
            return false;
        }
        if (Double.doubleToLongBits(this.wingSpan) != Double.doubleToLongBits(other.wingSpan)) {
            return false;
        }
        if (Double.doubleToLongBits(this.e) != Double.doubleToLongBits(other.e)) {
            return false;
        }
        if (!Objects.equals(this.modelId, other.modelId)) {
            return false;
        }
        if (!Objects.equals(this.maker, other.maker)) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.motorization, other.motorization)) {
            return false;
        }
        if (!Objects.equals(this.CDragList, other.CDragList)) {
            return false;
        }
        return true;
    }

    /**
     * ValIATAate the aircraft.
     *
     * @return True if aircraft is validate else returns false.
     */
    public boolean validate() {

        if (this.maker == null) {
            throw new IllegalArgumentException("Maker can't be null");
        }

        if (this.type == null) {
            throw new IllegalArgumentException("type can't be null");
        }

        if (this.motorization == null) {
            throw new IllegalArgumentException("Motorization can't be null");
        }

        if (this.emptyWeight == 0) {
            throw new IllegalArgumentException("Empty weight can't be zero");
        }

        if (this.MaximumTakeOffWeight == 0) {
            throw new IllegalArgumentException("Maximum Take Off Weight can't be zero");
        }

        if (this.maxPayLoad == 0) {
            throw new IllegalArgumentException("Max payload can't be zero");
        }

        if (this.MaximumFuelCapacity == 0) {
            throw new IllegalArgumentException("Maximum fuel capacity can't be zero");
        }

        if (this.VMO == 0) {
            throw new IllegalArgumentException("VMO can't be zero");
        }

        if (this.MMO == 0) {
            throw new IllegalArgumentException("MMO can't be zero");
        }

        if (this.wingArea == 0) {
            throw new IllegalArgumentException("Wing area can't be zero");
        }

        if (this.wingSpan == 0) {
            throw new IllegalArgumentException("Wing span can't be zero");
        }

        if (this.aspectRatio == 0) {
            throw new IllegalArgumentException("Aspect ratio can't be zero");
        }

        if (this.e == 0) {
            throw new IllegalArgumentException("E can't be zero");
        }

        if (this.CDragList == null) {
            throw new IllegalArgumentException("Cdrag list can't be null");
        }

        if (this.motorization.validate() == false) {
            return false;
        }
        return true;
    }

}

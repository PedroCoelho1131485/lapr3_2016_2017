
package lapr.project.model;

/**
 *
 * @author pedro
 */
public class Cdrag {

    private double speed;
    private double Cdrag_0;

    public Cdrag() {
    }

    public Cdrag(double speed, double Cdrag_0) {
        this.speed = speed;
        this.Cdrag_0 = Cdrag_0;
    }

    /**
     * @return the speed
     */
    public double getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(double speed) {
        this.speed = speed;
    }

    /**
     * @return the Cdrag_0
     */
    public double getCdrag_0() {
        return Cdrag_0;
    }

    /**
     * @param Cdrag_0 the Cdrag_0 to set
     */
    public void setCdrag_0(double Cdrag_0) {
        this.Cdrag_0 = Cdrag_0;
    }

    /**
     * Validate the cdrag.
     *
     * @return True if cdrag is valid else returns false.
     */
    public boolean validate() {

        if (this.speed < 0) {
            throw new IllegalArgumentException("speed must be greater than 0 or equal");
        }
        if (this.Cdrag_0 < 0) {
            throw new IllegalArgumentException("CDrag must be greater than 0 or equal");
        }

        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + (int) (Double.doubleToLongBits(this.speed) ^ (Double.doubleToLongBits(this.speed) >>> 32));
        hash = 73 * hash + (int) (Double.doubleToLongBits(this.Cdrag_0) ^ (Double.doubleToLongBits(this.Cdrag_0) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cdrag other = (Cdrag) obj;
        if (Double.doubleToLongBits(this.speed) != Double.doubleToLongBits(other.speed)) {
            return false;
        }
        if (Double.doubleToLongBits(this.Cdrag_0) != Double.doubleToLongBits(other.Cdrag_0)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Cdrag{" + "speed=" + speed + ", Cdrag_0=" + Cdrag_0 + '}';
    }

}

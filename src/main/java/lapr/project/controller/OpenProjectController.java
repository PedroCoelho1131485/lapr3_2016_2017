/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.List;
import lapr.project.model.ProjectRegistry;

/**
 * Represents an instance of OpenProjectController.
 *
 *
 */
public class OpenProjectController {

    /**
     * The project handler.
     */
    private ProjectRegistry ph;

    /**
     * Creates an instance of OpenProjectController.
     *
     *
     * @param proj
     */
    public OpenProjectController(ProjectRegistry proj) {
        this.ph = proj;
    }

    /**
     * Returns the list of existing projects in the database.
     *
     * @return list of existing projects
     */
    public List<String> getProjectList() {

        return ph.getProjectList();
    }

    /**
     * Changes the project in use by name.
     *
     * @param name name of new project in use
     * @return
     */
    public String selectProject(String name) {

        this.ph.setProjectByName(name);
        return this.ph.getNameOpenProject();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.ArrayList;
import java.util.List;
import lapr.project.AdjacencyMap.Graph;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class ProjectRegistryTest {
    
    public ProjectRegistryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of newProject method, of class ProjectRegistry.
     */
    @Test
    public void testNewProject() {
        System.out.println("newProject");
        ProjectRegistry instance = new ProjectRegistry();
        Project expResult = new Project();
        Project result = instance.newProject();
        assertEquals(expResult, result);
        System.out.println(result.toString() + " \n" + expResult.toString());
    }

    /**
     * Test of addProject method, of class ProjectRegistry.
     */
    @Test
    public void testAddProject() {
        System.out.println("addProject");
        Project p = new Project();
        ProjectRegistry instance = new ProjectRegistry();
        boolean expResult = false;
        boolean result = instance.addProject(p);
        assertEquals(expResult, result);
     
    }

    /**
     * Test of registerChanges method, of class ProjectRegistry.
     */
    @Test
    public void testRegisterChanges() {
        System.out.println("registerChanges");
        Project p = new Project("Name2","Description");
        Project oldP = new Project("Name1","Description1");
        Junction startNode1 = new Junction();
        Junction endNode1 = new Junction();
        startNode1.setName("Node1");
        endNode1.setName("Node2");
        Junction startNode2 = new Junction();
        Junction endNode2 = new Junction();
        startNode2.setName("Node3");
        endNode2.setName("Node4");
        Segment segment1 = new Segment();
        segment1.setEndNode(endNode1);
        segment1.setStartNode(startNode1);
        Segment segment2 = new Segment();
        segment2.setEndNode(endNode2);
        segment2.setStartNode(startNode2);
        p.addSegment(segment2);
        p.addSegment(segment1);
        oldP.addSegment(segment1);
        oldP.addSegment(segment2);
        ProjectRegistry instance = new ProjectRegistry();
        boolean expResult = true;
        boolean result = instance.registerChanges(p, oldP);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of registerCopy method, of class ProjectRegistry.
     */
    @Test
    public void testRegisterCopy() {
        System.out.println("registerCopy");
        Project project = new Project();
        project.setName("Project1");
        project.setDescription("Description1");
        ProjectRegistry instance = new ProjectRegistry();
        Junction startNode1 = new Junction();
        Junction endNode1 = new Junction();
        startNode1.setName("Node1");
        endNode1.setName("Node2");
        Junction startNode2 = new Junction();
        Junction endNode2 = new Junction();
        startNode2.setName("Node3");
        endNode2.setName("Node4");
        Segment segment1 = new Segment();
        segment1.setEndNode(endNode1);
        segment1.setStartNode(startNode1);
        Segment segment2 = new Segment();
        segment2.setEndNode(endNode2);
        segment2.setStartNode(startNode2);
        project.addSegment(segment2);
        project.addSegment(segment1);
        boolean expResult = true;
        boolean result = instance.registerCopy(project);
        assertEquals(expResult, result);

    }

    /**
     * Test of getProjectList method, of class ProjectRegistry.
     */
    @Test
    public void testGetProjectList() {
        System.out.println("getProjectList");
        ProjectRegistry instance = new ProjectRegistry();
        List<String> expResult = null;
        List<String> result = instance.getProjectList();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of getActiveProject method, of class ProjectRegistry.
     */
    @Test
    public void testGetActiveProject() {
        System.out.println("getActiveProject");
        ProjectRegistry instance = new ProjectRegistry();
        Project expResult = new Project();
        instance.setActiveProject(expResult);
        Project result = instance.getActiveProject();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setActiveProject method, of class ProjectRegistry.
     */
    @Test
    public void testSetActiveProject() {
        System.out.println("setActiveProject");
        Project activeProject = new Project();
        ProjectRegistry instance = new ProjectRegistry();
        instance.setActiveProject(activeProject);
        assertEquals(activeProject,instance.getActiveProject());
    }

    /**
     * Test of setProjectByName method, of class ProjectRegistry.
     */
    /*@Test
    public void testSetProjectByName() {
        System.out.println("setProjectByName");
        String name = "";
        ProjectRegistry instance = new ProjectRegistry();
        instance.setProjectByName(name);
        
    }/*

    /**
     * Test of getNameOpenProject method, of class ProjectRegistry.
     */
    @Test
    public void testGetNameOpenProject() {
        System.out.println("getNameOpenProject");
        ProjectRegistry instance = new ProjectRegistry();
        String expResult = "";
        Project project = new Project();
        instance.setActiveProject(project);
        String result = instance.getNameOpenProject();
        assertEquals(expResult, result);
        
    }
    
}

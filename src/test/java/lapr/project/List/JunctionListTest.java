/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.List;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import lapr.project.List.JunctionList;
import lapr.project.model.Junction;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class JunctionListTest {

    private JunctionList jl;
private   Junction j;
    public JunctionListTest() {
       j = new Junction("name", 1, 1);
        jl = new JunctionList();

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setJunctionList method, of class JunctionList.
     */
    @Test
    public void testGetAndSetJunctionList() {
        System.out.println("setJunctionList");
        List<Junction> expResult = new ArrayList();
        JunctionList instance = jl;
        expResult.add(j);
        instance.setJunctionList(expResult);
        
        instance.addJunction(j);
   
        assertEquals(expResult,instance.getjunctionList());
 
    }

    /**
     * Test of addJunction method, of class JunctionList.
     */
    @Test
    public void testAddJunction() {
        System.out.println("addJunction");
 
        JunctionList instance = jl;
        boolean expResult = true;
        boolean result = instance.addJunction(j); //true because theres no equal junction on the list
        assertEquals(expResult, result);
    }

    /**
     * Test of iterator method, of class JunctionList.
     */
    @Test
    public void testIterator() {
        System.out.println("iterator");
        JunctionList instance = jl;
        Iterator ite = instance.iterator();
        Iterator<Junction> expResult = ite;
        Iterator<Junction> result = ite;
        assertEquals(expResult, result);

    }

    /**
     * Test of removeJunction method, of class JunctionList.
     */
    @Test
    public void testRemoveJunction() {
        System.out.println("removeJunction");

        JunctionList instance = jl;
        boolean expResult =true;
        instance.addJunction(j);
        boolean result = instance.removeJunction(j);
        assertEquals(expResult, result);
      
    }

    /**
     * Test of getJunctionByName method, of class JunctionList.
     */
    @Test
    public void testGetJunctionByName() {
        System.out.println("getJunctionByName");
        String name = "Hello";
        JunctionList instance = jl;
        Junction expResult = j;
        expResult.setName(name);
        instance.addJunction(expResult);
        Junction result = instance.getJunctionByName(name);
        assertEquals(expResult, result);
    }

}

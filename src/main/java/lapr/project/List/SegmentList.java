
package lapr.project.List;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import lapr.project.model.Segment;

/**
 *
 *
 */
public class SegmentList {

    /**
     * List of segments
     */
    private List<Segment> segmentList;

    /**
     * Build an instance of SegmentList
     */
    public SegmentList() {
        this.segmentList = new ArrayList<>();
    }

    /**
     * Creates a clone of the target segmentlist
     *
     * @param source The target segment list to clone.
     */
    public SegmentList(SegmentList source) {
        segmentList = new ArrayList(source.segmentList);
        Collections.copy(segmentList, source.segmentList);
    }

    /**
     * Modify the list
     *
     * @param segmentList
     */
    public void setSegmentList(List<Segment> segmentList) {
        this.segmentList.clear();
        this.segmentList.addAll(segmentList);
    }

    /**
     * Return the number of section segments.
     *
     * @return
     */
    public int size() {
        return this.segmentList.size();
    }

    /**
     * Return segmentList
     *
     * @return segmentList
     */
    public List<Segment> getsegmentList() {
        return this.segmentList;
    }

    /**
     * Creates a new segment.
     *
     * @return New segment.
     */
    public Segment newSegment() {
        return new Segment();
    }

    /**
     * Add segmento to listSegment
     *
     * @param segment
     * @return Returns true if added and false otherwise
     */
    public boolean addSegment(Segment segment) {

        if (this.segmentList.contains(segment)) {
            return false;
        } else {
            segmentList.add(segment);
            return true;
        }

    }

    /**
     * Remove segment of listSegment
     *
     * @param segment
     */
    public boolean removeSegment(Segment segment) {
        return this.segmentList.remove(segment);
    }

    /**
     * Return segmentList
     *
     * @return segmentList
     */
    public List<Segment> getList() {
        return this.segmentList;
    }

    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }
        SegmentList otherSegmentList = (SegmentList) outroObjeto;
        return this.segmentList.equals(otherSegmentList.getsegmentList());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Segment List: \n");
        for (Segment s : segmentList) {
            sb.append(s);
            sb.append("\n");
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 19 * hash + Objects.hashCode(this.segmentList);
        return hash;
    }

    /**
     * Returns an iterator to allow running through the list of segments.
     *
     * @return Iterator.
     */
    public Iterator<Segment> iterator() {
        return this.segmentList.iterator();
    }

    public Segment getSegmentByName(String name) {
        for (Segment s : segmentList) {
            if (s.getName().equals(name)) {
                return s;
            }
        }
        return null;
    }

}

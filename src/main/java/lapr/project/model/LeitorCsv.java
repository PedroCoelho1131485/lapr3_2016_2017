/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import lapr.project.model.FlightPlan;
import lapr.project.model.NetworkAnalysis;

/**
 *
 * @author Utilizador
 */
public class LeitorCsv implements Importable {

    public LeitorCsv() {

    }

    @Override
    public boolean importNetwork(Project project, String filePath) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean importAirports(Project project, String filePath) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean importAircrafts(Project project, String filePath) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean importCSV(String path, FlightPlan na) {
        BufferedReader br = null;
        String line = "";

        ArrayList list;
        FlightPlan n = na;
        int cont = 0;
        try {
            br = new BufferedReader(new FileReader(path));
            while ((line = br.readLine()) != null) {
                String[] linha = line.split(",");
                list = new ArrayList();
                cont++;
                for (int i = 2; i < linha.length; i++) {
                    list.add(Double.parseDouble(linha[i]));
                }
                switch (cont) {
                    case 1:
                        n.setAltitude(list);
                        break;
                    case 2:
                        n.setVclimb(list);
                        break;
                    case 3:
                        n.setVdesc(list);
                        break;

                }
            }
            br.close();
            return true;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }
}


package lapr.project.model;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author Carlos Neiva
 */
public class ExportHTML implements Exportable {

    /**
     * Creates an instance of ExportHTML.
     */
    public ExportHTML() {

    }

    /**
     * Exports the results of the network analysis to an html page.
     *
     * @param nar list of network static analysis results.
     * @param filePath Path to export the file.
     */
    public void exportNetworkAnalysis(NetworkAnalysisResult nar, String filePath) {
        StringBuilder sb = new StringBuilder();

        sb.append("<!DOCTYPE html>\n<html>\n<head>\n"
                + "\t<title>LAPR3 - Results network analysis</title>\n"
                + "\t<style>\n"
                + "\t\tbody {\n"
                + "\t\t\tmargin: 50px;\n"
                + "\t\t}\n"
                + "\t\ttd, th {\n"
                + "\t\t\tborder: 1px solid #999;\n"
                + "\t\t\tpadding: 5px;\n"
                + "\t\t}\n"
                + "\t</style>\n"
                + "</head>\n<body>\n");
        sb.append("Results network analysis: </br>\n</br\n>");

        sb.append(nar.toStringHTML());
//        if(nar.get(0)!=null){
//            sb.append(nar.get(0).toStringHTML());
//        }
//        
//        if(nar.get(1)!=null){
//            sb.append(nar.get(1).toStringHTML());
//        }
//        
//        if(nar.get(2)!=null){
//            sb.append(nar.get(2).toStringHTML());
//        }
//        
//        if(nar.get(3)!=null){
//            sb.append(nar.get(3).toStringHTML());
//        }

        sb.append("</body>\n</html>\n");
        createHTML(filePath, sb);
    }

    /**
     * Writes the contents of a StringBuilder on the html page.
     *
     * @param path Path where to export the file.
     * @param stringBuilder contains the output format.
     */
    private void createHTML(String path, StringBuilder stringBuilder) {
        try {
            File file = new File(path);

            if (!file.exists()) {
                file.createNewFile();
            }

            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            bw.write(stringBuilder.toString());
            bw.close();

        } catch (IOException e) {
            throw new IllegalArgumentException("Error");
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class NetworkAnalysisResultTest {
    
    public NetworkAnalysisResultTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getId method, of class NetworkAnalysisResult.
     */
    @Test
    public void testGetId() {
        System.out.println("getId");
        NetworkAnalysisResult instance = new NetworkAnalysisResult();
        int expResult = 4;
        instance.setId(expResult);
        int result = instance.getId();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of getEnergyExpenditure method, of class NetworkAnalysisResult.
     */
    @Test
    public void testGetEnergyExpenditure() {
        System.out.println("getEnergyExpenditure");
        NetworkAnalysisResult instance = new NetworkAnalysisResult();
        double expResult = 0.0;
        double result = instance.getEnergyExpenditure();
        assertEquals(expResult, result, 0.0);
       
    }

    /**
     * Test of setEnergyExpenditure method, of class NetworkAnalysisResult.
     */
    @Test
    public void testSetEnergyExpenditure() {
        System.out.println("setEnergyExpenditure");
        double energyExpenditure = 0.0;
        NetworkAnalysisResult instance = new NetworkAnalysisResult();
        instance.setEnergyExpenditure(energyExpenditure);
        assertEquals(energyExpenditure,instance.getEnergyExpenditure(),0.0);
    }

    /**
     * Test of getAircraft method, of class NetworkAnalysisResult.
     */
    @Test
    public void testGetAircraft() {
        System.out.println("getAircraft");
        NetworkAnalysisResult instance = new NetworkAnalysisResult();
        String registration = "";
        String Company = "";
         int numberSeatsEconomic = 0;
          int numberSeatsExecutiv = 0;//aumentar no futuro quando se souber quantos tipos de classe ha
         int nElemCrew = 0;
         AircraftModel aircraftModel = new AircraftModel();
         int number = 0;
        Aircraft expResult = new Aircraft(registration,Company,numberSeatsEconomic,numberSeatsExecutiv,nElemCrew,aircraftModel,number);
        instance.setAircraft(expResult);
        Aircraft result = instance.getAircraft();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setAircraft method, of class NetworkAnalysisResult.
     */
    @Test
    public void testSetAircraft() {
        System.out.println("setAircraft");
        Aircraft aircraft = new Aircraft();
        NetworkAnalysisResult instance = new NetworkAnalysisResult();
        instance.setAircraft(aircraft);
        assertEquals(aircraft,instance.getAircraft());
    }

    /**
     * Test of getTravellingTime method, of class NetworkAnalysisResult.
     */
    @Test
    public void testGetTravellingTime() {
        System.out.println("getTravellingTime");
        NetworkAnalysisResult instance = new NetworkAnalysisResult();
        double expResult = 0.0;
        double result = instance.getTravellingTime();
        assertEquals(expResult, result, 0.0);
    
    }

    /**
     * Test of setTravellingTime method, of class NetworkAnalysisResult.
     */
    @Test
    public void testSetTravellingTime() {
        System.out.println("setTravellingTime");
        double travellingTime = 0.0;
        NetworkAnalysisResult instance = new NetworkAnalysisResult();
        instance.setTravellingTime(travellingTime);
      
    }

    /**
     * Test of setType method, of class NetworkAnalysisResult.
     */
    @Test
    public void testSetType() {
        System.out.println("setType");
        int type = 0;
        NetworkAnalysisResult instance = new NetworkAnalysisResult();
        instance.setType(type);
        assertEquals(type,instance.getType());
    }

    /**
     * Test of getPath method, of class NetworkAnalysisResult.
     */
    @Test
    public void testGetPath() {
        System.out.println("getPath");
        NetworkAnalysisResult instance = new NetworkAnalysisResult();
        List<Segment> expResult = instance.getPath();
        List<Segment> result = instance.getPath();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setPath method, of class NetworkAnalysisResult.
     */
    @Test
    public void testSetPath() {
        System.out.println("setPath");
        List<Segment> path = new ArrayList();
        NetworkAnalysisResult instance = new NetworkAnalysisResult();
        instance.setPath(path);
        assertEquals(path,instance.getPath());
    }

    /**
     * Test of getResults method, of class NetworkAnalysisResult.
     */
    @Test
    public void testGetResults() {
        System.out.println("getResults");
        NetworkAnalysisResult instance = new NetworkAnalysisResult();
        int expResult = -1;
        int result = instance.getResults();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getDestiny method, of class NetworkAnalysisResult.
     */
    @Test
    public void testGetDestiny() {
        System.out.println("getDestiny");
        NetworkAnalysisResult instance = new NetworkAnalysisResult();
         String name = "Junction";
         double latitude = 0.0;
         double longitude = 0.0;
        Junction j = new Junction(name,latitude,longitude);
        Junction expResult = j;
        instance.setDestiny(j);
        Junction result = instance.getDestiny();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setDestiny method, of class NetworkAnalysisResult.
     */
    @Test
    public void testSetDestiny() {
        System.out.println("setDestiny");
        Junction destiny = new Junction();
        NetworkAnalysisResult instance = new NetworkAnalysisResult();
        instance.setDestiny(destiny);
        assertEquals(destiny,instance.getDestiny());
    }

    /**
     * Test of getOrigin method, of class NetworkAnalysisResult.
     */
    @Test
    public void testGetOrigin() {
        System.out.println("getOrigin");
        NetworkAnalysisResult instance = new NetworkAnalysisResult();
        Junction expResult = new Junction();
        instance.setOrigin(expResult);
        Junction result = instance.getOrigin();
        assertEquals(expResult, result);
     
    }

    /**
     * Test of setOrigin method, of class NetworkAnalysisResult.
     */
    @Test
    public void testSetOrigin() {
        System.out.println("setOrigin");
        Junction Origin = new Junction();
        NetworkAnalysisResult instance = new NetworkAnalysisResult();
        instance.setOrigin(Origin);
        assertEquals(Origin,instance.getOrigin());
    }

    /**
     * Test of getFp method, of class NetworkAnalysisResult.
     */
    @Test
    public void testGetFp() {
        System.out.println("getFp");
        NetworkAnalysisResult instance = new NetworkAnalysisResult();
        FlightPlan expResult = new FlightPlan();
        instance.setFp(expResult);
        FlightPlan result = instance.getFp();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setFp method, of class NetworkAnalysisResult.
     */
    @Test
    public void testSetFp() {
        System.out.println("setFp");
        FlightPlan fp = null;
        NetworkAnalysisResult instance = new NetworkAnalysisResult();
        instance.setFp(fp);
        assertEquals(fp,instance.getFp());
    }


    /**
     * Test of setResults method, of class NetworkAnalysisResult.
     */
    @Test
    public void testSetResults() {
        System.out.println("setResults");
        int results = 0;
        NetworkAnalysisResult instance = new NetworkAnalysisResult();
        instance.setResults(results);
        assertEquals(results,instance.getResults());
    }

    /**
     * Test of setId method, of class NetworkAnalysisResult.
     */
    @Test
    public void testSetId() {
        System.out.println("setId");
        int id = 0;
        NetworkAnalysisResult instance = new NetworkAnalysisResult();
        instance.setId(id);
        assertEquals(id,instance.getId(),0);
    }

    /**
     * Test of getType method, of class NetworkAnalysisResult.
     */
    @Test
    public void testGetType() {
        System.out.println("getType");
        NetworkAnalysisResult instance = new NetworkAnalysisResult();
        int expResult = 0;
        instance.setType(expResult);
        int result = instance.getType();
        assertEquals(expResult, result,0);
      
    }

    /**
     * Test of toStringHTML method, of class NetworkAnalysisResult.
     */
    @Test
    public void testToStringHTML() {
        System.out.println("toStringHTML");
        NetworkAnalysisResult instance = new NetworkAnalysisResult();
        List<Segment> path = new ArrayList<>();
        instance.setPath(path);
        int type = 1;
        Aircraft aircraft = new Aircraft();
        instance.setAircraft(aircraft);
        List<Segment> caminho = new ArrayList<>();
        instance.setPath(caminho);
        instance.setType(type); // Type = Shortest Path
        StringBuilder expResult = new StringBuilder();
        for (Segment s : instance.getPath()) {
            expResult.append(" ").append(s.getName()).append("; ");
        }

        expResult.append("<tr>"
                + "<td>").append("Shortest Path").append("</td>\n"
                + "<td>").append(expResult.toString()).append("</br>\n"
                + "<td>").append("Id aircraft: ").append("</td>\n"
                + "<td>").append(instance.getAircraft().getRegistration()).append("</br\n"
                + "<td>").append("Travelling time: ").append("</td>\n"
                + "<td>").append(instance.getTravellingTime()).append(" s</br\n>"
                + "<td>").append("Energy Expenditure").append("</td>\n"
                + "<td>").append(instance.getEnergyExpenditure()).append(" J</td>");

        expResult.append("</br>\n-------------------------------------------</br\n");
        
        
        String result = instance.toStringHTML();
        assertEquals(expResult.toString(), result);
 
    }

}

package lapr.project.List;

import lapr.project.List.AircraftList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import lapr.project.model.Aircraft;
import lapr.project.model.AircraftModel;
import lapr.project.model.Airport;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class AircraftListTest {

    
    private AircraftList aircraftList;
    private Aircraft a;

    public AircraftListTest() {
        AircraftModel am = new AircraftModel();

        a = new Aircraft("a", "company", 1, 1, 1, am, 1);
        this.aircraftList = new AircraftList();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of size method, of class AircraftList.
     */
    @Test
    public void testSize() {

        System.out.println("size");
        AircraftList instance = aircraftList;
        instance.addAircraft(a);
        int expResult = 1;

        int result = instance.size();
        assertEquals(expResult, result);

    }

    /**
     * Test of getAircraftList method, of class AircraftList.
     */
    @Test
    public void testGetAircraftList() {
        Aircraft a = new Aircraft();
        AircraftList instance = aircraftList;
        List<Aircraft> expResult = new ArrayList();
        expResult.add(a);
        List<Aircraft> result = instance.getAircraftList();
        result.add(a);

        assertEquals(expResult, result);
    }

    /**
     * Test of newAirport method, of class AircraftList.
     */
    @Test
    public void testNewAirport() {                                                //-----------------------------
        System.out.println("newAirport");
        AircraftList instance = aircraftList;
        Airport expResult = new Airport();
        Airport result = instance.newAirport();
        assertEquals(expResult, result);

    }

    /**
     * Test of addAircraft method, of class AircraftList.
     */
    @Test
    public void testAddAircraft() {
        System.out.println("addAircraft");

        AircraftList instance = aircraftList;
        boolean expResult = true;
        instance.addAircraft(a);
        boolean result = instance.getAircraftList().contains(a);
        assertEquals(expResult, result);
        instance.addAircraft(a);
        assertTrue(instance.getAircraftList().contains(a));
    }

    /**
     * Test of removeAirport method, of class AircraftList.
     */
    @Test
    public void testRemoveAirport() {
        System.out.println("removeAirport");

        AircraftList instance = aircraftList;
        instance.addAircraft(a);
        boolean expResult = true;
        boolean result = instance.removeAirport(a);
        assertEquals(expResult, result);

    }

    /**
     * Test of iterator method, of class AircraftList.
     */
    @Test
    public void testIterator() {
        System.out.println("iterator");
        AircraftList instance = new AircraftList();
        Iterator ite = instance.iterator();
        Iterator<Aircraft> expResult = ite;
        Iterator<Aircraft> result = ite;
        assertEquals(expResult, result);

    }

    /**
     * Test of getAircraft method, of class AircraftList.
     */
    @Test
    public void testGetAircraft() {
        System.out.println("getAircraft");
        String name = "001";
        AircraftList instance = aircraftList;
        Aircraft expResult = a;
        expResult.setCompany("Boeing");
        expResult.setRegistration("001");
        instance.addAircraft(expResult);
        Aircraft result = instance.getAircraft(name); //name actually is registration
        assertEquals(expResult, result);
        name = "Aviao";
        assertNull(instance.getAircraft(name));
    }

    /**
     * Test of setAircraftList method, of class AircraftList.
     */
    @Test
    public void testSetAircraftList() {
        System.out.println("setAircraftList");
        List<Aircraft> aircraftList1 = new ArrayList();
        aircraftList1.add(a);
        AircraftList instance = aircraftList;
        
        
        instance.setAircraftList(aircraftList1);
        assertEquals(aircraftList1, instance.getAircraftList());
    }

}

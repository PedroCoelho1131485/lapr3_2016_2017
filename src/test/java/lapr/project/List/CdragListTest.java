
package lapr.project.List;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import lapr.project.model.Cdrag;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pedro
 */
public class CdragListTest {

    private CdragList cl;
 private Cdrag c;
    public CdragListTest() {
        cl = new CdragList();
        c= new Cdrag(1,1);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getCdrag method, of class CdragList.
     */
    @Test
    public void testGetAndSetCdrag() {
        System.out.println("getCdrag");
        CdragList instance = cl;
        List<Cdrag> expResult = new ArrayList();
        expResult.add(c);
        instance.setCdrag(expResult);
        List<Cdrag> result = instance.getCdrag();
        assertEquals(expResult, result);
    
    }

    

    /**
     * Test of size method, of class CdragList.
     */
    @Test
    public void testSize() {
        System.out.println("size");
        CdragList instance = cl;
        int expResult = 1;
        
        instance.addCdrag(c);
        int result = instance.size();
        assertEquals(expResult, result);
      
    }

  

    /**
     * Test of newCdrag method, of class CdragList.
     */
    @Test
    public void testNewCdrag() {
        System.out.println("newCdrag");
        CdragList instance =cl;
        Cdrag expResult =new Cdrag();
        Cdrag result = instance.newCdrag();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of addCdrag method, of class CdragList.
     */
    @Test
    public void testAddCdrag() {
        System.out.println("addCdrag");
      
        CdragList instance = cl;
        boolean expResult = true;
        boolean result = instance.addCdrag(c);
        assertEquals(expResult, result);
  
    }

    /**
     * Test of removeAirport method, of class CdragList.
     */
    @Test
    public void testRemoveCdrag() {
        System.out.println("removeCdrag");
            CdragList instance = cl;
        boolean expResult =false;
     
        instance.removeCdrag(c);
       boolean result= instance.getCdrag().contains(c);
        assertEquals(expResult, result);
       
    }

    /**
     * Test of getCdrag method, of class CdragList.
     */
    @Test
    public void testGetCdrag() {
        System.out.println("getCdrag");
        CdragList instance = new CdragList();
        List<Cdrag> expResult = new ArrayList();
        List<Cdrag> result = instance.getCdrag();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCdrag method, of class CdragList.
     */
    @Test
    public void testSetCdrag() {
        System.out.println("setCdrag");
        List<Cdrag> cdrag = new ArrayList();
        CdragList instance = new CdragList();
        instance.setCdrag(cdrag);
        assertEquals(cdrag,instance.getCdrag());
    }

    /**
     * Test of hashCode method, of class CdragList.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        CdragList instance = new CdragList();
        int expResult = 7;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of iterator method, of class CdragList.
     */
    @Test
    public void testIterator() {
        System.out.println("iterator");
        CdragList instance = new CdragList();
        Iterator<Cdrag> ite = instance.iterator();
        Iterator<Cdrag> expResult = ite;
        Iterator<Cdrag> result = ite;
        assertEquals(expResult, result);
   
    }

  
    /**
     * Test of equals method, of class CdragList.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = (CdragList) new CdragList();
        CdragList instance = new CdragList();
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
        expResult = false;
        Cdrag cdrag = new Cdrag();
        instance.addCdrag(cdrag);
        result = instance.equals(obj);
        assertEquals(expResult,result);
    }


 

}

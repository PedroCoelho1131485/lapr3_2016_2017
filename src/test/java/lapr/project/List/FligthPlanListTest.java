package lapr.project.List;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import lapr.project.model.Aircraft;
import lapr.project.model.AircraftModel;
import lapr.project.model.Airport;
import lapr.project.model.FlightPlan;
import lapr.project.model.Segment;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class FligthPlanListTest {

    private FligthPlanList fpl;
    private FlightPlan fp;

    public FligthPlanListTest() {
        
        AircraftModel am = new AircraftModel();
        Airport a = new Airport("a", "name", "town", "country", 1, 1, 1);
        Airport a1 = new Airport("a1", "name", "town", "country", 1, 1, 1);
        Aircraft air = new Aircraft("a", "company", 1, 1, 1, am, 1);
        List<Segment> stop = new ArrayList();
        fp = new FlightPlan(air, a, a1, "nome");
        fpl = new FligthPlanList();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of size method, of class FligthPlanList.
     */
    @Test
    public void testSize() {
        FlightPlan flightPlan = new FlightPlan();
        System.out.println("size");
        FligthPlanList instance = fpl;
        instance.addFlightPlan(fp);
        int expResult = 1;
        int result = instance.size();
        assertEquals(expResult, result);

    }

    /**
     * Test of getFlightPlanList method, of class FligthPlanList.
     */
    @Test
    public void testGetFlightPlanList() {
        System.out.println("getFlightPlanList");
        FligthPlanList instance = fpl;
        List<FlightPlan> expResult = new ArrayList();
        expResult.add(fp);
        List<FlightPlan> result = instance.getFlightPlanList();
        instance.addFlightPlan(fp);
        assertEquals(expResult, result);

    }

    /**
     * Test of newFlightPlan method, of class FligthPlanList.
     */
    @Test
    public void testNewFlightPlan() {
        System.out.println("newFlightPlan");
        FligthPlanList instance = fpl;
        FlightPlan expResult = new FlightPlan();
        FlightPlan result = instance.newFlightPlan();
        assertEquals(expResult, result);

    }

    /**
     * Test of addFlightPlan method, of class FligthPlanList.
     */
    @Test
    public void testAddFlightPlan() {
        System.out.println("addFlightPlan");

        FligthPlanList instance = fpl;
        boolean expResult = true;
        boolean result = instance.addFlightPlan(fp);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeFlightPlan method, of class FligthPlanList.
     */
    @Test
    public void testRemoveFlightPlan() {
        System.out.println("removeFlightPlan");

        FligthPlanList instance = fpl;

        boolean expResult = false;
        instance.removeFlightPlan(fp);
        boolean result = instance.getFlightPlanList().contains(fp);
        assertEquals(expResult, result);

    }

    /**
     * Test of iterator method, of class FligthPlanList.
     */
    @Test
    public void testIterator() {
        System.out.println("iterator");
        FligthPlanList instance = new FligthPlanList();
        Iterator ite = instance.iterator();
        Iterator<FlightPlan> expResult = ite;
        Iterator<FlightPlan> result = ite;
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class FligthPlanList.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        FligthPlanList instance = new FligthPlanList();
        int expResult = 7;
        int result = instance.hashCode();
        assertEquals(expResult, result);

    }

    /**
     * Test of equals method, of class FligthPlanList.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = (FligthPlanList) new FligthPlanList();
        FligthPlanList instance = new FligthPlanList();
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
        FlightPlan f = new FlightPlan();
        instance.addFlightPlan(f);
        expResult = false;
        result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of getFlightplanByName method, of class FligthPlanList.
     */
    @Test
    public void testGetFlightplanByName() {
        System.out.println("getFlightplanByName");
        String name = "";
        FligthPlanList instance = fpl;
        FlightPlan expResult = fp;
        expResult.setName(name);
        instance.addFlightPlan(fp);
        FlightPlan result = instance.getFlightplanByName(name);
        assertEquals(expResult, result);
    }

}

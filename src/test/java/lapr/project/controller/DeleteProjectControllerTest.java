/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.ArrayList;
import java.util.List;
import lapr.project.model.Junction;
import lapr.project.model.Project;
import lapr.project.model.ProjectRegistry;
import lapr.project.model.Segment;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Diogo Guedes <1150613@isep.ipp.pt>
 */
public class DeleteProjectControllerTest {

    private ProjectRegistry pr;
    private Project p;
    private DeleteProjectController op;

    public DeleteProjectControllerTest() {

        pr = new ProjectRegistry();
        p = new Project("nome", "descricao");
        Junction junction = new Junction("name", 1, 1);
        Junction junction1 = new Junction("name1", 1, 1);
        Segment s = new Segment();
        s.setStartNode(junction1);
        s.setEndNode(junction);
        p.addSegment(s);
        pr.addProject(p);
        op = new DeleteProjectController(pr);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getProjectList method, of class DeleteProjectController.
     */
    @Test
    public void testGetProjectList() {
        System.out.println("getProjectList");
        DeleteProjectController instance = op;
        List<String> expResult = new ArrayList();
        expResult.add(p.getName());
        List<String> result = instance.getProjectList();
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteProject method, of class DeleteProjectController.
     */
    @Test
    public void testDeleteProject() {
        System.out.println("deleteProject");
        String name = "nome";
        DeleteProjectController instance = op;
        boolean expResult = true;
        boolean result = instance.deleteProject(name);
        assertEquals(expResult, result);

    }

}

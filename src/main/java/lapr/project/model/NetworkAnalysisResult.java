/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.List;

/**
 *
 *
 */
public class NetworkAnalysisResult implements HTMLexportable {

    /**
     * Id of Analyse
     */
    private int id = 0000;
    private static int cont = 0;

    /**
     * Energy Expenditure
     */
    private double energyExpenditure;

    /**
     * Aircraft
     */
    private Aircraft aircraft;

    /**
     * Travelling time
     */
    private double travellingTime;

    /**
     * Type of results(Fastest, Shortest, LessEnergy)
     */
    private int type;

    /**
     * Constants for type.
     */
    public static final int FASTEST = 0, SHORTEST = 1, LESSENERGY = 2;

    /**
     * Path requested
     */
    private List<Segment> path;

    private List<Double> tasList;

    private List<Double> consumeList;

    private List<Double> compSegment;

    private List<Double> consumeRemanescente;

    private List<Double> altitudeSegment;

    private Double totalDistance;

    /**
     * One if the fuel is sufficient and zero otherwise
     */
    private int results;

    /**
     * Destiny of flight
     */
    private Junction destiny;

    /**
     * Origin of flight
     */
    private Junction Origin;

    private FlightPlan fp;

    public NetworkAnalysisResult() {
        this.id = id + cont;
        cont++;
        this.aircraft = null;
        this.energyExpenditure = 0;
        this.travellingTime = 0;
        this.type = -1;
        this.path = null;
        this.results = -1;
        this.destiny = null;
        this.Origin = null;
        this.fp = null;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the energyExpenditure
     */
    public double getEnergyExpenditure() {
        return energyExpenditure;
    }

    /**
     * @param energyExpenditure the energyExpenditure to set
     */
    public void setEnergyExpenditure(double energyExpenditure) {
        this.energyExpenditure = energyExpenditure;
    }

    /**
     * @return the Aircraft
     */
    public Aircraft getAircraft() {
        return aircraft;
    }

    /**
     * Modify aircraft
     *
     * @param aircraft
     */
    public void setAircraft(Aircraft aircraft) {
        this.aircraft = aircraft;
    }

    /**
     * @return the travellingTime
     */
    public double getTravellingTime() {
        return travellingTime;
    }

    /**
     * @param travellingTime the travellingTime to set
     */
    public void setTravellingTime(double travellingTime) {
        this.travellingTime = travellingTime;
    }

    /**
     * @return the totalDistance
     */
    public Double getTotalDistance() {
        return totalDistance;
    }

    /**
     * @param totalDistance the totalDistance to set
     */
    public void setTotalDistance(Double totalDistance) {
        this.totalDistance = totalDistance;
    }

    /**
     * Sets the type of the results
     *
     * @param type
     */
    public void setType(int type) {
        if (type != FASTEST && type != SHORTEST
                && type != LESSENERGY) {
            throw new IllegalArgumentException("Inputted type is not defined.");
        }

        this.type = type;
    }

    /**
     * @return the path
     */
    public List<Segment> getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(List<Segment> path) {
        this.path = path;
    }

    /**
     * @return the results
     */
    public int getResults() {
        return results;
    }

    /**
     * @return the destiny
     */
    public Junction getDestiny() {
        return destiny;
    }

    /**
     * @param destiny the destiny to set
     */
    public void setDestiny(Junction destiny) {
        this.destiny = destiny;
    }

    /**
     * @return the Origin
     */
    public Junction getOrigin() {
        return Origin;
    }

    /**
     * @param Origin the Origin to set
     */
    public void setOrigin(Junction Origin) {
        this.Origin = Origin;
    }

    /**
     * @return the fp
     */
    public FlightPlan getFp() {
        return fp;
    }

    /**
     * @param fp the fp to set
     */
    public void setFp(FlightPlan fp) {
        this.fp = fp;
    }

    /**
     * Returns the textual description of the object in html format.
     *
     * @return Textual description of the object.
     */
    @Override
    public String toStringHTML() {
        StringBuilder sb = new StringBuilder();

        String type1 = "";
        switch (this.getType()) {
            case 0:
                type1 = "Fastest Path:";
                break;
            case 1:
                type1 = "Shortest Path:";
                break;
            default:
                type1 = "Less Energy Consuption Path:";
                break;
        }

        StringBuilder path1 = new StringBuilder();
        for (Segment s : this.path) {
            path1.append(" ").append(s.getName()).append("; ");
        }

        StringBuilder pathq = new StringBuilder();
        for (int i = 0; i < this.path.size(); i++) {
            Segment s = this.path.get(i);
            pathq.append("<tr>"
                    + "<td>").append("Name segment: ").append("</td\n"
                    + "<td>").append(s.getName()).append("</br\n"
                    + "<td>").append("Initial Node: ").append("</td\n"
                    + "<td>").append(s.getStartNode().getName()).append("</br\n"
                    + "<td>").append("End Node: ").append("</td\n"
                    + "<td>").append(s.getEndNode().getName()).append("</br\n"
                    + "<td>").append("Length of Segment: ").append("</td\n"
                    + "<td>").append(this.compSegment.get(i)).append(" km</br\n"
                    + "<td>").append("Initial TAS: ").append("</td\n"
                    + "<td>").append(this.tasList.get(i)).append("  m/s</br\n"
                    + "<td>").append("Initial altitude: ").append("</td\n"
                    + "<td>").append(this.altitudeSegment.get(i)).append(" m</br\n"
                    + "<td>").append("Final TAS: ").append("</td\n"
                    + "<td>").append(this.tasList.get(i + 1)).append(" m/s</br\n"
                    + "<td>").append("Final altitude: ").append("</td\n"
                    + "<td>").append(this.altitudeSegment.get(i + 1)).append(" m</br\n"
                    + "<td>").append("Spent Fuel: ").append("</td\n"
                    + "<td>").append(this.consumeList.get(i)).append("</td\n"
                    + "<td>").append(" l").append("</br\n"
                    + "<td>").append("Remaining fuel: ").append("</td\n"
                    + "<td>").append(this.consumeRemanescente.get(i)).append(" l</br\n"
                    + "<td>").append("</br>");
        }

        sb.append("<tr>"
                + "<td>").append(type1).append("</td>\n"
                + "<td>").append(path1.toString()).append("</br>"
                + "<td>").append("</br>"
                        + "<td>").append(pathq.toString()).append("</br>"
                + "<td>").append("Aircraft: ").append("</td>\n"
                + "<td>").append(this.aircraft.getRegistration()).append(" s</br\n>"
                + "<td>").append("Travelling time: ").append("</td>\n"
                + "<td>").append(this.travellingTime).append(" s</br\n>"
                + "<td>").append("Energy Expenditure").append("</td>\n"
                + "<td>").append(this.energyExpenditure).append(" l</td>");

        sb.append("</br>\n-------------------------------------------</br\n");

        return sb.toString();
    }

    /**
     * @param results the results to set
     */
    public void setResults(int results) {
        this.results = results;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @return the tasList
     */
    public List<Double> getTasList() {
        return tasList;
    }

    /**
     * @param tasList the tasList to set
     */
    public void setTasList(List<Double> tasList) {
        this.tasList = tasList;
    }

    /**
     * @return the consumeList
     */
    public List<Double> getConsumeList() {
        return consumeList;
    }

    /**
     * @param consumeList the consumeList to set
     */
    public void setConsumeList(List<Double> consumeList) {
        this.consumeList = consumeList;
    }

    /**
     * @return the compSegment
     */
    public List<Double> getCompSegment() {
        return compSegment;
    }

    /**
     * @param compSegment the compSegment to set
     */
    public void setCompSegment(List<Double> compSegment) {
        this.compSegment = compSegment;
    }

    /**
     * @return the consumeRemanescente
     */
    public List<Double> getConsumeRemanescente() {
        return consumeRemanescente;
    }

    /**
     * @param consumeRemanescente the consumeRemanescente to set
     */
    public void setConsumeRemanescente(List<Double> consumeRemanescente) {
        this.consumeRemanescente = consumeRemanescente;
    }

    /**
     * @return the altitudeSegment
     */
    public List<Double> getAltitudeSegment() {
        return altitudeSegment;
    }

    /**
     * @param altitudeSegment the altitudeSegment to set
     */
    public void setAltitudeSegment(List<Double> altitudeSegment) {
        this.altitudeSegment = altitudeSegment;
    }

    @Override
    public String toString() {
        return "NetworkAnalysisResult id=" + id;
    }

}

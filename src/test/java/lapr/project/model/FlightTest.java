/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class FlightTest {

    private final FlightPlan flightPlan=new FlightPlan();
    private final Flight f = new Flight(); 


    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    
 
    /**
     * Test of getType method, of class Flight.
     */
    @Test
    public void testGetAndSetType() {
        System.out.println("getType");
        Flight instance = f;
        String expResult = "tipo";
        instance.setType(expResult);
        String result = instance.getType();
        assertEquals(expResult, result);

    }

    /**
     * Test of getDepartureDay method, of class Flight.
     */
    @Test
    public void testGetAndSetDepartureDay() {
        System.out.println("getDepartureDay");
        Flight instance = f;
        String expResult = "12";
        instance.setDepartureDay(expResult);
        String result = instance.getDepartureDay();
        assertEquals(expResult, result);

    }

    /**
     * Test of getMinStopTime method, of class Flight.
     */
    @Test
    public void testGetAndSetMinStopTime() {
        System.out.println("getMinStopTime");
        Flight instance = f;
        double expResult = 1;
        instance.setMinStopTime(expResult);
        double result = instance.getMinStopTime();
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of getScheduledArrival method, of class Flight.
     */
    @Test
    public void testGetAndSetScheduledArrival() {
        System.out.println("getScheduledArrival");
        Flight instance = f;
        String expResult = "12";
        instance.setScheduledArrival(expResult);
        String result = instance.getScheduledArrival();
        assertEquals(expResult, result);

    }

    /**
     * Test of getFlightPlan method, of class Flight.
     */
    @Test
    public void testGetAndSetFlightPlan() {
        System.out.println("getFlightPlan");
        Flight instance = f;
        FlightPlan expResult = new FlightPlan();
        instance.setFlightPlan(expResult);
        FlightPlan result = instance.getFlightPlan();
        assertEquals(expResult, result);

    }
    
    /**
     * Test of get and set id methods, of class Flight
     */
    
    @Test
    public void testGetAndSetId() {
        
        System.out.println("getAndSet of id in Flight");
        Flight instance = f;
        String expected = "Flight001";
        instance.setName(expected);
        assertEquals(expected,instance.getName());
    }
    
    /**
     * Test of the method toString (overried) of class Flight
     * 
     * return "Flight{" + "id=" + id + ", type=" + type + ", departureDay=" + departureDay + ", minStopTime=" + minStopTime + ", scheduledArrival=" + scheduledArrival + ", flightPlan=" + fp + '}';
     */

    @Test
    public void testToString() {
        
        System.out.println("toString");

        
        String name = "Flight001";
        String type = "Passenger";
        String departureDay = "31/01/2017";
        double minStopTime = 240;
        String scheduledArrival = "01/02/2017";
        FlightPlan instance = flightPlan;
        
        String expected = "Flight{" + "name=" + name + ", type=" + type + ", departureDay=" + departureDay + ", minStopTime=" + minStopTime + ", scheduledArrival=" + scheduledArrival + ", flightPlan=" + instance + '}';
    
        Flight flight = new Flight(name,type,departureDay,minStopTime,scheduledArrival,instance);
        
        String result = flight.toString();
        
        assertEquals(expected,result);
        
        System.out.println(result);
    }
    
    /**
     * Test of the method equals (override) of class Flight
     */
    
    @Test
    public void testEquals() {
        
        System.out.println("equals");
        Flight a = new Flight();
        Flight b = new Flight();
        
        assertTrue(a.equals(b));
        
       String id = "001";
        String type = "Passenger";
        String departureDay = "31/01/2017";
        double minStopTime = 240;
        String scheduledArrival = "01/02/2017";
        FlightPlan instance = flightPlan;
        
        Flight c = new Flight(id,type,departureDay,minStopTime,scheduledArrival,instance);
        
        assertFalse(a.equals(c));
    }

    /**
     * Test of getType method, of class Flight.
     */
    @Test
    public void testGetType() {
        System.out.println("getType");
        Flight instance = new Flight();
        String expResult = "";
        instance.setType(expResult);
        String result = instance.getType();
        assertEquals(expResult, result);
    
    }

    /**
     * Test of setType method, of class Flight.
     */
    @Test
    public void testSetType() {
        System.out.println("setType");
        String type = "";
        Flight instance = new Flight();
        instance.setType(type);
        assertEquals(type,instance.getType());
    }

    /**
     * Test of getDepartureDay method, of class Flight.
     */
    @Test
    public void testGetDepartureDay() {
        System.out.println("getDepartureDay");
        Flight instance = new Flight();
        String expResult = "";
        instance.setDepartureDay(expResult);
        String result = instance.getDepartureDay();
        assertEquals(expResult, result);

    }

    /**
     * Test of setDepartureDay method, of class Flight.
     */
    @Test
    public void testSetDepartureDay() {
        System.out.println("setDepartureDay");
        String departureDay = "";
        Flight instance = new Flight();
        instance.setDepartureDay(departureDay);
        assertEquals(departureDay,instance.getDepartureDay());
    }

    /**
     * Test of getMinStopTime method, of class Flight.
     */
    @Test
    public void testGetMinStopTime() {
        System.out.println("getMinStopTime");
        Flight instance = new Flight();
        double expResult = 0.0;
        double result = instance.getMinStopTime();
        instance.setMinStopTime(expResult);
        assertEquals(expResult, result, 0.0);
     
    }

    /**
     * Test of setMinStopTime method, of class Flight.
     */
    @Test
    public void testSetMinStopTime() {
        System.out.println("setMinStopTime");
        double minStopTime = 0.0;
        Flight instance = new Flight();
        instance.setMinStopTime(minStopTime);
        assertEquals(minStopTime,instance.getMinStopTime(),0.0);
    }

    /**
     * Test of getScheduledArrival method, of class Flight.
     */
    @Test
    public void testGetScheduledArrival() {
        System.out.println("getScheduledArrival");
        Flight instance = new Flight();
        String expResult = "";
        instance.setScheduledArrival(expResult);
        String result = instance.getScheduledArrival();
        assertEquals(expResult, result);

    }

    /**
     * Test of setScheduledArrival method, of class Flight.
     */
    @Test
    public void testSetScheduledArrival() {
        System.out.println("setScheduledArrival");
        String scheduledArrival = "";
        Flight instance = new Flight();
        instance.setScheduledArrival(scheduledArrival);
        assertEquals(scheduledArrival,instance.getScheduledArrival());
    }

    /**
     * Test of getFlightPlan method, of class Flight.
     */
    @Test
    public void testGetFlightPlan() {
        System.out.println("getFlightPlan");
        Flight instance = new Flight();
        FlightPlan expResult = new FlightPlan();
        instance.setFlightPlan(expResult);
        FlightPlan result = instance.getFlightPlan();
        assertEquals(expResult, result);

    }

    /**
     * Test of setFlightPlan method, of class Flight.
     */
    @Test
    public void testSetFlightPlan() {
        System.out.println("setFlightPlan");
        FlightPlan flightPlan = new FlightPlan();
        Flight instance = new Flight();
        instance.setFlightPlan(flightPlan);
        assertEquals(flightPlan,instance.getFlightPlan());
    }

    /**
     * Test of getName method, of class Flight.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Flight instance = new Flight();
        String expResult = "";
        instance.setName(expResult);
        String result = instance.getName();
        assertEquals(expResult, result);

    }

    /**
     * Test of setName method, of class Flight.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "";
        Flight instance = new Flight();
        instance.setName(name);
        assertEquals(name,instance.getName());
    }

    /**
     * Test of hashCode method, of class Flight.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Flight instance = new Flight();
        int expResult = 780215679;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }
}

/*
 * A collection of graph algorithms.
 */
package lapr.project.AdjacencyMap;

import java.util.ArrayList;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 *
 * @author DEI-ESINF
 */
public class GraphAlgorithms {

    /**
     * Performs breadth-first search of a Graph starting in a Vertex
     *
     * @param <V>
     * @param <E>
     * @param g Graph instance
     * @param vert
     * @return qbfs a queue with the vertices of breadth-first search
     */
    public static <V, E> LinkedList<V> BreadthFirstSearch(Graph<V, E> g, V vert) {

        if (!g.validVertex(vert)) {
            return null;
        }

        LinkedList<V> qbfs = new LinkedList<>();
        LinkedList<V> qaux = new LinkedList<>();
        boolean[] visited = new boolean[g.numVertices()];  //default initializ.: false

        qbfs.add(vert);
        qaux.add(vert);
        int vKey = g.getKey(vert);
        visited[vKey] = true;

        while (!qaux.isEmpty()) {
            vert = qaux.remove();
            for (Edge<V, E> edge : g.outgoingEdges(vert)) {
                V vAdj = g.opposite(vert, edge);
                vKey = g.getKey(vAdj);
                if (!visited[vKey]) {
                    qbfs.add(vAdj);
                    qaux.add(vAdj);
                    visited[vKey] = true;
                }
            }
        }
        return qbfs;
    }

    /**
     * Performs depth-first search starting in a Vertex
     *
     * @param g Graph instance
     * @param vOrig Vertex of graph g that will be the source of the search
     * @param visited set of discovered vertices
     * @param qdfs queue with vertices of depth-first search
     */
    private static <V, E> void DepthFirstSearch(Graph<V, E> g, V vOrig, boolean[] visited, LinkedList<V> qdfs) {

        qdfs.add(vOrig);

        visited[g.getKey(vOrig)] = true;
        for (Edge<V, E> e : g.outgoingEdges(vOrig)) {
            if (visited[g.getKey(e.getVDest())] == false) {
                DepthFirstSearch(g, e.getVDest(), visited, qdfs);
            }
        }
    }

    /**
     * @param <V>
     * @param <E>
     * @param g Graph instance
     * @param vert
     * @return qdfs a queue with the vertices of depth-first search
     */
    public static <V, E> LinkedList<V> DepthFirstSearch(Graph<V, E> g, V vert) {

        LinkedList<V> qdfs = new LinkedList<>();

        if (g.validVertex(vert) != false) {
            boolean visited[] = new boolean[g.numVertices()];
            for (int i = 0; i < visited.length; i++) {
                visited[i] = false;
            }
            DepthFirstSearch(g, vert, visited, qdfs);
            return qdfs;
        }
        return null;
    }

    /**
     * Performs depth-first search starting in a Vertex
     *
     * @param g Graph instance
     * @param vOrig Vertex of graph g that will be the source of the search
     * @param visited set of discovered vertices
     * @param qdfs queue with vertices of depth-first search
     */
    private static <V, E> void DepthFirstSearchWithinDistance(Graph<V, E> g, V vOrig, Set<V> qdfs, int cont, int lim) {
        if (cont > lim) {
            return;
        }
        qdfs.add(vOrig);
        cont++;
        for (Edge<V, E> e : g.outgoingEdges(vOrig)) {

            DepthFirstSearchWithinDistance(g, e.getVDest(), qdfs, cont, lim);

        }
    }

    /**
     * @param <V>
     * @param <E>
     * @param g Graph instance
     * @param vert
     * @param lim
     * @return qdfs a queue with the vertices of depth-first search
     */
    public static <V, E> Set<V> DepthFirstSearchWithinDistance(Graph<V, E> g, V vert, int lim) {

        Set<V> qdfs = new HashSet<>();

        if (g.validVertex(vert) != false) {

            int cont = 0;
            DepthFirstSearchWithinDistance(g, vert, qdfs, cont, lim);
            return qdfs;
        }
        return null;
    }

    /**
     * Returns all paths from vOrig to vDest
     *
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param vDest Vertex that will be the end of the path
     * @param visited set of discovered vertices
     * @param path stack with vertices of the current path (the path is in
     * reverse order)
     * @param paths ArrayList with all the paths (in correct order)
     */
    private static <V, E> void allPaths(Graph<V, E> g, V vOrig, V vDest, boolean[] visited,
            LinkedList<V> path, ArrayList<LinkedList<V>> paths) {

        path.push(vOrig);
        int vKey = g.getKey(vOrig);
        visited[vKey] = true;

        for (Edge<V, E> edge : g.outgoingEdges(vOrig)) {
            V vAdj = g.opposite(vOrig, edge);
            if (vAdj == vDest) {
                path.push(vAdj);
                Deque<V> revpath = revPath(path);
                paths.add(new LinkedList(revpath));  //save clone of reverse path
                path.pop();
            } else if (!visited[g.getKey(vAdj)]) {
                allPaths(g, vAdj, vDest, visited, path, paths);
            }
        }
        visited[vKey] = false;
        path.pop();
    }

    /**
     * @param g Graph instance
     * @param voInf information of the Vertex origin
     * @param vdInf information of the Vertex destination
     * @return paths ArrayList with all paths from voInf to vdInf
     */
    public static <V, E> ArrayList<LinkedList<V>> allPaths(Graph<V, E> g, V vOrig, V vDest) {

        LinkedList<V> path = new LinkedList<>();
        ArrayList<LinkedList<V>> paths = new ArrayList<>();
        boolean[] visited = new boolean[g.numVertices()];

        if (g.validVertex(vOrig) && g.validVertex(vDest)) {
            allPaths(g, vOrig, vDest, visited, path, paths);
        }

        return paths;
    }

    /**
     * Computes shortest-path distance from a source vertex to all reachable
     * vertices of a graph g with nonnegative edge weights This implementation
     * uses Dijkstra's algorithm
     *
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param visited set of discovered vertices
     * @param pathkeys minimum path vertices keys
     * @param dist minimum distances
     */
    private static <V, E> void shortestPathLength(Graph<V, E> g, V vOrig, V[] vertices,
            boolean[] visited, int[] pathKeys, double[] dist) {

        int vkeyOrig = g.getKey(vOrig);
        dist[vkeyOrig] = 0;
        while (vkeyOrig != -1) {
            visited[vkeyOrig] = true;
            for (V d : g.vertices()) {
                if (g.getKey(d) == vkeyOrig) {
                    vOrig = d;
                }
            }
            for (Edge<V, E> edge : g.outgoingEdges(vOrig)) {
                V vAdj = g.opposite(vOrig, edge);
                int vkeyAdj = g.getKey(vAdj);
                if (!visited[vkeyAdj] && dist[vkeyAdj] > dist[vkeyOrig] + edge.getWeight()) {
                    dist[vkeyAdj] = dist[vkeyOrig] + edge.getWeight();
                    pathKeys[vkeyAdj] = vkeyOrig;
                }
            }
            double minDist = Double.MAX_VALUE;
            vkeyOrig = -1;
            for (int i = 0; i < g.numVertices(); i++) {
                if (!visited[i] && dist[i] < minDist) {
                    minDist = dist[i];
                    vkeyOrig = i;
                }
            }
            if (minDist == Double.MAX_VALUE) {
                vkeyOrig = -1;
            }
        }
    }

    /**
     * Extracts from pathKeys the minimum path between voInf and vdInf The path
     * is constructed from the end to the beginning
     *
     * @param g Graph instance
     * @param voInf information of the Vertex origin
     * @param vdInf information of the Vertex destination
     * @param pathkeys minimum path vertices keys
     * @param path stack with the minimum path (correct order)
     */
    private static <V, E> void getPath(Graph<V, E> g, V vOrig, V vDest, V[] verts, int[] pathKeys, LinkedList<V> path) {

        if (vOrig != vDest) {
            path.push(vDest);
            V vert = vDest;
            int vKey = g.getKey(vert);
            int prevVKey = pathKeys[vKey];
            for (V vertice : g.vertices()) {
                if (g.getKey(vertice) == prevVKey) {
                    vert = vertice;
                }
            }

            getPath(g, vOrig, vert, verts, pathKeys, path);
        } else {
            path.push(vOrig);
        }
    }

    //shortest-path between voInf and vdInf
    public static <V, E> double shortestPath(Graph<V, E> g, V vOrig, V vDest, LinkedList<V> shortPath) {

        if (g.validVertex(vDest) == false || g.validVertex(vOrig) == false) {
            return 0;
        }
        int nverts = g.numVertices();
        boolean[] visited = new boolean[nverts]; //default value: false
        int[] pathKeys = new int[nverts];
        double[] dist = new double[nverts];

        for (int i = 0; i < nverts; i++) {
            dist[i] = Double.MAX_VALUE;
            pathKeys[i] = -1;
        }
        shortestPathLength(g, vOrig, g.allkeyVerts(), visited, pathKeys, dist);
        double lengthPath = dist[g.getKey(vDest)];
        if (lengthPath != Double.MAX_VALUE) {
            getPath(g, vOrig, vDest, g.allkeyVerts(), pathKeys, shortPath);
            return lengthPath;

        }
        return 0;
    }

    /**
     * Reverses the path
     *
     * @param path stack with path
     */
    private static <V, E> LinkedList<V> revPath(LinkedList<V> path) {

        LinkedList<V> pathcopy = new LinkedList<>(path);
        LinkedList<V> pathrev = new LinkedList<>();

        while (!pathcopy.isEmpty()) {
            pathrev.push(pathcopy.pop());
        }

        return pathrev;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.ArrayList;
import lapr.project.AdjacencyMap.Edge;
import lapr.project.AdjacencyMap.GraphAlgorithms;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import lapr.project.model.Junction;
import lapr.project.model.Segment;

/**
 *
 * @author Pedro
 */
public class NetworkAnalysis {

    private Junction originNode;
    private Junction destinyNode;
    private Project project;
    private int passengers;
    private int crew;
    private double cargoLoad;
    private double fuelLoad;
    private Aircraft aircraft;
    private FlightPlan fp;

    //Flight Pattern atributes
    private ArrayList altitude; //list to store all the values of line altitude of the flight pattern
    private ArrayList Vclimb; //list to store the values of line Vclimb of the flight pattern
    private ArrayList Vdesc; //list to store the value of line Vdesc of the flight pattern

    public NetworkAnalysis() {
    }

    public NetworkAnalysis(Project p, FlightPlan fp) {
        this.project = p;
        this.fp = fp;
        this.originNode = this.findJunction(fp.getOriginAirfield());
        this.destinyNode = this.findJunction(fp.getDesttinyAirfield());
        this.aircraft = fp.getAircraft();
        this.passengers = 0;
        this.cargoLoad = 0;
        this.crew = 0;
        this.fuelLoad = 0;
        this.altitude = new ArrayList();
        this.Vclimb = new ArrayList();
        this.Vdesc = new ArrayList();
    }

    /**
     * Returns the junctions with the same coordinates of the Airport passed.
     *
     * @param air
     * @return j
     */
    public Junction findJunction(Airport air) {
        Junction j = null;
        for (Junction junction : this.project.getJunctionList().getjunctionList()) {
            if (junction.getLatitude() == air.getLatitude() && junction.getLongitude() == air.getLongitude()) {
                j = junction;
            }
        }

        return j;
    }

    /**
     * Modify Aircraft
     *
     * @param aircraft
     */
    public void setAircraft(Aircraft aircraft) {
        this.aircraft = aircraft;
    }

    /**
     * Set the origin node
     *
     * @param originNode
     */
    public void setOri(Junction originNode) {
        this.originNode = originNode;
    }

    /**
     * @return the fp
     */
    public FlightPlan getFp() {
        return fp;
    }

    /**
     * @param fp the fp to set
     */
    public void setFp(FlightPlan fp) {
        this.fp = fp;
    }

    /**
     * Set the destiny node
     *
     * @param destinyNode
     */
    public void setDest(Junction destinyNode) {
        this.destinyNode = destinyNode;
    }

    /**
     * @return the project
     */
    public Project getProject() {
        return project;
    }

    /**
     * @param project the project to set
     */
    public void setProject(Project project) {
        this.project = project;
    }

    /**
     * @return the passengers
     */
    public int getPassengers() {
        return passengers;
    }

    /**
     * @param passengers the passengers to set
     */
    public void setPassengers(int passengers) {
        this.passengers = passengers;
    }

    /**
     * @return the crew
     */
    public int getCrew() {
        return crew;
    }

    /**
     * @param crew the crew to set
     */
    public void setCrew(int crew) {
        this.crew = crew;
    }

    /**
     * @return the cargoLoad
     */
    public double getCargoLoad() {
        return cargoLoad;
    }

    /**
     * @param cargoLoad the cargoLoad to set
     */
    public void setCargoLoad(double cargoLoad) {
        this.cargoLoad = cargoLoad;
    }

    /**
     * @return the fuelLoad
     */
    public double getFuelLoad() {
        return fuelLoad;
    }

    /**
     * @param fuelLoad the fuelLoad to set
     */
    public void setFuelLoad(double fuelLoad) {
        this.fuelLoad = fuelLoad;
    }

    /**
     * Calculate path
     *
     * @param resp(fastestPath=1, shortestPath=0, Less=2 )
     * @return nar
     */
    public NetworkAnalysisResult calculate(int resp) {
        NetworkAnalysisResult na = null;
        switch (resp) {
            case 1:
                na = calculateFastestPath();
                break;
            case 0:
                na = this.calculateShortestPath();
                break;
            case 2:
                na = calculateLessEnergyComsunption();
                break;
            default:
                break;
        }
        return na;
    }

    protected NetworkAnalysisResult calculateFastestPath() {
        List<LinkedList<Junction>> paths = GraphAlgorithms.allPaths(getProject().getGraph(), this.originNode, this.destinyNode);

        double lowest = Double.POSITIVE_INFINITY;
        Converter converter = new Converter();
        Calc c = new Calc();
        LinkedList<Segment> fastestPath = new LinkedList();//Fastest path
        List<Double> tasList = new ArrayList<>();//list of tas by segment
        List<Double> consumeList = new ArrayList<>();//Consume by segment
        List<Double> compSegment = new ArrayList<>();//Lenght of segments
        List<Double> consumeRemanescente = new ArrayList<>();// Remaining fuel
        List<Double> altitudeList = new ArrayList<>();//Altitudes by segment
        double consumo = 0;//Consume of aircraft
        Airport airportOrigin = getFp().getOriginAirfield();
        Airport airportDestiny = getFp().getDesttinyAirfield();
        altitudeList.add(airportOrigin.getAltitude());//Add Initial altitude to list
        double c_drag = this.calculateCdrag();//Return cDrag of aircraft

        double ias = 300;
        double altitude = converter.feetsConverterMeters(aircraft.getAircraftModel().getMotorization().getCruiseAltitude());//Initial altitude
        double fuel = converter.galonsConverterLitres(this.fuelLoad);
        double emptyWeight = (converter.poundConverterGrams(aircraft.getAircraftModel().getEmptyWeight())) / 1e3; //punds to kg
        double cruiseAltitude = converter.feetsConverterMeters(aircraft.getAircraftModel().getMotorization().getCruiseAltitude()); //converts the altitude of cruise mode of miles to meters
        double payLoad = converter.poundConverterGrams(this.cargoLoad) / 1e3; //pounds to kilograms
        double tsfc = aircraft.getAircraftModel().getMotorization().getTSFC(); //SI
        double auxTotalDistance = 0;
        double timeFligt = 0;
        double mtow = converter.poundConverterGrams(aircraft.getAircraftModel().getMaximumTakeOffWeight()) / 1e3; //kg
        double intialWeight = converter.poundConverterGrams(c.calculateInitialWeight(this.cargoLoad, this.fuelLoad, aircraft.getAircraftModel(), passengers, crew)) / 1e3; //kg

        for (LinkedList<Junction> path : paths) {

            LinkedList<Segment> aux = new LinkedList();
            List<Double> auxTasList = new ArrayList<>();
            auxTasList.add(0.0);
            List<Double> auxConsumeList = new ArrayList<>();
            List<Double> auxCompSegment = new ArrayList<>();
            List<Double> auxConsumeRemanescente = new ArrayList<>();
            List<Double> auxAltitudeList = new ArrayList<>();
            double totalDistance = 0;//Total distance
            double time = 0;//Time of path
            double velocity = 0;//velocity of aircraft
            double consume1 = 0;//Final consume

            for (int i = 0; i < (path.size()) - 1; i++) {
                Edge<Junction, Segment> e = getProject().getAirNetwork().getGraph().getEdge(path.get(i), path.get(i + 1));
                Segment s = e.getElement();

                //Calculate  bearing
                //double bearing = c.calculateBearing(s.getStartNode().getLatitude(), s.getStartNode().getLongitude(), s.getEndNode().getLatitude(), s.getEndNode().getLongitude());
                //double windSpeed = s.getWindSpeed();//Calculate wind speed
                double pressure = c.calculateAirPressure(cruiseAltitude);// current airPressure
                double temperature = c.calculateTemperatureAltitude(cruiseAltitude); //current temperature
                double airDensity = c.calculateAirDensity(pressure, temperature); //current air density
                double speedSound = c.calculateSpeedofSound(temperature); //current speed of sound relative to the temperature
                double mTrue = c.calculateTrueMachNumber(airDensity, ias); //mach number of the airplane in these conditions
                double tas = c.calculateTrueAirSpeed(mTrue, speedSound); //true air speed of airplane - metros por segundo
                auxTasList.add(tas);
                //double windAngle = c.calculateWindCorrectionAngle(s.getWindSpeed(), tas, bearing); //Wind correction angle
                ////velocity = c.calculateTrueGroundSpeed(windAngle, bearing, tas, windSpeed, s.getWindAngle()); //real speed of the airplane relative to the wind

                //Calculate distance of segment
                c.CalculateDistance(s.getStartNode().getLatitude(), s.getStartNode().getLongitude(), s.getEndNode().getLatitude(), s.getEndNode().getLongitude());
                double distance = c.getDistance();
                auxCompSegment.add(distance);
                totalDistance += c.getDistance();

                time += (distance * 1000 / tas) / 60 / 24; //hours

                consumo = c.calculateConsume(tas, distance, this.fuelLoad, this.cargoLoad, aircraft.getAircraftModel(), passengers, crew, tsfc, c_drag, pressure, temperature, mTrue, speedSound);
                //consume will be negative
                consumo = consumo * -1;
                consumo += converter.kilogramsConverterLiters(consumo); //consumo em litros
                auxConsumeList.add(consumo);
                fuel = fuel - consumo;
                auxConsumeRemanescente.add(fuel - consumo);

                //double mass = c.calculateMass(aircraft.getAircraftModel().getEmptyWeight(), cargoLoad, fuel); //whole mass of aircraft
                /*double drag = c.CalculateDragForce(airDensity, mass, c_drag, this.aircraft.getAircraftModel().getWingArea(), this.aircraft.getAircraftModel().getWingSpan(), velocity, this.aircraft.getAircraftModel().getE()); //dragForce - atrito
                double dhdt = c.calculateClimbRate(mass, tas, drag, this.aircraft.getAircraftModel().getMotorization().getThurst_0()); //m/s
                
                altitude = c.updateAltitudeAircraft(time, altitude, dhdt);*/
                auxAltitudeList.add(cruiseAltitude);
                aux.add(s);
            }

            if (lowest > time) {
                lowest = time;
                fastestPath = aux;
                tasList = auxTasList;
                tasList.add(0.0);//Not to give null pointer in NetworkAnalysisResults
                consumeList = auxConsumeList;
                compSegment = auxCompSegment;
                consumeRemanescente = auxConsumeRemanescente;
                altitudeList = auxAltitudeList;
                altitudeList.add(0.0);//Not to give null pointer in NetworkAnalysisResults
                auxTotalDistance = totalDistance;
                timeFligt = time;
                consumo = consume1;
            }
        }

        NetworkAnalysisResult nar = new NetworkAnalysisResult();
        nar.setAircraft(getAircraft());
        nar.setEnergyExpenditure(consumo);
        nar.setType(NetworkAnalysisResult.FASTEST);
        nar.setTravellingTime(timeFligt);
        nar.setPath(fastestPath);
        nar.setOrigin(originNode);
        nar.setDestiny(destinyNode);
        nar.setFp(getFp());
        nar.setTasList(tasList);
        nar.setConsumeList(consumeList);
        nar.setCompSegment(compSegment);
        nar.setConsumeRemanescente(consumeRemanescente);
        nar.setAltitudeSegment(altitudeList);
        nar.setTotalDistance(auxTotalDistance);

        if (intialWeight > mtow || consumo > converter.galonsConverterLitres(this.fuelLoad)) {
            nar.setResults(0);
        } else {
            nar.setResults(1);
            this.project.getNetworkAnalysisResultsList().addNetworkAnalysisResult(nar);

        }
        return nar;
    }

    protected NetworkAnalysisResult calculateLessEnergyComsunption() {
        List<LinkedList<Junction>> paths = GraphAlgorithms.allPaths(getProject().getGraph(), this.originNode, this.destinyNode);
        double lowest = Double.POSITIVE_INFINITY; //lowest consume
        LinkedList<Segment> lessPath = new LinkedList();
        Converter converter = new Converter(); //object for the conversions
        Calc c = new Calc(); //object for the calculations
        List<Double> tasList = new ArrayList<>();
        List<Double> consumeList = new ArrayList<>();
        List<Double> compSegment = new ArrayList<>();
        List<Double> consumeRemanescente = new ArrayList<>();
        List<Double> altitudeList = new ArrayList<>();

        Airport airportOrigin = this.getFp().getOriginAirfield();
        Airport airportDestiny = this.getFp().getDesttinyAirfield();
        altitudeList.add(airportOrigin.getAltitude());//Initial altitude
        c.CalculateDistance(airportOrigin.getLatitude(), airportOrigin.getLongitude(), airportDestiny.getLatitude(), airportDestiny.getLongitude());
        //double flightDistance = c.getDistance(); //distance of the flight

        double mtow = converter.poundConverterGrams(aircraft.getAircraftModel().getMaximumTakeOffWeight());
        double initialWeight = converter.poundConverterGrams(c.calculateInitialWeight(this.cargoLoad, this.fuelLoad, aircraft.getAircraftModel(), passengers, crew)) / 1e3; //kg

        //Return cDrag of aircraft
        double c_drag = 0;

        for (Cdrag cd : getAircraft().getAircraftModel().getCDragList().getCdrag()) {
            if (getAircraft().getAircraftModel().getMotorization().getCruiseSpeed() == cd.getSpeed()) {
                c_drag = cd.getCdrag_0();
            }
        }

        double ias = 300;
        //double altitudes = airportOrigin.getAltitude();//Initial altitude
        //double fuel = this.fuelLoad;
        double auxTotalDistance = 0;
        double consume = 0;
        double timeFligt = 0;
        double fuelBurn = 0;
        double tsfc = aircraft.getAircraftModel().getMotorization().getTSFC(); //SI

        for (LinkedList<Junction> path : paths) {
            double bearing = 0;
            LinkedList<Segment> aux = new LinkedList();
            List<Double> auxTasList = new ArrayList<>();
            auxTasList.add(0.0);
            List<Double> auxConsumeList = new ArrayList<>();
            List<Double> auxCompSegment = new ArrayList<>();
            List<Double> auxConsumeRemanescente = new ArrayList<>();
            List<Double> auxAltitudeList = new ArrayList<>();
            double totalDistance = 0;//Total distance
            // double remDistance = flightDistance; //remaining distance to the destination
            double time = 0;
            //double velocity = 0;
            double consume1 = 0; //fuel burn
            double thurst = 0;
            double totalThurst = 0;

            //int mode = 0; //mode of the flight , 0 = climb, 1 = cruise, 2 = descend
            double emptyWeight = (converter.poundConverterGrams(aircraft.getAircraftModel().getEmptyWeight())) / 1e3; //punds to kg
            double cruiseAltitude = converter.feetsConverterMeters(aircraft.getAircraftModel().getMotorization().getCruiseAltitude()); //converts the altitude of cruise mode of miles to meters
            double payLoad = converter.poundConverterGrams(this.cargoLoad) / 1e3; //pounds to kilograms
            double fuelLoaded = converter.galonsConverterLitres(this.fuelLoad); //gallons to liters
            auxAltitudeList.add(cruiseAltitude);

            for (int i = 0; i < (path.size()) - 1; i++) {
                Edge<Junction, Segment> e = getProject().getAirNetwork().getGraph().getEdge(path.get(i), path.get(i + 1));
                Segment s = e.getElement();

                double distance = 0; //distance
                thurst = 0;
                totalThurst = 0;

                double altitude = cruiseAltitude; //never changes altitude in cruise mode

                auxAltitudeList.add(altitude);
                aux.add(s);

                bearing = c.calculateBearing(airportOrigin.getLatitude(), airportOrigin.getLongitude(), airportDestiny.getLatitude(), airportDestiny.getLongitude());//Calculate the bearing of the path

                //calculate the real speed of the aircraft in relation with the wind
                double pressure = c.calculateAirPressure(altitude);// current airPressure
                double temperature = c.calculateTemperatureAltitude(altitude); //current temperature
                double airDensity = c.calculateAirDensity(pressure, temperature); //current air density
                double speedSound = c.calculateSpeedofSound(temperature); //current speed of sound relative to the temperature
                double mTrue = c.calculateTrueMachNumber(airDensity, ias); //mach number of the airplane in these conditions
                double tas = c.calculateTrueAirSpeed(mTrue, speedSound); //true air speed of airplane
                auxTasList.add(tas);
                //double windAngle = c.calculateWindCorrectionAngle(s.getWindSpeed(), tas, bearing); //Wind correction angle
                //velocity = c.calculateTrueGroundSpeed(windAngle, bearing, tas, s.getWindSpeed(), s.getWindAngle()); //real speed of the airplane relative to the wind

                //calculate Mass
                double mass = c.calculateMass(emptyWeight, payLoad, fuelLoaded); //current mass of aircraft

                //Calculate distance
                c.CalculateDistance(s.getStartNode().getLatitude(), s.getStartNode().getLongitude(), s.getEndNode().getLatitude(), s.getEndNode().getLongitude());
                distance = c.getDistance();
                auxCompSegment.add(distance);
                totalDistance += c.getDistance();
                time += (distance * 1000 / tas) / 60 / 24; //hours
                //remDistance-= totalDistance;

                //double drag = c.CalculateDragForce(airDensity, mass, c_drag, this.aircraft.getAircraftModel().getWingArea(), this.aircraft.getAircraftModel().getWingSpan(), velocity, this.aircraft.getAircraftModel().getE()); //dragForce - atrito
                //lambda
                double lambda = c.calculateLAMBDA(tas, aircraft.getAircraftModel().getMotorization().getThurst_max_speed(), aircraft.getAircraftModel().getMotorization().getThurst_0());

                // double dhdt = 0;
                //in cruiseMode dhDT will be always 0, because the airplane will not shift angle to climb or descend
                //in cruiseMode altitude will be constant
                //if (mode != 1) {
                //c.calculateClimbRate(mass, tas, drag, this.aircraft.getAircraftModel().getMotorization().getThurst_0()); //m/s
                //Calculate thurst and totalThurst
                thurst = c.calculateThurstClimb(aircraft.getAircraftModel().getMotorization().getThurst_0(), airDensity, lambda, mTrue, aircraft.getAircraftModel().getMotorization().getLapseRateFactor());
                totalThurst = c.calculateTotalThurst(thurst, aircraft.getAircraftModel().getNumberMotors());
                //the method to calculate thurst will depend on the mode of the flight
                /*switch (mode) {
                    //Climb mode
                    case 0:
                        thurst = 0;
                        thurst = c.calculateThurstClimb(thurst, airDensity, lambda, mTrue, aircraft.getAircraftModel().getMotorization().getLapseRateFactor());
                        totalThurst = c.calculateTotalThurst(thurst, aircraft.getAircraftModel().getNumberMotors());
                        break;
                    //Cruise mode
                    case 1:
                        thurst = 0;
                        thurst = c.calculateThurstCruise(totalThurst, aircraft.getAircraftModel().getNumberMotors());
                        break;
                    //descend mode
                    case 2:
                        thurst = 0;
                        thurst = c.calculateThurstDescend(thurst, airDensity, lambda, mTrue, aircraft.getAircraftModel().getMotorization().getLapseRateFactor());
                        totalThurst = c.calculateTotalThurst(thurst, aircraft.getAircraftModel().getNumberMotors());
                        break;
                }*/

                //Calculate the fuel consumed by the aircraft - fuelBurn
                //negative value
                fuelBurn = c.calculateConsume(tas, distance, this.fuelLoad, this.cargoLoad, aircraft.getAircraftModel(), passengers, crew, tsfc, c_drag, pressure, temperature, mTrue, speedSound);
                fuelBurn = fuelBurn * -1;
                fuelBurn = converter.kilogramsConverterLiters(fuelBurn);
                auxConsumeList.add(fuelBurn);
                consume1 += fuelBurn;

                auxConsumeRemanescente.add(fuelLoad - consume1);

                //update mass of the airplane
                mass = c.updateMassAircraft(mass, fuelBurn);

                /*  
                //if climb mode
                if (mode == 0) {
                    if (dhdt < 0.2 && altitudes == cruiseAltitude) {
                        mode = 1; //cruiseMode   
                    }
                }

                //if cruise mode
                if (mode == 1) {
                    //verificar se falta 3065 metros (comprimento padrão de aterragem) de distancia ate ao aeroporto final e aí sim começa descer
                    //mode = 2; //descend mode
                }

                //if descend mode
                if (mode == 2) {
                    if (altitudes == airportDestiny.getAltitude()) {
                        //chegou ao fim da viagem
                    }
                }
                
                 */
            }

            if (lowest > consume1) {
                lowest = consume1;
                lessPath = aux;
                tasList = auxTasList;
                consumeList = auxConsumeList;
                compSegment = auxCompSegment;
                consumeRemanescente = auxConsumeRemanescente;
                altitudeList = auxAltitudeList;
                auxTotalDistance = totalDistance;
                timeFligt = time;
                consume = consume1;
            }
        }

        NetworkAnalysisResult nar = new NetworkAnalysisResult();
        nar.setAircraft(getAircraft());
        nar.setEnergyExpenditure(consume);
        nar.setType(NetworkAnalysisResult.LESSENERGY);
        nar.setTravellingTime(timeFligt);
        nar.setPath(lessPath);
        nar.setOrigin(originNode);
        nar.setDestiny(destinyNode);
        nar.setFp(getFp());
        nar.setTasList(tasList);
        nar.setConsumeList(consumeList);
        nar.setCompSegment(compSegment);
        nar.setConsumeRemanescente(consumeRemanescente);
        nar.setAltitudeSegment(altitudeList);
        nar.setTotalDistance(auxTotalDistance);

        if (initialWeight > mtow || consume > converter.galonsConverterLitres(this.fuelLoad)) {
            nar.setResults(0);

        } else {
            nar.setResults(1);
            this.project.getNetworkAnalysisResultsList().addNetworkAnalysisResult(nar);

        }

        return nar;

    }

    protected NetworkAnalysisResult calculateShortestPath() {
        List<LinkedList<Junction>> paths = GraphAlgorithms.allPaths(getProject().getGraph(), this.originNode, this.destinyNode);
        double lowest = Double.POSITIVE_INFINITY;
        LinkedList<Segment> shortestPath = new LinkedList();
        Converter converter = new Converter();
        Calc c = new Calc();

        //LinkedList<Segment> fastestPath = new LinkedList();//Fastest path
        List<Double> tasList = new ArrayList<>();//list of tas by segment
        List<Double> consumeList = new ArrayList<>();//Consume by segment
        List<Double> compSegment = new ArrayList<>();//Lenght of segments
        List<Double> consumeRemanescente = new ArrayList<>();// Remaining fuel
        List<Double> altitudeList = new ArrayList<>();//Altitudes by segment

        Airport airportOrigin = getFp().getOriginAirfield();
        Airport airportDestiny = getFp().getDesttinyAirfield();

        altitudeList.add(airportOrigin.getAltitude());//Add Initial altitude to list
        double fuel = converter.galonsConverterLitres(this.fuelLoad);
        double consumo = 0;//Consume of aircraft
        double mtow = converter.poundConverterGrams(aircraft.getAircraftModel().getMaximumTakeOffWeight()) / 1e3; //kg
        double intitalWeight = c.calculateInitialWeight(this.cargoLoad, fuel, aircraft.getAircraftModel(), passengers, crew);
        intitalWeight = converter.poundConverterGrams(intitalWeight) / 1e3; //kg

        //cruise speed of xml file is in miles per second
        //converts to meters per second
        //double cruiseSpeed = converter.feetsConverterMeters(getAircraft().getAircraftModel().getMotorization().getCruiseSpeed()); //m/s
        ///double height = getAircraft().getAircraftModel().getMotorization().getCruiseAltitude();
        double tsfc = aircraft.getAircraftModel().getMotorization().getTSFC(); //SI
        double ias = 300; //indicated airspeed - knots
        double c_drag = this.calculateCdrag();

        double altitude = airportOrigin.getAltitude();//Initial altitude

        double auxTotalDistance = 0;
        double timeFligt = 0;

        for (LinkedList<Junction> path : paths) {
            List<Double> auxTasList = new ArrayList<>();
            auxTasList.add(0.0);
            List<Double> auxConsumeList = new ArrayList<>();
            List<Double> auxCompSegment = new ArrayList<>();
            List<Double> auxConsumeRemanescente = new ArrayList<>();
            List<Double> auxAltitudeList = new ArrayList<>();
            double totalDistance = 0;//Total distance
            double time = 0;//Time of path
            //double velocity = 0;//velocity of aircraft
            double consumo1 = 0;//Final consume            
            LinkedList<Segment> aux = new LinkedList();

            for (int i = 0; i < path.size() - 1; i++) {
                Edge<Junction, Segment> e = getProject().getAirNetwork().getGraph().getEdge(path.get(i), path.get(i + 1));
                Segment s = e.getElement();

                //define altitude
                altitude = converter.feetsConverterMeters(aircraft.getAircraftModel().getMotorization().getCruiseAltitude());

                //Calculate distance of segment
                c.CalculateDistance(s.getStartNode().getLatitude(), s.getStartNode().getLongitude(), s.getEndNode().getLatitude(), s.getEndNode().getLongitude());
                double distance = c.getDistance();
                auxCompSegment.add(distance);
                totalDistance += c.getDistance();

                //Calculate  bearing
                // double bearing = c.calculateBearing(s.getStartNode().getLatitude(), s.getStartNode().getLongitude(), s.getEndNode().getLatitude(), s.getEndNode().getLongitude());
                //double windSpeed = s.getWindSpeed();//Calculate wind speed
                double pressure = c.calculateAirPressure(altitude);// current airPressure
                double temperature = c.calculateTemperatureAltitude(altitude); //current temperature
                double airDensity = c.calculateAirDensity(pressure, temperature); //current air density
                double speedSound = c.calculateSpeedofSound(temperature); //current speed of sound relative to the temperature
                double mTrue = c.calculateTrueMachNumber(airDensity, ias); //mach number of the airplane in these conditions
                double tas = c.calculateTrueAirSpeed(mTrue, speedSound); //true air speed of airplane
                auxTasList.add(tas);
                //double windAngle = c.calculateWindCorrectionAngle(s.getWindSpeed(), tas, bearing); //Wind correction angle
                //velocity = c.calculateTrueGroundSpeed(windAngle, bearing, tas, windSpeed, s.getWindAngle()); //real speed of the airplane relative to the wind

                time += (distance * 1000 / tas) / 60 / 24; //hours

                double consume = c.calculateConsume(tas, distance, fuel, this.cargoLoad, aircraft.getAircraftModel(), passengers, crew, tsfc, c_drag, pressure, temperature, mTrue, speedSound);
                consume = converter.kilogramsConverterLiters(consume);
                auxConsumeList.add(consume);
                consumo1 += consume;
                fuel = fuel + consume;
                auxConsumeRemanescente.add(fuel);

                //double mass = c.calculateMass(emptyWeight, cargoLoad, fuel); //whole mass of aircraft
                //double drag = c.CalculateDragForce(airDensity, mass, c_drag, this.aircraft.getAircraftModel().getWingArea(), this.aircraft.getAircraftModel().getWingSpan(), velocity, this.aircraft.getAircraftModel().getE()); //dragForce - atrito
                // double dhdt = c.calculateClimbRate(mass, tas, drag, this.aircraft.getAircraftModel().getMotorization().getThurst_0()); //m/s
                //altitude = c.updateAltitudeAircraft(time, altitude, dhdt);
                auxAltitudeList.add(altitude);
                aux.add(s);
                //aux.add(s);
            }

            if (lowest > totalDistance) {
                lowest = totalDistance;
                shortestPath = aux;
                tasList = auxTasList;
                tasList.add(0.0);//Not to give null pointer in NetworkAnalysisResults
                consumeList = auxConsumeList;
                compSegment = auxCompSegment;
                consumeRemanescente = auxConsumeRemanescente;
                altitudeList = auxAltitudeList;
                altitudeList.add(0.0);//Not to give null pointer in NetworkAnalysisResults
                auxTotalDistance = totalDistance;
                timeFligt = time;
                consumo = consumo1;
            }
        }

        NetworkAnalysisResult nar = new NetworkAnalysisResult();
        nar.setAircraft(getAircraft());
        nar.setEnergyExpenditure(consumo);
        nar.setType(NetworkAnalysisResult.SHORTEST);
        nar.setTravellingTime(timeFligt);
        nar.setPath(shortestPath);
        nar.setOrigin(originNode);
        nar.setDestiny(destinyNode);
        nar.setFp(getFp());
        nar.setTasList(tasList);
        nar.setConsumeList(consumeList);
        nar.setCompSegment(compSegment);
        nar.setConsumeRemanescente(consumeRemanescente);
        nar.setAltitudeSegment(altitudeList);
        nar.setTotalDistance(auxTotalDistance);

        if (intitalWeight > mtow || consumo > fuel) {
            nar.setResults(0);
        } else {
            nar.setResults(1);
            this.project.getNetworkAnalysisResultsList().addNetworkAnalysisResult(nar);
        }

        return nar;
    }

    /**
     * @return the aircraft
     */
    public Aircraft getAircraft() {
        return aircraft;
    }

    /**
     * @return the altitude
     */
    public ArrayList getAltitude() {
        return altitude;
    }

    /**
     * @param altitude the altitude to set
     */
    public void setAltitude(ArrayList altitude) {
        this.altitude = altitude;
    }

    /**
     * @return the Vclimb
     */
    public ArrayList getVclimb() {
        return Vclimb;
    }

    /**
     * @param Vclimb the Vclimb to set
     */
    public void setVclimb(ArrayList Vclimb) {
        this.Vclimb = Vclimb;
    }

    /**
     * @return the Vdesc
     */
    public ArrayList getVdesc() {
        return Vdesc;
    }

    /**
     * @param Vdesc the Vdesc to set
     */
    public void setVdesc(ArrayList Vdesc) {
        this.Vdesc = Vdesc;
    }

    /**
     * @return cDrag of aircraft
     */
    public double calculateCdrag() {
        double cDrag = 0;
        for (Cdrag c : this.aircraft.getAircraftModel().getCDragList().getCdrag()) {
            if (getAircraft().getAircraftModel().getMotorization().getCruiseSpeed() == c.getSpeed()) {
                cDrag = c.getCdrag_0();

            }
        }
        return cDrag;
    }

}

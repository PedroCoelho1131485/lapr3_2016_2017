package lapr.project.model;

/**
 * Interface implemented by all classes whose responsability is to import the
 * data of a project , from a file format.
 *
 *
 */
public interface Importable {

    /**
     * Imports the data contained in a file into a project
     *
     * @param project
     * @param filePath Path to the file which contains the data.
     * @return True if the import is successfull.
     */
    boolean importNetwork(Project project, String filePath);

    /**
     * Imports the data contained in a file into a project
     *
     * @param project Project
     * @param filePath (String) Path to the file which contains the data.
     * @return True if the import is successfull.
     */
    boolean importAirports(Project project, String filePath);

    /**
     * Imports the data contained in a file into a project
     *
     * @param project Project
     * @param filePath (String) Path to the file which contains the data.
     * @return True if the import is successfull.
     */
    boolean importAircrafts(Project project, String filePath);

    boolean importCSV(String path, FlightPlan na);

}

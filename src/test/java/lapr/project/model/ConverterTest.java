/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro Oliveira
 */
public class ConverterTest {

    public ConverterTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * test of the construtor that receives a name to set on the new object , of class Converter
     */
    
    @Test
    public void testConstrutorConverter() {
        
        System.out.println("Converter Construtor");
        Converter instance = new Converter("");
        String expected = "Converter";
       assertEquals(expected,instance.getName());
       String msg = "Conversion of Physics";
       Converter duplicate = new Converter(msg);
       assertEquals(msg,duplicate.getName());
    }
    
    /**
     * Test of the construtor for class Coverter
     */
    @Test
    public void testDefaultConstrutorConverter() {

        System.out.println("testConstrutorConverter");
        Converter instance = new Converter();

        assertNotNull(instance);
    }

    /**
     * Test of the get and set of the atribute name, of the class Converter
     */
    
    @Test
    public void testGetAndSetName() {
        
        System.out.println("Get and Set Name test");
        Converter instance = new Converter("Name");
        String result = instance.getName();
        assertEquals("Name",result);
    }
        
    /**
     * test of the override method of to String, of class Converter
     */
    @Test
    public void testToString() {
        
        System.out.println("Test of the overrided method toString()");
        Converter instance = new Converter("Conversions");
        String expected = "Name : Conversions";
        String result = instance.toString();
        
        assertEquals(expected,result);
        
        System.out.println(result);
    }
    
    /**
     * test of the override method of hashCode, of class Converter
     */
    
    @Test
    public void testHashCode() {
        
        System.out.println("Test of the overrided method hashCode()");
        int expected = 1;
        Converter instance = new Converter();
        int result = instance.hashCode();
        assertEquals(expected,result,0);
    }
    
    /**
     * test of the overrided method of equals, of class Converter
     */
    
    @Test
    public void testEquals() {
        
        System.out.println("Test of the overrided method equals()");
        Converter instance = new Converter();
        Converter duplicated1 = new Converter(); //object it will be equal to instance if the method works fine
        
        assertTrue(instance.equals(duplicated1));
        
        //to test the case of return false
        //creation of an object different then instance
        
        Converter duplicated2 = new Converter("Peter");
        assertFalse(instance.equals(duplicated2));
        
        Object obj = null;
        assertFalse(instance.equals(obj));
        
        Calc c1 = new Calc();
        
        assertFalse(instance.equals(c1));
    }
    /**
     * Test of TSFConverterSI method, of class Converter.
     */
    @Test
    public void testTSFConverterSI() {
        System.out.println("TSFConverterSI");
        double x = 1;
        Converter instance = new Converter();
        double expResult = 101.972;
        double result = instance.TSFConverterSI(x);
        assertEquals(expResult, result, 0.0);
    }
    
    /**
     * Test of SIConverterTSFC method , of class Converter
     */
    @Test
    public void testSIConverterTSFC() {
        System.out.println("SIConverterTSFC");
        double x = 101.972;
        Converter instance = new Converter();
        double expResult = 1;
        double result = instance.SIConverterTSFC(x);
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of thrustConverterNewton method, of class Converter.
     */
    @Test
    public void testThrustConverterNewton() {
        System.out.println("thrustConverterNewton");
        double x = 1;
        Converter instance = new Converter();
        double expResult = 4.44822162;
        double result = instance.thrustConverterNewton(x);
        assertEquals(expResult, result, 0.0);

    }
    
    /**
     * Test of newtonConverterThurst method, of class Converter
     */
    @Test
    public void testNewtonConverterThurst() {
        
        System.out.println("newtonConverterThurst");
        double x = 4.45;
        Converter instance = new Converter();
        double expResult = 1;
        double result = instance.newtonConverter(x);
        assertEquals(expResult,Math.round(result), 0.0);
        System.out.println("Exact result : " + Double.toString(result));
    }

    /**
     * Test of poundConverterGrams method, of class Converter.
     */
    @Test
    public void testPoundConverterGrams() {
        System.out.println("poundConverterGrams");
        double x = 1;
        Converter instance = new Converter();
        double expResult = 453.59237;
        double result = instance.poundConverterGrams(x);
        assertEquals(expResult, result, 0.0);

    }
    
    /**
     * Test of gramsConverterPound method, of class Converter
     */
    @Test
    public void testGramsConverterPound() {
        System.out.println("gramsConverterPound");
        double x = 453.59;
        Converter instance = new Converter();
        double expResult = 1;
        double result = instance.gramsConverterPound(x);
        assertEquals(expResult,Math.round(result),0.0);
        System.out.println("Exact result : "  + Double.toString(result));
    }

    /**
     * Test of knotConverterMeterPerSecond method, of class Converter.
     */
    @Test
    public void testKnotConverterMeterPerSecond() {
        System.out.println("knotConverterMeterPerSecond");
        double x = 20;
        Converter instance = new Converter();
        double expResult = 10.3;
        double result = instance.knotConverterMeterPerSecond(x);
        assertEquals(expResult, Math.round(result * 10.0) / 10.0, 0.0);
        System.out.println("Final result (exact) : " + Double.toString(result));
    }
    
    /**
     * Test of meterPerSecondConverterKnots
     */
    
    @Test
    public void testMeterPerSecondConverterKnots() {
        
        System.out.println("meterPerSecondConverterKnots");
        double x = 100;
        Converter instance = new Converter();
        double expected = 194.3844;
        double result = instance.meterPerSecondConverterKnots(x);
        
        assertEquals(expected,Math.round(result * 10000.0) / 10000.0,0.0);
        
        System.out.println("Final Result : " + Double.toString(result));
    }

    /**
     * Test of litersConverterKilograms
     *
     */
    @Test
    public void testLitersConverterKilograms() {

        System.out.println("LitersConverterKilograms");
        double x = 1;
        Converter instance = new Converter();
        double expResult = 0.804;
        double result = instance.litersConverterKilograms(x);
        assertEquals(expResult, result, 0.0);
    }
    
    /**
     * Test of kilogramsConverterLiters
     */
    
    @Test
    public void testKilogramsConverterLiters() {
        
        System.out.println("Kilograms Converter Liters");
        double x = 0.804;
        Converter instance = new Converter();
        double expected = 1;
        double result = instance.kilogramsConverterLiters(x);
        assertEquals(expected,result,0.0);
    }

    /**
     * Test of feetsConverterMeters
     */
    @Test
    public void testFeetsConverterMeters() {
        System.out.println("FeetsConverterMeters");
        double x = 1;
        Converter instance = new Converter();
        double expResult = 30.48e-2;
        double result = instance.feetsConverterMeters(x);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of milesConverterMeters
     */
    @Test
    public void testMilesConverterMeters() {
        System.out.println("MilesConverterMeters");
        double x = 1;
        Converter instance = new Converter();
        double expResult = 1.61e3;
        double result = instance.milesConverterMetres(x);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of metersConverterMiles
     */
    @Test
    public void testmetersConverterMiles() {
        System.out.println("metersConverterMiles");
        double x = 500; //m
        Converter instance = new Converter();
        double expResult = 0.3106;
        double result = Math.round(instance.metersConverterMiles(x) * 10000.0) / 10000.0;
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of kilometresConverterMiles
     */
    @Test
    public void testKilometresConverterMiles() {
        System.out.println("kilometersConverterMiles");
        double x = 100;
        Converter instance = new Converter();
        double expResult = 62.1118;
        double result = Math.round(instance.kilometersConverterMiles(x) * 10000.0) / 10000.0;
        assertEquals(expResult, result, 0.0);
    }
    
    /***
     * Test of milesConverterKilometers
     */
    @Test
    public void testMilesConverterKilometers() {
        System.out.println("milesConverterKilometers");
        double x = 62.1118;
        double expResult = 100;
        Converter instance = new Converter();
        double result = instance.milesConverterKilometers(x);
        assertEquals(expResult,Math.round(result),0.0);
        System.out.println("Exact result : " + Double.toString(result));
    }

    /**
     * Test of gallonsConverterLitres 1 US gallon = 3.78541178 litres
     */
    @Test
    public void testGalonsConverterLitres() {
        System.out.println("gallonsConverterLitres");
        double x = 100; //galons
        Converter instance = new Converter();
        //return x * 3.78541178;

        double expResult = 378.541178;

        double result = instance.galonsConverterLitres(x);

        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of litresConverterGalons 1 US gallon = 3.78541178 litres
     */
    @Test
    public void testLitresConverterGalons() {

        System.out.println("litresConverterGalons");
        double x = 100; //litres
        Converter instance = new Converter();

        //return x / 3.78541178;
        double expResult = 26.417;

        double result = instance.litresConverterGalons(x);

        assertEquals(expResult, Math.round(result * 1000.0) / 1000.0, 0.0);

    }
    
    /**
     * Test of machNumberConverterMilesPerHour  1 M = 761,2071 mph
     */
    
    @Test
    public void testMachToMilesPerHour() {
        
        System.out.println("machNumberConverterMilesPerHour");
        double x = 1;
        Converter instance = new Converter();
        double expected = 0.0013;
        double result = instance.machNumberConverterMilesPerHour(x);
        
        assertEquals(expected,Math.round(result * 10000.0) / 10000.0,0.0);
        
        System.out.println("Final Result : " + Double.toString(result));
    }
    
    /**
     * Test of milesPerHourConverterMachNumber
     */
    
    @Test
    public void testMilesPerHourConverterMach() {
        
        System.out.println("milesPerHourConverterMachNumber");
        double x = 1;
        Converter instance = new Converter();
        double expected = 761.2071;
        double result = instance.milesPerHourConverterMachNumber(x);
        
        assertEquals(expected,result,0.0);
        
        System.out.println("Final Result  : " + Double.toString(result));
 
    }
    
    /**
     * Test of machNumberConverterKilometersPerHour , of class Converter
     */
    
    @Test
    public void testMachNumberConverterKilometersPerHour() {
        
        System.out.println("machNumberConverterKiometersPerHour");
        double x = 1;
        Converter instance = new Converter();
        double expected = 1225.044;
        double result = instance.machNumberConverterKilometersPerHour(x);
        
        assertEquals(expected,result,0.0);
        
        System.out.println("Final Result  : " + Double.toString(result));
    }
    
    /**
     * Test of kilometersPerHourConverterMachNumber, of class Converter
     */
    
    @Test
    public void testKilometersPerHourConverterMachNumber() {
        
        System.out.println("kilometersPerHourConverterMachNumber");
        double x = 100;
        Converter instance = new Converter();
        double expected = 0.0816;
        double result = instance.kilometersPerHourConverterMachNumber(x);
        
        assertEquals(expected,Math.round(result * 10000.0) / 10000.0,0.0);
        
        System.out.println("Final Result : " + Double.toString(result));
    }
    
    /**
     * test of the method that converts bar to pascal
     */
    
    @Test
    public void testBarConverterPascal() {
        
        System.out.println("Bar Convert to Pascal");
        Converter instance = new Converter();
        
        double x = 1;
        double expected = 1e5;
        
        double result = instance.barConverterPascal(x);
        
        assertEquals(expected,result,0.0);
    }
    
    /**
     * test of the method that converts pascal to bar
     */
    
    @Test
    public void testPascalConverterBar() {
        
        System.out.println("Pascal Convert to Bar"); //Pressure
        Converter instance = new Converter();
        
        double x = 1e5;
        
        double expected = 1;
        
        double result = instance.pascalConverterBar(x);
        
        assertEquals(expected,result,0.0);
    }
    
    /**
     * test of the method that converts kilopascal to pascal
     */
    
    @Test
    public void testKilopascalConvertPascal () {
        
        System.out.println("Kilopascal Convert to Pascal");
        Converter instance = new Converter();
        
        double x = 1e3;
        
        double expected = 1;
        
        double result = instance.kilopascalConverterPascal(x);
        
        assertEquals(expected,result,0.0);
    }
    
    /**
     * test of the method that converts pascal to kilopascal
     */
    
    @Test
    public void testPascalConverterKilopascal () {
        
        System.out.println("Pascal Convert to Kilopascal");
        Converter instance = new Converter();
        
        double x = 1;
        double expected = 1e3;
        
        double result = instance.pascalConverterKiloPascal(x);
        
       assertEquals(expected,result,0.0);
    }
    
    //// 1 psi = 6894.75 Pascal
    /**
     * test of the method tha converts psi to pascal
     */
    
    @Test
    public void testPSIConverterPascal () {
        
        System.out.println("Pounds per square inch Convert to Pascal");
        Converter instance = new Converter();
        
        double x = 1;
        
        double expected = 6894.75;
        
        double result = instance.psiConverterPascal(x);
        
        assertEquals(expected,result,0.0);
    }
    
    /**
     * ~test of the method that converts pascal to psi
     */
    
    @Test
    public void testPascalConverterPSI () {
        
        System.out.println("Pascal Convert to Pound per square inch");
        Converter instance = new Converter();
        
        double x = 6894.75;
        
        double expected = 1;
        
        double result = instance.pascalConverterPSI(x);
        
        assertEquals(expected,result,0.0);
    }
    
    /**
     * Test of the method that converts km/h to m/s , of class Converter
     */
    
    @Test
    public void testKMperHourConvertMPerSecond() {
        
        System.out.println("kilometersperhourConverterMeterspersecond");
        double x = 72; //km/h
        Converter instance = new Converter();
        double expResult = 20; //rounded in m/s
        double result = instance.kilometersperhourConverterMeterspersecond(x);
        assertEquals(expResult,result,0.0);

    }
    
    /**
     * Test of the method that converts m/s to km/h, of class Converter
     */
    @Test
    public void testMpersecondConverterKMperhour() {
        
        System.out.println("meterspersecondConverterKilometersperhour");
        Converter instance = new Converter();
        double x = 20; // m/s
        double expResult = 72; //km/h
        double result = instance.meterspersecondConverterKilometersperhour(x);
        assertEquals(expResult,result,0.0);
    }
    
    /**
     * Test of the method that converts miles per hour to meters per second
     */
    @Test
    public void testMilesperhourConverterMeterspersecond() {
        
        System.out.println("milesperhourConverterMeterspersecond");
        Converter instance = new Converter();
        double x = 0.0621; //miles = 99.98100000000001 meters
        System.out.println(Double.toString(x) + " miles = " + Double.toString(instance.milesConverterMetres(x)) + "meters");
        double expResult = 359931.6;
        double result = instance.milesperhourConverterMeterspersecond(x);
        assertEquals(expResult,Math.round(result * 10.0) / 10.0,0.0);
        System.out.println("Exact result : " + Double.toString(result));
    }
    
    /**
     * Test of the method that converts meters per second to miles per hour
     */
    @Test
    public void testMeterspersecondConverterMilesperhour() {
        
        System.out.println("meterspersecondConverterMilesperhour");
        Converter instance = new Converter();
        double x = 100;
        System.out.println(Double.toString(x) + "meters = " + Double.toString(instance.metersConverterMiles(x)) + " miles");
        //0.062111801242236024 / 3600 = 1.72e-5
        double expResult = 1.72e-5;
        double result = instance.meterspersecondConverterMilesperhour(x);
        assertEquals(expResult,result ,1.0); //with error margin of 1%
        System.out.println("Exact value : " + Double.toString(result));
    }

    /**
     * Test of newtonConverter method, of class Converter.
     * 
     * return x / 4.44822162;
     */
    @Test
    public void testNewtonConverter() {
        System.out.println("newtonConverter");
        double x = 4.44822162;
        Converter instance = new Converter();
        double expResult = 1;
        double result = instance.newtonConverter(x);
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of milesConverterMetres method, of class Converter.
     * 
     * return x * 1.61e3;
     */
    @Test
    public void testMilesConverterMetres() {
        System.out.println("milesConverterMetres");
        double x = 1;
        Converter instance = new Converter();
        double expResult = 1.61e3;
        double result = instance.milesConverterMetres(x);
        assertEquals(expResult, result, 0.0);
       
    }

    /**
     * Test of kilometersConverterMiles method, of class Converter.
     * 
     *   double m = x / 1.61e3; //x in meters

        return m * 1e3;
     */
    @Test
    public void testKilometersConverterMiles() {
        System.out.println("kilometersConverterMiles");
        double x = 1.61e3;
        Converter instance = new Converter();
        double expResult = 1e3;
        double result = instance.kilometersConverterMiles(x);
        assertEquals(expResult, result, 0.0);
       
    }

    /**
     * Test of metersConverterMiles method, of class Converter.
     * 
     * return x / 1.61e3;
     */
    @Test
    public void testMetersConverterMiles() {
        System.out.println("metersConverterMiles");
        double x = 1.61e3;
        Converter instance = new Converter();
        double expResult = 1;
        double result = instance.metersConverterMiles(x);
        assertEquals(expResult, result, 0.0);
    
    }

    /**
     * Test of machNumberConverterMilesPerHour method, of class Converter.
     * 
     * return x / 761.2071;
     */
    @Test
    public void testMachNumberConverterMilesPerHour() {
        System.out.println("machNumberConverterMilesPerHour");
        double x = 761.2071;
        Converter instance = new Converter();
        double expResult = 1.0;
        double result = instance.machNumberConverterMilesPerHour(x);
        assertEquals(expResult, result, 0.0);
       
    }

    /**
     * Test of milesPerHourConverterMachNumber method, of class Converter.
     * 
     * return x * 761.2071;
     */
    @Test
    public void testMilesPerHourConverterMachNumber() {
        System.out.println("milesPerHourConverterMachNumber");
        double x = 1;
        Converter instance = new Converter();
        double expResult = 761.2071;
        double result = instance.milesPerHourConverterMachNumber(x);
        assertEquals(expResult, result, 0.0);
       
    }

    /**
     * Test of pascalConverterKiloPascal method, of class Converter.
     * 
     * return x * 1e3;
     */
    @Test
    public void testPascalConverterKiloPascal() {
        System.out.println("pascalConverterKiloPascal");
        double x = 1;
        Converter instance = new Converter();
        double expResult = 1e3;
        double result = instance.pascalConverterKiloPascal(x);
        assertEquals(expResult, result, 0.0);
    
    }

    /**
     * Test of kilopascalConverterPascal method, of class Converter.
     * 
     * return x / 1e3;
     */
    @Test
    public void testKilopascalConverterPascal() {
        System.out.println("kilopascalConverterPascal");
        double x = 1e3;
        Converter instance = new Converter();
        double expResult = 1;
        double result = instance.kilopascalConverterPascal(x);
        assertEquals(expResult, result, 0.0);
     
    }

    /**
     * Test of psiConverterPascal method, of class Converter.
     * 
     * return x * 6894.75;
     */
    @Test
    public void testPsiConverterPascal() {
        System.out.println("psiConverterPascal");
        double x = 1;
        Converter instance = new Converter();
        double expResult = 6894.75;
        double result = instance.psiConverterPascal(x);
        assertEquals(expResult, result, 0.0);
       
    }

    /**
     * Test of kilometersperhourConverterMeterspersecond method, of class Converter.
     * 
     * return (x * 1000) / 3600; //m/s
     */
    @Test
    public void testKilometersperhourConverterMeterspersecond() {
        System.out.println("kilometersperhourConverterMeterspersecond");
        double x = 200.0;
        Converter instance = new Converter();
        double expResult = 55.56;
        double result = instance.kilometersperhourConverterMeterspersecond(x);
        assertEquals(expResult, Math.round(result * 100.0) / 100.0, 0.0);
        System.out.println("Result (Exact) : " + Double.toString(result));
    }

    /**
     * Test of meterspersecondConverterKilometersperhour method, of class Converter.
     * 
     * return (x / 1000) * 3600; //km/h
     */
    @Test
    public void testMeterspersecondConverterKilometersperhour() {
        System.out.println("meterspersecondConverterKilometersperhour");
        double x = 55.56;
        Converter instance = new Converter();
        double expResult = 200.0;
        double result = instance.meterspersecondConverterKilometersperhour(x);
        assertEquals(expResult, Math.round(result), 0.0);
        System.out.println("Result (Exact) : " + Double.toString(result));
    }

    /**
     * Test of getName method, of class Converter.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Converter instance = new Converter();
        String expResult = "";
        instance.setName(expResult);
        String result = instance.getName();
        assertEquals(expResult, result);

    }

    /**
     * Test of setName method, of class Converter.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "";
        Converter instance = new Converter();
        instance.setName(name);
        assertEquals(name,instance.getName());
    }

    /**
     * Test of gramsConverterKilograms method, of class Converter.
     */
    @Test
    public void testGramsConverterKilograms() {
        System.out.println("gramsConverterKilograms");
        double x = 1000.0;
        Converter instance = new Converter();
        double expResult = 1.0;
        double result = instance.gramsConverterKilograms(x);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of kilogramsConverterGrams method, of class Converter.
     */
    @Test
    public void testKilogramsConverterGrams() {
        System.out.println("kilogramsConverterGrams");
        double x = 1.0;
        Converter instance = new Converter();
        double expResult = 1e3;
        double result = instance.kilogramsConverterGrams(x);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of feetConverterMiles method, of class Converter.
     */
    @Test
    public void testFeetConverterMiles() {
        System.out.println("feetConverterMiles");
        double x = 100.0;
        Converter instance = new Converter();
        double expResult = 0.02;
        double result = instance.feetConverterMiles(x);
        assertEquals(expResult, Math.round(result * 100.0) / 100.0, 0.0);
        System.out.println("Final result (exact) : " + Double.toString(result));
    }

    /**
     * Test of milesConverterFeet method, of class Converter.
     */
    @Test
    public void testMilesConverterFeet() {
        System.out.println("milesConverterFeet");
        double x = 1.0;
        Converter instance = new Converter();
        double expResult = 5282.15;
        double result = instance.milesConverterFeet(x);
        assertEquals(expResult, Math.round(result * 100.0) / 100.0, 0.0);
        System.out.println("Final result (exact) :  " + Double.toString(result));
    }

    /**
     * Test of radiansConverterDegrees method, of class Converter.
     */
    @Test
    public void testRadiansConverterDegrees() {
        System.out.println("radiansConverterDegrees");
        double rad = Math.PI;
        Converter instance = new Converter();
        double expResult = 180.0;
        double result = instance.radiansConverterDegrees(rad);
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of degreesConverterRadians method, of class Converter.
     */
    @Test
    public void testDegreesConverterRadians() {
        System.out.println("degreesConverterRadians");
        double deg = 180.0;
        Converter instance = new Converter();
        double expResult = Math.PI;
        double result = instance.degreesConverterRadians(deg);
        assertEquals(expResult, result, 0.0);
    
    }

    /**
     * Test of celsiusConverterKelvin method, of class Converter.
     */
    @Test
    public void testCelsiusConverterKelvin() {
        System.out.println("celsiusConverterKelvin");
        double x = 1.0;
        Converter instance = new Converter();
        double expResult = 274.0;
        double result = instance.celsiusConverterKelvin(x);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of kelvinConverterCelsius method, of class Converter.
     */
    @Test
    public void testKelvinConverterCelsius() {
        System.out.println("kelvinConverterCelsius");
        double x = 274.0;
        Converter instance = new Converter();
        double expResult = 1.0;
        double result = instance.kelvinConverterCelsius(x);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of celsiusConverterFahrenheit method, of class Converter.
     */
    @Test
    public void testCelsiusConverterFahrenheit() {
        System.out.println("celsiusConverterFahrenheit");
        double x = 1.0;
        Converter instance = new Converter();
        double expResult = 33.80;
        double result = instance.celsiusConverterFahrenheit(x);
        assertEquals(expResult, result, 0.0);
 
    }

    /**
     * Test of FahrenheitConverterCelsius method, of class Converter.
     */
    @Test
    public void testFahrenheitConverterCelsius() {
        System.out.println("FahrenheitConverterCelsius");
        double x = 33.80;
        Converter instance = new Converter();
        double expResult = 1.0;
        double result = instance.FahrenheitConverterCelsius(x);
        assertEquals(expResult, Math.round(result), 0.0);
        System.out.println("Final Result (Exact) : " + Double.toString(result));
    }

    /**
     * Test of kelvinConverterFahrenheit method, of class Converter.
     */
    @Test
    public void testKelvinConverterFahrenheit() {
        System.out.println("kelvinConverterFahrenheit");
        double k = 1.0;
        Converter instance = new Converter();
        double expResult =-457.87;
        double result = instance.kelvinConverterFahrenheit(k);
        assertEquals(expResult, Math.round(result * 100.0) / 100.0, 0.0);
        System.out.println("Final result (exact) : " + Double.toString(result));
    }

    /**
     * Test of FahrenheitConverterKelvin method, of class Converter.
     */
    @Test
    public void testFahrenheitConverterKelvin() {
        System.out.println("FahrenheitConverterKelvin");
        double f = 1.0;
        Converter instance = new Converter();
        double expResult = 255.93;
        double result = instance.FahrenheitConverterKelvin(f);
        assertEquals(expResult, Math.round(result * 100.0) / 100.0, 0.0);
        System.out.println("Final result (exact) : " + Double.toString(result));
    }

    /**
     * Test for the construtor that has a parameter with a certain condition
     */
    
    @Test
    public void testConstrutor() {
     
        String name = null;
        Converter instance1 = new Converter(name);
        String expResult = "Converter";
        String result = instance1.getName();
        assertEquals(expResult,result);
        name = "Convertions for all";
        expResult = name;
        Converter instance2 = new Converter(name);
        result = instance2.getName();
        assertEquals(expResult,result);
        name = "";
        expResult = "Converter";
        Converter instance3 = new Converter(name);
        result = instance3.getName();
        assertEquals(expResult,result);
    }

}

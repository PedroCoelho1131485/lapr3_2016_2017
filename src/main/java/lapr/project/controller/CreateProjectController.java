/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import lapr.project.model.Importable;
import lapr.project.model.ImportableRegistry;
import lapr.project.model.Project;
import lapr.project.model.ProjectRegistry;

/**
 * Represents an instance of CreateProjectController.
 *
 */
public class CreateProjectController {

    /**
     * Project Handler of project.
     */
    private ProjectRegistry projectHandler;

    /**
     * Project created.
     */
    private Project project;

    private ImportableRegistry imp;

    /**
     * Creates an instance of CreateProjectController without parameters.
     *
     *
     */
    public CreateProjectController() {

    }

    public Project getProject() {
        return this.project;
    }

    /**
     * Creates a new project.
     *
     * @param pr
     */
    public void newProject(ProjectRegistry pr) {
        this.projectHandler = pr;
        this.project = this.projectHandler.newProject();
    }

    /**
     * Insert the name and description in the newly created project.
     *
     * @param name name of project
     * @param description description of project
     */
    public void setData(String name, String description) {
        this.project.setName(name);
        this.project.setDescription(description);

    }

    /**
     * Enter the path where the file is located with the data to import.
     *
     * @param path path to the file location to import
     */
    public void importNetwork(String path) {

        Importable importable = project.getImportableRegistry().getImportableOfType(path);
        importable.importNetwork(this.project, path);

    }

    /**
     * Enter the path where the file is located with the data to import.
     *
     *
     * @param path path to the file location to import
     */
    public void importAircrafts(String path) {

        Importable importable = project.getImportableRegistry().getImportableOfType(path);
        importable.importAircrafts(this.project, path);

    }

    /**
     * Enter the path where the file is located with the data to import.
     *
     *
     * @param path path to the file location to import
     */
    public void importAirports(String path) {

        Importable importable = project.getImportableRegistry().getImportableOfType(path);
        importable.importAirports(this.project, path);

    }

    /**
     * Save project on database.
     */
    public void addProject() {
        this.projectHandler.addProject(this.project);
    }

}


package lapr.project.model;

import java.util.Objects;

/**
 *
 *
 */
public class Aircraft {

    private String registration;
    private String Company;
    private int numberSeatsEconomic;
    private int numberSeatsExecutiv;//aumentar no futuro quando se souber quantos tipos de classe ha
    private int nElemCrew;
    private AircraftModel aircraftModel;
    private int number;

    public Aircraft(String registration, String Company, int numberSeatsExecutiv, int numberSeatsEconomic, int nElemCrew, AircraftModel aircraftModel, int number) {
        this.registration = registration;
        this.Company = Company;
        this.numberSeatsExecutiv = numberSeatsExecutiv;
        this.numberSeatsEconomic = numberSeatsEconomic;
        this.nElemCrew = nElemCrew;
        this.aircraftModel = aircraftModel;
        this.number = number;
        number = 2;

    }

    public Aircraft() {
        aircraftModel = new AircraftModel();
        number = 2;
    }

    /**
     * @return the aircraftModel
     */
    public AircraftModel getAircraftModel() {
        return aircraftModel;
    }

    /**
     * @param aircraftModel the aircraftModel to set
     */
    public void setAircraftModel(AircraftModel aircraftModel) {
        this.aircraftModel = aircraftModel;
    }

    /**
     * @return the registration
     */
    public String getRegistration() {
        return registration;
    }

    /**
     * @param registration the registration to set
     */
    public void setRegistration(String registration) {
        this.registration = registration;
    }

    /**
     * @return the Company
     */
    public String getCompany() {
        return Company;
    }

    /**
     * @param Company the Company to set
     */
    public void setCompany(String Company) {
        this.Company = Company;
    }

    /**
     * @return the numberSeatsExecutiv
     */
    public int getNumberSeatsExecutiv() {
        return numberSeatsExecutiv;
    }

    /**
     * @param numberSeats the numberSeatsExecutiv to set
     */
    public void setNumberSeatsExecutiv(int numberSeats) {
        this.numberSeatsExecutiv = numberSeats;
    }

    /**
     * @return the numberSeatsEconomic
     */
    public int getNumberSeatsEconomic() {
        return numberSeatsEconomic;
    }

    /**
     * @param numberSeats the numberSeatsEconomic to set
     */
    public void setNumberSeatsEconomic(int numberSeats) {
        this.numberSeatsEconomic = numberSeats;
    }

    /**
     * @return the nElemCrew
     */
    public int getnElemCrew() {
        return nElemCrew;
    }

    /**
     * @param nElemCrew the nElemCrew to set
     */
    public void setnElemCrew(int nElemCrew) {
        this.nElemCrew = nElemCrew;
    }

    @Override
    public String toString() {
        return "Registration=" + registration + ", Company=" + Company;

    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public int hashCode() {
        int hash = 7;

        hash = 29 * hash + Objects.hashCode(this.registration);
        hash = 29 * hash + Objects.hashCode(this.Company);
        hash = 29 * hash + this.numberSeatsExecutiv;
        hash = 29 * hash + this.numberSeatsEconomic;
        hash = 29 * hash + this.nElemCrew;
        hash = 29 * hash + Objects.hashCode(this.aircraftModel);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Aircraft other = (Aircraft) obj;
        if (this.numberSeatsEconomic != other.numberSeatsEconomic) {
            return false;
        }
        if (this.numberSeatsExecutiv != other.numberSeatsExecutiv) {
            return false;
        }
        if (this.nElemCrew != other.nElemCrew) {
            return false;
        }

        if (!Objects.equals(this.registration, other.registration)) {
            return false;
        }
        if (!Objects.equals(this.Company, other.Company)) {
            return false;
        }
        if (!Objects.equals(this.aircraftModel, other.aircraftModel)) {
            return false;
        }
        return true;
    }

    /**
     * ValIATAate the aircraft.
     *
     * @return True if aircraft is validate else returns false.
     */
    public boolean validate() {

        if (this.registration == null) {
            throw new IllegalArgumentException("Registration can't be null");
        }

        if (this.Company == null) {
            throw new IllegalArgumentException("company can't be null");
        }

        if (this.aircraftModel == null) {
            throw new IllegalArgumentException("aircraft model can't be null");
        }

        if (this.aircraftModel.validate() == false) {
            return false;
        }

        return true;
    }

}

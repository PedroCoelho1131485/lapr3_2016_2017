/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import lapr.project.model.Project;
import lapr.project.model.ProjectRegistry;
import lapr.project.model.datalayer.DataBaseManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Diogo Guedes <1150613@isep.ipp.pt>
 */
public class CreateProjectControllerTest {

    private Project proj;
    private ProjectRegistry pr;

    public CreateProjectControllerTest() {
        proj = new Project("teste", "13");
        pr = new ProjectRegistry();
    }

    /**
     * Test of getProject method, of class CreateProjectController.
     */
    @Test
    public void testGetProject() {
        System.out.println("getProject");
        CreateProjectController instance = new CreateProjectController();

        instance.newProject(pr);
        Project expResult = new Project();
        Project result = instance.getProject();
        assertEquals(expResult, result);

    }

    /**
     * Test of newProject method, of class CreateProjectController.
     */
    @Test
    public void testNewProject() {
        System.out.println("newProject");

        CreateProjectController instance = new CreateProjectController();
        instance.newProject(pr);
    }

    /**
     * Test of setData method, of class CreateProjectController.
     */
    @Test
    public void testSetData() {
        System.out.println("setData");

        String name = "teste";
        String description = "123";
        CreateProjectController instance = new CreateProjectController();
        instance.newProject(pr);
        instance.setData(name, description);

    }

    /**
     * Test of importNetwork method, of class CreateProjectController.
     */
    @Test
    public void testImportNetwork() {
        System.out.println("importNetwork");
        String path = "Network.xml";

        CreateProjectController instance = new CreateProjectController();
        instance.newProject(pr);
        instance.importNetwork(path);

    }

    /**
     * Test of importAircrafts method, of class CreateProjectController.
     */
    @Test
    public void testImportAircrafts() {
        System.out.println("importAircrafts");
        CreateProjectController instance = new CreateProjectController();
        instance.newProject(pr);
        String path = "Network.xml";
        instance.importNetwork(path);
        instance.importAircrafts("Aircraft.xml");

    }

    /**
     * Test of importAirports method, of class CreateProjectController.
     */
    @Test
    public void testImportAirports() {
        System.out.println("importAirports");
        CreateProjectController instance = new CreateProjectController();
        String path = "Network.xml";

        instance.newProject(pr);
        instance.importNetwork(path);
        instance.importAirports("Airports.xml");

    }

    /**
     * Test of addProject method, of class CreateProjectController.
     */
    @Test
    public void testAddProject() {
        System.out.println("addProject");
        CreateProjectController instance = new CreateProjectController();
        instance.newProject(pr);
        instance.setData("sa", "seq");
        instance.importNetwork("Network.xml");
        instance.importAirports("Airports.xml");
        instance.importAircrafts("Aircraft.xml");
        DataBaseManager.getInstance().getProjectData().insertProject(instance.getProject());

    }

}

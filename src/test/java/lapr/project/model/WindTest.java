/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author Diogo Guedes <1150613@isep.ipp.pt>
 */
public class WindTest {

    private final Wind wind;

    public WindTest() {
        this.wind = new Wind();
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of getAndSetWindAngle methods, of class Wind.
     */
    @Test
    public void testGetAndSetWindAngle() {
        System.out.println("getAndSetWindAngle");
        Wind instance = wind;
        double expResult = 13.2;
        instance.setWindAngle(expResult);
        double result = instance.getWindAngle();
        assertEquals(expResult, result,0.0);
    }

    /**
     * Test of getAndSetWindSpeed methods, of class Wind.
     */
    @Test
    public void testGetAndSetWindSpeed() {
        System.out.println("getAndSetWindSpeed");
        Wind instance = wind;
        double expResult = 13.2;
        instance.setWindSpeed(expResult);
        double result = instance.getWindSpeed();
        assertEquals(expResult, result,0.0);
    }
    
    /**
     * Test of the method equals (override) of class Wind
     */
    
    @Test
    public void testEquals() {
        System.out.println("equals");
        
        Wind a = new Wind();
        Wind b = new Wind();
        
        assertTrue(a.equals(b));
        
        String id = "001";
        double windSpeed = 48;
        double windAngle = 20;
        String windName= "Name";
        
        Wind c = new Wind(windAngle,windSpeed,windName);
        
        assertFalse(a.equals(c));
    }
    
    /**
     * Test of the method toString (override) of class Wind
     */

    @Test
    public void testToString() {
        
        System.out.println("toString");
        
        double windSpeed = 48;
        double windAngle = 20;
        String windName= "Name";
        
        Wind w = new Wind();
        w.setWindName(windName);
        w.setWindSpeed(windSpeed);
        w.setWindAngle(windAngle);
        
        
        String expected =  "Wind Angle: " + windAngle
                + "\nWind Speed: " + windSpeed + " m/s"
                + "\nWind Name: " + windName;
        
        String result = w.toString();
        
        assertEquals(expected,result);
        
        System.out.println(result);
    }

    /**
     * Test of getWindName method, of class Wind.
     */
    @Test
    public void testGetWindName() {
        System.out.println("getWindName");
        Wind instance = new Wind();
        String expResult = "";
        instance.setWindName(expResult);
        String result = instance.getWindName();
        assertEquals(expResult, result);

    }

    /**
     * Test of setWindName method, of class Wind.
     */
    @Test
    public void testSetWindName() {
        System.out.println("setWindName");
        String windName = "";
        Wind instance = new Wind();
        instance.setWindName(windName);
        assertEquals(windName,instance.getWindName());
    }

    /**
     * Test of getWindAngle method, of class Wind.
     */
    @Test
    public void testGetWindAngle() {
        System.out.println("getWindAngle");
        Wind instance = new Wind();
        double expResult = 0.0;
        instance.setWindAngle(expResult);
        double result = instance.getWindAngle();
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of setWindAngle method, of class Wind.
     */
    @Test
    public void testSetWindAngle() {
        System.out.println("setWindAngle");
        double windAngle = 0.0;
        Wind instance = new Wind();
        instance.setWindAngle(windAngle);
        assertEquals(windAngle,instance.getWindAngle(),0.0);
    }

    /**
     * Test of getWindSpeed method, of class Wind.
     */
    @Test
    public void testGetWindSpeed() {
        System.out.println("getWindSpeed");
        Wind instance = new Wind();
        double expResult = 0.0;
        instance.setWindSpeed(expResult);
        double result = instance.getWindSpeed();
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of setWindSpeed method, of class Wind.
     */
    @Test
    public void testSetWindSpeed() {
        System.out.println("setWindSpeed");
        double windSpeed = 0.0;
        Wind instance = new Wind();
        instance.setWindSpeed(windSpeed);
        assertEquals(windSpeed,instance.getWindSpeed(),0.0);
    }

    /**
     * Test of hashCode method, of class Wind.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Wind instance = new Wind();
        int expResult = 2645;
        int result = instance.hashCode();
        assertEquals(expResult, result);

    }

    /**
     * Test of validate method, of class Wind.
     */
    @Test
    public void testValidate() {
        System.out.println("validate");
        Wind instance = new Wind();
        boolean expResult = true;
        boolean result = instance.validate();
        assertEquals(expResult, result);

    }
}

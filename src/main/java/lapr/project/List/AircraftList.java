package lapr.project.List;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import lapr.project.model.Aircraft;
import lapr.project.model.Airport;

/**
 *
 * @author Diogo Guedes <1150613@isep.ipp.pt>
 */
public class AircraftList {

    private List<Aircraft> aircraftList;

    public AircraftList() {
        aircraftList = new ArrayList<>();
    }

    /**
     * Creates a clone of the target vehiclelist
     *
     * @param source The target vehicle list to clone.
     */
    public AircraftList(AircraftList source) {
        aircraftList = new ArrayList(source.getAircraftList());
        Collections.copy(aircraftList, source.aircraftList);
    }

    /**
     * Return the number of aircrafts.
     *
     * @return
     */
    public int size() {
        return this.getAircraftList().size();
    }

    /**
     * Return list of aircrafts
     *
     * @return aircraftList
     */
    public List<Aircraft> getAircraftList() {
        return this.aircraftList;
    }

    /**
     * Creates a new Aircraft.
     *
     * @return New Aircraft.
     */
    public Airport newAirport() {
        return new Airport();
    }

    /**
     * Add an Aircraft to the Aircrafts list.
     *
     * @param aircraft aircraft to be added.
     * @return True if the Aircraft was added , otherwise returns false.
     */
    public boolean addAircraft(Aircraft aircraft) {
        for (Aircraft aircraft1 : getAircraftList()) {
            if (aircraft1.getRegistration().equals(aircraft.getRegistration())) {
                int number = aircraft1.getNumber();
                aircraft.setRegistration(aircraft.getRegistration() + number);
                aircraft.setNumber(number++);
                aircraft1.setNumber(number++);
            }
        }

        return this.getAircraftList().add(aircraft);

    }

    /**
     * Remove the Aircraft received by parameter Aircraft list
     *
     * @param Aircraft Aircraft to be removed.
     * @return True if the Aircraft was added , otherwise returns false.
     */
    public boolean removeAirport(Aircraft Aircraft) {
        return this.getAircraftList().remove(Aircraft);
    }

    /**
     * Returns an iterator to allow running through the list of Aircrafts.
     *
     * @return Iterator.
     */
    public Iterator<Aircraft> iterator() {
        return this.getAircraftList().iterator();
    }

    public Aircraft getAircraft(String name) {

        for (Aircraft air : this.getAircraftList()) {
            if (name.equalsIgnoreCase(air.getRegistration())) {
                return air;

            }
        }
        return null;
    }

    /**
     * @param aircraftList the aircraftList to set
     */
    public void setAircraftList(List<Aircraft> aircraftList) {
        this.aircraftList = aircraftList;
    }

}

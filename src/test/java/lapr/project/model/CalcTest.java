package lapr.project.model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;

/*
 *
 * @author Pedro
 */
public class CalcTest {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {
    }

    //unit test for the construtor of the class Calc
    @Test
    public void testConstructor() {

        Calc c1 = new Calc();

    }

    //unit tests for each get and set
    //Lift force
    @Test
    public void testAtributeLiftForce() {

        Calc c1 = new Calc();

        double value = 1000.50;

        c1.setLiftForce(value);

        assertNotNull(c1.getLiftForce());
        assertEquals(value, c1.getLiftForce(), 0.0);
    }

    //Drag force
    @Test
    public void testAtributeDragForce() {

        Calc c = new Calc();

        double value = 500.10;

        c.setDragForce(value);

        assertNotNull(c.getDragForce());
        assertEquals(value, c.getDragForce(), 0.0);
    }

    //Range
    @Test
    public void testAtributeRange() {

        Calc c1 = new Calc();

        double value = 100.00;

        c1.setRangeAircraft(value);

        assertNotNull(c1.getRangeAircraft());
        assertEquals(value, c1.getRangeAircraft(), 0.0);

    }

    //Distance
    @Test
    public void testAtributeDistance() {

        Calc c1 = new Calc();

        double distance = 277.1;

        c1.setDistance(distance);

        assertNotNull(c1.getDistance());

        assertEquals(distance, c1.getDistance(), 0.0);
    }

    /**
     * Test of Calculate Distance Method
     *
     * Using already given data from a xml file (airport.xml)
     *
     * Of the first airport
     */
    @Test
    public void TestCalculateDistance() {

        /**
         * Calculations made by hand using the following information :
         *
         * airport1 - Francisco Sá Carneiro Porto:
         * <latitude>41.2481003</latitude>
         * <longitude>-8.6813898</longitude>
         *
         * airport 2 - Humberto Delgado Lisboa :
         * <latitude>38.7812996</latitude>
         * <longitude>-9.1359196</longitude>
         *
         * using the following formula in excel
         * =6371*ACOS(COS(PI()*(90-D3)/180)*COS((90-D2)*PI()/180)+SEN((90-D3)*PI()/180)*SEN((90-D2)*PI()/180)*COS((E2-E3)*PI()/180))
         *
         * expresult = 277,012226 round expresult = 277,01
         */
        Calc c1 = new Calc();
        Airport a1 = new Airport("1", "Francisco Sá Carneiro", "Porto", "Portugal", 41.2481003, -8.6813898, 69.0);
        Airport a2 = new Airport("2", "Humberto Delgado", "Lisboa", "Portugal", 38.7812996, -9.1359196, 114.0);

        System.out.println(a1.getLatitude() + "| " + a1.getLongitude());
        System.out.println(a2.getLatitude() + "| " + a2.getLongitude());
        double expected = 277.01;
        c1.CalculateDistance(a1.getLatitude(), a1.getLongitude(), a2.getLatitude(), a2.getLongitude());

        assertEquals(expected, Math.round(c1.getDistance() * 100.00) / 100.00, 0.0);
        System.out.println(Double.toString(Math.round(c1.getDistance() * 100.00) / 100.00));
    }

    //unit test of the method that calculates the coefficent of lift
    @Test
    public void testCalculateLiftCoefficent() {

        Calc c1 = new Calc();

        double expected = 13.1491;

        assertEquals(expected, Math.round(c1.calculateLiftCoefficent(16146.65, 0.24, 512, 14) * 10000.00) / 10000.00, 0.0);
    }

    //unit test of the method that calculates the mass of an aircraft
    /**
     * Empty Weight = 349.09e3 pounds = 1.58e5 kg
     *
     * G = 9.8
     *
     * p = m * g (=) m = p / g
     *
     * m = 1.58e5 g / 9.80665 = 1.61e4
     */
    @Test
    public void testCalculateMass() {

        AircraftModel a1 = new AircraftModel();
        Calc c1 = new Calc();

        double w = 158344.5;
        double fuel = 2000;
        double payload = 1000;
        double expected = 161344.5;
        
        double result = c1.calculateMass(w, payload, fuel);
        
        assertEquals(expected,result,0.0);
        
        System.out.println("Final Result : " + Double.toString(result));

    }

    //unit test of the method that calculates the airDensity
    /**
     * constantes gradiente adiabatico k/m L = 0.0065 k/m massa Molar do ar seco
     * M = 0.0289655 kg/mol temperatura ao nivel do mar K T0 = 288.15 k pressao
     * atmosferica ao nivel do mar P0 = 101.325 kPa constante do gas ideial (ar
     * seco) J/(mol . K) R = 8.31447
     *
     * height = 14km
     *
     * numerator denominator (p0((1-(Lh/T0))^(gM/RL))*M) / (R*(T0-Lh))
     *
     *
     */
    @Test
    public void testCalculateAirDensity() {

        Calc c1 = new Calc();

        double temperature = 288.2;
        double pressure = 101325;
        double expected = 1.2250;
        
        double result = c1.calculateAirDensity(pressure, temperature);

        assertEquals(expected,Math.round(result * 10000.0) / 10000.0,0.0);
        
        System.out.println("Final Result :" + Double.toString(result));
       
    }

    

    //unity test for the method that calculates the drag coefficent
    @Test
    public void testCalculateDragCoefficent() {

        Calc c1 = new Calc();

        double mass = 16146.65;
        double wingArea = 512;
        double e = 0.95;
        double wingSpan = 64.8;
        double airDensity = 0.24;
        double cdrag0 = 0.025;
        double speed = 14;
        double expected = 7.0888;

        assertEquals(expected, Math.round(c1.calculateDragCoefficent(mass, airDensity, speed, cdrag0, wingSpan, wingArea, e) * 10000.0) / 10000.0, 0.0);

    }

    //unit test for the method that calculates the drag force of an aircraft
    @Test
    public void testCalculateDragForce() {

        Calc c1 = new Calc();

        double speed = 14;
        double wingArea = 512;
        double wingSpan = 64.8;
        double e = 0.95;
        double mass = 16146.65;
        double cdrag0 = 0.025;
        double airDensity = 0.2435;

        double expected = 84147.0;
        double realResult = c1.CalculateDragForce(airDensity, mass, cdrag0, wingArea, wingSpan, speed, e);
        //(double densityAir, double mass, double cdrag0, double wingArea, double wingSpan, double speed ,double e)
        assertEquals(expected, Math.round(realResult), 0.0);
        System.out.println(Double.toString(realResult));
    }

    //unit test for the method that will calculate the final weight of an aircraft
    /*calculateFinalWeight(AircraftModel a, int passangers, int crew)*/
    @Test
    public void testCalculateInitialWeight() {

        Calc c1 = new Calc();
        AircraftModel a1 = new AircraftModel();

        Aircraft a = new Aircraft();
        a.setAircraftModel(a1);

        int passengers = 60;
        int crew = 20;

        a.getAircraftModel().setEmptyWeight(158344.5);
        double payload = 2000;
        double fuel = 3000;

        double expected = 169345;
        //double payload, double fuel,AircraftModel a, int passangers, int crew)
        double finalResult = c1.calculateInitialWeight(payload, fuel, a.getAircraftModel(), passengers, crew);

        assertEquals(expected, Math.round(finalResult), 0.0);

    }

    /**
     * Test of the method that calculates the range of an aircraft, of class Calc
     */
    
    @Test
    public void testCalculateRangeAircraft() {
        
        System.out.println("Calculate Range of an aircraft");
        
        Calc c1 = new Calc();
        
        double tas = 14;
        double tsfc = 0.56;
        
        double L = 500; //lift force
        
        double D = 250; //drag force
        
        double wi = 1500; //initial weight
        
        double wf = 1300; //final weight
        
        double expected = 7.155; //expected result of range rounded
        
        double result = c1.calculateRangeAircraft(tas, tsfc, L, D, wi, wf); //final and real result of the method , exact number
        
        assertEquals(expected, Math.round(result * 1000.0) / 1000.0 //round the exact result to the same decimail power as the expected result
        ,0.0);
        
        //Show the real result of the method, exact number
        
        System.out.println("Range of the aircraft (Exact) : " + Double.toString(result));
    }

    //unit test for the method that calculates the consume of an aircraft in a certain flight
    /*@Test
    public void testCalculateConsume() {

        Calc c1 = new Calc();
        Aircraft a = new Aircraft();
        AircraftModel a1 = new AircraftModel();
        a.setAircraftModel(a1);

        //calculateConsume(double distance, double speed, double height, double fuel, double payload, 
        //AircraftModel a, int passangers, int crew, double tsfc)
        double speed = 14;
        int passangers = 60;
        int crew = 20;
        double tsfc = 0.564;
        double payload = 2000;
        double fuel = 3000;
        
        double pressure = 101325;
        double temperature = 288.2;

        a.getAircraftModel().getMotorization().setCruiseSpeed(speed);
        a.getAircraftModel().setE(0.95);


        a.getAircraftModel().setEmptyWeight(158344.5);
        System.out.println("Initial Weight : " + Double.toString(c1.calculateInitialWeight(payload, fuel, a1, passangers, crew)));

        double cdrag_0 = 0.025;
        a.getAircraftModel().setWingArea(512);
        a.getAircraftModel().setWingSpan(64.8);
        double tas = 14;
        //System.out.println("Lift Force : " + Double.toString(c1.CalculateLiftForce(a1,payload,fuel)));
        //System.out.println("Drag Force : " + Double.toString(c1.CalculateDragForce(c1.calculateAirDensity(height), c1.calculateMass(a.getAircraftModel().getEmptyWeight(),payload,fuel), 0.025, 512, 64.8, 14, 0.95)));
        double distance = 277;

        double finalResult = c1.calculateConsume(tas,distance, fuel, payload, a1, passangers, crew, tsfc, cdrag_0,pressure,temperature);

        System.out.println("Final Weight : " + Double.toString(finalResult + c1.calculateInitialWeight(payload, fuel, a1, passangers, crew)));

        finalResult = Math.round(finalResult * 10.0) / 10.0;
        System.out.println("Final Result : " + Double.toString(finalResult));
        double expected = -169344.5;

        assertEquals(expected, finalResult, 0.0);
        
        System.out.println("Final Result : " + Double.toString(finalResult));
    }*/



    /**
     * test of the method that calculate the crosswind , of class Calc
     */
    @Test
    public void testCalculateCrossWind() {

        System.out.println("Calculate CrossWind");
        Calc c1 = new Calc();
        double angle = 30; //Degrees

        double windSpeed = 15; //knots

        double expected = 7.5; //knots

        double result = c1.calculateCrossWind(windSpeed, angle);

        assertEquals(expected, Math.round(result * 10.0) / 10.0, 0.0);

        System.out.println("Final Result : " + Double.toString(result));

    }

    /**
     * Test of the method that calculates the headwind or the tailwind, of class
     * Calc
     */
    @Test
    public void testCalculateHeadWind() {

        System.out.println("Calculate Head Wind");
        Calc c1 = new Calc();

        double windspeed = 15; //knots

        double angle = 30; //degrees

        double expected = 13; // knots

        double result = c1.calculateHeadWind(windspeed, angle);

        assertEquals(expected, Math.round(result), 0.0);

        System.out.println("Final Result : " + Double.toString(result));

    }
    
    /**
     * 
     * test of the method that calculates the (dh / dt) , of class Calc
     * 
     * =(totalThurst-drag)*tas/mass/g
     */
    
    @Test
    public void testCalculateDhDT () {
        
        System.out.println("Calculate dh / dt");
        Calc c1 = new Calc();
        
        double tas = 14;
        double totalThurst = 320;
        
        double mass = 500;
       
        double drag = 250;
        
        double expected = 19.221; //rounded
       
       double result = c1.calculateDhDT(totalThurst, drag, tas, mass);
       
       assertEquals(expected, Math.round(result * 1000.0) / 1000.0,0.0);
       
       System.out.println("Final result : " + Double.toString(result));
    }
    
    /**
     * test of the method that calculates the (dw / dT) , of class Calc
     */
    
    @Test
    public void testCalculateDwDt() {
        
        System.out.println("Calculate DW / DT");
        Calc c1 = new Calc();
        
        //return (totalThurst * timestep) * (tsfc * G); 
        
        double timestep = 120;
        double totalThurst = 1.13e06;
        double tsfc = 0.56;
        
        double expected = 744677774;
        
        double result = c1.calculateDwDt(totalThurst,timestep,tsfc);
        
        assertEquals(expected,Math.round(result),0.0);
        
        System.out.println("Final result : " + Double.toString(result));
    }
    

    /**
     * Test for the method that calculates the LAMBDA , of class Calc
     */
    
    @Test
    public void testCalculateLAMBDA() {
        
        System.out.println("Calculate LAMBDA");
        Calc c1 = new Calc();
        
        //return (thurstMach - thurstNewtons) / thurst_0_Newtons;
        
        double thurstMach = 0.9; //M
        double thurst_0_newton = 3.38e5;
        double thurstNewtons = 1.80e5;
        
        double expected = 175556;
        
        double result = c1.calculateLAMBDA(thurstMach,thurstNewtons,thurst_0_newton);
        
        assertEquals(expected,Math.round(result),0.0);
        
        System.out.println("Final Result : " + Double.toString(result));
    }

    /**
     * Test for the method that calculates a load factor
     */
    @Test
    public void testCalculateLoadFactor() {
        System.out.println("Calculate Load Factor\n");
        Calc c1 = new Calc();
        //calculateLoadFactor(double liftForce,double weight)
        double weight = 169344.5;
        double liftForce = 169344.5;

        //so load factor will be 1
        double expected = 1;
        double finalResult = c1.calculateLoadFactor(liftForce, weight);

        assertEquals(expected, finalResult, 0.0);
        System.out.println("Final Result : " + Double.toString(finalResult));
    }

    /**
     * Test for the method that calculates the stall speed of a certain aircraft
     */
    @Test
    public void testCalculateStallSpeed() {

        System.out.println("Calculate Stall Speed");
        Calc c1 = new Calc();

        //calculateStallSpeed(double weight,double wingArea, double maxCL,double airDensity)
        double weight = 169344.5;
        System.out.println("Result of mass : " + Double.toString(17268.33322) + "\nWeight / G \n");

        double wingArea = 512; //m ^2
        double maxCL = 13.1491;
        double fuel = 2000;
        double payload = 1000;
        double airDensity = 0.2436;

        double expected = 45.4;

        double finalResult = c1.calculateStallSpeed(weight, wingArea, maxCL, airDensity,payload,fuel);

        assertEquals(expected, Math.round(finalResult * 10.0) / 10.0, 0.0);

        System.out.println("Final Result : " + Double.toString(finalResult));
    }

    /**
     * test to the method that calculates the amount of fuel carried
     */
    @Test
    public void testCalculateTripFuel() {

        System.out.println("Calcuation of the amount of fuel for the trip");
        Calc c1 = new Calc();

        int crew = 20;
        int passengers = 80;

        double initialWeight = 169344.5;
        double emptyWeight = 158344.5;

        double payload = 2000;

        /*calculateTripFuel(double initialWeight,double payload, int crew, int passengers, double emptyWeight)
        
        return initialWeight - emptyWeight - (payload + (crew * PesoHumano) + (passengers * PesoHumano));
         */
        double finalResult = c1.calculateTripFuel(initialWeight, payload, crew, passengers, emptyWeight);
        double expected = 1500.0;

        assertEquals(expected, finalResult, 0.0);

        System.out.println("Final Result : " + Double.toString(finalResult));
    }

    /**
     * Test to the method that calculates the flight angle of an aircraft
     */
    @Test
    public void testCalculateFlightAngle() {

        System.out.println("Calculate Flight Angle");
        Calc c1 = new Calc();

        double thurst = 17382; // newtons / meter
        double mass = 16146.65; //kg
        double drag = 16200; // Newtons

        double expected = 0.428;

        double finalResult = c1.calculateFlightAngle(thurst, drag, mass);

        assertEquals(expected, Math.round(finalResult * 1000.0) / 1000.0, 0.0);

        System.out.println("Final Result : " + Double.toString(finalResult));

    }

  
    /**
     * test of the method that calculates the aspect ratio of an aircraft
     */
    @Test
    public void testCalculateAspectRatio() {

        System.out.println("Calculate Aspect Ratio");
        Calc c1 = new Calc();

        double expected = 8.20125;

        double wingSpan = 64.8;
        double wingArea = 512;

        double finalResult = c1.calculateAspectRatio(wingSpan, wingArea);

        assertEquals(expected, finalResult, 0.0);

        System.out.println("Final Result : " + Double.toString(finalResult));

    }

    /*
    test for the method that calculate the distance traveled by an aircracft of a certain flight
     */
    @Test
    public void testCalculateDistanceTravelled() {

        System.out.println("Calculate Distance Travelled");
        Calc c1 = new Calc();

        double expected = 12816.0;
        double angle = 0.154; //climbing angle
        double tas = 108.0832375; // meters per second
        double timestep = 120; //seconds

        double finalResult = c1.calculateDistanceTravelled(tas,angle, timestep);

        assertEquals(expected, Math.round(finalResult), 0.0);

        System.out.println("Final Result : " + Double.toString(finalResult));
    }
    
    /**
     * 
     * test of the method that will udpdate the current mass of an aircraft during flight
     * 
     * of class Calc
     * 
     * return previousMass - dwDT
     */
    
    @Test
    public void testUpdateMassAircraft() {
        
        System.out.println("Update the current mass of the aicraft");
        
        Calc c1 = new Calc();
        
        double previousMass = 515000;
        double dwDT = 2210;
        //515000 - 2210 = 512790
        double expected = 512790;
        
        double result = c1.updateMassAircraft(previousMass, dwDT);
        
        assertEquals(expected,result,0.0);
        
        System.out.println("Final Result : " + Double.toString(result));
    }

    /**
     * test for the method that updates the altitude of a certain aircraft
     * 
     *    public double updateAltitudeAircraft (double timestep, double altitude, double dhDT) {
     *   
     *    return altitude + (dhDT * timestep);
    }
     */
    @Test
    public void testUpdateAircraftAltitude() {

            System.out.println("Update the aircraft altitude");
            Calc c1 = new Calc();
            
            double timestep = 120; //seconds
            double altitude = 14e3;
            double dhDT = 30;
            
            double expected = 17600;
            
            double result = c1.updateAltitudeAircraft(timestep, altitude, dhDT);
            
            assertEquals(expected,result,0.0);
            
            System.out.println("Final Result : " + Double.toString(result));
    }
    
    /**
     * Test of the method that update the distance travelled by the aircraft, of class Calc
     * 
     *  public double updateDistanceTravelled (double previousDistance, double currentDistance) {
     *  
     *  return previousDistance + currentDistance;
    }
     */
    
    @Test
    public void testUpdateDistanceTravelled() {
        
        System.out.println("Update the distance travelled by the aircraft");
        Calc c1 = new Calc();
        
        double previousDistance = 200;
        double currentDistance = 300;
        
        double expected = 500;
        
        double result = c1.updateDistanceTravelled(previousDistance,currentDistance);
        
        assertEquals(expected,result,0.0);
        
        System.out.println("Distance travelled so far : " + Double.toString(result)) ;
    }

    /**
     * Test for the method that calculate the climb rate of a certain aircraft
     */
    @Test
    public void testCalculateClimbRate() {

        System.out.println("Calculate Climb Rate");

        Calc c1 = new Calc();

        double expected = 10.050; //calculated by hand

        double thurst = 17382;

        double mass = 16146.65;

        double drag = 16200;

        double TAS = 14;

        /*public double calculateClimbRate (double mass
        ,double TAS, double drag, double thurst, double dT, double dV)*/
        double finalResult = c1.calculateClimbRate(mass, TAS, drag, thurst);
        //(16146.65,14,17382,16200,10,8)
        assertEquals(expected, Math.round(finalResult * 1000.0) / 1000.0, 0.0);

        System.out.println("Final result :" + Double.toString(finalResult));
    }

    /**
     * Unit test of the method that calculates the air pressure given a certain
     * air density and temperature
     *
     * p (air pressure) = p (air density) * R (universal gas constant) * t
     * (absolute temperature)
     *
     * p (air density) = 0.2436; R = 8.31447 T = 8.5;
     *
     * p (air pressure ) = 0.2436 * 8.31774 * 8.5 = 17.222712444
     *
     * final result (rounded) = 17.223
     */
    @Test
    public void testCalculateAirPressure() {

        System.out.println("Calculate Air Pressure");
        Calc c1 = new Calc();
        Converter converter = new Converter();
        double altitude = converter.feetsConverterMeters(4.31e3);
        double expected = 86509; //exact


        double finalResult = c1.calculateAirPressure(altitude);

        assertEquals(expected,Math.round(finalResult), 0.0);

        System.out.println("Final Result : " + Double.toString(finalResult));
    }

    /**
     * Unit test for the method that calculate the rate of the change in the air
     * pressure
     *
     * Using some of the data shown in the previous test
     *
     * We got :
     *
     * airDensity = 0.2436;
     *
     * airPressure = 17.223;
     *
     * height = 14e3;
     *
     * G = 9.80665; //aceleration of the gravity force in m/s
     *
     * And the formula to calculate the wanted result :
     *
     * x = - (airPressure) * airDensity * G * height
     *
     * then we get :
     *
     * x = (-17.216) * 0.2436 * 9.80665 * 14e3;
     *
     * Final result (expected) = -2363638.01;
     */
    @Test
    public void testCalculateRateChangeAirPressure() {

        System.out.println("Calculate the range of the chane in air pressure");
        Calc c1 = new Calc();

        double expected = -2363638.01; //rounded

        double airPressure = 17.216;
        double height = 14e3;

        double finalResult = c1.calculateRateChangeAirPressure(airPressure,height);

        assertEquals(expected, Math.round(finalResult * 100.0) / 100.0, 0.0);

        System.out.println("Final Result : " + Double.toString(finalResult));
    }
    
    /**
     * Test of the method that increments the time step of the flight
     * 
     * on class Calc.java
     */

    @Test
    public void testIncrementTimeStep () {
        
        System.out.println("Calculate Increments time step");
        
        Calc c1 = new Calc();
        
        double timestep = 120; //seconds
        double previousTime = 0;
        double expected = 120; //seconds
        
        double result = c1.incrementTimeStep(previousTime,timestep); //new time
        
        assertEquals(expected,result,0.0);
        
        System.out.println("Result : " + Double.toString(result));
    }
    /**
     * Test of the method that calculates the speed of sound with a temperature
     * of a certain altitude
     */
    @Test
    public void testCalculateSpeedofSound() {

        System.out.println("Calculate the speed of sound");
        Calc c1 = new Calc();
        double temperature = 20; //Celsius

        double expected = 343.215; //m/s

        double result = c1.calculateSpeedofSound(temperature);

        assertEquals(expected, Math.round(result * 1000.0) / 1000.0, 0.0);

        System.out.println("Final Result : " + Double.toString(result));
    }

    /**
     * Test of the method calculate Temperature with Altitude
     * 
     */
    
    @Test
    public void testCalculateTemperatureAltitude() {
        //if altitude <= 11000
        System.out.println("Calculate Temperature Altitude");
        Calc c1 = new Calc();
        
        double altitude = 0;
        
        double expected = 288.2;
        //if altitude < 11000
        //!= 216.7
        double result = c1.calculateTemperatureAltitude(altitude);
        
        assertEquals(expected,Math.round(result * 10.0) / 10.0,0.0);
        
        System.out.println("Final result : " + Double.toString(result));
        //if altitude > 11000
        //then 216.7
        double newAltitude = 21000;        
        double newExpected = 216.7;
        double newResult = c1.calculateTemperatureAltitude(newAltitude);
        assertEquals(newExpected,newResult,0.0);
        System.out.println("New Result : "  + Double.toString(newResult));
    }
  
    
    /**
     * Test of the method that calculate the total thurst of the aircraft , of class Calc
     */
    
    @Test
    public void testCalculateTotalThurst() {
        
        System.out.println("Calculate the total thurst");
        Calc c1 = new Calc();
        
        int numberMotors = 4;
        
        double thurst = 2.82e05;
        
        double expected = 1.128e06;
        
        double result = c1.calculateTotalThurst(thurst, numberMotors);
        
        assertEquals(expected,Math.round(result),0.0);
        
        System.out.println("Final Result : " + Double.toString(result));
    }
    
    /**
     * Test of the method that calculates the specific air range of an aircraft
     * 
     * also know as the aircraft´s fuel efficiency
     * 
     *  public double calculateSAR(double fuelConsumed, double distance) {
     *  
     *  return distance / fuelConsumed;
     *  
     *  }
     */
    
    @Test
    public void testCalculateSAR() {
        
        System.out.println("Calculate the Specific Aircraft Range");
        Calc c1 = new Calc();
        
        double distance = 200;
        double fuelConsumed = 144.2;
        
        double expected = 1.387;
        
        double result = c1.calculateSAR(fuelConsumed, distance);
        
        assertEquals(expected,Math.round(result * 1000.0) / 1000.0,0.0);
        
        System.out.println("Final result (SAR) : " + Double.toString(result));
    }

    /**
     * Test of the overrided toString(), of class calc
     */
    @Test
    public void testToString() {

        System.out.println("toString");
        Calc c1 = new Calc();

        String expected = "Class for calculations related to the project";

        String result = c1.toString();

        assertEquals(expected, result);

        System.out.println(c1.toString());
    }

    /**
     * Test of the override hashCode(), of class calc
     */
    @Test
    public void testHashCode() {

        System.out.println("hashCode");

        Calc c1 = new Calc();

        int expected = 5;

        int result = c1.hashCode();

        assertEquals(expected, result, 0);
    }

    /**
     * Test of the override method Equals)=, of class Calc
     */
    @Test
    public void testEquals() {

        System.out.println("Equals");
        
        Calc instance = new Calc(); //object to test all comparations of the method equals
        
        Object obj = null; //compare to a null object 
        
        
    }

    /**
     * Test of updateAltitudeAircraft method, of class Calc.
     */
    @Test
    public void testUpdateAltitudeAircraft() {
        System.out.println("updateAltitudeAircraft");
        /**
         * return altitude + (dwDT * timestep);
         */
        double timestep = 120.0;
        double altitude = 0.0;
        double dhDT = 17.8; // cimb rate  //m/s
        Calc instance = new Calc();
        double expResult = 2136.0;
        double result = instance.updateAltitudeAircraft(timestep, altitude, dhDT);
        assertEquals(expResult, result, 0.0);
        
    }
    
    /**
     * Test for the method that calculate the climbing angle of an aircraft
     * of class Calc
     */
    @Test
    public void testCalculateClimbingAngle () {
        
        System.out.println("calculate Climbing Angle");
        Calc instance = new Calc();
        
        //teste for climb
        double tas = 108.0832375;
        double climbRate = 1.78e1;
       
        double expected = 1.65e-1;
        
        double result = instance.calculateClimbingAngle(tas, climbRate);
        
        assertEquals(expected,Math.round(result * 1000.0) / 1000.0,0.0);
        
        System.out.println("Final Result (Exact) : " + Double.toString(result));
        
        //test for descend
        
        double trueAirSpeed  = 224.7018767;
        double dhDT = -12.70; //negative because the plane is slowly landing
        
        double expected2 = -0.06;
        double result2 = instance.calculateClimbingAngle(trueAirSpeed,dhDT);
        
        assertEquals(expected2,Math.round(result2 * 100.0) / 100.0,0.0);
        
        System.out.println("Final Result 2 (Exact) : " + Double.toString(result2));
    }
    
    /**
     * Test of the method to calculate the idle potency of the motors
     */
    
    @Test
    public void testCalculateTotalIdlePotency() {
        
        System.out.println("calculateTotalIdlePotency");
        Calc instance = new Calc();
        double pontecy = 10;
        int numMotors = 4;
        
        double expected = 4;
        
        double result = instance.calculateTotalIdlePotency(pontecy, numMotors);
        
        assertEquals(expected,result,0.0);
    }

    /**
     * Test of getRangeAircraft method, of class Calc.
     */
    @Test
    public void testGetRangeAircraft() {
        System.out.println("getRangeAircraft");
        Calc instance = new Calc();
        double expResult = 0.0;
        instance.setRangeAircraft(expResult);
        double result = instance.getRangeAircraft();
        assertEquals(expResult, result, 0.0);
        expResult = 1000.0;
        instance.setRangeAircraft(expResult);
        result = instance.getRangeAircraft();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setRangeAircraft method, of class Calc.
     */
    @Test
    public void testSetRangeAircraft() {
        System.out.println("setRangeAircraft");
        double rangeAircraft = 0.0;
        Calc instance = new Calc();
        instance.setRangeAircraft(rangeAircraft);
        assertEquals(rangeAircraft,instance.getRangeAircraft(),0.0);
        rangeAircraft = 1000.0;
        instance.setRangeAircraft(rangeAircraft);
        assertEquals(rangeAircraft,instance.getRangeAircraft(),0.0);
        
    }

    /**
     * Test of getDistance method, of class Calc.
     */
    @Test
    public void testGetDistance() {
        System.out.println("getDistance");
        Calc instance = new Calc();
        double expResult = 0.0;
        instance.setDistance(expResult);
        double result = instance.getDistance();
        assertEquals(expResult, result, 0.0);
        expResult = 277.0;
        instance.setDistance(expResult);
        result = instance.getDistance();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setDistance method, of class Calc.
     */
    @Test
    public void testSetDistance() {
        System.out.println("setDistance");
        double distance = 0.0;
        Calc instance = new Calc();
        instance.setDistance(distance);
        assertEquals(distance,instance.getDistance(),0.0);
        distance = 277.0;
        instance.setDistance(distance);
        assertEquals(distance,instance.getDistance(),0.0);
    }

    /**
     * Test of getLiftForce method, of class Calc.
     */
    @Test
    public void testGetLiftForce() {
        System.out.println("getLiftForce");
        Calc instance = new Calc();
        double expResult = 0.0;
        instance.setLiftForce(expResult);
        double result = instance.getLiftForce();
        assertEquals(expResult, result, 0.0);
        expResult = 200.0;
        instance.setLiftForce(expResult);
        result = instance.getLiftForce();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setLiftForce method, of class Calc.
     */
    @Test
    public void testSetLiftForce() {
        System.out.println("setLiftForce");
        double liftForce = 0.0;
        Calc instance = new Calc();
        instance.setLiftForce(liftForce);
        assertEquals(liftForce,instance.getLiftForce(),0.0);
        liftForce = 14000.0;
        instance.setLiftForce(liftForce);
        assertEquals(liftForce,instance.getLiftForce(),0.0);
    }

    /**
     * Test of getDragForce method, of class Calc.
     */
    @Test
    public void testGetDragForce() {
        System.out.println("getDragForce");
        Calc instance = new Calc();
        double expResult = 0.0;
        instance.setDragForce(expResult);
        double result = instance.getDragForce();
        assertEquals(expResult, result, 0.0);
        expResult = 200.0;
        instance.setDragForce(expResult);
        result = instance.getDragForce();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setDragForce method, of class Calc.
     */
    @Test
    public void testSetDragForce() {
        System.out.println("setDragForce");
        double dragForce = 0.0;
        Calc instance = new Calc();
        instance.setDragForce(dragForce);
        assertEquals(dragForce,instance.getDragForce(),0.0);
        dragForce = 200.0;
        instance.setDragForce(dragForce);
        assertEquals(dragForce,instance.getDragForce(),0.0);
    }

    /**
     * Test of CalculateDistance method, of class Calc.
     */
    @Test
    public void testCalculateDistance() {
        System.out.println("CalculateDistance");
        double initialLatitude = 41.2481003;
        double initialLongitude = -8.6813898;
        //41.2481003, -8.6813898
        double finalLatitude = 38.7812996;
        double finalLongitude = -9.1359196;
        //38.7812996, -9.1359196,
        Calc instance = new Calc();
        instance.CalculateDistance(initialLatitude, initialLongitude, finalLatitude, finalLongitude);
        double expResult = 277.0;
        
        assertEquals(expResult,Math.round(instance.getDistance()),0.0);
        
        System.out.println("Final result of distance (exact) : " + Double.toString(instance.getDistance()));
    }

    /**
     * Test of calculateMaxFlightTime method, of class Calc.
     */
    @Test
    public void testCalculateMaxFlightTime() {
        System.out.println("calculateMaxFlightTime");
        double massFuel = 30.0;
        double tsfc = 24.0;
        double netThurst = 12.0;
        Calc instance = new Calc();
        double expResult = 0.02;
        double result = instance.calculateMaxFlightTime(massFuel, tsfc, netThurst);
        assertEquals(expResult, Math.round(result * 100.0) / 100.0, 0.0);
        System.out.println("Final result (exact) : " + Double.toString(result));
    }

    /**
     * Test of calculateThurstClimb method, of class Calc.
     */
    
    //double fst = thrust - (lambda * mTrue);
        
    //double snd = Math.pow(airDensity / D0,lapseRate);
        
    //return  fst * snd;
    
    @Test
    public void testCalculateThurstClimb() {
        System.out.println("calculateThurstClimb");
        double thrust = 3.38e05;
        double airDensity = 1.22501312;
        double lambda = 1.76e05;
        double mTrue = 0.317618939;
        double lapseRate = 9.60e-01;
        Calc instance = new Calc();
        double expResult = 282102;
        double result = instance.calculateThurstClimb(thrust, airDensity, lambda, mTrue, lapseRate);
        assertEquals(expResult, Math.round(result), 0.0);
        System.out.println("Final result (exact) : " + Double.toString(result));
    }

    /**
     * Test of calculateThurstDescend method, of class Calc.
     */
    
    //double fst = thrust - (lambda * mTrue);
        
    //double snd = Math.pow(airDensity / D0,lapseRate);
    //D0 = 1.225
    //return 0.1 * fst * snd;
    
    
    @Test
    public void testCalculateThurstDescend() {
        System.out.println("calculateThurstDescend");
        double thrust = 3.38e05;
        double airDensity = 0.40966425;
        double lambda = 1.76e05;
        double mTrue = 0.75100137;
        double lapseRate = 9.60e-01;
        Calc instance = new Calc();
        double expResult = 7191;
        double result = instance.calculateThurstDescend(thrust, airDensity, lambda, mTrue, lapseRate);
        assertEquals(expResult, Math.round(result), 0.0);
        System.out.println("Final result (exact) : " + Double.toString(result));
    }

    /**
     * Test of calculateThurstCruise method, of class Calc.
     */
    @Test
    public void testCalculateThurstCruise() {
        System.out.println("calculateThurstCruise");
        double totalThurst = 240575.7;
        int numberMotors = 4;
        Calc instance = new Calc();
        double expResult = 60143.93;
        double result = instance.calculateThurstCruise(totalThurst, numberMotors);
        assertEquals(expResult, Math.round(result * 100.0) / 100.0, 0.0);
        System.out.println("Final result (exact) : " + Double.toString(result));
    }

    /**
     * Test of calculateRangeFlightTimeMax method, of class Calc.
     */
    @Test
    public void testCalculateRangeFlightTimeMax() {
        System.out.println("calculateRangeFlightTimeMax");
        double maxFlightTime = 2400.0;
        double speed = 228.6961057;
        Calc instance = new Calc();
        double expResult = 548870.65; //distance
        double result = instance.calculateRangeFlightTimeMax(maxFlightTime, speed);
        assertEquals(expResult, Math.round(result * 100.0) / 100.0, 0.0);
        System.out.println("Final result (exact) : " + Double.toString(result));
    }

    /**
     * Test of calculateBearing method, of class Calc.
     */
    @Test
    public void testCalculateBearing() {
        System.out.println("calculateBearing");
        double latitude1 = 41.2481003;
        double longitude1 = -8.6813898;
        double latitude2 = 38.7812996;
        double longitude2 = -9.1359196;
        Calc instance = new Calc();
        double expResult = -2.832; //radians
        double result = instance.calculateBearing(latitude1, longitude1, latitude2, longitude2);
        assertEquals(expResult, Math.round(result * 1000.0) / 1000.0, 0.0);
        System.out.println("Final result (exact) : " + Double.toString(result));
        //conversion of radians to macthing Degrees with the Converter class
        Converter converter = new Converter();
        double expDeg = converter.radiansConverterDegrees(expResult);
        double deg = converter.radiansConverterDegrees(result);
        System.out.println("Expected in degrees :" + Double.toString(expDeg));
        System.out.println("Result in degrees : " + Double.toString(Math.round(deg * 1000.0) / 1000.0));
    }

    /**
     * Test of calculateTrueGroundSpeed method, of class Calc.
     */
    @Test
    public void testCalculateTrueGroundSpeed() {
        System.out.println("calculateTrueGroundSpeed");
        double windAngle = 0.467;
        double bearing = 2.0;
        double tas = 14.0;
        double windIntensity = 15.0;
        double windDirection = 4.0;
        Calc instance = new Calc();
        double expResult = 1.0;
        double result = instance.calculateTrueGroundSpeed(windAngle, bearing, tas, windIntensity, windDirection);
        assertEquals(expResult, Math.round(result), 0.0);
        System.out.println("Final Result (exact) : " + Double.toString(result));
    }

    /**
     * Test of calculateWindCorrectionAngle method, of class Calc.
     */
    @Test
    public void testCalculateWindCorrectionAngle() {
        System.out.println("calculateWindCorrectionAngle");
        double windIntensity = 15.0;
        double tas = 14.0;
        double bearing = 2.0;
        Calc instance = new Calc();
        double expResult = 0.467;
        double result = instance.calculateWindCorrectionAngle(windIntensity, tas, bearing);
        assertEquals(expResult, Math.round(result * 1000.0) / 1000.0, 0.0);
        System.out.println("Final result (exact) : " + Double.toString(result));
    }

    /**
     * Test of calculateTakeOffDistance method, of class Calc.
     */
    @Test
    public void testCalculateTakeOffDistance() {
        System.out.println("calculateTakeOffDistance");
        double groundSpeed = 140000.0;
        double totalThurst = 13100.0;
        Calc instance = new Calc();
        double expResult = 1496183.206;
        double result = instance.calculateTakeOffDistance(groundSpeed, totalThurst);
        assertEquals(expResult, Math.round(result * 1000.0) / 1000.0, 0.0);
        System.out.println("Final Result (exact) : " + Double.toString(result));
    }

    /**
     * Test of calculateTrueAirSpeed method, of class Calc.
     */
    @Test
    public void testCalculateTrueAirSpeed() {
        System.out.println("calculateTrueAirSpeed");
        double machNumber = 0.76929307;
        double speedSound = 297.2808605;
        Calc instance = new Calc();
        double expResult = 228.6961;
        double result = instance.calculateTrueAirSpeed(machNumber, speedSound);
        assertEquals(expResult, Math.round(result * 10000.0) / 10000.0, 0.0);
        System.out.println("Final result (exact) : " + Double.toString(result));
    }

    /**
     * Test of calculateMachNumber method, of class Calc.
     */
    
    //return Math.sqrt(GAMMA * R * temperature);
    
    //private static final double GAMMA = 1.4; //for air
    
    //private static final double R = 287; //N-m/K
    
    
    @Test
    public void testCalculateMachNumber() {
        System.out.println("calculateMachNumber");
        double temperature = 219.95;
        Calc instance = new Calc();
        double expResult = 297.281;
        double result = instance.calculateMachNumber(temperature);
        assertEquals(expResult, Math.round(result * 1000.0) / 1000.0, 0.0);
        System.out.println("Final result (exact) : " + Double.toString(result));
    }

    /**
     * Test of CalculateLiftForce method, of class Calc.
     */
    
    //double aircraft_mass = calculateMass(a.getEmptyWeight(),payload,fuel);
    //154935
    // double densityAir = calculateAirDensity(pressure,temperature);  pressure / (R * temperature);
    // 1.612094888
     //double v = calculateTrueAirSpeed(machNumber,speedSound);
    //170.15
     //double cL = calculateLiftCoefficent(aircraft_mass, densityAir, a.getWingArea(), v);
    //return (2 * mass * G) / (densityAir * wingArea * Math.pow(speed, 2));
    // 0.1271673845
    //return cL * ((densityAir * Math.pow(v, 2)) / 2) * a.getWingArea();
    //
    
    @Test
    public void testCalculateLiftForce() {
        System.out.println("CalculateLiftForce");
        AircraftModel a = new AircraftModel();
        a.setEmptyWeight(154335);
        a.setWingArea(512);
        double payload = 400.0;
        double fuel = 200.0;
        double pressure = 101325;
        double temperature = 219.0;
        double machNumber = 0.50;
        double speedSound = 340.3;
        Calc instance = new Calc();
        System.out.println("Note : None of the value represent in the console are rounded");
        System.out.println("Mass of aircraft : " + Double.toString(instance.calculateMass(a.getEmptyWeight(), payload, fuel)) );
        System.out.println("True AirSpeed : " + Double.toString(instance.calculateTrueAirSpeed(machNumber,speedSound)));
        System.out.println("Air Density : " + Double.toString(instance.calculateAirDensity(pressure, temperature)));
        System.out.println("Lift coefficent : " + Double.toString(instance.calculateLiftCoefficent(instance.calculateMass(a.getEmptyWeight(),payload,fuel),instance.calculateAirDensity(pressure, temperature),a.getWingArea(),instance.calculateTrueAirSpeed(machNumber, speedSound))));
        double expResult = 1519393.318;
        double result = instance.CalculateLiftForce(a, payload, fuel, pressure, temperature, machNumber, speedSound);
        assertEquals(expResult, Math.round(result * 1000.0) / 1000.0, 0.0);
        System.out.println("Final result  : " + Double.toString(result));
    }

    /**
     * Test of calculateConsume method, of class Calc.
     */
    
    /**
     *  double speed = 14;
        double wingArea = 512;
        double wingSpan = 64.8;
        double e = 0.95;
        double mass = 16146.65;
        double cdrag0 = 0.025;
        double airDensity = 0.2435;
     */
    @Test
    public void testCalculateConsume() {
        System.out.println("calculateConsume");
        AircraftModel a = new AircraftModel();
        a.setEmptyWeight(154335);
        a.setWingArea(512);
        a.setWingSpan(64.8);
        a.setE(0.95);
        double payload = 400.0;
        double fuel = 200.0;
        double pressure = 101325;
        double temperature = 219.0;
        double machNumber = 0.50;
        double speedSound = 340.3;
        int passangers = 60;
        int crew = 20;
        double tsfc = 0.564;
        double cdrag_0 = 0.025;
        double distance = 277.0;
        Calc instance = new Calc();
        double tas = instance.calculateTrueAirSpeed(machNumber,speedSound);
        
        /*
        double Lift = CalculateLiftForce(a,payload,fuel,pressure,temperature,machNumber,speedSound);

        double D = CalculateDragForce(calculateAirDensity(pressure,temperature),calculateMass(a.getEmptyWeight(),fuel,payload),cdrag_0, a.getWingArea(), a.getWingSpan(), a.getMotorization().getCruiseSpeed(), a.getE());
        double WI = calculateInitialWeight(payload, fuel, a, passangers, crew);
        double TSFC = tsfc;
        double F = (tas / TSFC) * (Lift / D);
        double wf = WI / (Math.pow(Math.E, distance / F));

        return wf - WI;
        */
        System.out.println("All value are reprsented in a exact form by JAVA");
        double lift = instance.CalculateLiftForce(a, payload, fuel, pressure, temperature, machNumber, speedSound);
        System.out.println("Lift Force : " + Double.toString(lift));
        double drag = instance.CalculateDragForce(instance.calculateAirDensity(pressure, temperature),instance.calculateMass(a.getEmptyWeight(), payload, fuel),cdrag_0,a.getWingArea(),a.getWingSpan(),instance.calculateTrueAirSpeed(machNumber,speedSound),a.getE());
        System.out.println("Drag Force : " + Double.toString(drag));
        double wi = instance.calculateInitialWeight(payload, fuel, a, passangers, crew);
        System.out.println("Initial Weight : " + Double.toString(wi));
        double f = (tas / tsfc) * (lift / drag);
        System.out.println("F : " + Double.toString(f));
        double wf = wi / (Math.pow(Math.E,distance / f));
        System.out.println("Final Weight : " + Double.toString(wf));
        
        
        double expResult = -27218.163;
        double result = instance.calculateConsume(tas, distance, fuel, payload, a, passangers, crew, tsfc, cdrag_0, pressure, temperature, machNumber, speedSound);
        assertEquals(expResult, Math.round(result * 1000.0) / 1000.0, 0.0);
        System.out.println("Final result : " + Double.toString(result));
    }

    /**
     * Test of calculateTrueMachNumber method, of class Calc.
     */
    @Test
    public void testCalculateTrueMachNumber() {
        System.out.println("calculateTrueMachNumber");
        double airDensity = 1.22501312;
        double ias = 210.0;
        Calc instance = new Calc();
        double expResult = 0.317618939;
        double result = instance.calculateTrueMachNumber(airDensity, ias);
        assertEquals(expResult, result, 0.0);
        
    }

    /**
     * Test of calculateTrueAirSpeed method, of class Calc.
     */
    
      //true air speed can be calculated with the relation (TruemachNumber * speedofSound)
    
    /*public double calculateTrueAirSpeed(double airDensity, double windSpeed) {

        //calculate the dynamic pressure
        // q = 1/2 * airDensity * v ^2
        double q = (airDensity * Math.pow(windSpeed, 2)) / 2;

        //calculate equivalent airspeed
        double eas = Math.sqrt((2 * q) / P0);

        return eas * (Math.sqrt(P0 / airDensity));
    }*/
   
}


package lapr.project.List;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import lapr.project.model.Airport;

/**
 *
 * @author Diogo Guedes <1150613@isep.ipp.pt>
 */
public class AirportList {

    private List<Airport> airports;

    public AirportList() {
        airports = new ArrayList<>();
    }

    /**
     * Creates a clone of the target airports
     *
     * @param source The target airport list to clone.
     */
    public AirportList(AirportList source) {
        airports = new ArrayList(source.airports);
        Collections.copy(airports, source.airports);
    }

    public List<Airport> getAirports() {
        return airports;
    }

    public void setAirports(List<Airport> airports) {
        this.airports = airports;
    }

    /**
     * Return the number of airports.
     *
     * @return
     */
    public int size() {
        return this.airports.size();
    }

    @Override
    public String toString() {
        return "SegmentList{" + "segmentList=" + airports + '}';
    }

    /**
     * Creates a new airport.
     *
     * @return New airport.
     */
    public Airport newAirport() {
        return new Airport();
    }

    /**
     * Add an airport to the airports list.
     *
     * @param airport Airport to be added.
     * @return True if the airport was added , otherwise returns false.
     */
    public boolean addAirport(Airport airport) {
        if (!this.airports.contains(airport)) {
            return this.airports.add(airport);
        }

        return false;
    }

    /**
     * Remove the airport received by parameter airport list
     *
     * @param airport Airport to be removed.
     * @return True if the airport was added , otherwise returns false.
     */
    public boolean removeAirport(Airport airport) {
        return this.airports.remove(airport);
    }

    /**
     * Returns an iterator to allow running through the list of airports.
     *
     * @return Iterator.
     */
    public Iterator<Airport> iterator() {
        return this.airports.iterator();
    }

    public Airport getAirport(String IATA) {

        for (Airport airp : this.airports) {
            if (IATA.equalsIgnoreCase(airp.getIATA())) {
                return airp;

            }
        }
        return null;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.List;
import lapr.project.model.Aircraft;
import lapr.project.model.Airport;
import lapr.project.model.FlightPlan;
import lapr.project.model.Importable;
import lapr.project.model.LeitorCsv;
import lapr.project.model.Project;
import lapr.project.model.Segment;
import lapr.project.model.datalayer.DataBaseManager;

/**
 *
 * @author cdsn_
 */
public class CreateFlightPlanController {

    /**
     * Instance of flightPlan
     */
    private FlightPlan flightPlan;

    /**
     * Instance of project
     */
    private Project p;

    /**
     * Origin Airfield
     */
    private Airport airportOrigin;

    /**
     * Destiny Airfield
     */
    private Airport airportDestiny;

    public CreateFlightPlanController(Project p) {
        this.p = p;
    }

    /**
     * Create an instance of FlightPlan
     */
    public void createFlightPlan() {
        this.setFlightPlan(new FlightPlan());
    }

    /**
     * @return the airportOrigin
     */
    public Airport getAirportOrigin() {
        return airportOrigin;
    }

    /**
     * @param airportOrigin the airportOrigin to set
     */
    public void setAirportOrigin(Airport airportOrigin) {
        this.flightPlan.setOriginAirfield(airportOrigin);
    }

    /**
     * @return the airportDestiny
     */
    public Airport getAirportDestiny() {
        return airportDestiny;
    }

    /**
     * @param airportDestiny the airportDestiny to set
     */
    public void setAirportDestiny(Airport airportDestiny) {
        if (airportDestiny.getIATA().equals(flightPlan.getOriginAirfield().getIATA())) {
            throw new IllegalArgumentException("Airport Destiny can't be the same as Airport Origin");
        }
        this.flightPlan.setDesttinyAirfield(airportDestiny);
    }

    /**
     * @return the flightPlan
     */
    public FlightPlan getFlightPlan() {
        return flightPlan;
    }

    /**
     * @param flightPlan the flightPlan to set
     */
    public void setFlightPlan(FlightPlan flightPlan) {
        this.flightPlan = flightPlan;
    }

    /**
     * Return to list of aircrafts.
     *
     * @return list
     */
    public List<Aircraft> getAircraftList() {
        return this.p.getAircraftList().getAircraftList();
    }

    /**
     * Select an aircraft
     *
     * @param air
     */
    public void setAircraftType(Aircraft air) {
        this.getFlightPlan().setAircraft(air);
    }

    /**
     * get an aircraft
     *
     * @return
     */
    public Aircraft getAircraft() {
        return this.getFlightPlan().getAircraft();
    }

    /**
     * Select the maximum number of passengers in each class.
     *
     * @param passengersExec the maximum number of passengers in class Executiv.
     * @param passengersEco passengers the maximum number of passengers in class
     * economic.
     *
     */
    public void setMaxNumberPassangers(int passengersExec, int passengersEco) {
        this.flightPlan.setMaxPassengersClasseEco(passengersEco);
        this.flightPlan.setMaxPassengersClasseExec(passengersExec);
    }

    /**
     * get number passengers exec
     *
     * @return
     */
    public int getPassengersExec() {
        return this.getFlightPlan().getMaxPassengersClasseExec();
    }

    /**
     * get number passengers eco
     *
     * @return
     */
    public int getPassengersEco() {
        return this.getFlightPlan().getMaxPassengersClasseEco();
    }

    /**
     * Select the crew
     *
     * @param crew
     */
    public void setCrew(int crew) {
        this.getFlightPlan().setCrew(crew);
    }

    /**
     * Return crew of aircraft
     *
     * @return crew
     */
    public int getCrew() {
        return this.getFlightPlan().getCrew();
    }

    public void setFlightPlanName(String name) {
        this.flightPlan.setName(name);
    }

    /**
     * Return a Airports list
     *
     * @return airports list
     */
    public List<Airport> getAirportsList() {
        return this.p.getAirportList().getAirports();
    }

    /**
     * Return a segments list
     *
     * @return segments list
     */
    public List<Segment> getSegmentsList() {
        return this.p.getSegmentList().getsegmentList();
    }

    public boolean addSegment(Segment s) {
        return this.getFlightPlan().addSegmentsToStops(s);
    }

    /**
     * Add flight plan to the list
     *
     * @return true if its added and false otherwise
     */
    public boolean addFlightPlanToList() {
        DataBaseManager.getInstance().getProjectData().insertFlightPlan(p, getFlightPlan());
        return this.p.getFlightPlanList().addFlightPlan(getFlightPlan());
    }

    public boolean readCSV(String path) {
        LeitorCsv lc = new LeitorCsv();
        return lc.importCSV(path, flightPlan);
    }

}


package lapr.project.List;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import lapr.project.model.Cdrag;

/**
 *
 * @author pedro
 */
public class CdragList {

    private List<Cdrag> cdrag;

    public CdragList() {
        this.cdrag = new ArrayList<>();
    }

    /**
     * @return the cdrag
     */
    public List<Cdrag> getCdrag() {
        return cdrag;
    }

    /**
     * @param cdrag the cdrag to set
     */
    public void setCdrag(List<Cdrag> cdrag) {
        this.cdrag = cdrag;
    }

    /**
     * returns the size
     *
     * @return
     */
    public int size() {
        return this.cdrag.size();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    /**
     * creates a new Cdrag
     *
     * @return
     */
    public Cdrag newCdrag() {
        return new Cdrag();
    }

    /**
     * Add an cdrag to the cdrag list.
     *
     * @param cdrag to be added.
     * @return True if the cdrag was added , otherwise returns false.
     */
    public boolean addCdrag(Cdrag cdrag) {
        if (!this.cdrag.contains(cdrag)) {
            return this.cdrag.add(cdrag);
        }

        return false;
    }

    /**
     * Remove the cdrag received by parameter cdrag
     *
     * @param cdrag CDRAG to be removed.
     * @return True if the cdrag was added , otherwise returns false.
     */
    public boolean removeCdrag(Cdrag cdrag) {
        return this.cdrag.remove(cdrag);
    }

     public Iterator<Cdrag> iterator() {
        return this.cdrag.iterator();
    }
    @Override
    public String toString() {
        return "CdragList{" + "cdrag=" + cdrag + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CdragList other = (CdragList) obj;
        if (!Objects.equals(this.cdrag, other.cdrag)) {
            return false;
        }
        return true;
    }

}

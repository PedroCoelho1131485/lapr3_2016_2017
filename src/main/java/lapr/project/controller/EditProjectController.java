/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import lapr.project.model.Importable;
import lapr.project.model.Project;
import lapr.project.model.ProjectRegistry;

/**
 * Controls the edition of a project. Using this class, a user interface can
 * easilly allow the edition of a project.
 *
 */
public class EditProjectController {

    /**
     * The project handler.
     */
    private ProjectRegistry ph;

    /**
     * The clone of the opened project.
     */
    private Project clone;

    /**
     * Creates an instance of EditProjectController with the specified
     * parameters.
     *
     *
     * @param projectHandler The handler of projects.
     */
    public EditProjectController(ProjectRegistry projectHandler) {
        ph = projectHandler;
        if (ph.getActiveProject() == null) {
            throw new IllegalArgumentException("You must open a project before you can edit it!");
        }
        clone = new Project(ph.getActiveProject());
    }

    /**
     * Returns the project
     *
     * @return clone
     */
    public Project getProject() {
        return clone;
    }

    /**
     * Returns the project's name.
     *
     * @return (String) The project's name.
     */
    public String getProjectName() {
        return clone.getName();
    }

    /**
     * Returns the project's description.
     *
     * @return (String) The project's description.
     */
    public String getProjectDescription() {
        return clone.getDescription();
    }

    /**
     * Sets the project's name.
     *
     * @param name (String) The new name of the project.
     */
    public void setProjectName(String name) {
        clone.setName(name);
    }

    /**
     * Sets the project's description.
     *
     * @param desc (String) The new description of the project.
     */
    public void setProjectDescription(String desc) {
        clone.setDescription(desc);
    }

    /**
     * Enter the path where the file is located with the data to import.
     *
     *
     * @param path path to the file location to import
     */
    public void importAircrafts(String path) {
        Importable importable = clone.getImportableRegistry().getImportableOfType(path);
        importable.importAircrafts(this.clone, path);
    }

    /**
     * Enter the path where the file is located with the data to import.
     *
     *
     * @param path path to the file location to import
     */
    public void importAirports(String path) {
        Importable importable = clone.getImportableRegistry().getImportableOfType(path);
        importable.importAirports(this.clone, path);
    }

    /**
     * Enter the path where the file is located with the data to import.
     *
     * @param path path to the file location to import
     */
    public void importNetwork(String path) {
        Importable importable = clone.getImportableRegistry().getImportableOfType(path);
        importable.importNetwork(clone, path);

    }

    /**
     * Registers the changes made to the project.
     *
     * @return (boolean) Returns true if successfully changed.
     */
    public boolean registerChanges() {
        return ph.registerChanges(clone, ph.getActiveProject());
    }

}

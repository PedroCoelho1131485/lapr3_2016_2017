
package lapr.project.model;

import java.util.Objects;

/**
 *
 *
 */
public class Motorization {

    private int numberMotors;
    private String motor;
    private String motorType;
    private String name;
    private double cruiseSpeed;
    private double cruiseAltitude;
    private double TSFC;
    private double lapseRateFactor;
    private double thurst_0;
    private double thurst_max_speed;
    private double max_speed;
    private static int cont;

    public Motorization(String name, int numberMotors, String motor, String motorType, double speed, double height, double tsfc, double lapse, double t0, double tmax, double max_speed) {
        this.name = "motor" + cont;
        this.numberMotors = numberMotors;
        this.motor = motor;
        this.motorType = motorType;
        this.cruiseSpeed = speed;
        this.cruiseAltitude = height;
        this.TSFC = tsfc;
        this.lapseRateFactor = lapse;
        this.thurst_0 = t0;
        this.thurst_max_speed = tmax;
        cont++;
        this.max_speed = max_speed;
    }

    public Motorization() {
        this.name = "motor" + cont;
        this.motor = "";
        this.motorType = "";
        this.numberMotors = 0;
        this.cruiseSpeed = 0.0;
        this.cruiseAltitude = 0.0;
        this.TSFC = 0.0;
        this.thurst_0 = 0.0;
        this.lapseRateFactor = 0.0;
        this.thurst_max_speed = 0.0;
        this.max_speed = 0.0;
        cont++;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Return number of motors
     *
     * @return numberMotors
     */
    public int getNumberMotors() {
        return numberMotors;
    }

    /**
     * Modify number of motors
     *
     * @param numberMotors
     */
    public void setNumberMotors(int numberMotors) {
        this.numberMotors = numberMotors;
    }

    /**
     * @return motor
     */
    public String getMotor() {
        return motor;
    }

    /**
     * @param motor
     */
    public void setMotor(String motor) {
        this.motor = motor;
    }

    /**
     * Return type of motor
     *
     * @return motorType
     */
    public String getMotorType() {
        return motorType;
    }

    /**
     * Modify motor type
     *
     * @param motorType
     */
    public void setMotorType(String motorType) {
        this.motorType = motorType;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Motorization other = (Motorization) obj;
        if (this.numberMotors != other.numberMotors) {
            return false;
        }
        if (Double.doubleToLongBits(this.cruiseSpeed) != Double.doubleToLongBits(other.cruiseSpeed)) {
            return false;
        }
        if (Double.doubleToLongBits(this.cruiseAltitude) != Double.doubleToLongBits(other.cruiseAltitude)) {
            return false;
        }
        if (Double.doubleToLongBits(this.TSFC) != Double.doubleToLongBits(other.TSFC)) {
            return false;
        }
        if (Double.doubleToLongBits(this.lapseRateFactor) != Double.doubleToLongBits(other.lapseRateFactor)) {
            return false;
        }
        if (Double.doubleToLongBits(this.thurst_0) != Double.doubleToLongBits(other.thurst_0)) {
            return false;
        }
        if (Double.doubleToLongBits(this.thurst_max_speed) != Double.doubleToLongBits(other.thurst_max_speed)) {
            return false;
        }
        if (Double.doubleToLongBits(this.max_speed) != Double.doubleToLongBits(other.max_speed)) {
            return false;
        }
        if (!Objects.equals(this.motor, other.motor)) {
            return false;
        }
        if (!Objects.equals(this.motorType, other.motorType)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Motorization{" + "numberMotors=" + numberMotors + ", motor=" + motor + ", motorType=" + motorType + ", name=" + name + ", cruiseSpeed=" + cruiseSpeed + ", cruiseAltitude=" + cruiseAltitude + ", TSFC=" + TSFC + ", lapseRateFactor=" + lapseRateFactor + ", thurst_0=" + thurst_0 + ", thurst_max_speed=" + thurst_max_speed + ", max_speed=" + max_speed + '}';
    }

    /**
     * @return the cruiseSpeed
     */
    public double getCruiseSpeed() {
        return cruiseSpeed;
    }

    /**
     * @param cruiseSpeed the cruiseSpeed to set
     */
    public void setCruiseSpeed(double cruiseSpeed) {
        this.cruiseSpeed = cruiseSpeed;
    }

    /**
     * @return the cruiseAltitude
     */
    public double getCruiseAltitude() {
        return cruiseAltitude;
    }

    /**
     * @param cruiseAltitude the cruiseAltitude to set
     */
    public void setCruiseAltitude(double cruiseAltitude) {
        this.cruiseAltitude = cruiseAltitude;
    }

    /**
     * @return the TSFC
     */
    public double getTSFC() {
        return TSFC;
    }

    /**
     * @param TSFC the TSFC to set
     */
    public void setTSFC(double TSFC) {
        this.TSFC = TSFC;
    }

    /**
     * @return the lapseRateFactor
     */
    public double getLapseRateFactor() {
        return lapseRateFactor;
    }

    /**
     * @param lapseRateFactor the lapseRateFactor to set
     */
    public void setLapseRateFactor(double lapseRateFactor) {
        this.lapseRateFactor = lapseRateFactor;
    }

    /**
     * @return the thurst_0
     */
    public double getThurst_0() {
        return thurst_0;
    }

    /**
     * @param thurst_0 the thurst_0 to set
     */
    public void setThurst_0(double thurst_0) {
        this.thurst_0 = thurst_0;
    }

    /**
     * @return the thurst_max_speed
     */
    public double getThurst_max_speed() {
        return thurst_max_speed;
    }

    /**
     * @param thurst_max_speed the thurst_max_speed to set
     */
    public void setThurst_max_speed(double thurst_max_speed) {
        this.thurst_max_speed = thurst_max_speed;
    }

    /**
     * @return the max_speed
     */
    public double getMax_speed() {
        return max_speed;
    }

    /**
     * @param max_speed the max_speed to set
     */
    public void setMax_speed(double max_speed) {
        this.max_speed = max_speed;
    }

    public boolean validate() {

        if (this.numberMotors == 0) {
            throw new IllegalArgumentException("Number of motors can't be zero");
        }

        if (this.motor == null) {
            throw new IllegalArgumentException("Motor can't be null");
        }

        if (this.motorType == null) {
            throw new IllegalArgumentException("Motor type can't be null");
        }

        if (this.cruiseAltitude == 0) {
            throw new IllegalArgumentException("Cruise altitude can't be zero");
        }

        if (this.cruiseSpeed == 0) {
            throw new IllegalArgumentException("Cruise speed can't be zero");
        }

        if (this.TSFC == 0) {
            throw new IllegalArgumentException("TSFC can't be zero");
        }

        if (this.lapseRateFactor == 0) {
            throw new IllegalArgumentException("Lapse Rate Factor can't be zero");
        }

        if (this.cruiseAltitude == 0) {
            throw new IllegalArgumentException("Cruise altitude can't be zero");
        }

        if (this.thurst_max_speed == 0) {
            throw new IllegalArgumentException("Thrust max speed can't be zero");
        }

        if (this.thurst_0 == 0) {
            throw new IllegalArgumentException("Thrust_0 can't be zero");
        }

        if (this.max_speed == 0) {
            throw new IllegalArgumentException("Max speed can't be zero");
        }

        return true;
    }
}

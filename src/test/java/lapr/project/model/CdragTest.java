/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Utilizador
 */
public class CdragTest {
    private Cdrag cdrag;
    public CdragTest() {
        cdrag= new Cdrag();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getSpeed method, of class Cdrag.
     */
    @Test
    public void testGetAndSetSpeed() {
        System.out.println("getSpeed");
        Cdrag instance =cdrag;
        double expResult = 1;
        instance.setSpeed(expResult);
        double result = instance.getSpeed();
        assertEquals(expResult, result, 0.0);
      
    }


    /**
     * Test of getCdrag_0 method, of class Cdrag.
     */
    @Test
    public void testGetAndSetCdrag_0() {
        System.out.println("getCdrag_0");
        Cdrag instance = cdrag;
        double expResult = 1;
        instance.setCdrag_0(expResult);
        double result = instance.getCdrag_0();
        assertEquals(expResult, result, 0.0);
     
    }

    /**
     * Test of getSpeed method, of class Cdrag.
     */
    @Test
    public void testGetSpeed() {
        System.out.println("getSpeed");
        Cdrag instance = new Cdrag();
        double expResult = 0.0;
        instance.setSpeed(expResult);
        double result = instance.getSpeed();
        assertEquals(expResult, result, 0.0);
 
    }

    /**
     * Test of setSpeed method, of class Cdrag.
     */
    @Test
    public void testSetSpeed() {
        System.out.println("setSpeed");
        double speed = 0.0;
        Cdrag instance = new Cdrag();
        instance.setSpeed(speed);
        assertEquals(speed,instance.getSpeed(),0.0);
    }

    /**
     * Test of getCdrag_0 method, of class Cdrag.
     */
    @Test
    public void testGetCdrag_0() {
        System.out.println("getCdrag_0");
        Cdrag instance = new Cdrag();
        double expResult = 0.0;
        instance.setCdrag_0(expResult);
        double result = instance.getCdrag_0();
        assertEquals(expResult, result, 0.0);
   
    }

    /**
     * Test of setCdrag_0 method, of class Cdrag.
     */
    @Test
    public void testSetCdrag_0() {
        System.out.println("setCdrag_0");
        double Cdrag_0 = 0.0;
        Cdrag instance = new Cdrag();
        instance.setCdrag_0(Cdrag_0);
        assertEquals(Cdrag_0,instance.getCdrag_0(),0.0);
    }

    /**
     * Test of validate method, of class Cdrag.
     */
    @Test
    public void testValidate() {
        System.out.println("validate");
        Cdrag instance = new Cdrag();
        boolean expResult = true;
        boolean result = instance.validate();
        assertEquals(expResult, result);

    }

    /**
     * Test of hashCode method, of class Cdrag.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Cdrag instance = new Cdrag();
        int expResult = 26645;
        int result = instance.hashCode();
        assertEquals(expResult, result);

    }

    /**
     * Test of equals method, of class Cdrag.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = (Cdrag) new Cdrag();
        Cdrag instance = new Cdrag();
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
        instance.setSpeed(10);
        expResult = false;
        result = instance.equals(obj);
        assertEquals(expResult,result);
    }

    /**
     * Test of toString method, of class Cdrag.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Cdrag instance = new Cdrag();
        double speed = 10.0;
        double Cdrag_0 = 0.25;
        instance.setSpeed(speed);
        instance.setCdrag_0(Cdrag_0);
        String expResult = "Cdrag{" + "speed=" + speed + ", Cdrag_0=" + Cdrag_0 + '}';;
        String result = instance.toString();
        assertEquals(expResult, result);
  
    }

   

  

  
    

    
}

CREATE OR REPLACE PROCEDURE INSERTPROJECT (project_name PROJECT.NAME%TYPE, 
                                          project_description PROJECT.DESCRIPTION%TYPE)
IS
BEGIN
  INSERT INTO PROJECT(id_project, name, description) 
  VALUES(seq_Project.NEXTVAL, project_name, project_description); 
END insertproject;

/

  CREATE OR REPLACE PROCEDURE UPDATEPROJECT (oldProjectName PROJECT.NAME%TYPE, 
										  newProjectName PROJECT.NAME%TYPE, 
                                          newProjectDescription PROJECT.DESCRIPTION%TYPE)
IS
  p_id project.id_project%TYPE;
BEGIN
  SELECT id_project into p_id FROM project where oldProjectName=name;
  UPDATE project SET name=newProjectName, description=newProjectDescription WHERE p_id=id_project;
END updateProject;

/


  CREATE OR REPLACE FUNCTION GETPROJECTLIST
return sys_refcursor
as curNameProjects sys_refcursor;
begin
 Open curNameProjects for SELECT name FROM Project ORDER BY id_project ASC;
  return curNameProjects;

end getprojectlist;


/
  CREATE OR REPLACE FUNCTION GETDESCRIPTIONPROJECT(p_name in varchar2) 
 return varchar2 
as 
  v_proj_description    project.description%type;  
begin
  select description  into v_proj_description
  from project P where P.name = p_name;  
  return v_proj_description;
    
end getdescriptionproject;

/

  CREATE OR REPLACE FUNCTION GETJUNCTIONLIST
(
  p_name in varchar2 
) return sys_refcursor
as 
  curJunctions       sys_refcursor;
  project_id       project.id_project%type;

begin
  SELECT id_project into project_id
  FROM Project P
  WHERE P.name = p_name;
  
  Open curJunctions 
    for 
      SELECT *
      FROM Junction J
      WHERE J.id_project = project_id;
      
      return curJunctions;
      
end getjunctionlist;
/

CREATE OR REPLACE FUNCTION GETSEGMENTLIST
(
  p_name in varchar2 
) return sys_refcursor
as 
  curSegments      sys_refcursor;
  project_id       project.id_project%type;

begin
  SELECT id_project into project_id
  FROM Project P
  WHERE P.name = p_name;
  
  Open curSegments
    for 
      SELECT *
      FROM Segment s
      WHERE S.id_project = project_id;
      
      return curSegments;
      
end getsegmentlist;
/


CREATE OR REPLACE FUNCTION GETAIRPORTLIST
(
  p_name in varchar2 
) return sys_refcursor
as 
  curAirports      sys_refcursor;
  project_id       project.id_project%type;

begin
  SELECT id_project into project_id
  FROM Project P
  WHERE P.name = p_name;
  
  Open curAirports
    for 
      SELECT *
      FROM Airport A
      WHERE A.id_project = project_id;
      
      return curAirports;
      
end getAirportlist;
/


  CREATE OR REPLACE FUNCTION GETAIRCRAFTLIST 
(
  name_proj in varchar2 
) return sys_refcursor
as 
  curAircrafts       sys_refcursor;
  v_proj_id           project.id_project%type;
begin  
  SELECT id_project   into v_proj_id
  FROM Project P
  WHERE P.name = name_proj;
  Open curAircrafts
      for 
          SELECT *
          FROM Aircraft A 
          WHERE A.id_project = v_proj_id;
            
      return curAircrafts; 
 
end getaircraftlist;
/


CREATE OR REPLACE FUNCTION GETFLIGHTLIST
(
  p_name in varchar2 
) return sys_refcursor
as 
  curFlights      sys_refcursor;
  project_id       project.id_project%type;

begin
  SELECT id_project into project_id
  FROM Project P
  WHERE P.name = p_name;
  
  Open curFlights
    for 
      SELECT *
      FROM Flight f
      WHERE F.id_project = project_id;
      
      return curFlights;
      
end getflightlist;
/

CREATE OR REPLACE FUNCTION GETFLIGHTPLANLIST
(
  p_name in varchar2 
) return sys_refcursor
as 
  curPlans      sys_refcursor;
  project_id       project.id_project%type;

begin
  SELECT id_project into project_id
  FROM Project P
  WHERE P.name = p_name;
  
  Open curPlans
    for 
      SELECT *
      FROM Flightplan FP
      WHERE FP.id_project = project_id;
      
      return curPlans;
      
end getflightplanlist;
/

  CREATE OR REPLACE FUNCTION GETCDRAGLIST
(
  am_id AIRCRAFTMODEL.modelID%TYPE
) return sys_refcursor
as 
  curDrag       sys_refcursor;
  model_id   AIRCRAFTMODEL.ID%TYPE;
begin  

 SELECT id into model_id
  FROM AIRCRAFTMODEL AM
  WHERE AM.modelID = am_id;

  Open curDrag
      for 
          SELECT C.*
          FROM CDRAG C
          WHERE C.id_aircraftmodel = model_id;
            
      return curDrag; 
 
end getcdraglist;
/




CREATE OR REPLACE FUNCTION GETFLIGHTPLANSEGMENTS
(
 name_proj PROJECT.NAME%type,
  fp_name FLIGHTPLAN.name%TYPE
) return sys_refcursor
as 
  curFSegments      sys_refcursor;
   plan_id   FLIGHTPLAN.ID%TYPE;
   v_proj_id    project.id_project%type;
begin  

 SELECT id into plan_id
  FROM FLIGHTPLAN FP
  WHERE FP.name = fp_name;
  
  SELECT id_project   into v_proj_id
  FROM Project P
  WHERE P.name = name_proj;


  Open curFSegments
      for 
          SELECT S.*
          FROM SEGMENT S
          WHERE S.id= (SELECT FPS.ID_SEGMENT
                       FROM FLIGHTPLAN_SEGMENT FPS , SEGMENT S, FLIGHTPLAN FP      
                        WHERE FPS.ID_FLIGHTPLAN=PLAN_ID AND FP.id_project = v_proj_id AND s.id_project = v_proj_id);
            
      return curFSegments; 
 
end getflightplansegments;
/




CREATE OR REPLACE FUNCTION GETJUNCTION(j_id in number) 
 return varchar2 
as 
  j_name    junction.name%type;  
begin
  select name  into j_name
  from JUNCTION J where J.id = j_id;  
  return j_name;
    
end getJunction;

/ 

CREATE OR REPLACE FUNCTION GETAircraft(a_id in number) 
 return varchar2 
as 
 a_name    aircraft.registration%TYPE;  
begin
  select registration  into a_name
  from AIRCRAFT A where A.id = a_id;  
  return a_name;
    
end getAircraft;

/

CREATE OR REPLACE FUNCTION GETAirport(a_id in number) 
 return varchar2 
as 
 a_name    airport.IATA%TYPE;  
begin
  select IATA  into a_name
  from AIRPORT A where A.id = a_id;  
  return a_name;
    
end getAirport;

/

CREATE OR REPLACE FUNCTION GETFlightPlan(a_id in number) 
 return varchar2 
as 
 a_name    FLIGHTPLAN.NAME%TYPE;  
begin
  select name  into a_name
  from FLIGHTPLAN FP where fp.id = a_id;  
  return a_name;
    
end getFlightPlan;

/


create or replace function getaircraftmodel
(
 
  id_model AIRCRAFTMODEL.ID%type 
) return sys_refcursor
as 
  curModel      sys_refcursor;
  
begin
  
   Open curModel 
      for 
      SELECT *
          FROM AIRCRAFTMODEL AM 
          WHERE AM.ID = id_model;
          
       return curModel;
end getaircraftmodel;
/


create or replace function getmotorization
(
  
  id_motor MOTORIZATION.ID%type
) return sys_refcursor
as 
  curMotor      sys_refcursor;
 
begin
  
   Open curMotor 
      for 
      SELECT *
          FROM MOTORIZATION M
          WHERE M.ID = id_motor;
          
       return curMotor;
end getmotorization;
/

create or replace function getwind
(
  
  id_wind WIND.ID%type 
) return sys_refcursor
as 
  curWind      sys_refcursor;
 
  
begin
  
   Open curWind 
      for 
      SELECT *
          FROM WIND W 
          WHERE W.ID = id_wind;
          
       return curWind;
end getwind;
/



  CREATE OR REPLACE PROCEDURE DELETEPROJECT(projName PROJECT.NAME%TYPE)
IS
BEGIN
  Delete project where name=projName;
END deleteProject;
/

  CREATE OR REPLACE PROCEDURE INSERTJUNCTION(project_name PROJECT.NAME%TYPE, junction_latitude JUNCTION.LATITUDE%TYPE,
                                         junction_longitude JUNCTION.LONGITUDE%TYPE,
                                          junction_name JUNCTION.NAME%TYPE)
IS
  i_project PROJECT.ID_PROJECT%TYPE;
BEGIN
    SELECT id_project into i_project FROM project WHERE NAME=project_name;
    
    INSERT INTO JUNCTION(id,latitude,longitude,id_project, name) 
    VALUES(seq_Junction.NEXTVAL, junction_latitude, junction_longitude, i_project, junction_name);
  
END insertJunction;
/

  CREATE OR REPLACE PROCEDURE INSERTSEGMENT(project_name PROJECT.NAME%TYPE, segment_direction SEGMENT.DIRECTION%TYPE,
                                          startNode JUNCTION.NAME%TYPE, endNode JUNCTION.NAME%TYPE,
                                          segment_name SEGMENT.NAME%TYPE, wind_name WIND.NAME%TYPE)
IS
  i_project PROJECT.ID_PROJECT%TYPE;
  s_BEGIN_NODE_ID JUNCTION.ID%TYPE;
  s_END_NODE_ID JUNCTION.ID%TYPE;
  s_WIND_ID WIND.ID%TYPE;
  
  
  
  
BEGIN
    SELECT P.id_project into i_project FROM project P WHERE p.NAME=project_name;
    SELECT j1.id into s_BEGIN_NODE_ID FROM junction J1 WHERE j1.name=startNode  AND j1.id_project=i_project;
    SELECT j2.id into s_END_NODE_ID FROM junction J2 WHERE j2.name=endNode  AND j2.id_project=i_project;
    SELECT w.id into s_WIND_ID FROM wind W WHERE W.NAME = wind_name;
      
    
    INSERT INTO SEGMENT(id,direction,startNode,endNode,id_project,id_wind,name) 
    VALUES(seq_Segment.NEXTVAL, segment_direction,s_begin_node_id,s_end_node_id,i_project,s_wind_id,segment_name);
  
END insertSegment;
/

  CREATE OR REPLACE PROCEDURE INSERTWIND(wind_angle WIND.WINDANGLE%TYPE, 
                                         wind_speed WIND.WINDSPEED%TYPE,
                                         wind_name WIND.NAME%TYPE)
                                         
IS
  
BEGIN
   
    INSERT INTO WIND(id,windAngle,windSpeed,name) 
    VALUES(seq_Wind.NEXTVAL, wind_angle,wind_speed,wind_name);
  
END insertWind;
/

  CREATE OR REPLACE PROCEDURE INSERTAIRPORT(project_name PROJECT.NAME%TYPE, airport_name AIRPORT.NAME%TYPE,
                                          airport_town AIRPORT.TOWN%TYPE, airport_country AIRPORT.COUNTRY%TYPE, 
                                          airport_longitude AIRPORT.LONGITUDE%TYPE, airport_latitude AIRPORT.LATITUDE%TYPE,
                                          airport_altitude AIRPORT.ALTITUDE%TYPE,airport_IATA AIRPORT.IATA%TYPE )
IS
  i_project PROJECT.ID_PROJECT%TYPE;


  
BEGIN
    SELECT P.id_project into i_project FROM project P WHERE p.NAME=project_name;
    
    
    INSERT INTO AIRPORT(id,name,town,country,longitude,latitude,altitude,id_project,IATA) 
    VALUES(seq_Airport.NEXTVAL, airport_name,airport_town, airport_country,airport_longitude,airport_latitude,airport_altitude,i_project,airport_IATA);
  
END insertAirport;
/

  CREATE OR REPLACE PROCEDURE INSERTFLIGHT(project_name PROJECT.NAME%TYPE, flight_type FLIGHT.TYPE%TYPE,
                                          flight_departure FLIGHT.DEPARTUREDAY%TYPE,  flight_minStop FLIGHT.MINSTOPTIME%TYPE, 
                                           flight_arrival FLIGHT.SCHEDULEARRIVAL%TYPE,  flightplan_name FLIGHTPLAN.NAME%TYPE,
                                           flight_name FLIGHT.NAME%TYPE )
IS
  i_project PROJECT.ID_PROJECT%TYPE;
  i_flight FLIGHTPLAN.ID%TYPE;


  
BEGIN
    SELECT P.id_project into i_project FROM project P WHERE p.NAME=project_name;
    SELECT FP.id into i_flight FROM flightplan FP WHERE fp.NAME=flightplan_name AND fp.id_project=i_project;  
    
    
    INSERT INTO FLIGHT(id,id_project,type,departureDay,minStopTime,scheduleArrival,id_flightplan,name) 
    VALUES(seq_Flight.NEXTVAL, i_project,flight_type,flight_departure,flight_minStop,flight_arrival,i_flight,flight_name);
  
END insertFlight;
/

  CREATE OR REPLACE PROCEDURE INSERTFLIGHTPLAN(project_name PROJECT.NAME%TYPE,  origin_Airfield AIRPORT.IATA%TYPE, 
                                           destiny_Airfield AIRPORT.IATA%TYPE,  aircraft_name AIRCRAFT.REGISTRATION%TYPE,
                                           plan_name FLIGHTPLAN.NAME%TYPE)
                                          
IS
  i_project PROJECT.ID_PROJECT%TYPE;
  i_aircraft AIRCRAFT.ID%TYPE;
  i_air1 AIRPORT.ID%TYPE;
  i_air2 AIRPORT.ID%TYPE;
   
  
BEGIN
    SELECT P.id_project into i_project FROM project P WHERE p.NAME=project_name;
    SELECT A.id into i_aircraft FROM aircraft A WHERE a.registration=aircraft_name AND a.id_project=i_project;  
    SELECT AIR1.id into i_air1 FROM airport AIR1 WHERE air1.IATA= origin_Airfield AND air1.id_project=i_project;
    SELECT AIR2.id into i_air2 FROM airport AIR2 WHERE air2.IATA= destiny_Airfield AND air2.id_project=i_project;
    
    INSERT INTO FLIGHTPLAN(name,originAirfield,destinyAirfield,id_project,id,id_aircraft) 
    VALUES(plan_name,i_air1,i_air2,i_project,seq_FlightPlan.NEXTVAL,i_aircraft);
  
END insertFlightPlan;
/

  CREATE OR REPLACE PROCEDURE INSERTAIRCRAFT(project_name PROJECT.NAME%TYPE, aircraft_registration AIRCRAFT.REGISTRATION%TYPE,
                                          aircraft_company AIRCRAFT.COMPANY%TYPE,  aircraft_numberEco AIRCRAFT.NUMBERSEATSECO%TYPE, 
                                          aircraft_nElem AIRCRAFT.NELEMCREW%TYPE,  model_id AIRCRAFTMODEL.MODELID%TYPE,
                                           aircraft_numberExec AIRCRAFT.NUMBERSEATSEXEC%TYPE)
                                          
IS
  i_project PROJECT.ID_PROJECT%TYPE;
  i_model AIRCRAFTMODEL.ID%TYPE;

   
  
BEGIN
    SELECT P.id_project into i_project FROM project P WHERE p.NAME=project_name;
    SELECT AM.id into i_model FROM aircraftmodel AM WHERE am.modelid=model_id;  
    
    INSERT INTO AIRCRAFT(registration,company,numberSeatsEco,nElemCrew,id_project,id_aircraftmodel,id,numberSeatsExec) 
    VALUES(aircraft_registration,aircraft_company,aircraft_numberEco,aircraft_nElem,i_project,i_model,seq_Aircraft.NEXTVAL,aircraft_numberExec);
  
END insertAircraft;
/

  CREATE OR REPLACE PROCEDURE INSERTAIRCRAFTMODEL(model_maker AIRCRAFTMODEL.MAKER%TYPE,model_empty AIRCRAFTMODEL.EMPTY_WEIGHT%TYPE,
                                        model_MTOW AIRCRAFTMODEL.MTOW%TYPE, model_payload AIRCRAFTMODEL.MAXPAYLOAD%TYPE,
                                        model_fuel AIRCRAFTMODEL.MAXIMUMFUELCAPACITY%TYPE, model_wingArea AIRCRAFTMODEL.WINGAREA%TYPE,
                                        model_wingSpan AIRCRAFTMODEL.WINGSPAN%TYPE, model_e AIRCRAFTMODEL.E%TYPE,
                                        model_VMO AIRCRAFTMODEL.VMO%TYPE, model_MMO AIRCRAFTMODEL.MMO%TYPE,
                                        model_type AIRCRAFTMODEL.TYPE%TYPE, model_aspectRatio AIRCRAFTMODEL.ASPECTRATIO%TYPE,
                                        motor_name MOTORIZATION.NAME%TYPE, model_id AIRCRAFTMODEL.MODELID%TYPE)                                     
                                    
IS

  i_motor MOTORIZATION.ID%TYPE;

BEGIN
    SELECT M.id into i_motor FROM MOTORIZATION M WHERE m.name=motor_name;  
    
    INSERT INTO AIRCRAFTMODEL(id,maker,empty_weight,MTOW,maxPayLoad,MaximumFuelCapacity,WingArea,WingSpan,e,VMO,MMO,Type,aspectRatio,id_motorization,modelID) 
    VALUES(seq_AircraftModel.NEXTVAL,model_maker,model_empty,model_MTOW,model_payload,model_fuel,model_wingArea,model_wingSpan,model_e,model_VMO,model_MMO,model_type,model_aspectRatio,i_motor,model_id);
  
END insertAircraftModel;
/

  CREATE OR REPLACE PROCEDURE INSERTMOTORIZATION(motor_type MOTORIZATION.MOTORTYPE%TYPE,motor_number MOTORIZATION.NUMBERMOTORS%TYPE,
                                       motor_cruiseSpeed MOTORIZATION.CRUISESPEED%TYPE, motor_cruiseAlt MOTORIZATION.CRUISEALTITUDE%TYPE,
                                        motor_TSFC MOTORIZATION.TSFC%TYPE, motor_lapseRate MOTORIZATION.LAPSERATEFACTOR%TYPE,
                                        motor_thrust0 MOTORIZATION.THRUST_0%TYPE,motor_thrustMax MOTORIZATION.THRUST_MAX_SPEED%TYPE,
                                        motor_maxSpeed MOTORIZATION.MAX_SPEED%TYPE,motor_name MOTORIZATION.NAME%TYPE)                                     
                                    
IS

BEGIN
   
    
    INSERT INTO MOTORIZATION(id,motorType,numberMotors,cruiseSpeed,cruiseAltitude,TSFC,lapseRateFactor,thrust_0,thrust_max_speed,max_speed,name) 
    VALUES(seq_Motorization.NEXTVAL,motor_type,motor_number,motor_cruiseSpeed,motor_cruiseAlt,motor_TSFC,motor_lapseRate,motor_thrust0,motor_thrustMax,motor_maxSpeed,motor_name);
  
END insertMotorization;
/

  CREATE OR REPLACE PROCEDURE INSERTCDRAG(drag_speed CDRAG.SPEED%TYPE,drag_0 CDRAG.CDRAG_0%TYPE, model_id AIRCRAFTMODEL.MODELID%TYPE)                                     
                                    
IS

 i_model AIRCRAFTMODEL.ID%TYPE;

BEGIN

    SELECT AM.id into i_model FROM aircraftmodel AM WHERE am.modelid=model_id;  
    
    INSERT INTO CDRAG(speed,Cdrag_0,id_aircraftmodel,id) 
    VALUES(drag_speed,drag_0,i_model,seq_Cdrag.NEXTVAL);
  
END insertCdrag;
/


CREATE OR REPLACE PROCEDURE INSERTFLIGHTPLANSEGMENTS(segment_name SEGMENT.NAME%TYPE,plan_name FLIGHTPLAN.NAME%TYPE)                                     
                                    
IS

 i_segment SEGMENT.ID%TYPE;
 i_plan FLIGHTPLAN.ID%TYPE;

BEGIN

    SELECT S.id into i_segment FROM SEGMENT S WHERE S.name=segment_name;  
    SELECT FP.id into i_plan FROM FlIGHTPLAN FP WHERE FP.name=plan_name;  
    
    INSERT INTO FLIGHTPLAN_SEGMENT(id_flightplan,id_segment) 
    VALUES(i_plan,i_segment);
  
END insertFLIGHTPLANSEGMENTS;
/


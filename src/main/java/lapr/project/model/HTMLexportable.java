/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

/**
 *
 * @author cdsn_
 */
public interface HTMLexportable {

    /**
     * Textual description of the object in the HTML format.
     *
     * @return textual description of the object in HTML.
     */
    public String toStringHTML();
}


package lapr.project.model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 *
 */
public class ExportableRegistry {

    /**
     * Map of exportables where each file format corresponds to an object that
     * imports it.
     */
    private Map<String, Exportable> exportablesList;

    /**
     * Creates an instance of Exportable Registry.
     */
    public ExportableRegistry() {
        this.exportablesList = new HashMap<>();
        this.exportablesList.put(".html", new ExportHTML());
    }

    public Exportable getExportableType(String path) {
        int indexOfType = path.lastIndexOf(".");

        // Testing if the given path contains a valid file format.
        if (indexOfType == -1) {
            throw new IllegalArgumentException("The given path contains a invalid file format");
        }

        // Extracting the file type.
        String type = path.substring(indexOfType);

        if (this.exportablesList.containsKey(type)) {
            return this.exportablesList.get(type);
        }

        throw new IllegalArgumentException("There is no exporter to the selected file type.");
    }
}

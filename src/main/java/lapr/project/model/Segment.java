
package lapr.project.model;

import java.util.Objects;

/**
 *
 *
 */
public class Segment {

    private String name;
    private Wind windStats;
    private Junction startNode;
    private Junction endNode;
    private String direction;

    //private boolean altitudeSlots; ?? verificar que tipo isto e
    public Segment(String name, Wind windStats, Junction startNode, Junction endNode, String direction) {
        this.name = name;
        this.windStats = windStats;
        this.startNode = startNode;
        this.endNode = endNode;
        this.direction = direction;
    }

    public Segment() {
        this.windStats = new Wind();

    }

    /**
     * @return the windAngle
     */
    public double getWindAngle() {
        return this.windStats.getWindAngle();
    }

    /**
     * @param windAngle the windAngle to set
     */
    public void setWindAngle(double windAngle) {
        this.windStats.setWindAngle(windAngle);
    }

    /**
     * @return the windSpeed
     */
    public double getWindSpeed() {
        return this.windStats.getWindSpeed();
    }

    /**
     * @param windSpeed the windSpeed to set
     */
    public void setWindSpeed(double windSpeed) {
        this.windStats.setWindSpeed(windSpeed);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Wind getWindStats() {
        return windStats;
    }

    public void setWindStats(Wind windStats) {
        this.windStats = windStats;
    }

    public Junction getStartNode() {
        return startNode;
    }

    public void setStartNode(Junction startNode) {
        this.startNode = startNode;
    }

    public Junction getEndNode() {
        return endNode;
    }

    public void setEndNode(Junction endNode) {
        this.endNode = endNode;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "Name=" + name;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.name);
        hash = 47 * hash + Objects.hashCode(this.windStats);
        hash = 47 * hash + Objects.hashCode(this.startNode);
        hash = 47 * hash + Objects.hashCode(this.endNode);
        hash = 47 * hash + Objects.hashCode(this.direction);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Segment other = (Segment) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.direction, other.direction)) {
            return false;
        }
        if (!Objects.equals(this.windStats, other.windStats)) {
            return false;
        }
        if (!Objects.equals(this.startNode, other.startNode)) {
            return false;
        }
        if (!Objects.equals(this.endNode, other.endNode)) {
            return false;
        }
        return true;
    }

    /**
     * Validate the segment.
     *
     * @return True if segment is valid else returns false.
     */
    public boolean validate() {

        windStats.validate();

        if (this.name == null) {
            throw new IllegalArgumentException("Id can't be null");
        }

        if (this.startNode == null) {
            throw new IllegalArgumentException("startNode can't be null");
        }

        if (this.endNode == null) {
            throw new IllegalArgumentException("endNode can't be null");
        }

        return true;
    }

}

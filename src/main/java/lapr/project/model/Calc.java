package lapr.project.model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 *
 */
public class Calc {

    /*
     * heat ratio at constant volume
     */
    private static final double GAMMA = 1.4; //for air
    /**
     * meduim weight of a human - passanger
     *
     * The gender and being irre
     */
    private static final double PESOHUMANO = 75.0; //kg
    /**
     * earth´s radius in meters
     */
    private static final double RAIOTERRA = 6371e3;
    /**
     * gradiente adiabatico K/m
     */
    private static final double L = 0.0065;

    /**
     * air density at sea level
     *
     */
    private static final double D0 = 1.225; //  kg/m^3

    /**
     * the temperature in the sea level in Kelvin
     */
    private static final double T0 = 288.2;
    /**
     * atmosphere at sea level
     */
    private static final double P0 = 101325;
    /**
     * Gravity accelaration in meters square
     */
    private static final double G = 9.80665;

    /**
     * universal gas constant
     */
    private static final double R = 287; //N-m/K

    /**
     * temperature lapse rate
     */
    private static final double LAPSE = -0.0065; //K/m

    private double _distance;
    private double _liftForce;
    private double _dragForce;
    private double _rangeAircraft;

    /**
     * constructor
     */
    public Calc() {

        this._distance = 0.0;
        this._liftForce = 0.0;
        this._dragForce = 0.0;
        this._rangeAircraft = 0.0;
    }

    /**
     * @return the _rangeAircraft
     */
    public double getRangeAircraft() {
        return _rangeAircraft;
    }

    /**
     * @param rangeAircraft the _rangeAircraft to set
     */
    public void setRangeAircraft(double rangeAircraft) {
        this._rangeAircraft = rangeAircraft;
    }

    /**
     * @return the _distance
     */
    public double getDistance() {
        return _distance;
    }

    /**
     * @param distance the _distance to set
     */
    public void setDistance(double distance) {
        this._distance = distance;
    }

    /**
     * @return the _liftForce
     */
    public double getLiftForce() {
        return _liftForce;
    }

    /**
     * @param liftForce the _liftForce to set
     */
    public void setLiftForce(double liftForce) {
        this._liftForce = liftForce;
    }

    /**
     * @return the _dragForce
     */
    public double getDragForce() {
        return _dragForce;
    }

    /**
     * @param dragForce the _dragForce to set
     */
    public void setDragForce(double dragForce) {
        this._dragForce = dragForce;
    }

    /**
     * Formula to calculate the distance between two airports using the
     * geographic coordenates
     *
     * Javascript: var φ1 = lat1.toRadians(), φ2 = lat2.toRadians(), Δλ =
     * (lon2-lon1).toRadians(), R = 6371e3; // gives d in metres var d =
     * Math.acos( Math.sin(φ1)*Math.sin(φ2) + Math.cos(φ1)*Math.cos(φ2) *
     * Math.cos(Δλ) ) * R;
     *
     * method more accurate but not the most efficient
     *
     *
     *
     * @param initialLatitude
     * @param initialLongitude
     * @param finalLatitude
     * @param finalLongitude
     */
    public void CalculateDistance(double initialLatitude, double initialLongitude, double finalLatitude, double finalLongitude) {

        double o1 = Math.toRadians(initialLatitude);
        double o2 = Math.toRadians(finalLatitude);
        double variacaoLong = Math.toRadians((finalLongitude) - (initialLongitude));

        double distance = Math.acos(Math.sin(o1) * Math.sin(o2) + Math.cos(o1) * Math.cos(o2) * Math.cos(variacaoLong)) * RAIOTERRA;

        //convert distance of meters to kilometres
        this._distance = distance / 1000.0;
    }

    //method that calculates the drag force of an aircraft
    /**
     *
     * p (density of the air) = 1.225 kg / m3
     *
     * formulas :
     *
     * cl (coeenficent lift)
     *
     * cl = 2 * m * g / p * A * v^2
     *
     * g being gravity force = 9.8
     *
     * AR
     *
     * AR = ws^2 / wa
     *
     * Cd (coefficent drag)
     *
     * Cd = Cd0 + Cl^2 / pi * AR * e
     *
     *
     *
     * D(drag force) 𝐷 = Cd * (𝜌 * 𝑣 ^2 / 2) * 𝐴
     *
     * Reference area used is wing area
     *
     * @param mass
     * @param densityAir
     * @param wingArea
     * @param speed
     * @return
     */
    //includes method to calculate the cl = lift coefficient
    public double calculateLiftCoefficent(double mass, double densityAir, double wingArea, double speed) {

        return (2 * mass * G) / (densityAir * wingArea * Math.pow(speed, 2));
    }

    //method that calculates the drag coefficent
    //AR = wing span ^2 / wing area
    public double calculateDragCoefficent(double mass, double densityAir, double speed, double cdrag0, double wingspan, double wingarea, double e) {

        double AR = Math.pow(wingspan, 2) / wingarea;

        double cL = calculateLiftCoefficent(mass, densityAir, wingarea, speed);

        return cdrag0 + ((Math.pow(cL, 2)) / (Math.PI * AR * e));
    }

    /**
     *
     * @param densityAir
     * @param mass
     * @param cdrag0
     * @param wingArea
     * @param wingSpan
     * @param speed
     * @param e
     * @return
     */
    public double CalculateDragForce(double densityAir, double mass, double cdrag0, double wingArea, double wingSpan, double speed, double e) {
        double cd = calculateDragCoefficent(mass, densityAir, speed, cdrag0, wingSpan, wingArea, e);
        double middle = densityAir * Math.pow(speed, 2) / 2;
        return cd * middle * wingArea;

    }

    //method that will calculate the lift force of an aircraft
    /**
     * Lift coeffiency (cl) = (2 * m * g) / (p * A * v^2)
     *
     * p(density of the air) = 1.225 g (gravitic force) = 9.8
     *
     * Lift Force (L) = cL * (p * v ^2 / 2) * A
     *
     * Reference area used is wing area
     *
     * @param a
     * @param payload
     * @param pressure
     * @param temperature
     * @param fuel
     * @return
     */
    public double CalculateLiftForce(AircraftModel a, double payload, double fuel, double pressure, double temperature, double machNumber, double speedSound) {

        double aircraft_mass = calculateMass(a.getEmptyWeight(), payload, fuel);

        double densityAir = calculateAirDensity(pressure, temperature);
        double v = calculateTrueAirSpeed(machNumber, speedSound);
        double cL = calculateLiftCoefficent(aircraft_mass, densityAir, a.getWingArea(), v);

        return cL * ((densityAir * Math.pow(v, 2)) / 2) * a.getWingArea();
    }

    //method to calculate a mass using the weight of an aircraft
    public double calculateMass(double emptyWeight, double payload, double fuel) {

        return emptyWeight + payload + fuel;
    }

    /**
     * method that calculates the air density based on Temperature and height
     * numerator denominator (p0((1-(Lh/T0))^(gM/RL))*M) / (R*(T0-Lh))
     *
     * @param pressure
     * @param temperature
     * @return
     */
    public double calculateAirDensity(double pressure, double temperature) {

        return pressure / (R * temperature);
    }

    /**
     * method that calculates true air speed
     *
     * @param machNumber
     * @param speedSound
     * @param airDensity
     * @param windSpeed
     * @return
     */
    //true air speed can be calculated with the relation (TruemachNumber * speedofSound)
    /*public double calculateTrueAirSpeed(double airDensity, double windSpeed) {
        
        //calculate the dynamic pressure
        // q = 1/2 * airDensity * v ^2
        double q = (airDensity * Math.pow(windSpeed,2)) / 2;
        
        //calculate equivalent airspeed
        
        double eas = Math.sqrt((2 * q) / P0);
        
        return eas * (Math.sqrt(P0 / airDensity));
    }*/
    public double calculateTrueAirSpeed(double machNumber, double speedSound) {

        return machNumber * speedSound;
    }

    //method that calculates the final weight of an aircraft using the emptyWeight of the same
    //including the passenger and the crew
    public double calculateInitialWeight(double payload, double fuel, AircraftModel a, int passangers, int crew) {

        double wi;

        wi = a.getEmptyWeight() + (passangers * PESOHUMANO) + (crew * PESOHUMANO) + payload + fuel;

        return wi;
    }

    /**
     * method that calculates the amount of fuel carried for the trip
     *
     * @param initialWeight
     * @param payload
     * @param crew
     * @param passengers
     * @param emptyWeight
     * @return
     */
    public double calculateTripFuel(double initialWeight, double payload, int crew, int passengers, double emptyWeight) {
        return initialWeight - emptyWeight - (payload + (crew * PESOHUMANO) + (passengers * PESOHUMANO));
    }

    //method that calculates the range of an aircraft in cruise flight
    /**
     * Formula to calculate the range of an aircraft
     *
     * R (range) = TAS(True Air Speed) / TSFC (thrust specific fuel consumption)
     * * (L(Lift Force) / D(Drag Force)) * ln (Wi(Initial Weight) / Wf(Final
     * weight))
     *
     * @param height
     * @param speed
     * @param a
     * @return
     */
    /**
     * Formula to calculate the range of an aircraft
     *
     * R (range) = TAS(True Air Speed) / TSFC (thrust specific fuel consumption)
     * * (L(Lift Force) / D(Drag Force)) * ln (Wi(Initial Weight) / Wf(Final
     * weight))
     *
     *
     * @param tas
     * @param tsfc
     * @param L
     * @param D
     * @param wi
     * @param wf
     * @return
     */
    public double calculateRangeAircraft(double tas, double tsfc, double L, double D, double wi, double wf) {

        double fst = tas / tsfc; //first operand

        double snd = L / D; //second operand, lift force / drag force

        double jFS = fst * snd; //multiplication of the first and second operand

        double trd = Math.log(wi / wf); //third operand

        //now to finish the equation and return the value of range
        return jFS * trd; //returns the distance that aircraft can fly with a certain fuel

    }

    /**
     * method to calculate dh / dt
     *
     * =(totalThurst-drag)*tas/mass/g
     *
     * @param totalThurst
     * @param drag
     * @param tas
     * @param mass
     * @return
     */
    //dh / dt - rate of climb or descent
    //rate of climb = its positive m/s
    //rate of descent = its negative m/s
    public double calculateDhDT(double totalThurst, double drag, double tas, double mass) {

        double fst = (totalThurst - drag) * tas; //first operand , tas = true air speed

        double snd = mass / G;

        return fst / snd;
    }

    /**
     * method that will calculate the dw / dt ratio
     *
     * totalThurst*timestep*tsfc/G
     *
     * @param totalThurst
     * @param timestep
     * @param tsfc
     * @return
     */
    //dw / dt = fuel burn
    public double calculateDwDt(double totalThurst, double timestep, double tsfc) {

        return (totalThurst * timestep) * (tsfc * G);
    }

    /**
     *
     * method to calculate the lambda
     *
     * (thurstMach-thurstNewtons)/thurst_0_Newtons
     *
     * @param thurstMach
     * @param thurstNewtons
     * @param thurst_0_Newtons
     * @return
     */
    public double calculateLAMBDA(double thurstMach, double thurstNewtons, double thurst_0_Newtons) {

        return (thurst_0_Newtons - thurstNewtons) / thurstMach;
    }

    /**
     * method that calculates the consume of an airplane in a certain distance
     *
     * @param tas
     * @param distance
     * @param fuel
     * @param payload
     * @param a
     * @param passangers
     * @param crew
     * @param tsfc
     * @param cdrag_0
     * @param pressure
     * @param temperature
     * @param machNumber
     * @param speedSound
     * @return Fuel consuption kg/h
     */
    //Transforming the formula of the range of the aircraft on the client´s statement we can calculate the weight of the fuel lost in the flight
    // There for know how much fuel was burn in weight
    public double calculateConsume(double tas, double distance, double fuel, double payload, AircraftModel a, int passangers, int crew, double tsfc, double cdrag_0, double pressure, double temperature, double machNumber, double speedSound) {

        double Lift = CalculateLiftForce(a, payload, fuel, pressure, temperature, machNumber, speedSound);

        double D = CalculateDragForce(calculateAirDensity(pressure, temperature), calculateMass(a.getEmptyWeight(), fuel, payload), cdrag_0, a.getWingArea(), a.getWingSpan(), calculateTrueAirSpeed(machNumber, speedSound), a.getE());
        double WI = calculateInitialWeight(payload, fuel, a, passangers, crew);
        double TSFC = tsfc;
        double F = (tas / TSFC) * (Lift / D);
        double wf = WI / (Math.pow(Math.E, distance / F));

        return wf - WI;

    }

    /**
     * method that will calculate the thrust of an aircraft given a certain
     * altitude
     *
     *
     * @param thrust
     * @param airDensity
     * @param lambda
     * @param lapseRate
     * @param mTrue
     * @return
     */
    public double calculateThurstClimb(double thrust, double airDensity, double lambda, double mTrue, double lapseRate) {

        double fst = thrust - (lambda * mTrue);

        double snd = Math.pow(airDensity / D0, lapseRate);

        return fst * snd;
    }

    /**
     * method that will calculate the thrust of an aircraft given a certain
     * altitude
     *
     *
     * @param thrust
     * @param airDensity
     * @param lambda
     * @param lapseRate
     * @param mTrue
     * @return
     */
    public double calculateThurstDescend(double thrust, double airDensity, double lambda, double mTrue, double lapseRate) {

        double fst = thrust - (lambda * mTrue);

        double snd = Math.pow(airDensity / D0, lapseRate);

        return 0.1 * fst * snd;
    }

    /**
     * Method to calculate the thurst in cruise
     *
     * @param totalThurst
     * @param numberMotors
     * @return
     */
    public double calculateThurstCruise(double totalThurst, int numberMotors) {

        return totalThurst / numberMotors;
    }

    /**
     * method that will calculate the total thurst of the airplane, with the
     * number of motors
     *
     * @param thurst
     * @param numberMotors
     * @return
     */
    //Total thurst = knowing the thurst of at least one motor, just multiply for the total of motor
    public double calculateTotalThurst(double thurst, int numberMotors) {

        return thurst * numberMotors;
    }

    /**
     * Method to calculate the crosswind
     *
     * @param windSpeed
     * @param angle
     * @return
     */
    /* 
    CW=sin(A) *  WS
    
    where :
    
    A = Angle from the wind of the direction of travel
    CW = Crosswind
    WS = The measure total windSpeed
     */
    public double calculateCrossWind(double windSpeed, double angle) {

        double sinA = Math.sin(Math.toRadians(angle)); //sin of the angle

        return sinA * windSpeed;
    }

    /**
     * Nethod to calculate the headwind
     *
     * @param windSpeed
     * @param angle
     * @return
     */
    /*
    HW=cos(A) *  WS
    
    where :
    
    A = Angle from the wind of the direction of travel
    HW = Headwind
    WS = The measure total windSpeed
     */
    public double calculateHeadWind(double windSpeed, double angle) {

        double cosA = Math.cos(Math.toRadians(angle)); //cos of the angle

        return cosA * windSpeed;
    }

    /**
     * Method that calculates the load factor of an aircraft given its lift
     * force and weight
     *
     * @param liftForce
     * @param weight
     * @return
     */
    /*
    n={  {L}  /{W}}
        where:

        n = Load factor
        L = Lift
        W = Weight
     */
    public double calculateLoadFactor(double liftForce, double weight) {

        return liftForce / weight;
    }

    /**
     * Method that will calculate the stall speed of an aircraft
     *
     * @param weight
     * @param wingArea
     * @param maxCL
     * @param airDensity
     * @param payload
     * @param fuel
     * @return
     */
    /*The exact formula is
        V = √( 2 W g / ρ S Clmax ) 

        Where,
            V = Stall Speed
            W = Weight of the aircraft
            g = 9.81 (Acc. Due to Gravity)
            ρ = Density of Air
            S = Wing Area
            Clmax = Coefficient of Lift at Stall*/
    public double calculateStallSpeed(double weight, double wingArea, double maxCL, double airDensity, double payload, double fuel) {

        double mass = calculateMass(weight, payload, fuel);

        double numerador = 2 * mass * G;

        double denominador = airDensity * wingArea * maxCL;

        return Math.sqrt(numerador / denominador);

    }

    /**
     * Method that calculates the flight angle of an aircraft
     *
     * @param thurst
     * @param drag
     * @param mass
     * @return
     */
    public double calculateFlightAngle(double thurst, double drag, double mass) {

        double numerador = thurst - drag;
        double denominador = mass * G;

        return Math.toDegrees(Math.asin(numerador / denominador));
    }

    /**
     * Method that calculates the Aspect ratio of the aircraft
     *
     * @param wingSpan
     * @param wingArea
     * @return
     */
    public double calculateAspectRatio(double wingSpan, double wingArea) {

        return Math.pow(wingSpan, 2) / wingArea;
    }

    /*
    Method to calculate the real distance traveled by the current aircraft
     */
    public double calculateDistanceTravelled(double tas, double angle, double timestep) {

        //angle = Climbing angle
        double cosen = Math.cos(angle);
        return tas * cosen * timestep;
    }

    /**
     * Method to increment the timestep at the current timestep
     *
     * @param previousTimeStep
     * @param timestep
     * @return
     */
    public double incrementTimeStep(double previousTimeStep, double timestep) {

        return previousTimeStep + timestep;
    }

    /**
     * Method to update the altitude of the aircraft
     *
     * @param timestep
     * @param altitude
     * @param dhDT
     * @return
     */
    //=altitude+dwDT*timestep
    public double updateAltitudeAircraft(double timestep, double altitude, double dhDT) {

        return altitude + (dhDT * timestep);
    }

    /**
     * Method to update the distance travelled by the aircraft
     *
     * @param previousDistance
     * @param currentDistance
     * @return
     */
    public double updateDistanceTravelled(double previousDistance, double currentDistance) {

        return previousDistance + currentDistance;
    }

    /**
     * method that will update the current mass of an aircraft during the flight
     *
     * return previousMass - dwDT
     *
     * @param previousMass
     * @param dwDT
     * @return
     */
    public double updateMassAircraft(double previousMass, double dwDT) {

        return previousMass - dwDT;
    }

    /**
     * Method to calculate the rate of climb of an aircraft while having the
     * other factors accounted for
     *
     * @param mass
     * @param TAS
     * @param drag
     * @param thurst
     * @return
     */
    public double calculateClimbRate(double mass, double TAS, double drag, double thurst) {

        return (thurst - drag) * TAS / mass * G;

    }

    /**
     * Method that calculates the climbing angle
     *
     * @param tas
     * @param climbRate
     * @return
     */
    public double calculateClimbingAngle(double tas, double climbRate) {

        return Math.asin(climbRate / tas); //return positive value for climb, negative for descend
        //in cruise flight it will be 0, constantly    
    }

    /**
     * Method that calculates air pressure
     *
     * @param altitude
     * @return
     */
    //=airPressureatsealevel*(1-0,0065*altitude/T0)^5,2561
    public double calculateAirPressure(double altitude) {

        return P0 * Math.pow(1 - L * (altitude / T0), 5.2561);
    }

    /**
     * Method that calculates the rate of change in air pressure
     *
     * @param airPressure
     * @param height
     * @return
     */
    public double calculateRateChangeAirPressure(double airPressure, double height) {

        double e = airPressure * (-1);

        double f = G * height;

        return e * f;
    }

    /**
     * *
     * Method to calculate the speed of sound given a temperature of a certain
     * altitude
     *
     * @param temperature
     * @return
     */
    public double calculateSpeedofSound(double temperature) {

        double trd = temperature / 273.15;
        double snd = 1 + trd;
        double st = Math.sqrt(snd);
        return 331.3 * st;
    }

    /**
     * method that calculate the temperature with the change of altitude
     *
     * @param altitude
     * @return
     */
    public double calculateTemperatureAltitude(double altitude) {

        double result;

        if (altitude <= 11000) {

            result = altitude * LAPSE + T0;
        } else {
            result = 216.7;
        }

        return result;
    }

    /**
     * Method to calculate the mach number
     *
     * @param temperature
     * @return
     */
    //must receive the temperature in Kelvin
    public double calculateMachNumber(double temperature) {

        return Math.sqrt(GAMMA * R * temperature);
    }

    /**
     * method to calculate the true mach number
     *
     * =RAIZQ(5*( (Constants!$B$3/Cruise!E16*
     * ((1+0,2*(Cruise!G16/661,5)^2)^3,5-1)+1) ^0,286-1)) E16 = airDensity G16 =
     * ias B3 = P0
     *
     * @param airDensity
     * @param ias
     * @return
     */
    public double calculateTrueMachNumber(double airDensity, double ias) {

        return Math.sqrt(5 * (Math.pow((D0 / airDensity) * (Math.pow(1 + 0.2 * Math.pow((ias / 661.5), 2), 3.5) - 1) + 1, 0.286) - 1));
    }

    /**
     * method to calculate Specific Air Range
     *
     * @param fuelConsumed
     * @param distance
     * @return
     */
    //Specific Air range is a messure of the air craft fuel efficiency
    public double calculateSAR(double fuelConsumed, double distance) {

        return distance / fuelConsumed; //miles per kg of fuel used.
    }

    /**
     * Method to calculate the idle potency of the motor
     *
     * potência de "idle"/residual do motor é 1/10 da potência em carga.
     *
     * @param potency of one motor
     * @param numMotors
     * @return
     */
    public double calculateTotalIdlePotency(double potency, int numMotors) {

        return (potency / 10) * numMotors;
    }

    /**
     * Override of the toString
     *
     * @return
     */
    /**
     * Method to calculate Max Flight time
     *
     * @param massFuel
     * @param tsfc
     * @param netThurst
     * @return
     *
     */
    public double calculateMaxFlightTime(double massFuel, double tsfc, double netThurst) {

        double flowRate = tsfc * massFuel;

        double MaxFlightTime = massFuel / flowRate;

        return MaxFlightTime / tsfc * netThurst;
    }

    /**
     * Nethod to calculate the range of the aircraft using the max flight time
     * of said aircraft
     *
     * @param maxFlightTime
     * @param speed
     * @return
     *
     */
    public double calculateRangeFlightTimeMax(double maxFlightTime, double speed) {

        return speed * maxFlightTime;
    }

    /**
     * Method to calculate the takeoff distance of an aircraft using the mass of
     * the given aircraft
     *
     * @param groundSpeed
     * @param totalThurst
     * @return
     */
    //rever metodo
    public double calculateTakeOffDistance(double groundSpeed, double totalThurst) {

        //double windAngle,double bearing, double tas, double windIntensity, double windDirection)
        double vT = Math.pow(groundSpeed, 2);
        double u = 0;
        double a = totalThurst;

        return (vT - u) / a;
    }

    /**
     * method to calculate the wind correction angle
     *
     * because it will only be helpufull to calculate the true ground speed
     *
     * which will a new method already
     *
     * @param windIntensity
     * @param tas
     * @param bearing
     * @return
     *
     *
     */
    public double calculateWindCorrectionAngle(double windIntensity, double tas, double bearing) {
        //sin (w-d)
        double snd = Math.sin(windIntensity - bearing);
        double fst = (windIntensity * snd) / tas;

        double x = Math.asin(fst);

        return x;
    }

    /**
     * Method to calculate the bearing
     *
     * @param latitude1
     * @param longitude1
     * @param latitude2
     * @param longitude2
     * @return
     *
     * Java script in algorithm : λ - longitude φ - latitude
     *
     * var y = Math.sin(λ2-λ1) * Math.cos(φ2); var x = Math.cos(φ1)*Math.sin(φ2)
     * - Math.sin(φ1)*Math.cos(φ2)*Math.cos(λ2-λ1); var brng = Math.atan2(y,
     * x).toDegrees();
     */
    public double calculateBearing(double latitude1, double longitude1, double latitude2, double longitude2) {

        double z = longitude2 - longitude1; //z  
        double y = Math.sin(z) * Math.cos(latitude2);
        double x = Math.cos(latitude1) * Math.sin(latitude2)
                - Math.sin(latitude1) * Math.cos(latitude2) * Math.cos(z);
        return Math.atan2(y, x);

    }

    /* A tailwind is a wind that blows in the direction of travel of an object,
    while a headwind blows against the direction of travel. A tailwind increases the object's speed and 
    reduces the time required to reach its destination, while a headwind has the opposite effect. 
    Tailwinds and headwinds are commonly measured in relation to the speed of vehicles — commonly air 
    and watercraft — as well as in running events — particularly sprints.*/
 /*Ground speed can be determined by the vector sum of the aircraft's true airspeed and the current wind
    speed and direction; a headwind subtracts from the ground speed, while a tailwind adds to it. 
    Winds at other angles to the heading will have components of either headwind or tailwind as well 
    as a crosswind component*/
    /**
     * Method to calculate the true ground speed / air speed in relation with
     * the wind of an aircraft
     *
     *
     * @param windAngle
     * @param bearing
     * @param tas
     * @param windIntensity
     * @param windDirection
     * @return
     */
    public double calculateTrueGroundSpeed(double windAngle, double bearing, double tas, double windIntensity, double windDirection) {

        double fst = Math.pow(tas, 2) + Math.pow(windIntensity, 2) - (2 * tas * windIntensity);

        double snd = Math.PI * (bearing - windDirection + windAngle) / 180;

        double thrd = Math.cos(snd);

        double finalResult = Math.sqrt(fst * thrd);

        return finalResult;
    }

    @Override
    public String toString() {

        return "Class for calculations related to the project";
    }

    /**
     * Override of hash method
     *
     * @return
     */
    @Override
    public int hashCode() {

        return 5;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Calc other = (Calc) obj;
        if (Double.doubleToLongBits(this._distance) != Double.doubleToLongBits(other._distance)) {
            return false;
        }
        if (Double.doubleToLongBits(this._liftForce) != Double.doubleToLongBits(other._liftForce)) {
            return false;
        }
        if (Double.doubleToLongBits(this._dragForce) != Double.doubleToLongBits(other._dragForce)) {
            return false;
        }
        return Double.doubleToLongBits(this._rangeAircraft) == Double.doubleToLongBits(other._rangeAircraft);
    }

}

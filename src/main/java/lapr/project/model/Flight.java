/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.Objects;

/**
 *
 *
 */
public class Flight {

    private String name;
    private String type;
    private String departureDay;
    private double minStopTime;
    private String scheduledArrival;
    private FlightPlan fp;

    public Flight() {
    }

    /**
     * class constructor
     *
     * @param type
     * @param departureDay
     * @param minStopTime
     * @param scheduledArrival
     * @param flightPlan
     */
    public Flight(String name, String type, String departureDay, double minStopTime, String scheduledArrival, FlightPlan flightPlan) {
        this.name = name;
        this.type = type;
        this.departureDay = departureDay;
        this.minStopTime = minStopTime;
        this.scheduledArrival = scheduledArrival;
        this.fp = flightPlan;

    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the departureDay
     */
    public String getDepartureDay() {
        return departureDay;
    }

    /**
     * @param departureDay the departureDay to set
     */
    public void setDepartureDay(String departureDay) {
        this.departureDay = departureDay;
    }

    /**
     * @return the minStopTime
     */
    public double getMinStopTime() {
        return minStopTime;
    }

    /**
     * @param minStopTime the minStopTime to set
     */
    public void setMinStopTime(double minStopTime) {
        this.minStopTime = minStopTime;
    }

    /**
     * @return the scheduledArrival
     */
    public String getScheduledArrival() {
        return scheduledArrival;
    }

    /**
     * @param scheduledArrival the scheduledArrival to set
     */
    public void setScheduledArrival(String scheduledArrival) {
        this.scheduledArrival = scheduledArrival;
    }

    /**
     * @return the flightPlan
     */
    public FlightPlan getFlightPlan() {
        return fp;
    }

    /**
     * @param flightPlan the flightPlan to set
     */
    public void setFlightPlan(FlightPlan flightPlan) {
        this.fp = flightPlan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.name);
        hash = 37 * hash + Objects.hashCode(this.type);
        hash = 37 * hash + Objects.hashCode(this.departureDay);
        hash = 37 * hash + (int) (Double.doubleToLongBits(this.minStopTime) ^ (Double.doubleToLongBits(this.minStopTime) >>> 32));
        hash = 37 * hash + Objects.hashCode(this.scheduledArrival);
        hash = 37 * hash + Objects.hashCode(this.fp);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Flight other = (Flight) obj;
        if (Double.doubleToLongBits(this.minStopTime) != Double.doubleToLongBits(other.minStopTime)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        if (!Objects.equals(this.departureDay, other.departureDay)) {
            return false;
        }
        if (!Objects.equals(this.scheduledArrival, other.scheduledArrival)) {
            return false;
        }
        if (!Objects.equals(this.fp, other.fp)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Flight{" + "name=" + name + ", type=" + type + ", departureDay=" + departureDay + ", minStopTime=" + minStopTime + ", scheduledArrival=" + scheduledArrival + ", flightPlan=" + fp + '}';
    }

}

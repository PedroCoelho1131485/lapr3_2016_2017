
package lapr.project.model;

import lapr.project.AdjacencyMap.Graph;
import lapr.project.List.AircraftList;
import lapr.project.List.AirportList;
import lapr.project.List.FligthPlanList;
import lapr.project.List.SegmentList;
import lapr.project.List.FlightList;
import lapr.project.List.JunctionList;
import lapr.project.List.NetworkAnalysisResultsList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class ProjectTest {

    private Project p;

    public ProjectTest() {
        p = new Project();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getName method, of class Project.
     */
    @Test
    public void testGetAndSetName() {
        System.out.println("getName");
        Project instance = p;
        String expResult = "a";
        instance.setName(expResult);
        String result = instance.getName();
        assertEquals(expResult, result);

    }

    /**
     * Test of getDescription method, of class Project.
     */
    @Test
    public void testGetAndSetDescription() {
        System.out.println("getDescription");
        Project instance = p;
        String expResult = "as";
        instance.setDescription(expResult);
        String result = instance.getDescription();
        assertEquals(expResult, result);

    }

    /**
     * Test of getGraph method, of class Project.
     */
    @Test
    public void testGetGraph() {
        System.out.println("getGraph");
        Project instance = p;
        Graph<Junction, Segment> expResult = new Graph(true);
        Graph<Junction, Segment> result = instance.getGraph();
        assertEquals(expResult, result);

    }

    /**
     * Test of newJunction method, of class Project.
     */
    @Test
    public void testNewJunction() {
        System.out.println("newJunction");
        Project instance = p;
        Junction expResult = new Junction();
        Junction result = instance.newJunction();
        assertEquals(expResult, result);
    }

    /**
     * Test of addJunction method, of class Project.
     */
    @Test
    public void testAddJunction() {
        System.out.println("addJunction");
        Junction junction = new Junction("1", 1, 1);  //---------------
        Project instance = p;
        boolean expResult = true;
        boolean result = instance.addJunction(junction);
        assertEquals(expResult, result);

    }

    /**
     * Test of addAircraft method, of class Project.
     */
    @Test
    public void testAddAircraft() {
        System.out.println("addAircraft");
        Aircraft air = new Aircraft();
        Project instance = p;
        boolean expResult = true;
        boolean result = instance.addAircraft(air);
        assertEquals(expResult, result);
    }

    /**
     * Test of findJunction method, of class Project.
     */
    @Test
    public void testFindJunction() {
        System.out.println("findJunction"); //----------------
        String id = "a";
        Project instance = p;
        Junction expResult = new Junction("a", 1, 1);

        instance.addJunction(expResult);
        Junction result = instance.findJunction(id);
        assertEquals(expResult, result);

    }

    /**
     * Test of addSegment method, of class Project.
     */
    @Test
    public void testAddSegment() {
        System.out.println("addSegment");
        Segment node = new Segment();     //---------
        Project instance = p;
        boolean expResult = true;
        boolean result = instance.addSegment(node);
        assertEquals(expResult, result);

    }

    /**
     * Test of getSegmentList method, of class Project.
     */
    @Test
    public void testGetSegmentList() {
        System.out.println("getSegmentList");
        Project instance = p;
        SegmentList expResult = new SegmentList();
        SegmentList result = instance.getSegmentList();
        assertEquals(expResult, result);

    }

    /**
     * Test of getAirNetwork method, of class Project.
     */
    @Test
    public void testGetAirNetwork() {
        System.out.println("getAirNetwork");
        Project instance = p;
        AirNetwork expResult = new AirNetwork();
        AirNetwork result = instance.getAirNetwork();
        assertEquals(expResult, result);

    }

    /**
     * Test of getAirportList method, of class Project.
     */
    @Test
    public void testGetAndSetAirportList() {
        System.out.println("getAirportList");
        Project instance = p;
        AirportList expResult = new AirportList();
        instance.setAirportList(expResult);
        AirportList result = instance.getAirportList();
        assertEquals(expResult, result);

    }

    /**
     * Test of getAircraftList method, of class Project.
     */
    @Test
    public void testGetAndSetAircraftList() {
        System.out.println("getAircraftList");
        Project instance = p;
        AircraftList expResult = new AircraftList();
        instance.setAircraftList(expResult);
        AircraftList result = instance.getAircraftList();
        assertEquals(expResult, result);

    }



    /**
     * Test of validate method, of class Project.
    */
   /*@Test
   public void testValidate() {
        System.out.println("validate");
      boolean validateNetwork = true;
      boolean validateAircrafts = true;
       Project instance = new Project("nome","descricao");
       
       boolean expResult = true;           //--------------------------
       boolean result = instance.validate(validateNetwork, validateAircrafts);
      assertEquals(expResult, result);
     
    }*/

    /**
     * Test of getName method, of class Project.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Project instance = new Project();
        String expResult = "";
        instance.setName(expResult);
        String result = instance.getName();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setName method, of class Project.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "";
        Project instance = new Project();
        instance.setName(name);
        assertEquals(name,instance.getName());
    }

    /**
     * Test of getDescription method, of class Project.
     */
    @Test
    public void testGetDescription() {
        System.out.println("getDescription");
        Project instance = new Project();
        String expResult = "";
        instance.setDescription(expResult);
        String result = instance.getDescription();
        assertEquals(expResult, result);
     
    }

    /**
     * Test of setDescription method, of class Project.
     */
    @Test
    public void testSetDescription() {
        System.out.println("setDescription");
        String description = "";
        Project instance = new Project();
        instance.setDescription(description);
        assertEquals(description,instance.getDescription());
    }

    /**
     * Test of getAirportList method, of class Project.
     */
    @Test
    public void testGetAirportList() {
        System.out.println("getAirportList");
        Project instance = new Project();
        AirportList expResult = new AirportList();
        instance.setAirportList(expResult);
        AirportList result = instance.getAirportList();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAirportList method, of class Project.
     */
    @Test
    public void testSetAirportList() {
        System.out.println("setAirportList");
        AirportList airportList = new AirportList();
        Project instance = new Project();
        instance.setAirportList(airportList);
        assertEquals(airportList,instance.getAirportList());
    }

    /**
     * Test of getAircraftList method, of class Project.
     */
    @Test
    public void testGetAircraftList() {
        System.out.println("getAircraftList");
        Project instance = new Project();
        AircraftList expResult = new AircraftList();
        instance.setAircraftList(expResult);
        AircraftList result = instance.getAircraftList();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAircraftList method, of class Project.
     */
    @Test
    public void testSetAircraftList() {
        System.out.println("setAircraftList");
        AircraftList aircraftList = new AircraftList();
        Project instance = new Project();
        instance.setAircraftList(aircraftList);
        assertEquals(aircraftList,instance.getAircraftList());
    }

 
    /**
     * Test of validate method, of class Project.
     */
    @Test
    public void testValidate() {
        System.out.println("validate");
        boolean validateNetwork = false;
        boolean validateAircrafts = false;
        Project instance = new Project();
        instance.setName("Project");
        instance.setDescription("description");
        boolean expResult = true;
        boolean result = instance.validate(validateNetwork, validateAircrafts);
        assertEquals(expResult, result);
      
    }

    /**
     * Test of getFlightPlanList method, of class Project.
     */
    @Test
    public void testGetFlightPlanList() {
        System.out.println("getFlightPlanList");
        Project instance = new Project();
        FligthPlanList expResult = new FligthPlanList();
        instance.setFlightPlanList(expResult);
        FligthPlanList result = instance.getFlightPlanList();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFlightPlanList method, of class Project.
     */
    @Test
    public void testSetFlightPlanList() {
        System.out.println("setFlightPlanList");
        FligthPlanList flightPlanList = new FligthPlanList();
        Project instance = new Project();
        instance.setFlightPlanList(flightPlanList);
        assertEquals(flightPlanList,instance.getFlightPlanList());
    }

    /**
     * Test of getNaList method, of class Project.
     */
    @Test
    public void testGetNaList() {
        System.out.println("getNaList");
        Project instance = new Project();
        NetworkAnalysisResultsList expResult = new NetworkAnalysisResultsList();
        instance.setNaList(expResult);
        NetworkAnalysisResultsList result = instance.getNaList();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNaList method, of class Project.
     */
    @Test
    public void testSetNaList() {
        System.out.println("setNaList");
        NetworkAnalysisResultsList naList = new NetworkAnalysisResultsList();
        Project instance = new Project();
        instance.setNaList(naList);
        assertEquals(naList,instance.getNaList());
    }

    /**
     * Test of getJunctionList method, of class Project.
     */
    @Test
    public void testGetJunctionList() {
        System.out.println("getJunctionList");
        Project instance = new Project();
        JunctionList expResult = new JunctionList();
        instance.setJunctionList(expResult);
        JunctionList result = instance.getJunctionList();
        assertEquals(expResult, result);
    }

    /**
     * Test of setJunctionList method, of class Project.
     */
    @Test
    public void testSetJunctionList() {
        System.out.println("setJunctionList");
        JunctionList junctionList = new JunctionList();
        Project instance = new Project();
        instance.setJunctionList(junctionList);
        assertEquals(junctionList,instance.getJunctionList());
    }

    /**
     * Test of hashCode method, of class Project.
     */
    /*@Test
    public void testHashCode() {
        System.out.println("hashCode");
        Project instance = new Project();
        int expResult = 1199806153;
        int result = instance.hashCode();
        assertEquals(expResult, result);
        System.out.println("hashcode :" + Integer.toString(result));
    }*/

    /**
     * Test of equals method, of class Project.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        Project instance = new Project();
        assertFalse(instance.equals(obj));
        obj = new Object();
        assertFalse(instance.equals(obj));
        Project p = new Project(); 
        //the construtor of each project
        //Creates a new kinda of atributes
        //only two atributes are defined by the user
        //name and descriptioon
        assertFalse(instance.equals(p));
        instance.setName("Name");
        instance.setDescription("descricao");
        obj = (Project) instance; //reference
        assertTrue(instance.equals(obj));
       
    }

    /**
     * Test of toString method, of class Project.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String name ="";
        String description = "";
        Project instance = new Project(name,description);
        AirNetwork airNetwork = instance.getAirNetwork();
        String expResult = "Project{" + "name=" + name + ", description=" + description + ", airNetwork=" + airNetwork + '}';;
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNetworkAnalysisResultsList method, of class Project.
     */
    @Test
    public void testGetNetworkAnalysisResultsList() {
        System.out.println("getNetworkAnalysisResultsList");
        Project instance = new Project();
        NetworkAnalysisResultsList result = instance.getNetworkAnalysisResultsList();
        assertNotNull(result);

    }

    /**
     * Test of getFlightList method, of class Project.
     */
    @Test
    public void testGetFlightList() {
        System.out.println("getFlightList");
        Project instance = new Project();
        FlightList result = instance.getFlightList();
        assertNotNull(result);
    }

    /**
     * Test of getImportableRegistry method, of class Project.
     */
    @Test
    public void testGetImportableRegistry() {
        System.out.println("getImportableRegistry");
        Project instance = new Project();
        ImportableRegistry result = instance.getImportableRegistry();
        assertNotNull(result);
    }

    /**
     * Test of getImp method, of class Project.
     */
    @Test
    public void testGetImp() {
        System.out.println("getImp");
        Project instance = new Project();
        ImportableRegistry result = instance.getImp();
        assertNotNull(result);        
    }

    /**
     * Test of hashCode method, of class Project.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Project instance = new Project();
        int expResult = 1199806153;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }
}

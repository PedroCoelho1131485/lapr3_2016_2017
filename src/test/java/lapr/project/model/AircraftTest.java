
package lapr.project.model;

import lapr.project.List.CdragList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;


public class AircraftTest {

    Aircraft air;

    public AircraftTest() {
        this.air = new Aircraft();
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
    /**
     * Test of the default construtor, of class Aircraft
     */
    
    @Test
    public void testDefaultConstrutor() {
        
        System.out.println("Default Construtor");
        Aircraft a = new Aircraft();
        assertNotNull(a);
    }
    
    /**
     * Test of the construtor that receives the info , for the class Aircraft
     */
    
    @Test
    public void testRealConstrutor() {
        //info
        
     String registration = "001";
     String Company = "Boeing";
     int numberSeatsEconomic = 60;
     int numberSeatsExecutiv = 40;//aumentar no futuro quando se souber quantos tipos de classe ha
     int nElemCrew = 20;
     AircraftModel am = new AircraftModel();
     int number = 2;
     
     Aircraft a = new Aircraft(registration,Company,numberSeatsExecutiv,numberSeatsEconomic,nElemCrew,am,number);
     
     assertNotNull(a);
     assertEquals(registration,a.getRegistration());
     assertEquals(Company,a.getCompany());
     assertEquals(numberSeatsEconomic,a.getNumberSeatsEconomic(),0);
     assertEquals(numberSeatsExecutiv,a.getNumberSeatsExecutiv(),0);
     assertEquals(am,a.getAircraftModel());
     assertEquals(number,a.getNumber(),0);
    }

    /**
     * Test
     */
    @Test
    public void testSetAndGetRegistration() {
        System.out.println("setAngetRegistration");
        Aircraft instance = air;
        String expResult = "regist";
        instance.setRegistration(expResult);
        String result = instance.getRegistration();
        assertEquals(expResult, result);
    }

    /**
     * Test of set and getCompany methods, of class Aircraft.
     */
    @Test
    public void testSetAndGetCompany() {
        System.out.println("setAndGetCompany");
        Aircraft instance = air;
        String expResult = "boeing";
        instance.setCompany(expResult);
        String result = instance.getCompany();
        assertEquals(expResult, result);
    }

    /**
     * Test of get and setNumberSeats methods, of class Aircraft.
     */
    @Test
    public void testSetAndGetNumberSeats() {
        Aircraft instance = air;
        int expResult = 80;
        instance.setNumberSeatsExecutiv(expResult);
        int result = instance.getNumberSeatsExecutiv();
        assertEquals(expResult, result,0);
        instance.setNumberSeatsEconomic(expResult);
        assertEquals(expResult,instance.getNumberSeatsEconomic(),0);
    }

    /**
     * Test of get and setnElemCrew methods, of class Aircraft.
     */
    @Test
    public void testSetAndGetnElemCrew() {
        System.out.println("getAndSetnElemCrew");
        Aircraft instance = air;
        int expResult = 10;
        instance.setnElemCrew(expResult);
        int result = instance.getnElemCrew();
        assertEquals(expResult, result,0);
    }
    
    /**
     * Test of set and get number , of class Aircraft
     */

    @Test
    public void testSetandGetNumber() {
        System.out.println("getAndSetNumber");
        Aircraft instance = air;
        int number = 10;
        instance.setNumber(number);
        int result = instance.getNumber();
        assertEquals(number,result,0);
    }
    
    /**
     * Test of the method equals(override) of the class Aircraft
     */
    
    @Test
    public void testEquals() {
        
        System.out.println("Equals");
        
        Aircraft a = new Aircraft();
        Aircraft b = new Aircraft();
        
        assertTrue(a.equals(b));
        
        String registration = "001";
        String Company = "Boeing";
        int numberSeatsEconomic = 60;
         int numberSeatsExecutiv = 40;//aumentar no futuro quando se souber quantos tipos de classe ha
        int nElemCrew = 20;
        AircraftModel am = new AircraftModel();
        int number = 2;
        
        Aircraft c = new Aircraft(registration,Company,numberSeatsExecutiv,numberSeatsEconomic,nElemCrew,am,number);
        
        assertFalse(a.equals(c));
    }
    
    /**
     * Test of the method toString (override) of class Aircraft
     */
    
    @Test
    public void testToString(){
        
        System.out.println("To String");
        Aircraft instance = new Aircraft();
     
        String registration = "001";
        String Company = "Boeing";
        int number = 2;
        int numberSeatsEconomic = 30;
        int numberSeatsExecutiv = 60;
        
        int nElemCrew = 16;
        
        AircraftModel aircraftmodel = new AircraftModel();
            
        String expected = "Aircraft{" + "registration=" + registration + ", Company=" + Company + ", numberSeatsEconomic=" + numberSeatsEconomic + ", numberSeatsExecutiv=" + numberSeatsExecutiv + ", nElemCrew=" + nElemCrew + ", aircraftModel=" + aircraftmodel + ", number=" + number + '}';
        instance.setAircraftModel(aircraftmodel);
        instance.setNumber(number);
        instance.setNumberSeatsEconomic(numberSeatsEconomic);
        instance.setNumberSeatsExecutiv(numberSeatsExecutiv);
        instance.setnElemCrew(nElemCrew);
        instance.setCompany(Company);
        instance.setRegistration(registration);
        
        String result = instance.toString();
        
        assertEquals(expected,result);
        
        System.out.println(result);
    }

    /**
     * Test of getAircraftModel method, of class Aircraft.
     */
    @Test
    public void testGetAircraftModel() {
        System.out.println("getAircraftModel");
        Aircraft instance = new Aircraft();
        AircraftModel expResult = new AircraftModel();
        instance.setAircraftModel(expResult);
        AircraftModel result = instance.getAircraftModel();
        assertEquals(expResult, result);

    }

    /**
     * Test of setAircraftModel method, of class Aircraft.
     */
    @Test
    public void testSetAircraftModel() {
        System.out.println("setAircraftModel");
        AircraftModel aircraftModel = new AircraftModel();
        Aircraft instance = new Aircraft();
        instance.setAircraftModel(aircraftModel);
        assertEquals(aircraftModel,instance.getAircraftModel());
    }

    /**
     * Test of getRegistration method, of class Aircraft.
     */
    @Test
    public void testGetRegistration() {
        System.out.println("getRegistration");
        Aircraft instance = new Aircraft();
        String expResult = "Registration";
        instance.setRegistration(expResult);
        String result = instance.getRegistration();
        assertEquals(expResult, result);
   
    }

    /**
     * Test of setRegistration method, of class Aircraft.
     */
    @Test
    public void testSetRegistration() {
        System.out.println("setRegistration");
        String registration = "Registration";
        Aircraft instance = new Aircraft();
        instance.setRegistration(registration);
        assertEquals(registration,instance.getRegistration());
    }

    /**
     * Test of getCompany method, of class Aircraft.
     */
    @Test
    public void testGetCompany() {
        System.out.println("getCompany");
        Aircraft instance = new Aircraft();
        String expResult = "Company";
        instance.setCompany(expResult);
        String result = instance.getCompany();
        assertEquals(expResult, result);

    }

    /**
     * Test of setCompany method, of class Aircraft.
     */
    @Test
    public void testSetCompany() {
        System.out.println("setCompany");
        String Company = "Company";
        Aircraft instance = new Aircraft();
        instance.setCompany(Company);
        assertEquals(Company,instance.getCompany());
    }

    /**
     * Test of getNumberSeatsExecutiv method, of class Aircraft.
     */
    @Test
    public void testGetNumberSeatsExecutiv() {
        System.out.println("getNumberSeatsExecutiv");
        Aircraft instance = new Aircraft();
        int expResult = 0;
        instance.setNumberSeatsExecutiv(expResult);
        int result = instance.getNumberSeatsExecutiv();
        assertEquals(expResult, result,0);
        expResult = 20;
        instance.setNumberSeatsExecutiv(expResult);
        result = instance.getNumberSeatsExecutiv();
        assertEquals(expResult,result,0);
    }

    /**
     * Test of setNumberSeatsExecutiv method, of class Aircraft.
     */
    @Test
    public void testSetNumberSeatsExecutiv() {
        System.out.println("setNumberSeatsExecutiv");
        int numberSeats = 0;
        Aircraft instance = new Aircraft();
        instance.setNumberSeatsExecutiv(numberSeats);
        assertEquals(numberSeats,instance.getNumberSeatsExecutiv(),0);
        numberSeats = 100;
        instance.setNumberSeatsExecutiv(numberSeats);
        assertEquals(numberSeats,instance.getNumberSeatsExecutiv(),0);
    }

    /**
     * Test of getNumberSeatsEconomic method, of class Aircraft.
     */
    @Test
    public void testGetNumberSeatsEconomic() {
        System.out.println("getNumberSeatsEconomic");
        Aircraft instance = new Aircraft();
        int expResult = 0;
        instance.setNumberSeatsEconomic(expResult);
        int result = instance.getNumberSeatsEconomic();
        assertEquals(expResult, result,0);
        expResult = 20;
        instance.setNumberSeatsEconomic(expResult);
        result = instance.getNumberSeatsEconomic();
        assertEquals(expResult,result,0);
    }

    /**
     * Test of setNumberSeatsEconomic method, of class Aircraft.
     */
    @Test
    public void testSetNumberSeatsEconomic() {
        System.out.println("setNumberSeatsEconomic");
        int numberSeats = 0;
        Aircraft instance = new Aircraft();
        instance.setNumberSeatsEconomic(numberSeats);
        assertEquals(numberSeats,instance.getNumberSeatsEconomic(),0);
        numberSeats = 30;
        instance.setNumberSeatsEconomic(numberSeats);
        assertEquals(numberSeats,instance.getNumberSeatsEconomic(),0);
    }

    /**
     * Test of getnElemCrew method, of class Aircraft.
     */
    @Test
    public void testGetnElemCrew() {
        System.out.println("getnElemCrew");
        Aircraft instance = new Aircraft();
        int expResult = 0;
        instance.setnElemCrew(expResult);
        int result = instance.getnElemCrew();
        assertEquals(expResult, result);
        expResult = 100;
        instance.setnElemCrew(expResult);
        result = instance.getnElemCrew();
        assertEquals(expResult,result);
    }

    /**
     * Test of setnElemCrew method, of class Aircraft.
     */
    @Test
    public void testSetnElemCrew() {
        System.out.println("setnElemCrew");
        int nElemCrew = 0;
        Aircraft instance = new Aircraft();
        instance.setnElemCrew(nElemCrew);
        assertEquals(nElemCrew,instance.getnElemCrew(),0);
        nElemCrew = 80;
        instance.setnElemCrew(nElemCrew);
        assertEquals(nElemCrew,instance.getnElemCrew(),0);
    }

    /**
     * Test of getNumber method, of class Aircraft.
     */
    @Test
    public void testGetNumber() {
        System.out.println("getNumber");
        Aircraft instance = new Aircraft();
        int expResult = 0;
        instance.setNumber(expResult);
        int result = instance.getNumber();
        assertEquals(expResult, result);
        expResult = 1;
        instance.setNumber(expResult);
        result = instance.getNumber();
        assertEquals(expResult,result,0);

    }

    /**
     * Test of setNumber method, of class Aircraft.
     */
    @Test
    public void testSetNumber() {
        System.out.println("setNumber");
        int number = 0;
        Aircraft instance = new Aircraft();
        instance.setNumber(number);
        assertEquals(number,instance.getNumber(),0);
        number = 1;
        instance.setNumber(number);
        assertEquals(number,instance.getNumber(),0);
    }

    /**
     * Test of hashCode method, of class Aircraft.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Aircraft instance = new Aircraft();
        int expResult = -1104025984;
        int result = instance.hashCode();
        assertEquals(expResult, result,0);
       
    }

    /**
     * Test of validate method, of class Aircraft.
     */
    @Test
    public void testValidate() {
        System.out.println("validate");
        Aircraft instance = new Aircraft();
        instance.setCompany("Company");
        instance.setNumber(1);
        instance.setNumberSeatsEconomic(80);
        instance.setNumberSeatsExecutiv(30);
        
        AircraftModel aircraftmodel = new AircraftModel();
        
        String modelId = "001";
        String maker = "Maker";
        String Type = "Cargo"; //cargo or passenger or  mixed
        Motorization motorization = new Motorization();
        double emptyWeight = 156400.0;
        double MaximumTakeOffWeight = 25000.0;
        double MaximumFuelCapacity = 3000.0;
        double maxPayLoad = 200.0;
        double aspectRatio = 9;
        double VMO = 2000;
        double MMO  = 3500;
        double wingArea = 512;
        double wingSpan = 64.8;

         double e = 0.96;
         CdragList c = new CdragList();
         
         aircraftmodel.setModelId(modelId);
         aircraftmodel.setAspectRatio(aspectRatio);
         aircraftmodel.setCDragList(c);
         aircraftmodel.setE(e);
         aircraftmodel.setEmptyWeight(emptyWeight);
         aircraftmodel.setMMO(MMO);
         aircraftmodel.setMaker(maker);
         aircraftmodel.setMaxPayLoad(maxPayLoad);
         aircraftmodel.setNumberMotors(4);
         aircraftmodel.setMotor("Motor");
         aircraftmodel.setMaximumFuelCapacity(MaximumFuelCapacity);
         aircraftmodel.setMaximumTakeOffWeight(MaximumTakeOffWeight);
         aircraftmodel.setMotorType("Fast");
         aircraftmodel.setMotorization(motorization);
         aircraftmodel.setVMO(VMO);
         aircraftmodel.setType(Type);
         aircraftmodel.setWingArea(wingArea);
         aircraftmodel.setWingSpan(wingSpan);
        
         instance.setRegistration("00001");
         instance.setAircraftModel(aircraftmodel);
         instance.getAircraftModel().getMotorization().setNumberMotors(4);
         instance.getAircraftModel().getMotorization().setCruiseAltitude(2000);
         instance.getAircraftModel().getMotorization().setCruiseSpeed(1000);
         instance.getAircraftModel().getMotorization().setLapseRateFactor(10);
         instance.getAircraftModel().getMotorization().setMax_speed(4000);
         instance.getAircraftModel().getMotorization().setMotor("Motor");
         instance.getAircraftModel().getMotorization().setMotorType("Type");
         instance.getAircraftModel().getMotorization().setName("Name");
         instance.getAircraftModel().getMotorization().setNumberMotors(4);
         instance.getAircraftModel().getMotorization().setTSFC(100);
         instance.getAircraftModel().getMotorization().setThurst_0(0.9);
         instance.getAircraftModel().getMotorization().setThurst_max_speed(40000);
         
         
        assertTrue(instance.validate());
    
    }
    
}


package lapr.project.model;

import lapr.project.AdjacencyMap.Graph;
import java.util.Objects;

/**
 *
 *
 */
public class AirNetwork {

    /**
     * Instance of graph
     */
    private Graph<Junction, Segment> graph;

    public AirNetwork() {
        this.graph = new Graph<>(true);
    }

    public AirNetwork(AirNetwork source) {
        graph = new Graph<>(true);
        this.setGraph(source.getGraph());
    }

    /**
     * @param graph
     */
    public void setAirNetwork(Graph<Junction, Segment> graph) {
        this.graph = graph;
    }

    /**
     * @return graph
     */
    public Graph<Junction, Segment> getGraph() {
        return this.graph;
    }

    /**
     * Adds junction to graph.
     *
     * @param node
     * @return true if added and false otherwise
     */
    public boolean addJunction(Junction node) {
        if (graph.getVertices().get(node) == null) {
            graph.insertVertex(node);
            return true;
        } else {
            return false;
        }
    }

    public Junction findJunction(String name) {
        Iterable<Junction> vertices = this.graph.vertices();

        for (Junction junction : vertices) {
            if (junction.getName().equals(name)) {
                return junction;
            }
        }
        return null;
    }

    public Junction findJunction(double latitude, double longitude) {
        Iterable<Junction> vertices = this.graph.vertices();
        for (Junction junction : vertices) {
            if (junction.getLatitude() == latitude && junction.getLongitude() == longitude) {
                return junction;
            }
        }
        return null;
    }

    /**
     * Adds segment to graph.
     *
     * @param node
     * @return true if added and false otherwise
     */
    public boolean addSegment(Segment node) {
        if (graph.getEdge(node.getStartNode(), node.getEndNode()) == null) {

            graph.insertEdge(node.getStartNode(), node.getEndNode(), node, 1);

            return true;
        } else {
            return false;
        }
    }

    public void setGraph(Graph<Junction, Segment> graph) {
        this.graph = graph;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.graph);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AirNetwork other = (AirNetwork) obj;
        if (!Objects.equals(this.graph, other.graph)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "AirNetwork{" + "graph=" + graph + '}';
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.utils;

import java.util.Formatter;

/**
 *
 * @author Pedro
 */
public class HtmlCode {

    /**
     * Iniciar HTML
     *
     * @param pag
     */
    public static void iniciarPagina(Formatter pag) {
        String titulo = "Apresentacão em HTML";
        pag.format("<!DOCTYPE html>%n");
        pag.format("<html>%n");
        pag.format("<head>%n");
        pag.format("<meta charset='utf-8'>%n");
        pag.format("<title>%s</title>%n", titulo);
        pag.format("</head>%n");
        pag.format("<body>%n");
    }

    /**
     * Fechar a estrutura da página
     *
     * @param pag
     * @param n
     * @param conteudo
     */
    public static void criarCabecalho(Formatter pag, int n, String conteudo) {
        pag.format("<h%d>%s</h%d>%n", n, conteudo, n);
    }

    /**
     * Mensagem da página
     *
     * @param pag
     * @param mensagem
     */
    public static void mensagem(Formatter pag, String mensagem) {
        pag.format("<p>%s</p>", mensagem);
    }

    /**
     * Fechar página
     *
     * @param pag
     */
    public static void fecharPagina(Formatter pag) {
        pag.format("</body>%n</html>");
        pag.close();
    }
}

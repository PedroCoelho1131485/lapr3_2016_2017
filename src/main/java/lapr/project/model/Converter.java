/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.Objects;

/**
 *
 * @author Pedro
 */
public class Converter {

    /**
     * atributes
     *
     */
    private String name; //just idenfication for the object

    public Converter() {

        this.name = "Converter";
    }

    public Converter(String name) {
        if (name != null && !"".equals(name)) {
            this.name = name;
        } else {
            this.name = "Converter";
        }
    }

    /**
     * converts TSFC in SI unit
     *
     * @param x
     * @return
     */
    public double TSFConverterSI(double x) {
        return x * 101.972;

    }

    /**
     * converts SI in TSFC unit
     *
     * @param x
     * @return
     */
    public double SIConverterTSFC(double x) {

        return x / 101.972;
    }

    /**
     * converts thrust in newton units
     *
     * @param x
     * @return
     */
    public double thrustConverterNewton(double x) {
        return x * 4.44822162;
    }

    /**
     * converts newton to thurst units
     *
     * @param x
     * @return
     */
    public double newtonConverter(double x) {

        return x / 4.44822162;
    }

    /**
     * converts pound in grams
     *
     * @param x
     * @return
     */
    public double poundConverterGrams(double x) {

        return x * 453.59237;
    }

    /**
     * converts grams to pounds
     *
     * @param x
     * @return
     */
    public double gramsConverterPound(double x) {

        return x / 453.59237;
    }

    /**
     * converts knot in meters per second
     *
     * @param x
     * @return
     */
    public double knotConverterMeterPerSecond(double x) {
        return x * 0.514444444;
    }

    /**
     * converts meters per second in knots
     *
     * @param x
     * @return
     */
    public double meterPerSecondConverterKnots(double x) {

        return x / 0.514444444;
    }

    /**
     * convert liters to kilograms (fuel)
     *
     * 1 L = 0.804 kg
     *
     * @param x
     * @return
     */
    public double litersConverterKilograms(double x) {

        return x * 0.804;
    }

    /**
     * convert kilograms to liters
     *
     * @param x
     * @return
     */
    public double kilogramsConverterLiters(double x) {

        return x / 0.804;
    }

    /**
     * Method that converts grams to kilograms
     *
     * @param x
     * @return
     */
    public double gramsConverterKilograms(double x) {

        return x / 1e3;
    }

    /**
     * Method that converts kilograms to grams
     *
     * @param x
     * @return
     */
    public double kilogramsConverterGrams(double x) {

        return x * 1e3;
    }

    /**
     * converter feets to meters
     *
     * 1 foot = 30.48 centimeters
     *
     * 30.48 centimetres = 30.48 e -2 metres
     *
     * @param x
     * @return
     */
    public double feetsConverterMeters(double x) {

        return x * 30.48e-2;
    }

    /**
     * Methods that converts feet to miles
     *
     * @param x
     * @return
     */
    public double feetConverterMiles(double x) {

        double y = x * 30.48e-2; //meters

        return y / 1.61e3;
    }

    /**
     * Method that converts miles to feet
     *
     * @param x
     * @return
     */
    public double milesConverterFeet(double x) {

        double y = x * 1.61e3; //meters

        return y / 30.48e-2;
    }

    /**
     * converter miles to meters 1 mile = 1.609344 kilometers 1.61e3 meters
     *
     * @param x
     * @return
     */
    public double milesConverterMetres(double x) {

        return x * 1.61e3;
    }

    /**
     * converter kilometres to miles 1 mile = 1.609344 kilometers
     *
     * @param x
     * @return
     */
    public double kilometersConverterMiles(double x) {
        //x in km
        double m = x / 1.61e3; //x in meters

        return m * 1e3;
    }

    /**
     * converter miles to kilometers
     *
     * @param x
     * @return
     */
    public double milesConverterKilometers(double x) {

        double m = x * 1.61e3;

        return m / 1e3;
    }

    //conveter metres to miles
    public double metersConverterMiles(double x) {

        return x / 1.61e3;
    }

    //1 US gallon = 3.78541178 litres
    /**
     * method to convert galons to litres
     *
     * @param x
     * @return
     */
    public double galonsConverterLitres(double x) {

        return x * 3.78541178;
    }

    //1 US gallon = 3.78541178 litres
    /**
     * method to convert liters to gallons
     *
     * @param x
     * @return
     */
    public double litresConverterGalons(double x) {

        return x / 3.78541178;
    }

    // 1 M = 761,2071 mph
    /**
     * method to convert mach number into miles per hour (mph)
     *
     * @param x
     * @return
     */
    public double machNumberConverterMilesPerHour(double x) {

        return x / 761.2071;
    }

    // 1 M = 761,2071 mph
    /**
     * method to convert miles per bour into mach number
     *
     * @param x
     * @return
     */
    public double milesPerHourConverterMachNumber(double x) {

        return x * 761.2071;
    }

    //1 Mach= 1225.044kph
    /**
     * method to convert mach number into kilometres per hour
     *
     * @param x
     * @return
     */
    public double machNumberConverterKilometersPerHour(double x) {

        return x * 1225.044;
    }

    //1Mach= 1225.044kph
    /**
     * method to converte kilometers per hour into mach number
     *
     * @param x
     * @return
     */
    public double kilometersPerHourConverterMachNumber(double x) {

        return x / 1225.044;
    }

    /**
     * method to convert bar to pascal
     *
     * @param x
     * @return
     */
    //1 bar = 10000
    public double barConverterPascal(double x) {

        return x * 1e5;
    }

    /**
     * method to convert pascal to bar
     *
     * @param x
     * @return
     */
    public double pascalConverterBar(double x) {

        return x / 1e5;
    }

    /**
     * method to convert pascal to kilopascal
     *
     * @param x
     * @return
     */
    // 1 pascal = 1000 kilopascal
    public double pascalConverterKiloPascal(double x) {

        return x * 1e3;
    }

    /**
     * method to convert kilopascal to pascal
     *
     * @param x
     * @return
     */
    public double kilopascalConverterPascal(double x) {

        return x / 1e3;
    }

    // 1 psi = 6894.75 Pascal
    /**
     * method that convertes pounds per square inch to pascal
     *
     * @param x
     * @return
     */
    public double psiConverterPascal(double x) {

        return x * 6894.75;
    }

    /**
     * *
     * method that converters pascal to psi
     *
     * @param x
     * @return
     */
    public double pascalConverterPSI(double x) {

        return x / 6894.75;
    }

    /**
     * method that converts kilometres per hour to meters per second
     *
     * @param x 1 h - 3600s 1km = 1000m
     * @return
     */
    public double kilometersperhourConverterMeterspersecond(double x) {

        return (x * 1000) / 3600; //m/s
    }

    /**
     * method that converts meters per second to kilometres per hour
     *
     * @param x 1 h - 3600s 1km = 1000m
     * @return
     */
    public double meterspersecondConverterKilometersperhour(double x) {

        return (x / 1000) * 3600; //km/h
    }

    /**
     * Method that converts miles per hour to meters per second
     *
     * @param x
     * @return
     */
    public double milesperhourConverterMeterspersecond(double x) {

        //miles per hour to meters per hour
        double meters = this.milesConverterMetres(x);

        return meters * 3600; //m/s
    }

    /**
     * Method that converts meters per second to miles per hour
     *
     * @param x
     * @return
     */
    public double meterspersecondConverterMilesperhour(double x) {
        //converts meters per second to miles per second
        double miles = this.metersConverterMiles(x);
        return miles / 3600; //miles per hour
    }

    /**
     * Method that converts Radians to Degrees
     *
     * if PI radians are 180 degrees then x radians will be result
     *
     * therefore
     *
     * result = x * 180 / Math.PI;
     *
     * @param rad
     * @return
     */
    public double radiansConverterDegrees(double rad) {

        return rad * 180 / Math.PI;
    }

    /**
     * Method that converts Degrees to Radians
     *
     * if PI radians are 180 degrees then result radians will be x degrees
     *
     * therefore:
     *
     * result = x * PI / 180;
     *
     * @param deg
     * @return
     */
    public double degreesConverterRadians(double deg) {

        return deg * Math.PI / 180;
    }

    /**
     * Method that converts temperature celsius to kelvin
     *
     * K = C + 273
     *
     * @param x
     * @return
     */
    public double celsiusConverterKelvin(double x) {

        return x + 273;
    }

    /**
     * Method that converts temperature kelvin to celsuis
     *
     * @param x
     * @return
     */
    public double kelvinConverterCelsius(double x) {

        return x - 273;
    }

    /**
     * Method that converts temperature in celsius to temperature in Fahrenheit
     *
     * ℉ =℃ * 1.8000 + 32.00
     *
     * @param x
     * @return
     */
    public double celsiusConverterFahrenheit(double x) {

        return x * 1.8000 + 32.00;
    }

    /**
     * Method that converts temperature in Fahrenheit to celsius
     *
     * C = F - 32.00 / 1.8000
     *
     * @param x
     * @return
     */
    public double FahrenheitConverterCelsius(double x) {

        return (x - 32.00) / 1.8000;
    }

    /**
     * Method that converts temperature in Kelvin to Fahrenheit
     *
     *
     * // ℉ =(K - 273.15)* 1.8000 + 32.00
     *
     * @param k
     * @return
     */
    public double kelvinConverterFahrenheit(double k) {

        return (k - 273.15) * 1.8000 + 32.00;
    }

    /**
     * Method that converts temperature in Fahrenheit to Kelvin
     *
     *
     * // K = (℉ - 32 / 1.8000) + 273.15
     *
     * @param f
     * @return
     */
    public double FahrenheitConverterKelvin(double f) {

        return ((f - 32) / 1.8000) + 273.15;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    //methods of override
    @Override
    public String toString() {

        return "Name : " + getName();
    }

    @Override
    public int hashCode() {

        return 1;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Converter other = (Converter) obj;
        return Objects.equals(this.name, other.name);
    }

}

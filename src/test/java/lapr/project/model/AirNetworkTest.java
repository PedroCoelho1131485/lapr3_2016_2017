/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import lapr.project.AdjacencyMap.Graph;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Pedro
 */
public class AirNetworkTest {

    private AirNetwork an;

    public AirNetworkTest() {
        an = new AirNetwork();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setAirNetwork method, of class AirNetwork.
     */
    @Test
    public void testGetAndSetAirNetwork() {
        System.out.println("setAirNetwork");
        Graph<Junction, Segment> graph = new Graph<>(true);
        AirNetwork instance = an;
        instance.setAirNetwork(graph);
        assertEquals(graph, instance.getGraph());

    }

    /**
     * Test of getGraph method, of class AirNetwork.
     */
    @Test
    public void testGetAndSetGraph() {
        System.out.println("getGraph");
        AirNetwork instance = an;
        Graph<Junction, Segment> expResult = new Graph<>(true);
        instance.setGraph(expResult);
        Graph<Junction, Segment> result = instance.getGraph();
        assertEquals(expResult, result);

    }

    /**
     * Test of addJunction method, of class AirNetwork.
     */
    @Test
    public void testAddJunction() {
        System.out.println("addJunction");
        Junction node = new Junction();
        AirNetwork instance = an;
        boolean expResult = true;
        boolean result = instance.addJunction(node);
        assertEquals(expResult, result);

    }

    /**
     * Test of findJunction method, of class AirNetwork.
     */
    @Test
    public void testFindJunction() {
        System.out.println("findJunction");
        String id = "1";
        AirNetwork instance = an;
        Junction node = new Junction("1", 1, 1);
        instance.addJunction(node);
        Junction expResult = node;
        Junction result = instance.findJunction(id);
        assertEquals(expResult, result);

    }

    /**
     * Test of addSegment method, of class AirNetwork.
     */
    @Test
    public void testAddSegment() {
        System.out.println("addSegment");
        Segment node = new Segment();
        AirNetwork instance = an;
        boolean expResult = true;
        boolean result = instance.addSegment(node);
        assertEquals(expResult, result);

    }

    /**
     * Test of setAirNetwork method, of class AirNetwork.
     */
    @Test
    public void testSetAirNetwork() {
        System.out.println("setAirNetwork");
        Graph<Junction, Segment> graph = new Graph<>(true);
        AirNetwork instance = new AirNetwork();
        instance.setAirNetwork(graph);
        assertEquals(graph,instance.getGraph());
    }

    /**
     * Test of getGraph method, of class AirNetwork.
     */
    @Test
    public void testGetGraph() {
        System.out.println("getGraph");
        AirNetwork instance = new AirNetwork();
        Graph<Junction, Segment> expResult = new Graph<>(true);
        Graph<Junction, Segment> result = instance.getGraph();
        assertEquals(expResult, result);
    }

    /**
     * Test of findJunction method, of class AirNetwork.
     */
    @Test
    public void testFindJunction_String() {
        System.out.println("findJunction");
        String name = "";
        AirNetwork instance = new AirNetwork();
        Junction expResult = new Junction();
        expResult.setName(name);
        instance.addJunction(expResult);
        Junction result = instance.findJunction(name);
        assertEquals(expResult, result);

    }

    /**
     * Test of findJunction method, of class AirNetwork.
     */
    @Test
    public void testFindJunction_double_double() {
        System.out.println("findJunction");
        double latitude = 0.0;
        double longitude = 0.0;
        AirNetwork instance = new AirNetwork();
        Junction expResult = new Junction();
        expResult.setName("1");
        expResult.setLatitude(latitude);
        expResult.setLongitude(longitude);
        instance.addJunction(expResult);
        Junction result = instance.findJunction(latitude, longitude);
        assertEquals(expResult, result);
    }

    /**
     * Test of setGraph method, of class AirNetwork.
     */
    @Test
    public void testSetGraph() {
        System.out.println("setGraph");
        Graph<Junction, Segment> graph = new Graph<>(true);
        AirNetwork instance = new AirNetwork();
        instance.setGraph(graph);
        assertEquals(graph,instance.getGraph());
    }

    /**
     * Test of hashCode method, of class AirNetwork.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        AirNetwork instance = new AirNetwork();
        int expResult = 447116;
        int result = instance.hashCode();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of equals method, of class AirNetwork.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = null;
        AirNetwork instance = new AirNetwork();
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
        obj = (AirNetwork) new AirNetwork();
        expResult = true;
        result = instance.equals(obj);
        assertEquals(expResult,result);
    }

    /**
     * Test of toString method, of class AirNetwork.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        AirNetwork instance = new AirNetwork();
        Graph graph = new Graph<>(true);
        instance.setGraph(graph);
        String expResult = "AirNetwork{" + "graph=" + graph + '}';
        String result = instance.toString();
        assertEquals(expResult, result);
      
    }
}
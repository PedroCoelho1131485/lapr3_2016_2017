package lapr.project.model.datalayer;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import lapr.project.model.Aircraft;
import lapr.project.model.AircraftModel;
import lapr.project.model.Airport;
import lapr.project.model.Cdrag;
import lapr.project.model.Flight;
import lapr.project.model.FlightPlan;
import lapr.project.model.Junction;
import lapr.project.model.Motorization;
import lapr.project.model.Project;
import lapr.project.model.Segment;
import lapr.project.model.Wind;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author Diogo Guedes <1150613@isep.ipp.pt>
 */
public class ProjectData {

    /**
     * An oracle SQL database connection.
     */
    private Connection connection;

    /**
     * Empty Constructor
     *
     */
    public ProjectData() {
    }

    /**
     * Sets the oracle SQL connection of this {@link ProjectDAO}.
     *
     * @param connection (Connection) The new connection to the database.
     */
    protected void setConnection(Connection connection) {
        this.connection = connection;
    }

    /**
     * Checks whether the connection to the database is closed or not.
     *
     * @return (boolean) True if connection is still alive.
     * @throws SQLException
     */
    public boolean isConnectionAlive() throws SQLException {
        boolean result = connection != null;
        if (result) {
            result = !connection.isClosed();
        }
        return result;
    }

    /**
     * Closes the connection to the database of this {@link ProjectDAO}.
     */
    protected void closeConnection() throws SQLException {
        if (connection != null) {
            if (!connection.isClosed()) {
                connection.close();
            }
        }
    }

    /**
     * Returns the list of existing projects in the database.
     *
     * @return list of existing projects
     */
    public List<String> getProjectList() {

        List<String> projectList = new ArrayList<>();

        try {
            CallableStatement stmt = connection.prepareCall("{? = call getprojectlist()}");

            stmt.registerOutParameter(1, OracleTypes.CURSOR, "SYS_REFCURSOR");
            stmt.execute();

            ResultSet rs = (ResultSet) stmt.getObject(1);

            while (rs.next()) {
                projectList.add(rs.getString("name"));
            }

            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new IllegalArgumentException(
                    "there are no projects to display");
        }
        return projectList;

    }

    /**
     * Returns the project whose name is received as parameter.
     *
     * @param name name project
     * @return project
     */
    public Project getProject(String name) {
        Project project = new Project();
        project.setName(name);
        getDescriptionProject(project);
        getAirportsProject(project);
        getJunctionsProject(project);
        getAircraftsProject(project);

        getSegmentsProject(project);
        getFlightPlansProject(project);
        getFlightsProject(project);
        return project;
    }

    /**
     * Adds the description to the project.
     *
     * @param project project
     */
    private void getDescriptionProject(Project project) {
        String name = project.getName();
        SQLDataBundle bundle = new SQLDataBundle();
        bundle.addData(new SQLData(name, String.class));
        try {
            DataBaseManager.getInstance().enableDBMSOutput(connection, 20000);
            SQLDataBundle result = DataBaseManager.getInstance().callFunction(connection, "STRING", "getdescriptionproject(?)", bundle);
            project.setDescription(String.class.cast(result.getData(0).getValue()));

        } catch (SQLException ex) {
            throw new IllegalArgumentException(
                    "There are no project with the specified name");
        }
    }

    /**
     * Adds a list of junctions to the project.
     *
     * @param project project
     */
    private void getJunctionsProject(Project project) {
        String name = project.getName();
        try {
            SQLDataBundle bundle2 = new SQLDataBundle();
            bundle2.addData(new SQLData(name, String.class));
            SQLDataBundle result = DataBaseManager.getInstance().callFunction(connection, "CURSOR", "getjunctionlist(?)", bundle2);

            for (int i = 0; i < result.size() - 1; i++) {

                ResultSet rs = (ResultSet) result.getData(i).getValue();
                while (rs.next()) {
                    Junction j = new Junction();
                    j.setName(rs.getString("name"));
                    j.setLatitude(rs.getDouble("latitude"));
                    j.setLongitude(rs.getDouble("longitude"));

                    project.getJunctionList().addJunction(j);
                    project.addJunction(j);
                }

            }
            ((CallableStatement) result.getData(result.size() - 1).getValue()).close();

        } catch (SQLException ex) {
            throw new IllegalArgumentException(
                    "There are no junctions with the specified project's name");
        }
    }

    /**
     * Adds a list of segments to the project.
     *
     * @param project project
     */
    private void getSegmentsProject(Project project) {
        String name = project.getName();
        try {
            SQLDataBundle bundle2 = new SQLDataBundle();
            bundle2.addData(new SQLData(name, String.class));
            SQLDataBundle result = DataBaseManager.getInstance().callFunction(connection, "CURSOR", "getsegmentlist(?)", bundle2);

            for (int i = 0; i < result.size() - 1; i++) {

                ResultSet rs = (ResultSet) result.getData(i).getValue();
                while (rs.next()) {
                    Segment s = new Segment();
                    s.setName(rs.getString("name"));
                    s.setDirection(rs.getString("direction"));

                    String wind = rs.getString("id_wind");
                    getWind(s, wind);

                    String origin = rs.getString("startNode");
                    String destiny = rs.getString("endNode");

                    getStartNode(project, s, origin);
                    getEndNode(project, s, destiny);
                    project.getSegmentList().addSegment(s);
                    project.addSegment(s);

                }

            }
            ((CallableStatement) result.getData(result.size() - 1).getValue()).close();

        } catch (SQLException ex) {
            throw new IllegalArgumentException(
                    "There are no segments with the specified project's name");
        }
    }

    /**
     * Adds Wind to Segment
     *
     * @param project project
     */
    private void getWind(Segment s, String id) {

        try {
            SQLDataBundle bundle2 = new SQLDataBundle();
            bundle2.addData(new SQLData(id, String.class));

            SQLDataBundle result = DataBaseManager.getInstance().callFunction(connection, "CURSOR", "getwind(?)", bundle2);

            for (int i = 0; i < result.size() - 1; i++) {

                ResultSet rs = (ResultSet) result.getData(i).getValue();
                while (rs.next()) {
                    Wind w = new Wind();

                    w.setWindAngle(rs.getDouble("windAngle"));
                    w.setWindSpeed(rs.getDouble("WindSpeed"));
                    w.setWindName(rs.getString("name"));

                    s.setWindStats(w);

                }

            }
            ((CallableStatement) result.getData(result.size() - 1).getValue()).close();

        } catch (SQLException ex) {
            throw new IllegalArgumentException(
                    "There are no wind with the specified project's name");
        }
    }

    /**
     * Adds a list of airports to the project.
     *
     * @param project project
     */
    private void getAirportsProject(Project project) {
        String name = project.getName();
        try {
            SQLDataBundle bundle2 = new SQLDataBundle();
            bundle2.addData(new SQLData(name, String.class));
            SQLDataBundle result = DataBaseManager.getInstance().callFunction(connection, "CURSOR", "getairportlist(?)", bundle2);

            for (int i = 0; i < result.size() - 1; i++) {

                ResultSet rs = (ResultSet) result.getData(i).getValue();
                while (rs.next()) {
                    Airport a = new Airport();
                    a.setName(rs.getString("name"));
                    a.setTown(rs.getString("town"));
                    a.setCountry(rs.getString("country"));
                    a.setLongitude(rs.getDouble("longitude"));
                    a.setLatitude(rs.getDouble("latitude"));
                    a.setAltitude(rs.getDouble("altitude"));
                    a.setIATA(rs.getString("IATA"));

                    project.getAirportList().addAirport(a);

                }

            }
            ((CallableStatement) result.getData(result.size() - 1).getValue()).close();

        } catch (SQLException ex) {
            throw new IllegalArgumentException(
                    "There are no airports with the specified project's name");
        }
    }

    /**
     * Adds a list of flights to the project.
     *
     * @param project project
     */
    private void getFlightsProject(Project project) {
        String name = project.getName();
        try {
            SQLDataBundle bundle2 = new SQLDataBundle();
            bundle2.addData(new SQLData(name, String.class));
            SQLDataBundle result = DataBaseManager.getInstance().callFunction(connection, "CURSOR", "getflightlist(?)", bundle2);

            for (int i = 0; i < result.size() - 1; i++) {

                ResultSet rs = (ResultSet) result.getData(i).getValue();
                while (rs.next()) {
                    Flight f = new Flight();
                    f.setType(rs.getString("type"));
                    f.setDepartureDay(rs.getString("departureDay"));
                    f.setMinStopTime(rs.getDouble("minStopTime"));
                    f.setScheduledArrival(rs.getString("scheduleArrival"));
                    f.setName(rs.getString("name"));

                    String s = rs.getString("id_flightplan");
                    getFlightPlan(project, f, s);

                    project.getFlightList().addFlight(f);

                }

            }
            ((CallableStatement) result.getData(result.size() - 1).getValue()).close();

        } catch (SQLException ex) {
            throw new IllegalArgumentException(
                    "There are no flights with the specified project's name");
        }
    }

    /**
     * Adds a list of aicrafts to the project.
     *
     * @param project project
     */
    private void getAircraftsProject(Project project) {
        String name = project.getName();
        try {
            SQLDataBundle bundle2 = new SQLDataBundle();
            bundle2.addData(new SQLData(name, String.class));
            SQLDataBundle result = DataBaseManager.getInstance().callFunction(connection, "CURSOR", "getaircraftlist(?)", bundle2);

            for (int i = 0; i < result.size() - 1; i++) {

                ResultSet rs = (ResultSet) result.getData(i).getValue();
                while (rs.next()) {
                    Aircraft a = new Aircraft();
                    a.setRegistration(rs.getString("registration"));
                    a.setCompany(rs.getString("Company"));
                    a.setNumberSeatsEconomic(rs.getInt("numberSeatsEco"));
                    a.setnElemCrew(rs.getInt("nElemCrew"));
                    a.setNumberSeatsExecutiv(rs.getInt("numberSeatsExec"));

                    String s = rs.getString("id_aircraftmodel");
                    getAircraftModel(project, a, s);

                    project.getAircraftList().addAircraft(a);

                }

            }
            ((CallableStatement) result.getData(result.size() - 1).getValue()).close();

        } catch (SQLException ex) {
            throw new IllegalArgumentException(
                    "There are no aircrafts with the specified project's name");
        }
    }

    /**
     * Adds Model to Aircraft
     *
     * @param project project
     */
    private void getAircraftModel(Project p, Aircraft a, String id) {

        try {
            SQLDataBundle bundle2 = new SQLDataBundle();
            bundle2.addData(new SQLData(id, String.class));

            SQLDataBundle result = DataBaseManager.getInstance().callFunction(connection, "CURSOR", "getaircraftmodel(?)", bundle2);

            for (int i = 0; i < result.size() - 1; i++) {

                ResultSet rs = (ResultSet) result.getData(i).getValue();
                while (rs.next()) {
                    AircraftModel am = new AircraftModel();

                    am.setMaker(rs.getString("maker"));
                    am.setEmptyWeight(rs.getDouble("empty_weight"));
                    am.setMaximumTakeOffWeight(rs.getDouble("MTOW"));
                    am.setMaxPayLoad(rs.getDouble("maxPayLoad"));
                    am.setMaximumFuelCapacity(rs.getDouble("MaximumFuelCapacity"));
                    am.setWingArea(rs.getDouble("wingArea"));
                    am.setWingSpan(rs.getDouble("wingSpan"));
                    am.setE(rs.getDouble("e"));
                    am.setVMO(rs.getDouble("VMO"));
                    am.setMMO(rs.getDouble("MMO"));
                    am.setType(rs.getString("Type"));
                    am.setAspectRatio(rs.getDouble("aspectRatio"));

                    String model = rs.getString("modelID");
                    am.setModelId(model);

                    String s = rs.getString("id_motorization");
                    getMotorization(am, s);
                    getCDragList(p, model, am);

                    a.setAircraftModel(am);

                }

            }
            ((CallableStatement) result.getData(result.size() - 1).getValue()).close();

        } catch (SQLException ex) {
            throw new IllegalArgumentException(
                    "There are no junctions with the specified project's name");
        }
    }

    /**
     * Adds Model to Aircraft
     *
     * @param project project
     */
    private void getMotorization(AircraftModel am, String id) {

        try {
            SQLDataBundle bundle2 = new SQLDataBundle();
            bundle2.addData(new SQLData(id, String.class));

            SQLDataBundle result = DataBaseManager.getInstance().callFunction(connection, "CURSOR", "getmotorization(?)", bundle2);

            for (int i = 0; i < result.size() - 1; i++) {

                ResultSet rs = (ResultSet) result.getData(i).getValue();
                while (rs.next()) {

                    Motorization m = new Motorization();

                    m.setMotorType(rs.getString("motorType"));
                    m.setNumberMotors(rs.getInt("numberMotors"));
                    m.setCruiseSpeed(rs.getDouble("cruiseSpeed"));
                    m.setCruiseAltitude(rs.getDouble("cruiseAltitude"));
                    m.setTSFC(rs.getDouble("TSFC"));
                    m.setLapseRateFactor(rs.getDouble("lapseRateFactor"));
                    m.setThurst_0(rs.getDouble("thrust_0"));
                    m.setThurst_max_speed(rs.getDouble("thrust_max_speed"));
                    m.setMax_speed(rs.getDouble("max_speed"));
                    m.setName(rs.getString("name"));

                    am.setMotorization(m);

                }

            }
            ((CallableStatement) result.getData(result.size() - 1).getValue()).close();

        } catch (SQLException ex) {
            throw new IllegalArgumentException(
                    "There are no junctions with the specified project's name");
        }
    }

    /**
     * Adds CDragList to AircraftModel
     *
     * @param project project
     */
    private void getCDragList(Project p, String model, AircraftModel am) {

        try {
            SQLDataBundle bundle3 = new SQLDataBundle();
            bundle3.addData(new SQLData(p.getName(), String.class));
            bundle3.addData(new SQLData(model, String.class));

            SQLDataBundle result = DataBaseManager.getInstance().callFunction(connection, "CURSOR", "getcdraglist(?,?)", bundle3);

            for (int i = 0; i < result.size() - 1; i++) {

                ResultSet rs = (ResultSet) result.getData(i).getValue();
                while (rs.next()) {

                    Cdrag c = new Cdrag();

                    c.setCdrag_0(rs.getDouble("Cdrag_0"));
                    c.setSpeed(rs.getDouble("speed"));

                    am.getCDragList().addCdrag(c);

                }

            }
            ((CallableStatement) result.getData(result.size() - 1).getValue()).close();

        } catch (SQLException ex) {
            throw new IllegalArgumentException(
                    "There are no cdrags with the specified model's name");
        }
    }

    /**
     * Adds a list of flightplan to the project.
     *
     * @param project project
     */
    public void getFlightPlansProject(Project project) {
        String name = project.getName();
        try {
            SQLDataBundle bundle2 = new SQLDataBundle();
            bundle2.addData(new SQLData(name, String.class));
            SQLDataBundle result = DataBaseManager.getInstance().callFunction(connection, "CURSOR", "getflightplanlist(?)", bundle2);

            for (int i = 0; i < result.size() - 1; i++) {

                ResultSet rs = (ResultSet) result.getData(i).getValue();
                while (rs.next()) {
                    FlightPlan fp = new FlightPlan();
                    fp.setName(rs.getString("name"));

                    String origin = rs.getString("originAirfield");
                    String destiny = rs.getString("destinyAirfield");
                    String aircraft = rs.getString("id_aircraft");

                    getAirportOrigin(project, fp, origin);
                    getAirportDestiny(project, fp, destiny);
                    getAircraft(project, fp, aircraft);
                    getFlightPlanSegments(project, fp);

                    project.getFlightPlanList().addFlightPlan(fp);

                }

            }
            ((CallableStatement) result.getData(result.size() - 1).getValue()).close();

        } catch (SQLException ex) {
            throw new IllegalArgumentException(
                    "There are no junctions with the specified project's name");
        }
    }

    /**
     * Adds Airport to FlightPlan
     *
     * @param project project
     */
    private void getAirportOrigin(Project project, FlightPlan fp, String id) {

        SQLDataBundle bundle2 = new SQLDataBundle();
        bundle2.addData(new SQLData(id, String.class));

        try {
            DataBaseManager.getInstance().enableDBMSOutput(connection, 20000);
            SQLDataBundle result = DataBaseManager.getInstance().callFunction(connection, "STRING", "getairport(?)", bundle2);
            String name = String.class.cast(result.getData(0).getValue());

            Airport a = project.getAirportList().getAirport(name);
            fp.setOriginAirfield(a);

        } catch (SQLException ex) {
            throw new IllegalArgumentException(
                    "There are no project with the specified name");

        }
    }

    /**
     * Adds Airport to FlightPlan
     *
     * @param project project
     */
    private void getAirportDestiny(Project project, FlightPlan fp, String id) {

        SQLDataBundle bundle2 = new SQLDataBundle();
        bundle2.addData(new SQLData(id, String.class));

        try {
            DataBaseManager.getInstance().enableDBMSOutput(connection, 20000);
            SQLDataBundle result = DataBaseManager.getInstance().callFunction(connection, "STRING", "getairport(?)", bundle2);
            String name = String.class.cast(result.getData(0).getValue());

            Airport a = project.getAirportList().getAirport(name);
            fp.setDesttinyAirfield(a);

        } catch (SQLException ex) {
            throw new IllegalArgumentException(
                    "There are no project with the specified name");

        }
    }

    /**
     * Adds Aircraft to FlightPlan
     *
     * @param project project
     */
    private void getAircraft(Project project, FlightPlan fp, String id) {

        SQLDataBundle bundle2 = new SQLDataBundle();
        bundle2.addData(new SQLData(id, String.class));

        try {
            DataBaseManager.getInstance().enableDBMSOutput(connection, 20000);
            SQLDataBundle result = DataBaseManager.getInstance().callFunction(connection, "STRING", "getaircraft(?)", bundle2);
            String name = String.class.cast(result.getData(0).getValue());

            Aircraft a = project.getAircraftList().getAircraft(name);
            fp.setAircraft(a);

        } catch (SQLException ex) {
            throw new IllegalArgumentException(
                    "There are no project with the specified name");

        }
    }

    /**
     * Adds FlightPlan to Flight
     *
     * @param project project
     */
    private void getFlightPlan(Project project, Flight f, String id) {

        SQLDataBundle bundle2 = new SQLDataBundle();
        bundle2.addData(new SQLData(id, String.class));

        try {
            DataBaseManager.getInstance().enableDBMSOutput(connection, 20000);
            SQLDataBundle result = DataBaseManager.getInstance().callFunction(connection, "STRING", "getflightplan(?)", bundle2);
            String name = String.class.cast(result.getData(0).getValue());

            FlightPlan fp = project.getFlightPlanList().getFlightplanByName(name);
            f.setFlightPlan(fp);

        } catch (SQLException ex) {
            throw new IllegalArgumentException(
                    "There are no project with the specified name");

        }
    }

    /**
     * Adds Junction to Segment
     *
     * @param project project
     */
    private void getStartNode(Project project, Segment s, String id) {

        SQLDataBundle bundle2 = new SQLDataBundle();
        bundle2.addData(new SQLData(id, String.class));

        try {
            DataBaseManager.getInstance().enableDBMSOutput(connection, 20000);
            SQLDataBundle result = DataBaseManager.getInstance().callFunction(connection, "STRING", "getJunction(?)", bundle2);
            String name = String.class.cast(result.getData(0).getValue());

            Junction j = project.getJunctionList().getJunctionByName(name);
            s.setStartNode(j);

        } catch (SQLException ex) {
            throw new IllegalArgumentException(
                    "There are no junction with the specified id");

        }
    }

    /**
     * Adds Junction to Segment
     *
     * @param project project
     */
    private void getEndNode(Project project, Segment s, String id) {

        SQLDataBundle bundle2 = new SQLDataBundle();
        bundle2.addData(new SQLData(id, String.class));

        try {
            DataBaseManager.getInstance().enableDBMSOutput(connection, 20000);
            SQLDataBundle result = DataBaseManager.getInstance().callFunction(connection, "STRING", "getjunction(?)", bundle2);
            String name = String.class.cast(result.getData(0).getValue());

            Junction j = project.getJunctionList().getJunctionByName(name);
            s.setEndNode(j);

        } catch (SQLException ex) {
            throw new IllegalArgumentException(
                    "There are no project with the specified name");

        }
    }

    /**
     * Adds Junction to Segment
     *
     * @param project project
     */
    private void getFlightPlanSegments(Project project, FlightPlan fp) {

        String name = project.getName();
        String plan = fp.getName();
        try {
            SQLDataBundle bundle2 = new SQLDataBundle();
            bundle2.addData(new SQLData(name, String.class));
            bundle2.addData(new SQLData(plan, String.class));
            SQLDataBundle result = DataBaseManager.getInstance().callFunction(connection, "CURSOR", "getflightplansegments(?,?)", bundle2);

            for (int i = 0; i < result.size() - 1; i++) {

                ResultSet rs = (ResultSet) result.getData(i).getValue();
                while (rs.next()) {

                    Segment s = project.getSegmentList().getSegmentByName(name);
                    fp.getStops().add(s);

                }

            }
            ((CallableStatement) result.getData(result.size() - 1).getValue()).close();

        } catch (SQLException ex) {
            throw new IllegalArgumentException(
                    "There are no junctions with the specified project's name");
        }
    }

    public boolean insertProject(Project project) {
        SQLDataBundle bundle0 = new SQLDataBundle();
        bundle0.addData(new SQLData(project.getName(), String.class));
        bundle0.addData(new SQLData(project.getDescription(), String.class));

        try {
            DataBaseManager.getInstance().enableDBMSOutput(connection, 20000);
            DataBaseManager.getInstance().callVoidProcedure(connection, "insertProject(?,?)", bundle0);
        } catch (SQLException ex) {
            try {
                this.connection.rollback();
            } catch (SQLException ex1) {
                throw new RuntimeException();
            }
            throw new IllegalArgumentException(ex.getMessage());
        }

        Iterator<Airport> iterator1 = project.getAirportList().iterator();
        while (iterator1.hasNext()) {
            Airport air = iterator1.next();
            insertAirport(air, project);
        }

        Iterator<Junction> iterator4 = project.getJunctionList().iterator();
        while (iterator4.hasNext()) {
            Junction j = iterator4.next();
            insertJunction(j, project);
        }

        Iterator<Segment> iterator = project.getSegmentList().iterator();
        while (iterator.hasNext()) {
            Segment segment = iterator.next();
            insertSegment(segment, project);
        }

        Iterator<Aircraft> iterator2 = project.getAircraftList().iterator();
        while (iterator2.hasNext()) {
            Aircraft aircraft = iterator2.next();
            insertAircraft(project, aircraft);
        }

        Iterator<FlightPlan> iterator3 = project.getFlightPlanList().iterator();
        while (iterator3.hasNext()) {
            FlightPlan fp = iterator3.next();
            insertFlightPlan(project, fp);
        }

        Iterator<Flight> iterator5 = project.getFlightList().iterator();
        while (iterator5.hasNext()) {
            Flight fl = iterator5.next();
            insertFlight(project, fl);
        }

        try {
            this.connection.commit();
            closeConnection();
            return true;
        } catch (SQLException ex) {
            throw new RuntimeException();
        }
    }

    private void insertJunction(Junction junction, Project project) {
        SQLDataBundle bundle = new SQLDataBundle();
        bundle.addData(new SQLData(project.getName(), String.class));
        bundle.addData(new SQLData(junction.getLatitude(), Double.class));
        bundle.addData(new SQLData(junction.getLongitude(), Double.class));
        bundle.addData(new SQLData(junction.getName(), String.class));

        try {
            DataBaseManager.getInstance().callVoidProcedure(connection, "insertJunction(?,?,?,?)", bundle);
        } catch (SQLException ex) {
            ex.printStackTrace();
            try {
                this.connection.rollback();
            } catch (SQLException ex1) {
                throw new RuntimeException();
            }
            throw new IllegalArgumentException(ex.getMessage());
        }

    }

    private void insertWind(Wind wind, Project proj) {
        SQLDataBundle bundle1 = new SQLDataBundle();
        bundle1.addData(new SQLData(proj.getName(), String.class));
        bundle1.addData(new SQLData(wind.getWindAngle(), Double.class));
        bundle1.addData(new SQLData(wind.getWindSpeed(), Double.class));
        bundle1.addData(new SQLData(wind.getWindName(), String.class));

        try {
            DataBaseManager.getInstance().callVoidProcedure(connection, "insertWind(?,?,?,?)", bundle1);
        } catch (SQLException ex) {
            ex.printStackTrace();
            try {
                this.connection.rollback();
            } catch (SQLException ex1) {
                throw new RuntimeException();
            }
            throw new IllegalArgumentException(ex.getMessage());
        }
    }

    private void insertSegment(Segment segment, Project project) {
        Junction startNode = segment.getStartNode();
        Junction endNode = segment.getEndNode();
        Wind wind = segment.getWindStats();
        insertWind(wind, project);

        SQLDataBundle bundle2 = new SQLDataBundle();
        bundle2.addData(new SQLData(project.getName(), String.class));
        bundle2.addData(new SQLData(segment.getDirection(), String.class));
        bundle2.addData(new SQLData(startNode.getName(), String.class));
        bundle2.addData(new SQLData(endNode.getName(), String.class));
        bundle2.addData(new SQLData(segment.getName(), String.class));
        bundle2.addData(new SQLData(wind.getWindName(), String.class));

        try {
            DataBaseManager.getInstance().callVoidProcedure(connection, "insertSegment(?,?,?,?,?,?)", bundle2);
        } catch (SQLException ex) {
            ex.printStackTrace();
            try {
                this.connection.rollback();
            } catch (SQLException ex1) {
                throw new RuntimeException();
            }
            throw new IllegalArgumentException(ex.getMessage());
        }
    }

    private void insertAirport(Airport airport, Project project) {
        SQLDataBundle bundle3 = new SQLDataBundle();
        bundle3.addData(new SQLData(project.getName(), String.class));
        bundle3.addData(new SQLData(airport.getName(), String.class));
        bundle3.addData(new SQLData(airport.getTown(), String.class));
        bundle3.addData(new SQLData(airport.getCountry(), String.class));
        bundle3.addData(new SQLData(airport.getLongitude(), Double.class));
        bundle3.addData(new SQLData(airport.getLatitude(), Double.class));
        bundle3.addData(new SQLData(airport.getAltitude(), Double.class));
        bundle3.addData(new SQLData(airport.getIATA(), String.class));

        try {
            DataBaseManager.getInstance().callVoidProcedure(connection, "insertAirport(?,?,?,?,?,?,?,?)", bundle3);
        } catch (SQLException ex) {
            ex.printStackTrace();
            try {
                this.connection.rollback();
            } catch (SQLException ex1) {
                throw new RuntimeException();
            }
            throw new IllegalArgumentException(ex.getMessage());
        }
    }

    private void insertMotorization(Motorization motorization, Project p) {
        SQLDataBundle bundle4 = new SQLDataBundle();
        bundle4.addData(new SQLData(p.getName(), String.class));
        bundle4.addData(new SQLData(motorization.getMotorType(), String.class));
        bundle4.addData(new SQLData(motorization.getNumberMotors(), Integer.class));
        bundle4.addData(new SQLData(motorization.getCruiseSpeed(), Double.class));
        bundle4.addData(new SQLData(motorization.getCruiseAltitude(), Double.class));
        bundle4.addData(new SQLData(motorization.getTSFC(), Double.class));
        bundle4.addData(new SQLData(motorization.getLapseRateFactor(), Double.class));
        bundle4.addData(new SQLData(motorization.getThurst_0(), Double.class));
        bundle4.addData(new SQLData(motorization.getThurst_max_speed(), Double.class));
        bundle4.addData(new SQLData(motorization.getMax_speed(), Double.class));
        bundle4.addData(new SQLData(motorization.getName(), String.class));

        try {
            DataBaseManager.getInstance().callVoidProcedure(connection, "insertMotorization(?,?,?,?,?,?,?,?,?,?,?)", bundle4);
        } catch (SQLException ex) {
            ex.printStackTrace();
            try {
                this.connection.rollback();
            } catch (SQLException ex1) {
                throw new RuntimeException();
            }
            throw new IllegalArgumentException(ex.getMessage());
        }

    }

    private void insertCdrag(Project p, Cdrag cdrag, AircraftModel model) {
        SQLDataBundle bundle5 = new SQLDataBundle();
        bundle5.addData(new SQLData(p.getName(), String.class));
        bundle5.addData(new SQLData(cdrag.getSpeed(), Double.class));
        bundle5.addData(new SQLData(cdrag.getCdrag_0(), Double.class));
        bundle5.addData(new SQLData(model.getModelId(), String.class));

        try {
            DataBaseManager.getInstance().callVoidProcedure(connection, "insertCdrag(?,?,?,?)", bundle5);
        } catch (SQLException ex) {
            ex.printStackTrace();
            try {
                this.connection.rollback();
            } catch (SQLException ex1) {
                throw new RuntimeException();
            }
            throw new IllegalArgumentException(ex.getMessage());
        }
    }

    private void insertAircraftModel(AircraftModel model, Project p) {
        SQLDataBundle bundle6 = new SQLDataBundle();

        Motorization m = model.getMotorization();
        insertMotorization(m, p);
        bundle6.addData(new SQLData(p.getName(), String.class));
        bundle6.addData(new SQLData(model.getMaker(), String.class));
        bundle6.addData(new SQLData(model.getEmptyWeight(), Double.class));
        bundle6.addData(new SQLData(model.getMaximumTakeOffWeight(), Double.class));
        bundle6.addData(new SQLData(model.getMaxPayLoad(), Double.class));
        bundle6.addData(new SQLData(model.getMaximumFuelCapacity(), Double.class));
        bundle6.addData(new SQLData(model.getWingArea(), Double.class));
        bundle6.addData(new SQLData(model.getWingSpan(), Double.class));
        bundle6.addData(new SQLData(model.getE(), Double.class));
        bundle6.addData(new SQLData(model.getVMO(), Double.class));
        bundle6.addData(new SQLData(model.getMMO(), Double.class));
        bundle6.addData(new SQLData(model.getType(), String.class));
        bundle6.addData(new SQLData(model.getAspectRatio(), Double.class));
        bundle6.addData(new SQLData(m.getName(), String.class));
        bundle6.addData(new SQLData(model.getModelId(), String.class));

        try {
            DataBaseManager.getInstance().callVoidProcedure(connection, "insertAircraftModel(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", bundle6);
        } catch (SQLException ex) {
            ex.printStackTrace();
            try {
                this.connection.rollback();
            } catch (SQLException ex1) {
                throw new RuntimeException();
            }

            throw new IllegalArgumentException(ex.getMessage());

        }

        Iterator<Cdrag> iterator = model.getCDragList().iterator();
        while (iterator.hasNext()) {
            Cdrag c = iterator.next();
            insertCdrag(p, c, model);
        }

    }

    private void insertAircraft(Project project, Aircraft aircraft) {
        SQLDataBundle bundle7 = new SQLDataBundle();

        insertAircraftModel(aircraft.getAircraftModel(), project);
        bundle7.addData(new SQLData(project.getName(), String.class));
        bundle7.addData(new SQLData(aircraft.getRegistration(), String.class));
        bundle7.addData(new SQLData(aircraft.getCompany(), String.class));
        bundle7.addData(new SQLData(aircraft.getNumberSeatsEconomic(), Integer.class));
        bundle7.addData(new SQLData(aircraft.getnElemCrew(), Integer.class));
        AircraftModel model = aircraft.getAircraftModel();
        bundle7.addData(new SQLData(model.getModelId(), String.class));
        bundle7.addData(new SQLData(aircraft.getNumberSeatsExecutiv(), Integer.class));

        try {
            DataBaseManager.getInstance().callVoidProcedure(connection, "insertAircraft(?,?,?,?,?,?,?)", bundle7);
        } catch (SQLException ex) {
            ex.printStackTrace();
            try {
                this.connection.rollback();
            } catch (SQLException ex1) {
                throw new RuntimeException();
            }
            throw new IllegalArgumentException(ex.getMessage());
        }

    }

    public void insertFlight(Project project, Flight flight) {
        SQLDataBundle bundle8 = new SQLDataBundle();
        bundle8.addData(new SQLData(project.getName(), String.class));
        bundle8.addData(new SQLData(flight.getType(), String.class));
        bundle8.addData(new SQLData(flight.getDepartureDay(), String.class));
        bundle8.addData(new SQLData(flight.getMinStopTime(), Double.class));
        bundle8.addData(new SQLData(flight.getScheduledArrival(), String.class));
        FlightPlan fp = flight.getFlightPlan();
        bundle8.addData(new SQLData(fp.getName(), String.class));
        bundle8.addData(new SQLData(flight.getName(), String.class));

        try {
            DataBaseManager.getInstance().callVoidProcedure(connection, "insertFlight(?,?,?,?,?,?,?)", bundle8);
        } catch (SQLException ex) {
            ex.printStackTrace();
            try {
                this.connection.rollback();
            } catch (SQLException ex1) {
                throw new RuntimeException();
            }
            throw new IllegalArgumentException(ex.getMessage());
        }

    }

    public void insertFlightPlan(Project project, FlightPlan flightPlan) {
        SQLDataBundle bundle10 = new SQLDataBundle();
        bundle10.addData(new SQLData(project.getName(), String.class));
        Airport origin = flightPlan.getOriginAirfield();
        Airport destiny = flightPlan.getDesttinyAirfield();
        Aircraft air = flightPlan.getAircraft();
        bundle10.addData(new SQLData(origin.getIATA(), String.class));
        bundle10.addData(new SQLData(destiny.getIATA(), String.class));
        bundle10.addData(new SQLData(air.getRegistration(), String.class));
        bundle10.addData(new SQLData(flightPlan.getName(), String.class));

        try {
            DataBaseManager.getInstance().callVoidProcedure(connection, "insertFlightPlan(?,?,?,?,?)", bundle10);
        } catch (SQLException ex) {
            ex.printStackTrace();
            try {
                this.connection.rollback();
            } catch (SQLException ex1) {
                throw new RuntimeException();
            }
            throw new IllegalArgumentException(ex.getMessage());
        }

        Iterator<Segment> iterator = flightPlan.getStops().iterator();
        while (iterator.hasNext()) {
            Segment s = iterator.next();
            insertFlightPlanSegments(s, flightPlan);

        }
    }

    private void insertFlightPlanSegments(Segment segment, FlightPlan flightPlan) {
        SQLDataBundle bundle9 = new SQLDataBundle();
        bundle9.addData(new SQLData(segment.getName(), String.class));
        bundle9.addData(new SQLData(flightPlan.getName(), String.class));

        try {
            DataBaseManager.getInstance().callVoidProcedure(connection, "insertFlightPlanSegments(?,?)", bundle9);
        } catch (SQLException ex) {
            ex.printStackTrace();
            try {
                this.connection.rollback();
            } catch (SQLException ex1) {
                throw new RuntimeException();
            }
            throw new IllegalArgumentException(ex.getMessage());
        }
    }

    public boolean deleteProject(String name) {
        SQLDataBundle bundle = new SQLDataBundle();
        bundle.addData(new SQLData(name, String.class));

        try {
            DataBaseManager.getInstance().enableDBMSOutput(connection, 20000);
            DataBaseManager.getInstance().callVoidProcedure(connection, "deleteProject(?)", bundle);
        } catch (SQLException ex) {
            try {
                this.connection.rollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace();
                throw new RuntimeException();
            }
            throw new IllegalArgumentException(ex.getMessage());
        }

        try {
            this.connection.commit();
            closeConnection();
            return true;
        } catch (SQLException ex) {
            throw new RuntimeException();
        }
    }

    public boolean updateProject(Project newProject, Project old) {
        SQLDataBundle bundle = new SQLDataBundle();
        bundle.addData(new SQLData(old.getName(), String.class));
        bundle.addData(new SQLData(newProject.getName(), String.class));
        bundle.addData(new SQLData(newProject.getDescription(), String.class));

        try {
            DataBaseManager.getInstance().enableDBMSOutput(connection, 20000);
            DataBaseManager.getInstance().callVoidProcedure(connection, "updateProject(?,?,?)", bundle);
        } catch (SQLException ex) {
            try {
                this.connection.rollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace();
                throw new RuntimeException();
            }
            throw new IllegalArgumentException(ex.getMessage());
        }

        for (Aircraft air : newProject.getAircraftList().getAircraftList()) {
            if (!old.getAircraftList().getAircraftList().contains(air)) {
                insertAircraft(newProject, air);
            }
        }

        for (Segment seg : newProject.getSegmentList().getsegmentList()) {
            if (!old.getSegmentList().getsegmentList().contains(seg)) {
                insertSegment(seg, newProject);
            }
        }

        for (Airport airport : newProject.getAirportList().getAirports()) {
            if (!old.getAirportList().getAirports().contains(airport)) {
                insertAirport(airport, newProject);
            }
        }

        for (Junction junction : newProject.getJunctionList().getjunctionList()) {
            if (!old.getJunctionList().getjunctionList().contains(junction)) {
                insertJunction(junction, newProject);
            }
        }

        try {
            this.connection.commit();
            closeConnection();
            return true;
        } catch (SQLException ex) {
            throw new RuntimeException();
        }

    }
}

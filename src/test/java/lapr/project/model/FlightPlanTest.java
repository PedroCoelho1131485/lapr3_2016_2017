/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author pedro
 */
public class FlightPlanTest {

    private FlightPlan fp;

    public FlightPlanTest() {

        fp = new FlightPlan();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getName method, of class FlightPlan.
     */
    @Test
    public void testGetAndSetName() {

        System.out.println("getName");
        FlightPlan instance = fp;
        String expResult = "nome";
        instance.setName(expResult);
        String result = instance.getName();
        assertEquals(expResult, result);

    }

    /**
     * Test of getAircraft method, of class FlightPlan.
     */
    @Test
    public void testGetAndSetAircraft() {
        System.out.println("getAircraft");
        FlightPlan instance = fp;
        Aircraft expResult = new Aircraft();
        instance.setAircraft(expResult);
        Aircraft result = instance.getAircraft();
        assertEquals(expResult, result);

    }

    /**
     * Test of getMaxPassengersClasseEco method, of class FlightPlan.
     */
    @Test
    public void testGetAndSetMaxPassengersClasseEco() {
        System.out.println("getMaxPassengersClasseEco");
        FlightPlan instance = fp;
        Aircraft aircraft = new Aircraft();
        instance.setAircraft(aircraft);
        int expResult = 2;
        instance.setMaxPassengersClasseEco(expResult);
        int result = instance.getMaxPassengersClasseEco();
        assertEquals(expResult, result);

    }

    /**
     * Test of getMaxPassengersClasseExec method, of class FlightPlan.
     */
    @Test
    public void testGetAndSetMaxPassengersClasseExec() {
        System.out.println("getMaxPassengersClasseExec");
        FlightPlan instance = fp;
        Aircraft aircraft = new Aircraft();
        instance.setAircraft(aircraft);
        int expResult = 4;
        instance.setMaxPassengersClasseExec(expResult);
        int result = instance.getMaxPassengersClasseExec();
        assertEquals(expResult, result);

    }

    /**
     * Test of getCrew method, of class FlightPlan.
     */
    @Test
    public void testGetAndSetCrew() {
        System.out.println("getCrew");
        FlightPlan instance = fp;
        Aircraft aircraft = new Aircraft();
        instance.setAircraft(aircraft);
        int expResult = 3;
        instance.setCrew(expResult);
        int result = instance.getCrew();
        assertEquals(expResult, result);

    }

    /**
     * Test of getOriginAirfield method, of class FlightPlan.
     */
    @Test
    public void testGetAndSetOriginAirfield() {
        System.out.println("getOriginAirfield");
        FlightPlan instance = fp;
        Airport expResult = new Airport();
        instance.setOriginAirfield(expResult);
        Airport result = instance.getOriginAirfield();
        assertEquals(expResult, result);

    }

    /**
     * Test of getDesttinyAirfield method, of class FlightPlan.
     */
    @Test
    public void testGetAndSetDesttinyAirfield() {
        System.out.println("getDesttinyAirfield");
        FlightPlan instance = fp;
        Airport expResult = new Airport();
        instance.setDesttinyAirfield(expResult);
        Airport result = instance.getDesttinyAirfield();
        assertEquals(expResult, result);

    }

    /**
     * Test of getName method, of class FlightPlan.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        FlightPlan instance = new FlightPlan();
        String expResult = "";
        instance.setName(expResult);
        String result = instance.getName();
        assertEquals(expResult, result);

    }

    /**
     * Test of setName method, of class FlightPlan.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "";
        FlightPlan instance = new FlightPlan();
        instance.setName(name);
        assertEquals(name,instance.getName());
    }

    /**
     * Test of getAircraft method, of class FlightPlan.
     */
    @Test
    public void testGetAircraft() {
        System.out.println("getAircraft");
        FlightPlan instance = new FlightPlan();
        Aircraft expResult = new Aircraft();
        instance.setAircraft(expResult);
        Aircraft result = instance.getAircraft();
        assertEquals(expResult, result);

    }

    /**
     * Test of getMaxPassengersClasseEco method, of class FlightPlan.
     */
    @Test
    public void testGetMaxPassengersClasseEco() {
        System.out.println("getMaxPassengersClasseEco");
        FlightPlan instance = new FlightPlan();
        Aircraft a = new Aircraft();
        a.setNumberSeatsEconomic(0);
        instance.setAircraft(a);
        int expResult = 0;
        instance.setMaxPassengersClasseEco(expResult);
        int result = instance.getMaxPassengersClasseEco();
        assertEquals(expResult, result);
     
    }

    /**
     * Test of setMaxPassengersClasseEco method, of class FlightPlan.
     */
    @Test
    public void testSetMaxPassengersClasseEco() {
        System.out.println("setMaxPassengersClasseEco");
        int maxPassengersClasseEco = 0;
        FlightPlan instance = new FlightPlan();
        Aircraft a = new Aircraft();
        instance.setAircraft(a);
        instance.setMaxPassengersClasseEco(maxPassengersClasseEco);
        assertEquals(maxPassengersClasseEco,instance.getMaxPassengersClasseEco(),0);
    }

    /**
     * Test of getMaxPassengersClasseExec method, of class FlightPlan.
     */
    @Test
    public void testGetMaxPassengersClasseExec() {
        System.out.println("getMaxPassengersClasseExec");
        FlightPlan instance = new FlightPlan();
        Aircraft a = new Aircraft();
        a.setNumberSeatsExecutiv(0);
        instance.setAircraft(a);
        int expResult = 0;
        instance.setMaxPassengersClasseExec(expResult);
        int result = instance.getMaxPassengersClasseExec();
        assertEquals(expResult, result);

    }

    /**
     * Test of setMaxPassengersClasseExec method, of class FlightPlan.
     */
    @Test
    public void testSetMaxPassengersClasseExec() {
        System.out.println("setMaxPassengersClasseExec");
        int maxPassengersClasseExec = 0;
        FlightPlan instance = new FlightPlan();
        Aircraft a = new Aircraft();
        instance.setAircraft(a);
        instance.setMaxPassengersClasseExec(maxPassengersClasseExec);
        assertEquals(maxPassengersClasseExec,instance.getMaxPassengersClasseExec(),0);
    }

    /**
     * Test of setAircraft method, of class FlightPlan.
     */
    @Test
    public void testSetAircraft() {
        System.out.println("setAircraft");
        Aircraft aircraft = new Aircraft();
        FlightPlan instance = new FlightPlan();
        instance.setAircraft(aircraft);
        assertEquals(aircraft,instance.getAircraft());
    }

    /**
     * Test of getCrew method, of class FlightPlan.
     */
    @Test
    public void testGetCrew() {
        System.out.println("getCrew");
        FlightPlan instance = new FlightPlan();
        Aircraft a = new Aircraft();
        a.setnElemCrew(0);
        instance.setAircraft(a);
        int expResult = 0;
        instance.setCrew(expResult);
        int result = instance.getCrew();
        assertEquals(expResult, result);

    }

    /**
     * Test of setCrew method, of class FlightPlan.
     */
    @Test
    public void testSetCrew() {
        System.out.println("setCrew");
        int crew = 0;
        FlightPlan instance = new FlightPlan();
        Aircraft a = new Aircraft();
        instance.setAircraft(a);
        instance.setCrew(crew);
        assertEquals(crew,instance.getCrew(),0);
    }

    /**
     * Test of getOriginAirfield method, of class FlightPlan.
     */
    @Test
    public void testGetOriginAirfield() {
        System.out.println("getOriginAirfield");
        FlightPlan instance = new FlightPlan();
        Airport expResult = new Airport();
        instance.setOriginAirfield(expResult);
        Airport result = instance.getOriginAirfield();
        assertEquals(expResult, result);

    }

    /**
     * Test of setOriginAirfield method, of class FlightPlan.
     */
    @Test
    public void testSetOriginAirfield() {
        System.out.println("setOriginAirfield");
        Airport originAirfield = new Airport();
        FlightPlan instance = new FlightPlan();
        instance.setOriginAirfield(originAirfield);
        assertEquals(originAirfield,instance.getOriginAirfield());
    }

    /**
     * Test of getDesttinyAirfield method, of class FlightPlan.
     */
    @Test
    public void testGetDesttinyAirfield() {
        System.out.println("getDesttinyAirfield");
        FlightPlan instance = new FlightPlan();
        Airport expResult = new Airport();
        instance.setDesttinyAirfield(expResult);
        Airport result = instance.getDesttinyAirfield();
        assertEquals(expResult, result);

    }

    /**
     * Test of setDesttinyAirfield method, of class FlightPlan.
     */
    @Test
    public void testSetDesttinyAirfield() {
        System.out.println("setDesttinyAirfield");
        Airport desttinyAirfield = null;
        FlightPlan instance = new FlightPlan();
        instance.setDesttinyAirfield(desttinyAirfield);
        assertEquals(desttinyAirfield,instance.getDesttinyAirfield());
    }

     /**
     * Test of addStop method, of class FlightPlan.
     */
    @Test
    public void testAddStop() {
        System.out.println("addStop");
        Segment segment = new Segment();
        FlightPlan instance = new FlightPlan();
        boolean expResult = true;
        boolean result = instance.addStop(segment);
        assertEquals(expResult, result);
     
    }

    /**
     * Test of hashCode method, of class FlightPlan.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        FlightPlan instance = new FlightPlan();
        int expResult = 5;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class FlightPlan.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = (FlightPlan) new FlightPlan();
        FlightPlan instance = new FlightPlan();
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
        expResult = false;
         instance.setName("001");
        result = instance.equals(obj);
        assertEquals(expResult,result);
    }

  }

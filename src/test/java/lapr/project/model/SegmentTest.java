
package lapr.project.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;


public class SegmentTest {
    
    public SegmentTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getWindAngle method, of class Segment.
     */
    @Test
    public void testGetWindAngle() {
        System.out.println("getWindAngle");
        Segment instance = new Segment();
        double expResult = 0.0;
        instance.setWindAngle(expResult);
        double result = instance.getWindAngle();
        assertEquals(expResult, result, 0.0);
 
    }

    /**
     * Test of setWindAngle method, of class Segment.
     */
    @Test
    public void testSetWindAngle() {
        System.out.println("setWindAngle");
        double windAngle = 0.0;
        Segment instance = new Segment();
        instance.setWindAngle(windAngle);
        assertEquals(windAngle,instance.getWindAngle(),0.0);
    }

    /**
     * Test of getWindSpeed method, of class Segment.
     */
    @Test
    public void testGetWindSpeed() {
        System.out.println("getWindSpeed");
        Segment instance = new Segment();
        double expResult = 0.0;
        instance.setWindSpeed(expResult);
        double result = instance.getWindSpeed();
        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of setWindSpeed method, of class Segment.
     */
    @Test
    public void testSetWindSpeed() {
        System.out.println("setWindSpeed");
        double windSpeed = 0.0;
        Segment instance = new Segment();
        instance.setWindSpeed(windSpeed);
        assertEquals(windSpeed,instance.getWindSpeed(),0.0);
    }

    /**
     * Test of getName method, of class Segment.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Segment instance = new Segment();
        String expResult = "";
        instance.setName(expResult);
        String result = instance.getName();
        assertEquals(expResult, result);

    }

    /**
     * Test of setName method, of class Segment.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "";
        Segment instance = new Segment();
        instance.setName(name);
        assertEquals(name,instance.getName());
    }

    /**
     * Test of getWindStats method, of class Segment.
     */
    @Test
    public void testGetWindStats() {
        System.out.println("getWindStats");
        Segment instance = new Segment();
        Wind expResult = new Wind();
        instance.setWindStats(expResult);
        Wind result = instance.getWindStats();
        assertEquals(expResult, result);
     
    }

    /**
     * Test of setWindStats method, of class Segment.
     */
    @Test
    public void testSetWindStats() {
        System.out.println("setWindStats");
        Wind windStats = new Wind();
        Segment instance = new Segment();
        instance.setWindStats(windStats);
        assertEquals(windStats,instance.getWindStats());
    }

    /**
     * Test of getStartNode method, of class Segment.
     */
    @Test
    public void testGetStartNode() {
        System.out.println("getStartNode");
        Segment instance = new Segment();
        Junction expResult = new Junction();
        instance.setStartNode(expResult);
        Junction result = instance.getStartNode();
        assertEquals(expResult, result);
  
    }

    /**
     * Test of setStartNode method, of class Segment.
     */
    @Test
    public void testSetStartNode() {
        System.out.println("setStartNode");
        Junction startNode = new Junction();
        Segment instance = new Segment();
        instance.setStartNode(startNode);
        assertEquals(startNode,instance.getStartNode());
    }

    /**
     * Test of getEndNode method, of class Segment.
     */
    @Test
    public void testGetEndNode() {
        System.out.println("getEndNode");
        Segment instance = new Segment();
        Junction expResult = new Junction();
        instance.setEndNode(expResult);
        Junction result = instance.getEndNode();
        assertEquals(expResult, result);
       
    }

    /**
     * Test of setEndNode method, of class Segment.
     */
    @Test
    public void testSetEndNode() {
        System.out.println("setEndNode");
        Junction endNode = new Junction();
        Segment instance = new Segment();
        instance.setEndNode(endNode);
        assertEquals(endNode,instance.getEndNode());
    }

    /**
     * Test of getDirection method, of class Segment.
     */
    @Test
    public void testGetDirection() {
        System.out.println("getDirection");
        Segment instance = new Segment();
        String expResult = "";
        instance.setDirection(expResult);
        String result = instance.getDirection();
        assertEquals(expResult, result);

    }

    /**
     * Test of setDirection method, of class Segment.
     */
    @Test
    public void testSetDirection() {
        System.out.println("setDirection");
        String direction = "";
        Segment instance = new Segment();
        instance.setDirection(direction);
        assertEquals(direction,instance.getDirection());
    }

    /**
     * Test of toString method, of class Segment.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Segment instance = new Segment();
        String name = "";
        Wind windStats = new Wind();
        Junction startNode = new Junction();
        Junction endNode = new Junction();
        String direction = "";
        instance.setName(name);
        instance.setDirection(direction);
        instance.setEndNode(endNode);
        instance.setStartNode(startNode);
        instance.setWindStats(windStats);
        String expResult = "Segment{" + "name=" + name + ", windStats=" + windStats + ", startNode=" + startNode + ", endNode=" + endNode + ", direction=" + direction + '}';;
        String result = instance.toString();
        assertEquals(expResult, result);
     
    }

    /**
     * Test of hashCode method, of class Segment.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Segment instance = new Segment();
        int expResult = 1421336870;
        int result = instance.hashCode();
        assertEquals(expResult, result);

    }

    /**
     * Test of equals method, of class Segment.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = (Segment)new Segment();
        Segment instance = new Segment();
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
        instance.setName("Segment1");
        result = instance.equals(obj);
        expResult = false;
        assertEquals(expResult,result);
    }

    /**
     * Test of validate method, of class Segment.
     */
    @Test
    public void testValidate() {
        System.out.println("validate");
        Segment instance = new Segment();
        String name = "001";
        instance.setName(name);
        Junction startNode = new Junction();
        Junction endNode = new Junction();
        instance.setEndNode(endNode);
        instance.setStartNode(startNode);
        boolean expResult = true;
        boolean result = instance.validate();
        assertEquals(expResult, result);
    
    }
    
}

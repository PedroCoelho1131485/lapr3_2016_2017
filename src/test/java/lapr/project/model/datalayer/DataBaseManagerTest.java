/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model.datalayer;

import java.sql.Connection;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Diogo Guedes <1150613@isep.ipp.pt>
 */
public class DataBaseManagerTest {

    public DataBaseManagerTest() {
    }

    /**
     * Test of getInstance method, of class DataBaseManager.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");

        assertTrue("Should be true", DataBaseManager.getInstance() != null);

        String result = DataBaseManager.getInstance().getClass().getSimpleName();
        assertTrue("Should be a same class", result.equalsIgnoreCase("DataBaseManager"));

    }

    /**
     * Test of resetDataBaseManagerToTest method, of class DataBaseManager.
     */
    @Test
    public void testResetDataBaseManagerToTest() {
        System.out.println("resetDataBaseManagerToTest");

        DataBaseManager instance = DataBaseManager.getInstance();

    }

    /**
     * Test of isEnableDBMSonAll method, of class DataBaseManager.
     */
    @Test
    public void testIsEnableDBMSonAll() {
        System.out.println("isEnableDBMSonAll");
        DataBaseManager instance = DataBaseManager.getInstance();
        boolean expResult = false;
        boolean result = instance.isEnableDBMSonAll();
        assertEquals(expResult, result);

        instance.setEnableDBMSonAll(true);
        expResult = true;
        assertEquals(expResult, instance.isEnableDBMSonAll());
    }

}

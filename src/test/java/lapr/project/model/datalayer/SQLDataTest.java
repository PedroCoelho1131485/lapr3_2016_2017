/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.model.datalayer;

import lapr.project.model.ProjectRegistry;
import lapr.project.model.Project;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Diogo Guedes <1150613@isep.ipp.pt>
 */
public class SQLDataTest {
    
    public SQLDataTest() {
    }
    


      @Test
    public void testSetGetValue() {
        System.out.println("setValue");
        Object value = new Project();
        SQLData instance = new SQLData();
        instance.setValue(value);
        assertEquals(value,instance.getValue());
    }

    /**
     * Test of setClazz method, of class SQLData.
     */
    @Test
    public void testSetGetClazz() {
        System.out.println("setClazz");
        Class clazz = ProjectRegistry.class;
        SQLData instance = new SQLData();
        instance.setClazz(clazz);
    }
}

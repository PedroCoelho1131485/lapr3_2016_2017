package lapr.project.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class MotorizationTest {

    private Motorization m;

    public MotorizationTest() {
        m = new Motorization();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getName method, of class Motorization.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        Motorization instance = new Motorization();
        String expResult = "name";
        instance.setName(expResult);
        String result = instance.getName();
        assertEquals(expResult, result);
    
    }

    /**
     * Test of setName method, of class Motorization.
     */
    @Test
    public void testSetName() {
        System.out.println("setName");
        String name = "Name";
        Motorization instance = new Motorization();
        instance.setName(name);
        assertEquals(name,instance.getName());
    }

    /**
     * Test of getNumberMotors method, of class Motorization.
     */
    @Test
    public void testGetNumberMotors() {
        System.out.println("getNumberMotors");
        Motorization instance = new Motorization();
        int expResult = 0;
        instance.setNumberMotors(expResult);
        int result = instance.getNumberMotors();
        assertEquals(expResult, result,0);
      ;
    }

    /**
     * Test of setNumberMotors method, of class Motorization.
     */
    @Test
    public void testSetNumberMotors() {
        System.out.println("setNumberMotors");
        int numberMotors = 0;
        Motorization instance = new Motorization();
        instance.setNumberMotors(numberMotors);
        assertEquals(numberMotors,instance.getNumberMotors(),0);
    }

    /**
     * Test of getMotor method, of class Motorization.
     */
    @Test
    public void testGetMotor() {
        System.out.println("getMotor");
        Motorization instance = new Motorization();
        String expResult = "";
        String result = instance.getMotor();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of setMotor method, of class Motorization.
     */
    @Test
    public void testSetMotor() {
        System.out.println("setMotor");
        String motor = "";
        Motorization instance = new Motorization();
        instance.setMotor(motor);
        assertEquals(motor,instance.getMotor());
    }

    /**
     * Test of getMotorType method, of class Motorization.
     */
    @Test
    public void testGetMotorType() {
        System.out.println("getMotorType");
        Motorization instance = new Motorization();
        String expResult = "Type";
        instance.setMotorType(expResult);
        String result = instance.getMotorType();
        assertEquals(expResult, result);
    }

    /**
     * Test of setMotorType method, of class Motorization.
     */
    @Test
    public void testSetMotorType() {
        System.out.println("setMotorType");
        String motorType = "motor";
        Motorization instance = new Motorization();
        instance.setMotorType(motorType);
        assertEquals(motorType,instance.getMotorType());
    }

    /**
     * Test of hashCode method, of class Motorization.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Motorization instance = new Motorization();
        int expResult = 7;
        int result = instance.hashCode();
        assertEquals(expResult, result);

    }

    /**
     * Test of equals method, of class Motorization.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object obj = (Motorization) new Motorization();
        Motorization instance = new Motorization();
        assertTrue(instance.equals(obj));
        instance.setCruiseAltitude(100);
        assertFalse(instance.equals(obj));
        Object obj2 = null;
        assertFalse(instance.equals(obj2));
        
    }

    /**
     * Test of toString method, of class Motorization.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Motorization instance = new Motorization();
        int numberMotors = 0;
        String motor = "Motor";
        String motorType = "MotorType";
        String name = "motor";
        double cruiseSpeed = 1440.0;
        double cruiseAltitude = 2000.0;
        double TSFC = 12.0;
        double lapseRateFactor = 4.0;
        double thurst_0 = 0.4;
        double thurst_max_speed = 10.0;
        double max_speed = 2000.0;
        instance.setCruiseAltitude(cruiseAltitude);
        instance.setNumberMotors(numberMotors);
        instance.setMotorType(motorType);
        instance.setMotor(motor);
        instance.setTSFC(TSFC);
        instance.setThurst_0(thurst_0);
        instance.setThurst_max_speed(thurst_max_speed);
        
        //(String name,int numberMotors, String motor, String motorType, double speed, double height,
        //double tsfc, double lapse, double t0, double tmax, double max_speed) {
        
        String expResult = "Motorization{" + "numberMotors=" + numberMotors + ", motor=" + motor + 
                ", motorType=" + motorType + ", name=" + name + ", cruiseSpeed=" + cruiseSpeed 
                + ", cruiseAltitude=" + cruiseAltitude + ", TSFC=" + TSFC + ", lapseRateFactor=" + lapseRateFactor 
                + ", thurst_0=" + thurst_0 + ", thurst_max_speed=" + thurst_max_speed + ", max_speed=" + max_speed + '}';

        
        String result = instance.toString();
        assertEquals(expResult, result);
      
    }

    /**
     * Test of getCruiseSpeed method, of class Motorization.
     */
    @Test
    public void testGetCruiseSpeed() {
        System.out.println("getCruiseSpeed");
        Motorization instance = new Motorization();
        double expResult = 0.0;
        instance.setCruiseAltitude(expResult);
        double result = instance.getCruiseSpeed();
        assertEquals(expResult, result, 0.0);
        expResult = 14000.0;
        instance.setCruiseAltitude(expResult);
        result = instance.getCruiseAltitude();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setCruiseSpeed method, of class Motorization.
     */
    @Test
    public void testSetCruiseSpeed() {
        System.out.println("setCruiseSpeed");
        double cruiseSpeed = 0.0;
        Motorization instance = new Motorization();
        instance.setCruiseSpeed(cruiseSpeed);
        assertEquals(cruiseSpeed,instance.getCruiseSpeed(),0.0);
        cruiseSpeed = 200000.00;
        instance.setCruiseSpeed(cruiseSpeed);
        assertEquals(cruiseSpeed,instance.getCruiseSpeed(),0.0);
    }

    /**
     * Test of getCruiseAltitude method, of class Motorization.
     */
    @Test
    public void testGetCruiseAltitude() {
        System.out.println("getCruiseAltitude");
        Motorization instance = new Motorization();
        double expResult = 0.0;
        instance.setCruiseAltitude(expResult);
        double result = instance.getCruiseAltitude();
        assertEquals(expResult, result, 0.0);
        expResult = 100.0;
        instance.setCruiseAltitude(expResult);
        result = instance.getCruiseAltitude();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setCruiseAltitude method, of class Motorization.
     */
    @Test
    public void testSetCruiseAltitude() {
        System.out.println("setCruiseAltitude");
        double cruiseAltitude = 0.0;
        Motorization instance = new Motorization();
        instance.setCruiseAltitude(cruiseAltitude);
        assertEquals(cruiseAltitude,instance.getCruiseAltitude(),0.0);
        cruiseAltitude = 1000.0;
        instance.setCruiseAltitude(cruiseAltitude);
        assertEquals(cruiseAltitude,instance.getCruiseAltitude(),0.0);
    }

    /**
     * Test of getTSFC method, of class Motorization.
     */
    @Test
    public void testGetTSFC() {
        System.out.println("getTSFC");
        Motorization instance = new Motorization();
        double expResult = 0.0;
        instance.setTSFC(expResult);
        double result = instance.getTSFC();
        assertEquals(expResult, result, 0.0);
        expResult = 1000.0;
        instance.setTSFC(expResult);
        result = instance.getTSFC();
        assertEquals(expResult,result,0.0);
       
    }

    /**
     * Test of setTSFC method, of class Motorization.
     */
    @Test
    public void testSetTSFC() {
        System.out.println("setTSFC");
        double TSFC = 0.0;
        Motorization instance = new Motorization();
        instance.setTSFC(TSFC);
        assertEquals(TSFC,instance.getTSFC(),0.0);
        TSFC = 1000.0;
        instance.setTSFC(TSFC);
        assertEquals(TSFC,instance.getTSFC(),0.0);
    }

    /**
     * Test of getLapseRateFactor method, of class Motorization.
     */
    @Test
    public void testGetLapseRateFactor() {
        System.out.println("getLapseRateFactor");
        Motorization instance = new Motorization();
        double expResult = 0.0;
        instance.setLapseRateFactor(expResult);
        double result = instance.getLapseRateFactor();
        assertEquals(expResult, result, 0.0);
        expResult = 20.0;
        instance.setLapseRateFactor(expResult);
        result = instance.getLapseRateFactor();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setLapseRateFactor method, of class Motorization.
     */
    @Test
    public void testSetLapseRateFactor() {
        System.out.println("setLapseRateFactor");
        double lapseRateFactor = 0.0;
        Motorization instance = new Motorization();
        instance.setLapseRateFactor(lapseRateFactor);
        assertEquals(lapseRateFactor,instance.getLapseRateFactor(),0.0);
        lapseRateFactor = 8.2;
        instance.setLapseRateFactor(lapseRateFactor);
        assertEquals(lapseRateFactor,instance.getLapseRateFactor(),0.0);
    }

    /**
     * Test of getThurst_0 method, of class Motorization.
     */
    @Test
    public void testGetThurst_0() {
        System.out.println("getThurst_0");
        Motorization instance = new Motorization();
        double expResult = 0.0;
        instance.setThurst_0(expResult);
        double result = instance.getThurst_0();
        assertEquals(expResult, result, 0.0);
        expResult = 0.9;
        instance.setThurst_0(expResult);
        result = instance.getThurst_0();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setThurst_0 method, of class Motorization.
     */
    @Test
    public void testSetThurst_0() {
        System.out.println("setThurst_0");
        double thurst_0 = 0.0;
        Motorization instance = new Motorization();
        instance.setThurst_0(thurst_0);
        assertEquals(thurst_0,instance.getThurst_0(),0.0);
        thurst_0 = 0.9;
        instance.setThurst_0(thurst_0);
        assertEquals(thurst_0,instance.getThurst_0(),0.0);
    }

    /**
     * Test of getThurst_max_speed method, of class Motorization.
     */
    @Test
    public void testGetThurst_max_speed() {
        System.out.println("getThurst_max_speed");
        Motorization instance = new Motorization();
        double expResult = 0.0;
        instance.setThurst_max_speed(expResult);
        double result = instance.getThurst_max_speed();
        assertEquals(expResult, result, 0.0);
        expResult = 200.0;
        instance.setThurst_max_speed(expResult);
        result = instance.getThurst_max_speed();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setThurst_max_speed method, of class Motorization.
     */
    @Test
    public void testSetThurst_max_speed() {
        System.out.println("setThurst_max_speed");
        double thurst_max_speed = 0.0;
        Motorization instance = new Motorization();
        instance.setThurst_max_speed(thurst_max_speed);
        assertEquals(thurst_max_speed,instance.getThurst_max_speed(),0.0);
        thurst_max_speed = 300.0;
        instance.setThurst_max_speed(thurst_max_speed);
        assertEquals(thurst_max_speed,instance.getThurst_max_speed(),0.0);
    }

    /**
     * Test of getMax_speed method, of class Motorization.
     */
    @Test
    public void testGetMax_speed() {
        System.out.println("getMax_speed");
        Motorization instance = new Motorization();
        double expResult = 0.0;
        instance.setMax_speed(expResult);
        double result = instance.getMax_speed();
        assertEquals(expResult, result, 0.0);
        expResult = 100.0;
        instance.setMax_speed(expResult);
        result = instance.getMax_speed();
        assertEquals(expResult,result,0.0);
    }

    /**
     * Test of setMax_speed method, of class Motorization.
     */
    @Test
    public void testSetMax_speed() {
        System.out.println("setMax_speed");
        double max_speed = 12.0;
        Motorization instance = new Motorization();
        instance.setMax_speed(max_speed);
        assertEquals(max_speed,instance.getMax_speed(),0.0);
        max_speed = 0;
        instance.setMax_speed(max_speed);
        assertEquals(max_speed,instance.getMax_speed(),0.0);
    }

    /**
     * Test of validate method, of class Motorization.
     */
    @Test
    public void testValidate() {
        System.out.println("validate");
        Motorization instance = new Motorization();
        int numberMotors = 4;
        String motor = "Motor";
        String motorType = "MotorType";
        String name  = "Name";
        double cruiseSpeed = 1400000.0;
        double cruiseAltitude = 4000.0;
        double TSFC = 100.0;
        double lapseRateFactor = 9;
        double thurst_0 = 0.8;
        double thurst_max_speed = 33.6;
        double max_speed = 4000.0;
        instance.setNumberMotors(numberMotors);
        instance.setMotor(motor);
        instance.setMotorType(motorType);
        instance.setCruiseSpeed(cruiseSpeed);
        instance.setLapseRateFactor(lapseRateFactor);
        instance.setCruiseAltitude(cruiseAltitude);
        instance.setTSFC(TSFC);
        instance.setThurst_0(thurst_0);
        instance.setName(name);
        instance.setThurst_max_speed(thurst_max_speed);
        instance.setMax_speed(max_speed);
        boolean expResult = true;
        boolean result = instance.validate();
        assertEquals(expResult, result);

    }

}

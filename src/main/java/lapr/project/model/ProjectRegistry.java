
package lapr.project.model;

import java.util.List;
import lapr.project.model.datalayer.ProjectData;
import lapr.project.model.datalayer.DataBaseManager;

/**
 * Represents an instance of ProjectRegistry through [variables].
 *
 */
public class ProjectRegistry {

    private Project activeProject;

    /**
     * Creates an instance of ProjectHandler without parameters.
     *
     */
    public ProjectRegistry() {

    }

    /**
     * Creates a new project
     *
     * @return project
     */
    public Project newProject() {
        Project p = new Project();
        return p;
    }

    /**
     * Adds a project in the database.
     *
     * @param p project to insert
     * @return true adding successfully and false if it does not insert
     */
    public boolean addProject(Project p) {

        DataBaseManager dataManager = DataBaseManager.getInstance();
        ProjectData projectData = dataManager.getProjectData();
        return projectData.insertProject(p);
    }

    /**
     * Registers the target Project changes.
     *
     * @param p The project that contains the information to update.
     * @param oldP The project that contains all the original information.
     * @return (boolean) True if successfully registered.
     */
    public boolean registerChanges(Project p, Project oldP) {
        boolean result = p.validate(true, true);
        if (result) {
            result = DataBaseManager.getInstance().getProjectData().updateProject(p, oldP);
        }
        return result;
    }

    /**
     * Copies the project.
     *
     * @param project The project that contains the information to update.
     *
     * @return True if successfully registered.
     */
    public boolean registerCopy(Project project) {
        boolean result = project.validate(true, true);
        if (result) {
            result = DataBaseManager.getInstance().getProjectData().insertProject(project);
        }
        return result;
    }

    /**
     * Returns the list of existing projects in the database.
     *
     * @return list of existing projects
     */
    public List<String> getProjectList() {
        return DataBaseManager.getInstance().getProjectData().getProjectList();
    }

    /**
     * Deletes the open project
     *
     * @param name
     * @return name name of open project
     */
    public boolean deleteProject(String name) {
        return DataBaseManager.getInstance().getProjectData().deleteProject(name);
    }

    /**
     * @return the activeProject
     */
    public Project getActiveProject() {
        return activeProject;
    }

    /**
     * @param activeProject the activeProject to set
     */
    public void setActiveProject(Project activeProject) {
        this.activeProject = activeProject;
    }

    /**
     * Changes the project in use by name.
     *
     * @param name name of new project in use
     */
    public void setProjectByName(String name) {
        ProjectData projectData = DataBaseManager.getInstance().getProjectData();
        setActiveProject(projectData.getProject(name));
    }

    /**
     * Returns the name of open project.
     *
     * @return name name of open project
     */
    public String getNameOpenProject() {
        return activeProject.getName();
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.List;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import lapr.project.model.FlightPlan;

/**
 *
 * @author cdsn_
 */
public class FligthPlanList {

    /**
     * List of flight plan
     */
    private List<FlightPlan> fligthPlanList;

    public FligthPlanList() {
        fligthPlanList = new ArrayList<>();
    }

    /**
     * Creates a clone of the target fligthPlanList
     *
     * @param source The target flight list to clone.
     */
    public FligthPlanList(FligthPlanList source) {
        fligthPlanList = new ArrayList(source.fligthPlanList);
        Collections.copy(fligthPlanList, source.fligthPlanList);
    }

    /**
     * Return the number of fligth plan.
     *
     * @return
     */
    public int size() {
        return this.fligthPlanList.size();
    }

    /**
     * Return list of flightplan
     *
     * @return flightPlanList
     */
    public List<FlightPlan> getFlightPlanList() {
        return this.fligthPlanList;
    }

    /**
     * Creates a new FlightPlan
     *
     * @return New FlightPlan
     */
    public FlightPlan newFlightPlan() {
        return new FlightPlan();
    }

    /**
     * Add an FlightPlan to the flightPlanList
     *
     * @param flightPlan FlightPlan to be added.
     * @return True if the flightPlan was added , otherwise returns false.
     */
    public boolean addFlightPlan(FlightPlan flightPlan) {
        int flag = 0;
        boolean flag1 = false;
        for (FlightPlan flightP : fligthPlanList) {
            if (flightP.getName().equals(flightPlan.getName())) {
                flag += 1;
            }
        }

        if (flag == 0) {
            this.fligthPlanList.add(flightPlan);
            flag1 = true;
        }

        return flag1;

    }

    /**
     * Remove the FlightPlan received by parameter Flight Plan List
     *
     * @param fp FlightPlan to be removed.
     * @return True if the fp was removed , otherwise returns false.
     */
    public boolean removeFlightPlan(FlightPlan fp) {
        return this.fligthPlanList.remove(fp);
    }

    /**
     * Returns an iterator to allow running through the list of FlightsPlan.
     *
     * @return Iterator.
     */
    public Iterator<FlightPlan> iterator() {
        return this.fligthPlanList.iterator();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FligthPlanList other = (FligthPlanList) obj;
        if (!Objects.equals(this.fligthPlanList, other.fligthPlanList)) {
            return false;
        }
        return true;
    }

    public FlightPlan getFlightplanByName(String name) {
        for (FlightPlan flightplan : fligthPlanList) {
            if (flightplan.getName().equals(name)) {
                return flightplan;
            }
        }
        return null;
    }

}

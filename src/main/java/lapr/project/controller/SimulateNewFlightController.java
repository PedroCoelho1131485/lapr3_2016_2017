/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.controller;

import java.util.List;
import lapr.project.model.Flight;
import lapr.project.model.FlightPlan;
import lapr.project.model.NetworkAnalysis;
import lapr.project.model.NetworkAnalysisResult;
import lapr.project.model.ProjectRegistry;
import lapr.project.model.Segment;
import lapr.project.model.datalayer.DataBaseManager;
import lapr.project.model.LeitorCsv;

/**
 *
 * @author cdsn_
 */
public class SimulateNewFlightController {

    /**
     * Instance of ProjectRegistry.
     */
    private ProjectRegistry pr;

    /**
     * Instance of FlightPlan
     */
    private FlightPlan flightPlan;

    /**
     * Instance of NetworkAnalysis
     */
    private NetworkAnalysis na;

    /**
     * Instance ofNetworkAnalysisResult
     */
    private NetworkAnalysisResult nar;

    private Flight flight;

    /**
     * Creates an instance of SimulateNewFlightController receiving a project.
     *
     * @param pr
     */
    public SimulateNewFlightController(ProjectRegistry pr) {
        this.pr = pr;
        this.flight = new Flight();
    }

    public void setNameFlight(String name) {
        this.flight.setName(name);
    }

    public void setTypeFlight(String type) {
        this.flight.setType(type);
    }

    public void setDepartureDay(String departure) {
        this.flight.setDepartureDay(departure);
    }

    public void setMiniStop(Double miniStop) {
        this.flight.setMinStopTime(miniStop);
    }

    public void setScheduledArrival(String shedule) {
        this.flight.setScheduledArrival(shedule);
    }

    /**
     * Select an existing fligthPlan.
     *
     * @param flightPlan
     */
    public void setFlightPlan(FlightPlan flightPlan) {
        this.flightPlan = flightPlan;
    }

    /**
     * get the flight plan
     */
    public FlightPlan getFlightPlan() {
        return this.flightPlan;
    }

    /**
     * Return a list of flight plan
     *
     * @return list
     */
    public List<FlightPlan> getFlightPlanList() {
        DataBaseManager.getInstance().getProjectData().getFlightPlansProject(pr.getActiveProject());
        return this.pr.getActiveProject().getFlightPlanList().getFlightPlanList();
    }

    /**
     * Create an instance of NetworkAnalysis
     */
    public void createNetworkAnalysis() {
        this.na = new NetworkAnalysis(this.pr.getActiveProject(), this.flightPlan);
    }

    /**
     * Selects the total number of passengers
     *
     * @param passengersExecutive
     * @param passengersEconomic
     */
    public void setPassengers(int passengersExecutive, int passengersEconomic) {
        this.na.setPassengers(passengersEconomic + passengersExecutive);
    }

    /**
     * gets nr passengers
     *
     * @return
     */
    public int getPassengers() {
        return this.na.getPassengers();
    }

    /**
     * Selects crew
     *
     * @param crew
     */
    public void setCrew(int crew) {
        this.na.setCrew(crew);
    }

    public int getCrew() {
        return this.na.getCrew();
    }

    /**
     * Selects cargo load
     *
     * @param cargoLoad
     */
    public void setCargoLoad(double cargoLoad) {
        this.na.setCargoLoad(cargoLoad);
    }

    /**
     * get cargo load
     *
     * @return
     */
    public double getCargoLoad() {
        return this.na.getCargoLoad();
    }

    /**
     * Selects fuel load
     *
     * @param fuelLoad
     */
    public void setFuelLoad(double fuelLoad) {
        this.na.setFuelLoad(fuelLoad);
    }

    /**
     * get fuel load
     */
    public double getFuelLoad() {
        return this.na.getFuelLoad();
    }

    /**
     * Calculates one of three paths((fastestPath=1, shortestPath=0) falta o
     * terceiro caminho
     *
     * @param num
     */
    public void calculate(int num) {
        this.nar = this.na.calculate(num);
    }

    /**
     * Return list of segments of path
     *
     * @return listSegments
     */
    public List<Segment> getListOfSegments() {
        return this.getNar().getPath();
    }

    /**
     * Returns the chosen aircraft
     *
     * @return aircraft
     */
    public String getAircraft() {
        return this.getNar().getAircraft().getRegistration();
    }

    /**
     * Returns the travelling time.
     *
     * @return aircraft
     */
    public double getTravellingTime() {
        return this.getNar().getTravellingTime();
    }

    /**
     * Returns the energy consumption
     *
     * @return energy
     */
    public double getEnergyConsumption() {
        return this.getNar().getEnergyExpenditure();
    }

    public boolean addFlight() {
        DataBaseManager.getInstance().getProjectData().insertFlight(pr.getActiveProject(), flight);
        return pr.getActiveProject().getFlightList().addFlight(flight);
    }

    /**
     * @return the nar
     */
    public NetworkAnalysisResult getNar() {
        return nar;
    }

}
